#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Color clusters of dimer spheres.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection
import numpy as np
import h5py
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="HDF5 input filename")
parser.add_argument("--step", type=int, required=True, help="integration step")
parser.add_argument("--figsize", type=float, nargs=2, default=(6.0, 6.0), help="figure size")
parser.add_argument("--count", type=int, default=4, help="minimum number of spheres per cluster")
args = parser.parse_args()

mpl.rc("pgf", texsystem="lualatex", preamble=(r"\usepackage{amsmath,amssymb,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=10)
mpl.rc("legend", fontsize=10, borderpad=0.2, borderaxespad=0.1, labelspacing=0.2, columnspacing=1.0, handletextpad=0.2, handlelength=1, numpoints=1)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)

def read(group, step):
  offset = group["step"].attrs["offset"]
  interval = group["step"][()]
  index = (step - offset) // interval
  assert step == offset + index*interval
  return group["value"][index]

with h5py.File(args.input, "r") as f:
  param = json.loads(f.attrs["parameters"])
  diameter = param["diameter"]
  edges = f["particles/dimer/box/edges"][:]
  position = read(f["particles/dimer/position"], args.step)
  species = read(f["particles/dimer/species"], args.step)
  value = read(f["observables/dimer/cluster"], args.step)

counts = np.unique(value, return_counts=True)[1]

# Set grey color for clusters below threshold size.
cmap = np.tile((0., 0., 0., 0.5), (len(counts), 1))

# Set random colors from a color map for clusters above threshold size.
colors = np.linspace(0., 1., len(counts[counts >= args.count]))
np.random.shuffle(colors)
cmap[counts >= args.count] = map(plt.cm.jet, colors)

patches_C = [Circle(r, diameter["C"]/2.) for r in position[0::2, 0:2]]
patches_N = [Circle(r, diameter["N"]/2.) for r in position[1::2, 0:2]]

ax.add_collection(PatchCollection(patches_C, facecolors=cmap[value[0::2]], edgecolor="none"))
ax.add_collection(PatchCollection(patches_N, facecolors=cmap[value[1::2]], edgecolor="none"))
ax.set_xlim(0, edges[0])
ax.set_ylim(0, edges[1])
ax.set_aspect("equal")

fig.tight_layout(pad=0)
fig.savefig(args.output)
