#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot orientation autocorrelation of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
parser.add_argument("--block", type=int, default=2, help="block level")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input[0], "r") as f:
  g = f["dynamics/dimer/orientation_autocorrelation"]
  time = g["time"][:]
  index = g["count"][:] != 0
  mean = np.zeros(shape=g["value"].shape)
  error = np.zeros(shape=g["value"].shape)

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["dynamics/dimer/orientation_autocorrelation"]
    assert np.all(time == g["time"])
    value = g["value"][:]
    count += 1
    delta = value - mean
    mean += delta / count
    error += delta * (value - mean)

error = np.sqrt(error / (count - 1) / count)

time = time[args.block]
index = index[args.block]
mean = mean[args.block]
error = error[args.block]

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.errorbar(time[index[:, 0]], mean[index[:, 0], 0], error[index[:, 0], 0], errorevery=10, label="x")
ax.errorbar(time[index[:, 1]], mean[index[:, 1], 1], error[index[:, 1], 1], errorevery=10, label="y")
ax.errorbar(time[index[:, 2]], mean[index[:, 2], 2], error[index[:, 2], 2], errorevery=10, label="z")
ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\langle\hat{\bm{z}}(t)\cdot\hat{\bm{z}}_0\rangle$")
ax.legend(loc="best", fancybox=True)
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
