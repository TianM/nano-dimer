#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Render trajectory of sphere-dimer clusters.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license.
#

from __future__ import division
import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
import numpy as np
import h5py as h5
from mpi4py import MPI
import json
import argparse
import sys

"""
ffmpeg -y -f image2pipe -r 25 -vcodec png -i - -vf scale=1000:-1 -sws_flags lanczos -c:v libx264 -pix_fmt yuv420p -preset veryslow -tune animation -crf 20 dimer.mp4
"""

parser = argparse.ArgumentParser()
parser.add_argument("input", help="HDF5 trajectory file")
parser.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout, help="image file")
parser.add_argument("--zoom", type=float, default=1.0, help="camera zoom")
parser.add_argument("--interval", type=int, default=1, help="frame interval")
parser.add_argument("--size", nargs=2, type=int, default=(4000, 4000), help="image size")
parser.add_argument("--linewidth", type=int, default=4, help="wireframe thickness")
parser.add_argument("--progress", action="store_true", help="show progress")
args = parser.parse_args()

f = h5.File(args.input, "r")
g = f["particles/dimer"]
position = g["position/value"]
time = g["position/time"][()]*np.arange(position.shape[0]) + g["position/time"].attrs["offset"]
L = g["box/edges"][:]
boundary = g["box"].attrs["boundary"]
g = f["observables/dimer"]
cluster = g["cluster/value"]
assert np.all(time == g["cluster/time"][()]*np.arange(cluster.shape[0]) + g["cluster/time"].attrs["offset"])
param = json.loads(f.attrs["parameters"])
diameter = param["diameter"]

renderer = vtk.vtkRenderer()
renderer.SetBackground(0.0, 0.0, 0.0)

# Increase number of lights
lightkit = vtk.vtkLightKit()
lightkit.SetKeyLightIntensity(1.0)
lightkit.SetKeyToFillRatio(1.5)
lightkit.AddLightsToRenderer(renderer)

# Render outline of box
outline = vtk.vtkOutlineSource()
outline.SetBounds(0.0, L[0], 0.0, L[1], 0.0, L[2])
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(outline.GetOutputPort())
actor = vtk.vtkActor()
actor.SetMapper(mapper)
actor.GetProperty().SetColor(1.0, 1.0, 1.0)
actor.GetProperty().SetLineWidth(args.linewidth)
renderer.AddActor(actor)

# Set camera view
camera = renderer.GetActiveCamera()
camera.SetPosition(0.0, 0.0, 1.0)
camera.SetFocalPoint(0.0, 0.0, 0.0)
camera.SetViewUp(0.0, 1.0, 0.0)
renderer.ResetCamera()
camera.Zoom(args.zoom)

Nd = position.shape[1]
rd = np.empty((Nd, 3))
d = np.empty((Nd//2, 3))
label = np.empty(Nd//2, dtype=int)

# Render dimer-sphere axis
points = vtk.vtkPoints()
points.SetData(numpy_to_vtk(rd))
lines = vtk.vtkCellArray()
for i in range(0, Nd, 2):
  lines.InsertNextCell(2)
  lines.InsertCellPoint(i)
  lines.InsertCellPoint(i + 1)

# Render dimer spheres
color = np.empty(Nd)
color.fill(np.nan)
scale = np.zeros((Nd, 3))
scale[0::2, 0] = diameter["C"]
scale[1::2, 0] = diameter["N"]
periodic = boundary == "periodic"

source = vtk.vtkProgrammableSource()

def read_position():
  rd[:] = position[read_position.offset, :, :]
  d[:] = rd[0::2, :] - rd[1::2, :]
  d[:, periodic] -= np.round(d[:, periodic]/L[periodic]) * L[periodic]
  rd[0::2, :] = rd[1::2, :] + d[:]
  label[:] = cluster[read_position.offset, 1::2]
  count = np.bincount(label)
  color[1::2] = np.log(count[label])
  polydata = source.GetPolyDataOutput()
  polydata.SetPoints(points)
  polydata.SetLines(lines)
  polydata.GetPointData().SetScalars(numpy_to_vtk(color))
  polydata.GetPointData().SetVectors(numpy_to_vtk(scale))

read_position.offset = 0

source.SetExecuteMethod(read_position)

tubefilter = vtk.vtkTubeFilter()
tubefilter.SetInputConnection(source.GetOutputPort())
tubefilter.SetRadius(0.25)
tubefilter.SetNumberOfSides(20)
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(tubefilter.GetOutputPort())
mapper.SetScalarVisibility(False)
actor = vtk.vtkActor()
actor.SetMapper(mapper)
renderer.AddActor(actor)

spheresource = vtk.vtkSphereSource()
spheresource.SetCenter(0.0, 0.0, 0.0)
spheresource.SetRadius(0.5)
spheresource.SetPhiResolution(20)
spheresource.SetThetaResolution(20)

sphereglyph = vtk.vtkGlyph3D()
sphereglyph.SetSourceConnection(spheresource.GetOutputPort())
sphereglyph.SetInputConnection(source.GetOutputPort())
sphereglyph.SetColorModeToColorByScalar()
sphereglyph.SetScaleModeToScaleByVector()

colors = vtk.vtkLookupTable()
colors.SetHueRange(0.0, 2/3)
colors.SetSaturationRange(1.0, 1.0)
colors.SetValueRange(1.0, 1.0)
colors.SetNanColor(1.0, 1.0, 1.0, 1.0)

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(sphereglyph.GetOutputPort())
mapper.SetColorModeToMapScalars()
mapper.SetScalarRange(0, np.log(Nd//2))
mapper.SetLookupTable(colors)
actor = vtk.vtkActor()
actor.SetMapper(mapper)
renderer.AddActor(actor)

# Render simulation time
text = vtk.vtkTextActor()
text.GetTextProperty()
text.GetTextProperty().SetFontSize(12)
text.GetTextProperty().SetColor(1.0, 1.0, 1.0)
text.SetTextScaleModeToViewport()
renderer.AddActor2D(text)

window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)
window.SetWindowName(args.input)
window.SetOffScreenRendering(True)
window.SetSize(args.size)
image = vtk.vtkWindowToImageFilter()
image.SetInput(window)
writer = vtk.vtkPNGWriter()
writer.SetInputConnection(image.GetOutputPort())
writer.SetWriteToMemory(True)

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nranks = comm.Get_size()

for offset in range(0, time.shape[0], args.interval*nranks):
  sendcount = np.zeros(1, np.int)
  sendbuf = None

  if args.progress and rank == 0:
    sys.stderr.write(("\r[%-60s] %d%%") % ("="*(60*offset//time.shape[0]), 100*offset//time.shape[0]))
    sys.stderr.flush()

  offset += args.interval*rank

  if offset < time.shape[0]:
    read_position.offset = offset
    source.Modified()
    text.SetInput("%.1f" % time[read_position.offset])
    window.Render()
    image.Modified()
    writer.Write()
    sendbuf = vtk_to_numpy(writer.GetResult())
    sendcount[0] = sendbuf.shape[0]

  recvcounts = np.empty(nranks, np.int) if rank == 0 else None
  comm.Gather((sendcount, MPI.LONG), (recvcounts, MPI.LONG), 0)
  recvbuf = np.empty(np.sum(recvcounts), np.byte) if rank == 0 else None
  comm.Gatherv((sendbuf, MPI.BYTE), (recvbuf, recvcounts, MPI.BYTE), 0)

  if rank == 0:
    args.output.write(recvbuf.tobytes())

if args.progress and rank == 0:
  sys.stderr.write(("\r[%-60s] 100%%\n") % ("="*60))
  sys.stderr.flush()
