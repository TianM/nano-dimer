#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot fraction of dimers in largest cluster.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="HDF5 input filename")
parser.add_argument("--average", type=int, default=1, help="interval of moving average")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="lualatex", preamble=(r"\usepackage{amsmath,amssymb,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=10)
mpl.rc("legend", fontsize=10, borderpad=0.2, borderaxespad=0.1, labelspacing=0.2, columnspacing=1.0, handletextpad=0.2, handlelength=1, numpoints=1)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)

with h5py.File(args.input, "r") as f:
  g = f["observables/dimer/cluster"]
  value = g["value"][::, ::2]
  time = g["time"][()]*np.arange(value.shape[0]) + g["time"].attrs["offset"]

count = np.zeros(value.shape[0])

for i in range(value.shape[0]):
  count[i] = np.max(np.unique(value[i], return_counts=True)[1])

# Normalize by total number of dimers.
count[:] *= 1./value.shape[1]

# Compute moving average over interval.
n = args.average
count[:] = np.cumsum(count[:])
count[n:] = count[n:] - count[:-n]
count = count[n-1:]/n
time = time[n-1:]

ax.plot(time, count)
ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$N_\text{max} / N_\text{total}$")
ax.set_ylim(0., 1.)
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
