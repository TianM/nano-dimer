-- Edge lengths of simulation domain.
L = {300, 300, 30}
-- Periodic boundary conditions.
periodic = {true, true, false}
-- Number of subdomains per dimension.
ndom = {2, 2, 1}
-- Number of dimer spheres.
Nd = 2 * 160
-- Diameters of dimer spheres.
diameter = {C = 4.0, N = 8.0}
-- Solvent number density.
rho = 9.0
-- Masses of neutrally buoyant dimer spheres.
mass = {C = rho * (math.pi/6) * diameter.C^3, N = rho * (math.pi/6) * diameter.N^3}
-- Average number of solvent particles per subdomain.
Ns = math.floor((rho*L[1]*L[2]*L[3] - (Nd/2)*(mass.C + mass.N))/(ndom[1]*ndom[2]*ndom[3]))
-- Maximum number of solvent particles per subdomain.
Ns_max = math.floor(Ns * 1.1)
-- Number of placeholders for sending solvent particles.
Ns_send_max = 20000
-- Cutoff radius of potential in units of σ.
r_cut = 2^(1/6)
-- Minimum number of steps between solvent particle sorts.
sort = {interval = 500}
-- Solvent temperature.
temp = 1/6
-- Distance of dimer spheres.
bond = (diameter.C + diameter.N)/2 * r_cut
-- Potential well depths of dimer spheres with solvent particles.
epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 10.0, CC = 10.0, CN = 10.0, NN = 10.0}
-- Potential diameters.
sigma = {C = diameter.C/2, N = diameter.N/2, CC = diameter.C + 1, CN = (diameter.C+diameter.N)/2 + 1, NN = diameter.N + 1}
-- Forward and backward reaction of solvent particles.
reaction = {rate = 0.01}
-- Verlet neighbour list skin.
skin = 1.0
-- Number of placeholders per dimer-sphere neighbour list.
nlist_size = {ds = 5000, dd = 20}
-- Number of bins per dimension for dimer spheres.
nbin = {math.floor((L[1]/ndom[1])/2), math.floor((L[2]/ndom[2])/2), math.floor((L[3]/ndom[3])/2)}
-- Number of placeholders per bin.
bin_size = 10
-- Multi-particle collision dynamics.
mpcd = {interval = 50, bin_size = 50}
-- Integration time-step.
timestep = 0.01
-- Measurement interval.
interval = 10000
-- Number of steps.
nsteps = 20*interval
-- Initial measurement step.
initial = 10*interval
-- OpenCL platform.
platform = 1
-- Random number generator seed.
seed = "0xc25bed88ef46598c"
-- JSON output filename.
output = "confined_dimer.json"
-- JSON file author.
author = {name = "B.J. Alder", email = "bjalder@example.org"}
-- JSON file creator.
creator = {name = "confined_dimer.lua", version = "1.0"}
