------------------------------------------------------------------------------
-- Label clusters of adjacent dimers.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local array = require("nanomotor.array")
local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local ceil, floor, max, min = math.ceil, math.floor, math.max, math.min
-- Cache C types.
local int_1 = ffi.typeof("int[1]")

--[[
Returns an observable that labels clusters of adjacent dimers.

At a given time-step, each cluster of adjacent dimers is labelled with a
unique integer, where a pair of dimers is considered adjacent if any of
its constituent spheres are less than a given cutoff distance apart.

The clusters are determined using a variant of the [Hoshen–Kopelman]
algorithm for percolation clusters, which is more commonly known as
the [union–find] algorithm.

[Hoshen–Kopelman]: https://www.ocf.berkeley.edu/~fricke/projects/hoshenkopelman/hoshenkopelman.html
[union–find]: https://en.wikipedia.org/wiki/Disjoint-set_data_structure
]]
return function(integrate, file, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank() or 0
  local nranks = comm and comm:size() or 1
  local timestep = integrate.timestep
  local initial, final, interval = args.initial, args.final, args.interval
  local cutoff = args.cutoff
  local bin_size = args.bin_size

  local Nd = int_1(dom.Nd)
  if comm then mpi.allreduce(mpi.in_place, Nd, 1, mpi.int, mpi.sum, comm) end
  Nd = Nd[0]

  local rd = comm and ffi.new("cl_double3[?]", Nd) or dom.rd
  local spd = comm and ffi.new("cl_char4[?]", Nd) or dom.spd

  local L = box.L_global
  local a = max(cutoff.CC, cutoff.CN, cutoff.NN)
  local nbin_x, nbin_y = floor(L[1]/a), floor(L[2]/a)
  local frac_x, frac_y = nbin_x/L[1], nbin_y/L[2]

  --[[
  Converts the given position to cell coordinates.
  --]]
  local function coords(x, y)
    x = min(max(floor(x*frac_x), 0), nbin_x-1)
    y = min(max(floor(y*frac_y), 0), nbin_y-1)
    return x, y
  end

  local bin_count = ffi.typeof("int[$][$]", nbin_x, nbin_y)()
  local bins = ffi.typeof("int[$][$][$]", nbin_x, nbin_y, bin_size)()

  --[[
  Decomposes dimer spheres into a grid of square cells.

  The edge length of a cell is taken as the maximum of the cutoff distances
  for all combinations of dimer-sphere species. The decomposition reduces
  the time complexity of a search for adjacent spheres from O(N²) to O(N).

  This function assumes that the dimer spheres lie roughly in a plane.
  --]]
  local function bin()
    ffi.fill(bin_count, ffi.sizeof(bin_count))
    for i = 0, Nd-1 do
      local x, y = coords(rd[i].x, rd[i].y)
      local n = bin_count[x][y]
      if n == bin_size then error("bin overflow") end
      bins[x][y][n] = i
      bin_count[x][y] = n + 1
    end
  end

  --[[
  The union–find algorithm operates on disjoint trees, where each tree
  corresponds to a cluster, and each node of a tree corresponds to a
  dimer sphere in the cluster.

  The element indices of this array are equivalent to the particle indices
  of the dimer spheres. Two nodes are linked together by setting the value
  of one element to the index of the other element, which corresponds to a
  pair of adjacent spheres belonging to the same cluster. The root node of
  a tree is indicated by setting the value of an element to its own index,
  which corresponds to the representative sphere of a cluster.
  --]]
  local labels = ffi.new("int[?]", Nd)

  --[[
  Returns the cluster label for the given dimer-sphere index.

  This function traverses a path on the tree corresponding to a cluster,
  starting at the node of the given sphere, and ending at the root node,
  which is the representative sphere of the cluster.

  To reduce the time of subsequent traversals, the path is compressed
  by linking all nodes along the path directly to the root node.
  --]]
  local function find(i)
    local j = i
    while labels[j] ~= j do
      j = labels[j]
    end
    while labels[i] ~= i do
      local k = labels[i]
      labels[i] = j
      i = k
    end
    return j
  end

  --[[
  Links a pair of adjacent dimer spheres.

  This function retrieves the root nodes of the trees corresponding to the
  clusters of each sphere, and merges the trees by linking one root node
  to the other root node, which turns the latter into the root node of the
  merged tree.
  --]]
  local function union(i, j)
    i = find(i)
    j = find(j)
    labels[i] = j
  end

  local dist2 = {
    [0] = {[0] = cutoff.CC^2, [1] = cutoff.CN^2},
    [1] = {[0] = cutoff.CN^2, [1] = cutoff.NN^2},
  }

  --[[
  Stores a cluster label for each dimer sphere in the array `value`.

  The labels are initialised by defining a separate cluster for each dimer
  that contains the spheres of the dimer. The clusters are iteratively
  merged based on the distance between neighbouring spheres.

  The label of a cluster is the particle index of an arbitrary sphere
  belonging to that cluster. Therefore the set of cluster labels is
  discontinuous.
  --]]
  local function label(value)
    for i = 0, Nd-1, 2 do
      for j = 0, 1 do
        labels[i+j] = i
      end
    end
    -- Iterating over all spheres in an inner loop, and over adjacent
    -- cells in the outer loops, yields the most stable performance.
    for dx = 0, 1 do
      for dy = -1, dx do
        for i = 0, Nd-1 do
          local x, y = coords(rd[i].x, rd[i].y)
          x = (x + dx) % nbin_x
          y = (y + dy) % nbin_y
          for n = 0, bin_count[x][y]-1 do
            local j = bins[x][y][n]
            local sp1 = spd[i].s0
            local sp2 = spd[j].s0
            local dx = rd[j].x - rd[i].x
            local dy = rd[j].y - rd[i].y
            local dz = rd[j].z - rd[i].z
            dx, dy, dz = box.mindist(dx, dy, dz)
            local d2 = dx*dx + dy*dy + dz*dz
            if d2 < dist2[sp1][sp2] then
              union(i, j)
            end
          end
        end
      end
    end
    for i = 0, Nd-1 do
      value[i] = find(i)
    end
  end

  --[[
  Reassigns cluster labels in the array `value` from a sequence.

  Continuous cluster labels can be used as array indices, which simplifies
  post-simulation analysis, e.g., storing the number of dimers per cluster.
  --]]
  local function relabel(value)
    local n = 0
    for i = 0, Nd-1 do
      labels[i] = -1
    end
    for i = 0, Nd-1 do
      local j = value[i]
      if labels[j] == -1 then
        labels[j] = n
        n = n + 1
      end
      value[i] = labels[j]
    end
  end

  local step = {interval = interval, offset = initial}
  local time = {interval = interval*timestep, offset = initial*timestep}
  local obs_cluster = h5md.create_observable(file, "observables/dimer/cluster", step, time, hdf5.int, {Nd})
  obs_cluster:close()

  local size = max(0, ceil((final - initial + 1)/interval))
  if comm then size = ceil(size/nranks) end
  local value = array.new("int", Nd*size)
  local count = 0

  local function write()
    if count == 0 then return end
    local filespace = obs_cluster:append(count)
    local offset = size*rank
    count = max(0, min(size, count - offset))
    filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
    local memspace = hdf5.create_simple_space({size, Nd})
    memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
    local dxpl = hdf5.create_plist("dataset_xfer")
    if comm then dxpl:set_dxpl_mpio("collective") end
    obs_cluster.value:write(value, hdf5.int, memspace, filespace, dxpl)
    filespace:close()
    memspace:close()
    dxpl:close()
    count = 0
  end

  local recvcount = comm and ffi.new("int[?]", nranks)
  local recvdispl = comm and ffi.new("int[?]", nranks)

  local self = {step = initial}

  function self.observe()
    obs_cluster = h5md.open_observable(file, "observables/dimer/cluster")
    while self.step <= final do
      coroutine.yield()
      local root = floor(count/size)
      if comm then
        mpi.gather(int_1(dom.Nd), 1, mpi.int, recvcount, 1, mpi.int, root, comm)
        if rank == root then
          for i = 1, comm:size()-1 do
            recvdispl[i] = recvdispl[i-1] + recvcount[i-1]
          end
        end
        mpi.gatherv(dom.rd, dom.Nd, dom.type_rd, rd, recvcount, recvdispl, dom.type_rd, root, comm)
        mpi.gatherv(dom.spd, dom.Nd, dom.type_spd, spd, recvcount, recvdispl, dom.type_spd, root, comm)
      end
      if rank == root then
        local value = value + Nd*(count - size*rank)
        bin()
        label(value)
        relabel(value)
      end
      count = count + 1
      self.step = self.step + interval
    end
    write()
    obs_cluster:close()
  end

  function self.snapshot(group)
    local filespace = hdf5.create_simple_space({count, Nd})
    local dset = group:create_dataset("value", hdf5.int, filespace)
    local offset = size*rank
    local count = max(0, min(size, count - offset))
    filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
    local memspace = hdf5.create_simple_space({size, Nd})
    memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
    local dxpl = hdf5.create_plist("dataset_xfer")
    if comm then dxpl:set_dxpl_mpio("collective") end
    dset:write(value, hdf5.int, memspace, filespace, dxpl)
    filespace:close()
    memspace:close()
    dxpl:close()
    dset:close()
  end

  function self.restore(group)
    local dset = group:open_dataset("value")
    local filespace = dset:get_space()
    count = filespace:get_simple_extent_dims()[1]
    local offset = size*rank
    local count = max(0, min(size, count - offset))
    filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
    local memspace = hdf5.create_simple_space({size, Nd})
    memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
    local dxpl = hdf5.create_plist("dataset_xfer")
    if comm then dxpl:set_dxpl_mpio("collective") end
    dset:read(value, hdf5.int, memspace, filespace, dxpl)
    filespace:close()
    memspace:close()
    dxpl:close()
    dset:close()
  end

  return self
end
