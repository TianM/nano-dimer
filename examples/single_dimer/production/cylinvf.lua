------------------------------------------------------------------------------
-- Cylindrical velocity field.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local hdf5 = require("hdf5")
local ffi = require("ffi")

return function(integrate, file, args)
  local dom = integrate.dom
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local cutoff, nbin, bin_size = args.cutoff, args.nbin, args.bin_size
  local initial, final, interval = args.initial, args.final, args.interval

  local program = compute.program(context, "cylinvf.cl", {
    dom = dom,
    box = box,
    nbin = nbin,
    bin_size = bin_size,
    cutoff = cutoff,
  })

  local d_bin = context:create_buffer(nbin.z*nbin.r*bin_size*ffi.sizeof("cl_float4"))
  local d_bin_count = context:create_buffer(nbin.z*nbin.r*ffi.sizeof("cl_uint"))
  local bin_err = ffi.new("cl_uint[1]")
  local d_bin_err = context:create_buffer("use_host_ptr", ffi.sizeof(bin_err), bin_err)

  local fill do
    local kernel = program:create_kernel("cylinvf_fill")
    local work_size = kernel:get_work_group_info(device, "preferred_work_group_size_multiple")
    local glob_size = math.ceil(nbin.z*nbin.r/work_size) * work_size

    function fill()
      kernel:set_arg(0, d_bin_count)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local bin do
    local kernel = program:create_kernel("cylinvf_bin")
    local work_size = kernel:get_work_group_info(device, "preferred_work_group_size_multiple")
    local glob_size = math.ceil(dom.Ns/work_size) * work_size

    function bin()
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_vd)
      kernel:set_arg(2, dom.d_rs)
      kernel:set_arg(3, dom.d_vs)
      kernel:set_arg(4, d_bin)
      kernel:set_arg(5, d_bin_count)
      kernel:set_arg(6, d_bin_err)
      queue:enqueue_write_buffer(dom.d_rd, true, dom.rd)
      queue:enqueue_write_buffer(dom.d_vd, true, dom.vd)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local count = ffi.typeof("cl_ulong[$][$]", nbin.z, nbin.r)()
  local value = ffi.typeof("cl_float4[$][$]", nbin.z, nbin.r)()
  local d_count = context:create_buffer("use_host_ptr", ffi.sizeof(count), count)
  local d_value = context:create_buffer("use_host_ptr", ffi.sizeof(value), value)

  local sum do
    local kernel = program:create_kernel("cylinvf_sum")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]
    local glob_size = nbin.z*nbin.r * work_size

    function sum()
      kernel:set_arg(0, d_bin)
      kernel:set_arg(1, d_bin_count)
      kernel:set_arg(2, d_count)
      kernel:set_arg(3, d_value)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_bin_err, true, "read")
      if bin_err[0] ~= 0 then error("bin overflow of "..bin_err[0].." solvent particles") end
      queue:enqueue_unmap_mem_object(d_bin_err, bin_err)
    end
  end

  local group = file:create_group("fields/solvent/cylindrical_velocity")

  do
    local z = ffi.new("float[?]", nbin.z)
    local delta_z = (cutoff.z[2]-cutoff.z[1]) / nbin.z
    for i = 0, nbin.z-1 do
      z[i] = (i+0.5)*delta_z + cutoff.z[1]
    end
    local space_z = hdf5.create_simple_space({nbin.z, 1})
    local space_cutoff_z = hdf5.create_simple_space({2})
    local dset_z = group:create_dataset("axial", hdf5.float, space_z)
    local attr_cutoff_z = dset_z:create_attribute("cutoff", hdf5.double, space_cutoff_z)
    dset_z:write(z, hdf5.float)
    attr_cutoff_z:write(ffi.new("double[2]", cutoff.z), hdf5.double)
    space_z:close()
    space_cutoff_z:close()
    dset_z:close()
    attr_cutoff_z:close()
  end

  do
    local r = ffi.new("float[?]", nbin.r)
    local delta_r = cutoff.r / nbin.r
    for i = 0, nbin.r-1 do
      r[i] = (i+0.5)*delta_r
    end
    local space_r = hdf5.create_simple_space({1, nbin.r})
    local space_cutoff_r = hdf5.create_space("scalar")
    local dset_r = group:create_dataset("radial", hdf5.float, space_r)
    local attr_cutoff_r = dset_r:create_attribute("cutoff", hdf5.double, space_cutoff_r)
    dset_r:write(r, hdf5.float)
    attr_cutoff_r:write(ffi.new("double[1]", cutoff.r), hdf5.double)
    space_r:close()
    space_cutoff_r:close()
    dset_r:close()
    attr_cutoff_r:close()
  end

  local space_count = hdf5.create_simple_space({nbin.z, nbin.r})
  local space_value = hdf5.create_simple_space({nbin.z, nbin.r, 2, 2})
  local dset_count = group:create_dataset("count", hdf5.uint64, space_count)
  local dset_value = group:create_dataset("value", hdf5.float, space_value)
  space_count:close()
  space_value:close()
  dset_count:close()
  dset_value:close()

  local function write()
    queue:enqueue_map_buffer(d_count, true, "read")
    queue:enqueue_map_buffer(d_value, true, "read")
    dset_count:write(count, hdf5.uint64)
    dset_value:write(value, hdf5.float)
    queue:enqueue_unmap_mem_object(d_count, count)
    queue:enqueue_unmap_mem_object(d_value, value)
  end

  group:close()

  local self = {step = initial}

  function self.observe()
    local group = file:open_group("fields/solvent/cylindrical_velocity")
    dset_count = group:open_dataset("count")
    dset_value = group:open_dataset("value")
    group:close()
    while self.step <= final do
      coroutine.yield()
      fill()
      bin()
      sum()
      self.step = self.step + interval
    end
    write()
    dset_count:close()
    dset_value:close()
  end

  function self.snapshot(group)
    local space_count = hdf5.create_simple_space({nbin.z, nbin.r})
    local space_value = hdf5.create_simple_space({nbin.z, nbin.r, 4})
    local dset_count = group:create_dataset("count", hdf5.uint64, space_count)
    local dset_value = group:create_dataset("value", hdf5.float, space_value)
    queue:enqueue_map_buffer(d_count, true, "read")
    queue:enqueue_map_buffer(d_value, true, "read")
    dset_count:write(count, hdf5.uint64, space_count)
    dset_value:write(value, hdf5.float, space_value)
    queue:enqueue_unmap_mem_object(d_count, count)
    queue:enqueue_unmap_mem_object(d_value, value)
    space_count:close()
    space_value:close()
    dset_count:close()
    dset_value:close()
  end

  function self.restore(group)
    local space_count = hdf5.create_simple_space({nbin.z, nbin.r})
    local space_value = hdf5.create_simple_space({nbin.z, nbin.r, 4})
    local dset_count = group:open_dataset("count")
    local dset_value = group:open_dataset("value")
    queue:enqueue_map_buffer(d_count, true, "write")
    queue:enqueue_map_buffer(d_value, true, "write")
    dset_count:read(count, hdf5.uint64, space_count)
    dset_value:read(value, hdf5.float, space_value)
    queue:enqueue_unmap_mem_object(d_count, count)
    queue:enqueue_unmap_mem_object(d_value, value)
    space_count:close()
    space_value:close()
    dset_count:close()
    dset_value:close()
  end

  return self
end
