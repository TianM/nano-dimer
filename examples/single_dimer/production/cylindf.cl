/*
 * Cylindrical density function.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

__kernel void
cylindf_bin(__global const double3 *restrict d_rd,
            __global const double3 *restrict d_rs,
            __global const char4 *restrict d_sps,
            __global uint *restrict d_bin)
{
  const uint gid = get_global_id(0);
  if (gid < ${dom.Ns}) {
    const double3 z = normalize(mindist(d_rd[0] - d_rd[1]));
    const double3 d = mindist(d_rs[gid] - d_rd[1]);
    const double dz = dot(d, z);
    if (dz > ${cutoff.z[1]} && dz < ${cutoff.z[2]}) {
      const double r2 = dot(d, d) - dz*dz;
      if (r2 < ${cutoff.r^2}) {
        const uint bin_z = clamp(convert_int_rtn((dz - ${cutoff.z[1]}) * ${nbin.z/(cutoff.z[2]-cutoff.z[1])}), 0, ${nbin.z-1});
        const uint bin_r = min(convert_int_rtn(sqrt(r2) * ${nbin.r/cutoff.r}), ${nbin.r-1});
        const char sp = d_sps[gid].s0;
        if (sp == 0) atomic_inc(&d_bin[bin_z*${nbin.r*2} + bin_r*2 + 0]);
        if (sp == 1) atomic_inc(&d_bin[bin_z*${nbin.r*2} + bin_r*2 + 1]);
      }
    }
  }
}
