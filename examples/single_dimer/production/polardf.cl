/*
 * Polar density function.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

__kernel void
polardf_bin(__global const double3 *restrict d_rd,
            __global const double3 *restrict d_rs,
            __global const char4 *restrict d_spd,
            __global const char4 *restrict d_sps,
            __global uint *restrict d_bin)
{
  const uint gid = get_global_id(0);
  if (gid < ${dom.Ns}) {
    const double3 rs = d_rs[gid];
    const char sps = d_sps[gid].s0;
    for (uint i = 0; i < 2; ++i) {
      const double3 z = normalize(mindist(d_rd[i^1] - d_rd[i]));
      const double3 d = mindist(rs - d_rd[i]);
      const double r2 = dot(d, d);
      if (r2 < ${cutoff.r^2}) {
        const double r = sqrt(r2);
        const double theta = acos(dot(d, z) / r);
        const uint bin_r = min(convert_int_rtn(r * ${nbin.r/cutoff.r}), ${nbin.r-1});
        const uint bin_theta = clamp(convert_int_rtn(theta * ${nbin.theta/math.pi}), 0, ${nbin.theta-1});
        const char spd = d_spd[i].s0;
        if (spd == 0 && sps == 0) atomic_inc(&d_bin[bin_r*${nbin.theta*4} + bin_theta*4 + 0]);
        if (spd == 0 && sps == 1) atomic_inc(&d_bin[bin_r*${nbin.theta*4} + bin_theta*4 + 1]);
        if (spd == 1 && sps == 0) atomic_inc(&d_bin[bin_r*${nbin.theta*4} + bin_theta*4 + 2]);
        if (spd == 1 && sps == 1) atomic_inc(&d_bin[bin_r*${nbin.theta*4} + bin_theta*4 + 3]);
      }
    }
  }
}
