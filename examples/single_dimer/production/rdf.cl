/*
 * Radial density function.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

__kernel void
rdf_bin(__global const double3 *restrict d_rd,
        __global const double3 *restrict d_rs,
        __global const char4 *restrict d_spd,
        __global const char4 *restrict d_sps,
        __global uint *restrict d_bin)
{
  const uint gid = get_global_id(0);
  if (gid < ${dom.Ns}) {
    const double3 rs = d_rs[gid];
    const char sps = d_sps[gid].s0;
    for (uint i = 0; i < 2; ++i) {
      const double3 d = mindist(rs - d_rd[i]);
      const double d2 = dot(d, d);
      if (d2 < ${cutoff^2}) {
        const uint bin = min(convert_int_rtn(sqrt(d2) * ${nbin/cutoff}), ${nbin-1});
        const char spd = d_spd[i].s0;
        const double3 z = mindist(d_rd[i] - d_rd[i^1]);
        if (dot(d, z) > 0) {
          if (spd == 0 && sps == 0) atomic_inc(&d_bin[8*bin + 0]);
          if (spd == 0 && sps == 1) atomic_inc(&d_bin[8*bin + 2]);
          if (spd == 1 && sps == 0) atomic_inc(&d_bin[8*bin + 4]);
          if (spd == 1 && sps == 1) atomic_inc(&d_bin[8*bin + 6]);
        }
        else {
          if (spd == 0 && sps == 0) atomic_inc(&d_bin[8*bin + 1]);
          if (spd == 0 && sps == 1) atomic_inc(&d_bin[8*bin + 3]);
          if (spd == 1 && sps == 0) atomic_inc(&d_bin[8*bin + 5]);
          if (spd == 1 && sps == 1) atomic_inc(&d_bin[8*bin + 7]);
        }
      }
    }
  }
}
