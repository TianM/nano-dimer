#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot probability density of dimer velocity projected onto dimer axis.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--nbin", type=int, default=100, help="number of histogram bins")
parser.add_argument("--range", type=float, nargs=2, default=(-0.1, 0.1), help="range of histogram")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=[], size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

count = 0
hist = np.zeros([args.nbin])
mean = 0.
var = 0.
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["particles/dimer"]
    r = g["position"]["value"]
    v = g["velocity"]["value"]
    L = g["box/edges"][:]
    param = json.loads(f.attrs["parameters"])
    m = param["mass"]
    d = r[:, 0] - r[:, 1]
    d -= np.round(d / L) * L
    z = d / np.sqrt(np.sum(d * d, axis=1)).reshape(-1, 1)
    v = (1 / (1 + m["N"]/m["C"])) * v[:, 0] + (1 / (m["C"]/m["N"] + 1)) * v[:, 1]
    vz = np.sum(v*z, axis=1)
    h, bins = np.histogram(vz, bins=args.nbin, range=args.range, density=True)
    count += 1
    hist += (h - hist) / count
    mean += (np.mean(vz) - mean) / count
    var += (np.var(vz) - var) / count

f = lambda v: 1/np.sqrt(2*np.pi*var) * np.exp(-(v - mean)**2 / (2*var))
v = np.linspace(*args.range, num=500)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.bar(bins[:-1], hist, width=bins[1]-bins[0])
ax.plot(v, f(v), c="r")
ax.axvline(mean, ls="--", c="r")
ax.minorticks_on()
ax.set_xlim(args.range)
ax.set_xlabel(r"$V_{\hat{\bm{z}}}$")
ax.set_ylabel(r"$f(V_{\hat{\bm{z}}})$")
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
