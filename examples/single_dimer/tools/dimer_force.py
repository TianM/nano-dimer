#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot dimer force projected onto dimer axis.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filenames")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=[], size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input, "r") as f:
  g = f["particles/dimer"]
  r = g["position"]["value"]
  F = g["force"]["value"]
  t = g["force"]["time"][()]*np.arange(F.shape[0]) + g["force"]["time"].attrs["offset"]
  L = g["box"]["edges"][:]
  d = r[:, 0] - r[:, 1]
  d -= np.round(d / L) * L
  z = d / np.sqrt(np.sum(d * d, axis=1)).reshape(-1, 1)
  F = np.sum(F, axis=1)
  Fz = np.sum(F * z, axis=1)
  mean = np.mean(Fz)
  sigma = np.std(Fz)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, Fz)
ax.axhline(mean, dashes=(2, 2), c="k")
ax.axhspan(mean-sigma, mean+sigma, alpha=0.25, edgecolor="none")
ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$F_{\hat{\bm{z}}}$")
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
