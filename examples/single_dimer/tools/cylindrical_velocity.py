#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot cylindrical velocity field.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import h5py as h5
import json
import argparse
from mpl_toolkits.axes_grid1 import make_axes_locatable

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--samples", type=int, default=100, help="minimum number of samples per point")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.5), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=10, numpoints=1)

with h5.File(args.input[0], "r") as f:
  g = f["fields/solvent/cylindrical_velocity"]
  radial = g["radial"][:]
  axial =  g["axial"][:]
  value = np.zeros(shape=g["value"].shape)
  count = np.zeros(shape=g["count"].shape)
  cutoff_radial = g["radial"].attrs["cutoff"]
  cutoff_axial = g["axial"].attrs["cutoff"]

for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["fields/solvent/cylindrical_velocity"]
    assert np.all(radial == g["radial"])
    assert np.all(axial == g["axial"])
    i = np.nonzero(g["count"])
    value[i] += (g["value"][:][i] - value[i]) / (count[i] / g["count"][:][i] + 1)[..., np.newaxis, np.newaxis]
    count[i] += g["count"][:][i]

radial = np.swapaxes(radial, 0, 1)
axial = np.swapaxes(axial, 0, 1)
value = np.swapaxes(value, 0, 1)
count = np.swapaxes(count, 0, 1)
x, y = np.meshgrid(axial, radial)
u, v = value[..., 0], value[..., 1]
i = count >= args.samples
x, y, u, v = x[i], y[i], u[i], v[i]
c = np.sqrt(u*u + v*v)

fig, axes = plt.subplots(2, figsize=args.figsize, sharex=True)

for i, ax in enumerate(axes):
  Q = ax.quiver(x, y, u[..., i], v[..., i], c[..., i], pivot="tail", cmap=plt.cm.copper, zorder=2)
  divider = make_axes_locatable(ax)
  cax = divider.append_axes("right", size=0.1, pad=0.1)
  cbar = fig.colorbar(Q, cax=cax)
  cbar.ax.set_ylabel(r"$v$")
  ax.minorticks_on()
  ax.set_aspect("equal")
  ax.set_xlim(cutoff_axial)
  ax.set_ylim(0, cutoff_radial)
  ax.set_ylabel(r"$\rho$")

plt.setp(axes[0].get_xticklabels(), visible=False)
axes[1].set_xlabel(r"$z$")

with h5.File(args.input[0], "r") as f:
  param = json.loads(f.attrs["parameters"])
  bond = param["bond"]
  diameter = param["diameter"]

for ax in axes:
  for S, z in [("N", 0), ("C", bond)]:
    ax.add_patch(patches.Circle([z, 0], diameter[S]/2, color="0.75", fill=True))
    ax.text(z, 0, S, horizontalalignment="center", color="w")

fig.tight_layout(pad=0.1)
fig.savefig(args.output)
