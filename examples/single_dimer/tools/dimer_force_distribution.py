#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot probability density of dimer force projected onto dimer axis.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--nbin", type=int, default=100, help="number of histogram bins")
parser.add_argument("--range", type=float, nargs=2, default=(-100., 100.), help="range of histogram")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=[], size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

count = 0
hist = np.zeros([args.nbin])
mean = 0.
var = 0.
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["particles/dimer"]
    r = g["position"]["value"]
    F = g["force"]["value"]
    L = g["box"]["edges"][:]
    d = r[:, 0] - r[:, 1]
    d -= np.round(d / L) * L
    z = d / np.sqrt(np.sum(d * d, axis=1)).reshape(-1, 1)
    F = np.sum(F, axis=1)
    Fz = np.sum(F*z, axis=1)
    h, bins = np.histogram(Fz, bins=args.nbin, range=args.range, density=True)
    count += 1
    hist += (h - hist) / count
    mean += (np.mean(Fz) - mean) / count
    var += (np.var(Fz) - var) / count

f = lambda v: 1/np.sqrt(2*np.pi*var) * np.exp(-(v - mean)**2 / (2*var))
F = np.linspace(*args.range, num=500)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.bar(bins[:-1], hist, width=bins[1]-bins[0])
ax.plot(F, f(F), c="r")
ax.axvline(mean, ls="--", c="r")
ax.minorticks_on()
ax.set_xlim(args.range)
ax.set_xlabel(r"$F_{\hat{\bm{z}}}$")
ax.set_ylabel(r"$f(F_{\hat{\bm{z}}})$")
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
