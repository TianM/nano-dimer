#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot cylindrical density function.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import h5py as h5
import json
import argparse
from mpl_toolkits.axes_grid1 import make_axes_locatable

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--range", type=float, nargs=2, default=(0., 20.), help="range of contour levels")
parser.add_argument("--species", type=int, help="solvent species")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 3.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input[0], "r") as f:
  g = f["structure/solvent/cylindrical_density"]
  radial = g["radial"][:]
  axial = g["axial"][:]
  mean = np.zeros(shape=g["value"].shape[:2])

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["structure/solvent/cylindrical_density"]
    assert np.all(radial == g["radial"])
    assert np.all(axial == g["axial"])
    value = g["value"][:, :, args.species] if args.species else np.sum(g["value"], axis=-1)
    count += 1
    mean += (value - mean) / count

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
im = np.swapaxes(mean, 0, 1)
extent = [axial[0, 0], axial[-1, 0], radial[0, 0], radial[0, -1]]
norm = mpl.colors.Normalize(*args.range)
csf = ax.imshow(im, extent=extent, cmap=plt.cm.afmhot, origin="lower", norm=norm)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size=0.1, pad=0.1)
cbar = fig.colorbar(csf, cax=cax)
cbar.ax.set_ylabel(r"$\varrho_\textrm{s}$")
ax.minorticks_on()
ax.set_aspect("equal")
ax.set_xlabel(r"$z$")
ax.set_ylabel(r"$\rho$")

with h5.File(args.input[0], "r") as f:
  param = json.loads(f.attrs["parameters"])
  diameter = param["diameter"]
  bond = param["bond"]

for S, z in [("N", 0), ("C", bond)]:
  arc = patches.Arc([z, 0], diameter[S], diameter[S], theta2=180, color="w", lw=0.5)
  ax.add_patch(arc)
  ax.text(z, 0, S, horizontalalignment="center", color="w")

fig.tight_layout(pad=0.1)
fig.savefig(args.output)
