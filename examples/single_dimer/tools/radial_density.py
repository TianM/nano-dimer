#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot radial density function.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 6.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input[0], "r") as f:
  g = f["structure/solvent/radial_density"]
  radial = g["radial"][:]
  value = np.zeros(shape=g["value"].shape)
  species = g.attrs["species"][:]

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["structure/solvent/radial_density"]
    assert np.all(radial == g["radial"])
    count += 1
    value += (g["value"][:] - value) / count

fig, axes = plt.subplots(len(species), 1, sharex=True, figsize=args.figsize)

for i, ax in enumerate(axes):
  ax.plot(radial, np.sum(value[:, i, :, 0], axis=-1), label="+".join(species[i, :]))
  ax.plot(radial, value[:, i, 0, 0], label=species[i, 0])
  ax.plot(radial, value[:, i, 1, 0], label=species[i, 1])
  ax.set_color_cycle(None)
  ax.plot(radial, np.sum(value[:, i, :, 1], axis=-1), ls=":")
  ax.plot(radial, value[:, i, 0, 1], ls=":")
  ax.plot(radial, value[:, i, 1, 1], ls=":")
  ax.minorticks_on()
  ax.set_ylabel(r"$\varrho_s(r)$")
  ax.legend(loc="best", fancybox=True)

ax.set_xlabel(r"$r$")
fig.tight_layout(pad=0.1)
fig.savefig(args.output)
