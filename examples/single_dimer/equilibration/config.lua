-- Edge lengths of simulation domain.
L = {50, 50, 50}
-- Periodic boundary conditions.
periodic = {true, true, true}
-- Number of dimer spheres.
Nd = 2
-- Diameters of dimer spheres.
diameter = {C = 4.0, N = 8.0}
-- Solvent number density.
rho = 9.0
-- Number of solvent particles.
Ns = math.floor(rho*L[1]*L[2]*L[3])
-- Cutoff radius of potential in units of σ.
r_cut = 2^(1/6)
-- Minimum number of steps between solvent particle sorts.
sort = {interval = 500}
-- Solvent temperature.
temp = 1/6
-- Masses of neutrally buoyant dimer spheres.
mass = {C = rho * (math.pi/6) * diameter.C^3, N = rho * (math.pi/6) * diameter.N^3}
-- Distance of dimer spheres.
bond = (diameter.C + diameter.N)/2 * r_cut
-- Potential well depths of dimer spheres with solvent particles.
epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
-- Potential diameters.
sigma = {C = diameter.C/2, N = diameter.N/2}
-- Forward and backward reaction of solvent particles.
reaction = {radius = math.max(L[1], L[2], L[3])/2}
-- Verlet neighbour list skin.
skin = 1.0
-- self defined
external_force = 2.5
-- Number of placeholders per dimer-sphere neighbour list.
nlist_size = {ds = 5000}
-- Number of bins per dimension for dimer spheres.
nbin = {math.floor(L[1]/2), math.floor(L[2]/2), math.floor(L[3]/2)}
-- Number of placeholders per bin.
bin_size = 2
-- Multi-particle collision dynamics.
mpcd = {interval = 50, bin_size = 50}
-- Integration time-step.
timestep = 0.01
-- Number of steps.
nsteps = 100000
-- Thermodynamic variables.
thermo = {initial = 0, final = nsteps, interval = 100, datatype = "double"}
-- Dimer trajectory.
trajd = {initial = 0, final = nsteps, interval = 100, datatype = "double", species = {"C", "N"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Solvent trajectory.
trajs = {initial = nsteps, final = nsteps, interval = nsteps, datatype = "double", species = {"A", "B"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Count solvent species.
species = {initial = 0, final = nsteps, interval = 1000, species = {"A", "B"}}
-- OpenCL platforms.
platform = 1
-- OpenCL device per platform.
device = 1
-- Random number generator seed.
seed = "0xb6002b4159803644"
-- H5MD output filename.
output = "single_dimer.h5"
-- H5MD file author.
author = {name = "B.J. Alder", email = "bjalder@example.org"}
-- H5MD file creator.
creator = {name = "single_dimer.lua", version = "1.0"}
