------------------------------------------------------------------------------
-- Velocity autocorrelation function of dimer.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")
local mpi = require("mpi")

-- Cache C types.
local int_1 = ffi.typeof("int[1]")
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom, file)
  local box = dom.box
  local comm = box.comm
  local rank, nranks = comm:rank(), comm:size()

  local self = {
    group = "dynamics/dimer/velocity_autocorrelation",
    dims = {}, -- scalar
  }

  local Nd = int_1(dom.Nd)
  mpi.allreduce(mpi.in_place, Nd, 1, mpi.int, mpi.sum, comm)
  Nd = Nd[0]

  local vd, idd
  if rank == 0 then
    vd = ffi.new("cl_double3[?]", Nd)
    idd = ffi.new("cl_int[?]", Nd)
  end

  local recvcount, recvdispl
  if rank == 0 then
    recvcount = ffi.new("int[?]", nranks)
    recvdispl = ffi.new("int[?]", nranks)
  end

  function self.sample()
    mpi.gather(int_1(dom.Nd), 1, mpi.int, recvcount, 1, mpi.int, 0, comm)
    if rank == 0 then
      for i = 1, comm:size()-1 do
        recvdispl[i] = recvdispl[i-1] + recvcount[i-1]
      end
    end
    mpi.gatherv(dom.vd, dom.Nd, dom.type_vd, vd, recvcount, recvdispl, dom.type_vd, 0, comm)
    mpi.gatherv(dom.idd, dom.Nd, dom.type_idd, idd, recvcount, recvdispl, dom.type_idd, 0, comm)
    local sample = {}
    if rank == 0 then
      sample.v_cm = vector_n(Nd/2)
      local m_C = 1 / (1 + dom.mass.N/dom.mass.C)
      local m_N = 1 / (1 + dom.mass.C/dom.mass.N)
      for i = 0, Nd-1, 2 do
        local j = idd[i]/2
        sample.v_cm[j].x = m_C*vd[i].x + m_N*vd[i+1].x
        sample.v_cm[j].y = m_C*vd[i].y + m_N*vd[i+1].y
        sample.v_cm[j].z = m_C*vd[i].z + m_N*vd[i+1].z
      end
    end
    return sample
  end

  function self.correlate(sample, sample0, result)
    if rank == 0 then
      local v, v0 = sample.v_cm, sample0.v_cm
      local m = 0
      for i = 0, Nd/2-1 do
        local x = v[i].x*v0[i].x
        local y = v[i].y*v0[i].y
        local z = v[i].z*v0[i].z
        m = m + ((x + y + z) - m) / (i+1)
      end
      result(m)
    end
  end

  function self.snapshot(group, sample)
    local space = hdf5.create_simple_space({Nd/2, 3})
    local dset = group:create_dataset("v_cm", hdf5.double, space)
    if rank == 0 then
      dset:write(sample.v_cm, hdf5.double, space)
    end
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({Nd/2, 3})
    local dset = group:open_dataset("v_cm")
    local sample = {}
    if rank == 0 then
      sample.v_cm = vector_n(Nd/2)
      dset:read(sample.v_cm, hdf5.double, space)
    end
    space:close()
    dset:close()
    return sample
  end

  return self
end
