------------------------------------------------------------------------------
-- Mean-square displacement of dimer.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")
local mpi = require("mpi")

-- Cache C types.
local int_1 = ffi.typeof("int[1]")
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom, file)
  local box = dom.box
  local comm = box.comm
  local rank, nranks = comm:rank(), comm:size()

  local self = {
    group = "dynamics/dimer/mean_square_displacement",
    dims = {}, -- scalar
  }

  local Nd = int_1(dom.Nd)
  mpi.allreduce(mpi.in_place, Nd, 1, mpi.int, mpi.sum, comm)
  Nd = Nd[0]

  local rd, imd, idd
  if rank == 0 then
    rd = ffi.new("cl_double3[?]", Nd)
    imd = ffi.new("cl_short3[?]", Nd)
    idd = ffi.new("cl_int[?]", Nd)
  end

  local recvcount, recvdispl
  if rank == 0 then
    recvcount = ffi.new("int[?]", nranks)
    recvdispl = ffi.new("int[?]", nranks)
  end

  function self.sample()
    mpi.gather(int_1(dom.Nd), 1, mpi.int, recvcount, 1, mpi.int, 0, comm)
    if rank == 0 then
      for i = 1, comm:size()-1 do
        recvdispl[i] = recvdispl[i-1] + recvcount[i-1]
      end
    end
    mpi.gatherv(dom.rd, dom.Nd, dom.type_rd, rd, recvcount, recvdispl, dom.type_rd, 0, comm)
    mpi.gatherv(dom.imd, dom.Nd, dom.type_imd, imd, recvcount, recvdispl, dom.type_imd, 0, comm)
    mpi.gatherv(dom.idd, dom.Nd, dom.type_idd, idd, recvcount, recvdispl, dom.type_idd, 0, comm)
    local sample = {}
    if rank == 0 then
      sample.r_cm = vector_n(Nd/2)
      local L_global = box.L_global
      local m_N = 1 / (1 + dom.mass.C/dom.mass.N)
      for i = 0, Nd-1, 2 do
        local dx = rd[i+1].x - rd[i].x
        local dy = rd[i+1].y - rd[i].y
        local dz = rd[i+1].z - rd[i].z
        dx, dy, dz = box.mindist(dx, dy, dz)
        local j = idd[i]/2
        sample.r_cm[j].x = rd[i].x + L_global[1]*imd[i].x + m_N*dx
        sample.r_cm[j].y = rd[i].y + L_global[2]*imd[i].y + m_N*dy
        sample.r_cm[j].z = rd[i].z + L_global[3]*imd[i].z + m_N*dz
      end
    end
    return sample
  end

  function self.correlate(sample, sample0, result)
    if rank == 0 then
      local r, r0 = sample.r_cm, sample0.r_cm
      local m = 0
      for i = 0, Nd/2-1 do
        local dx = r[i].x - r0[i].x
        local dy = r[i].y - r0[i].y
        local dz = r[i].z - r0[i].z
        m = m + ((dx*dx + dy*dy + dz*dz) - m) / (i+1)
      end
      result(m)
    end
  end

  function self.snapshot(group, sample)
    local space = hdf5.create_simple_space({Nd/2, 3})
    local dset = group:create_dataset("r_cm", hdf5.double, space)
    if rank == 0 then
      dset:write(sample.r_cm, hdf5.double, space)
    end
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({Nd/2, 3})
    local dset = group:open_dataset("r_cm")
    local sample = {}
    if rank == 0 then
      sample.r_cm = vector_n(Nd/2)
      dset:read(sample.r_cm, hdf5.double, space)
    end
    space:close()
    dset:close()
    return sample
  end

  return self
end
