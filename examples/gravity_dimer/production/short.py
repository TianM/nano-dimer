import h5py
import numpy as np
import scipy
import scipy.signal
import json
import matplotlib.pyplot as plt

a = h5py.File('gravity_dimer_bulk4.h5', 'r')

v = a['/particles/dimer/velocity/value'][:]
r = a['/particles/dimer/position/value'][:]
im = a['/particles/dimer/image/value'][:]
L = a['particles/dimer/box/edges'][:]
for i in range(r.shape[2]):
    r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

g_p = a["dynamics/dimer/velocity_autocorrelation_parallel"]
g_n = a["dynamics/dimer/velocity_autocorrelation_normal"]
g = a["dynamics/dimer/velocity_autocorrelation"]

time = g["time"][0]
mean_p = g_p["value"][0]
mean_n = g_n["value"][0]
mean = g["value"][0]

param = json.loads(a.attrs["parameters"].decode('utf-8'))
m = param["mass"]
m_C,m_N = m["C"], m["N"]
v = (m_C*v[:,0,:]+m_N*v[:,1,:])/(m_C+m_N)
N = v.shape[0]
t = np.arange(N)*a['/particles/dimer/velocity/time'][()]

u = r[:,1,:]-r[:,0,:]

du = np.linalg.norm(u[0])
u = u/du

vu = np.einsum('ij,ij->i', u, v)

vp = np.einsum('i,ij->ij', vu,u)
vn = v - vp

vn_norm = np.linalg.norm(vn, axis=1)

vacf_n = scipy.signal.fftconvolve(vn[:,0], vn[::-1,0])[N-1:]
for i in np.arange(1,vn.shape[1]):
    vacf_n += scipy.signal.fftconvolve(vn[:,i], vn[::-1,i])[N-1:]
vacf_n /= (N - np.arange(N))

vacf_n_norm = scipy.signal.fftconvolve(vn_norm, vn_norm[::-1])[N-1:]
vacf_n_norm /= (N - np.arange(N))


vacf_p = scipy.signal.fftconvolve(vp[:,0], vp[::-1,0])[N-1:]
for i in np.arange(1,vn.shape[1]):
    vacf_p += scipy.signal.fftconvolve(vp[:,i], vp[::-1,i])[N-1:]
vacf_p /= (N - np.arange(N))

vacf_p_norm = scipy.signal.fftconvolve(vu, vu[::-1])[N-1:]
vacf_p_norm /= (N - np.arange(N))



vacf_x = scipy.signal.fftconvolve(v[:,0], v[::-1,0])[N-1:]
vacf_x /= (N - np.arange(N))

vacf_y = scipy.signal.fftconvolve(v[:,1], v[::-1,1])[N-1:]
vacf_y /= (N - np.arange(N))

vacf_z = scipy.signal.fftconvolve(v[:,2], v[::-1,2])[N-1:]
vacf_z /= (N - np.arange(N))
plt.axhline(1/(m_C+m_N))
plt.axhline(2/(m_C+m_N))
plt.axhline(3/(m_C+m_N))

#plt.plot(t, scipy.integrate.cumtrapz(vacf_x, x=t, initial=0))
#plt.plot(t, scipy.integrate.cumtrapz(vacf_y, x=t, initial=0))
#plt.plot(t, scipy.integrate.cumtrapz(vacf_z, x=t, initial=0))


print('<V(0)V(0)> parallel     %.3e' %vacf_p[0])
print('<V(0)V(0)> perp         %.3e' %vacf_n[0])
print('<V(0)V(0)> x            %.3e' %vacf_x[0])
print('<V(0)V(0)> y            %.3e' %vacf_y[0])
print('<V(0)V(0)> z            %.3e' %vacf_z[0])
print('<V(0)V(0)> total x+y+z  %.3e' %(vacf_x[0]+vacf_y[0]+vacf_z[0]))
print('<V(0)V(0)> total p+n    %.3e' %(vacf_p[0]+vacf_n[0]))



# plt.plot(t, vacf_x+vacf_y+vacf_z, label='total')
# plt.plot(time, (mean_n+mean_p), label='total sim')
# plt.plot(time, mean_p, label='parallel sim')
# plt.plot(t, vacf_p, label='parallel')
# plt.plot(time, mean_n, label='perp sim')
# plt.plot(t, vacf_n, label='perp')
# plt.ylabel('vacf')
# plt.xlabel('t')
# plt.xlim(1, 10**2)
# plt.ylim(0, 0.0012)
# plt.legend(loc="upper center")


plt.semilogx(t, scipy.integrate.cumtrapz(vacf_n, x=t, initial=0), label='perp')
plt.plot(time, scipy.integrate.cumtrapz(1*mean_n, x=time, initial=0), label='perp sim')
plt.xlim(1, 10**3)
plt.ylim(0, 0.015)
plt.plot(t, scipy.integrate.cumtrapz(vacf_p, x=t, initial=0), label='parallel')
plt.plot(time, scipy.integrate.cumtrapz(1*mean_p, x=time, initial=0), label='parallel sim')
plt.plot(t, scipy.integrate.cumtrapz(vacf_p+vacf_n, x=t, initial=0), label='total')
plt.plot(time, scipy.integrate.cumtrapz(1*(mean_p+mean_n), x=time, initial=0), label='total sim')
plt.legend(loc="upper left")



#plt.plot(t, scipy.integrate.cumtrapz(vacf_x+vacf_y+vacf_z, x=t, initial=0), label='total')
#plt.plot(t, scipy.integrate.cumtrapz(vacf_p+vacf_n, x=t, initial=0), label='total n+p')

plt.show()
