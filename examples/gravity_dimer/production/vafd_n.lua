------------------------------------------------------------------------------
-- Velocity autocorrelation function of dimer.
-- Copyright © 2013–2016 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local sqrt = math.sqrt
-- Cache C types.
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom)
  local box = dom.box
  local self = {
    group = "dynamics/dimer/velocity_autocorrelation_perp",
    dims = {}, -- scalar
  }

  function self.sample()
    local v_k = vector_n(dom.Nd/2)
    local vd = dom.vd
    local rd, imd, L = dom.rd, dom.imd, box.L_global
    local m_C = 1 / (1 + dom.mass.N/dom.mass.C)
    local m_N = 1 / (1 + dom.mass.C/dom.mass.N)
    for i = 0, dom.Nd/2-1 do
      local dx = rd[2*i+1].x - rd[2*i].x
      local dy = rd[2*i+1].y - rd[2*i].y
      local dz = rd[2*i+1].z - rd[2*i].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      local dn = 1 / sqrt(dx*dx + dy*dy + dz*dz)
      dx, dy, dz = dx*dn, dy*dn, dz*dn

      local v_cmx = m_C*vd[2*i].x + m_N*vd[2*i+1].x
      local v_cmy = m_C*vd[2*i].y + m_N*vd[2*i+1].y
      local v_cmz = m_C*vd[2*i].z + m_N*vd[2*i+1].z

      local v_u = v_cmx*dx + v_cmy*dy + v_cmz*dz

      v_k[i].x, v_k[i].y, v_k[i].z = v_cmx - v_u*dx, v_cmy -  v_u*dy, v_cmz - v_u*dz

    end
    return v_k
  end

  function self.correlate(v, v0, result)
    local m = 0
    for i = 0, dom.Nd/2-1 do
      local x = v[i].x*v0[i].x
      local y = v[i].y*v0[i].y
      local z = v[i].z*v0[i].z
      m = m + ((x + y + z) - m) / (i+1)
    end
    result(m)
  end

  function self.snapshot(group, v_n)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:create_dataset("v_n", hdf5.double, space)
    dset:write(v_n, hdf5.double, space)
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:open_dataset("v_n")
    local v_n = vector_n(dom.Nd/2)
    dset:read(v_n, hdf5.double, space)
    space:close()
    dset:close()
    return v_n
  end

  return self
end
