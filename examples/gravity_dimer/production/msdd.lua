------------------------------------------------------------------------------
-- Mean-square displacement of dimer.
-- Copyright © 2013–2016 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache C types.
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom)
  local box = dom.box

  local self = {
    group = "dynamics/dimer/mean_square_displacement",
    dims = {}, -- scalar
  }

  function self.sample()
    local r_cm = vector_n(dom.Nd/2)
    local rd, imd, L = dom.rd, dom.imd, box.L_global
    local m_N = 1 / (1 + dom.mass.C/dom.mass.N)
    for i = 0, dom.Nd/2-1 do
      local dx = rd[2*i+1].x - rd[2*i].x
      local dy = rd[2*i+1].y - rd[2*i].y
      local dz = rd[2*i+1].z - rd[2*i].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      r_cm[i].x = rd[2*i].x + L[1]*imd[2*i].x + m_N*dx
      r_cm[i].y = rd[2*i].y + L[2]*imd[2*i].y + m_N*dy
      r_cm[i].z = rd[2*i].z + L[3]*imd[2*i].z + m_N*dz
    end
    return r_cm
  end

  function self.correlate(r, r0, result)
    local m = 0
    for i = 0, dom.Nd/2-1 do
      local dx = r[i].x - r0[i].x
      local dy = r[i].y - r0[i].y
      local dz = r[i].z - r0[i].z
      m = m + ((dx*dx + dy*dy + dz*dz) - m) / (i+1)
    end
    result(m)
  end

  function self.snapshot(group, r_cm)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:create_dataset("r_cm", hdf5.double, space)
    dset:write(r_cm, hdf5.double, space)
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:open_dataset("r_cm")
    local r_cm = vector_n(dom.Nd/2)
    dset:read(r_cm, hdf5.double, space)
    space:close()
    dset:close()
    return r_cm
  end

  return self
end
