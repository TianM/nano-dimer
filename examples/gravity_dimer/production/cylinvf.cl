/*
 * Cylindrical velocity field.
 * Copyright © 2013–2016 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

|local bit = require("bit")

__kernel void
cylinvf_fill(__global uint *restrict d_bin_count)
{
  const uint gid = get_global_id(0);
  if (gid < ${nbin.r*nbin.z}) d_bin_count[gid] = 0;
}

__kernel void
cylinvf_bin(__global const double3 *restrict d_rd,
            __global const double3 *restrict d_vd,
            __global const double3 *restrict d_rs,
            __global const double3 *restrict d_vs,
            __global float4 *restrict d_bin,
            __global uint *restrict d_bin_count,
            __global uint *restrict d_bin_err)
{
  const uint gid = get_global_id(0);
  if (gid < ${dom.Ns}) {
    double3 z = mindist(d_rd[0] - d_rd[1]);
    const double zz = dot(z, z);
    const double3 r_cm = d_rd[1] + ${1/(1 + dom.mass.N/dom.mass.C)}*z;
    const double3 v_cm = ${1/(1 + dom.mass.N/dom.mass.C)}*d_vd[0] + ${1/(1 + dom.mass.C/dom.mass.N)}*d_vd[1];
    const double3 omega = cross(z, d_vd[0] - d_vd[1]) / zz;
    z *= rsqrt(zz);
    const double3 d = mindist(d_rs[gid] - d_rd[1]);
    const double dz = dot(d, z);
    if (dz > ${cutoff.z[1]} && dz < ${cutoff.z[2]}) {
      const double r2 = dot(d, d) - dz*dz;
      if (r2 < ${cutoff.r^2}) {
        const double r = sqrt(r2);
        const double3 n = (d - dz*z) / r;
        const double3 vs = d_vs[gid];
        const double3 rd = mindist(d_rs[gid] - r_cm);
        const double3 vd = vs - v_cm - cross(omega, rd);
        const float4 value = (float4)(dot(vs, z), dot(vs, n), dot(vd, z), dot(vd, n));
        const uint bin_z = clamp(convert_int_rtn((dz - ${cutoff.z[1]})*${nbin.z / (cutoff.z[2] - cutoff.z[1])}), 0, ${nbin.z-1});
        const uint bin_r = min(convert_int_rtn(r*${nbin.r/cutoff.r}), ${nbin.r-1});
        const uint bin = bin_z*${nbin.r} + bin_r;
        const uint count = atomic_inc(&d_bin_count[bin]);
        if (count < ${bin_size}) d_bin[count + bin*${bin_size}] = value;
        else atomic_max(d_bin_err, count+1);
      }
    }
  }
}

|local work_size = 32

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
cylinvf_sum(__global const float4 *restrict d_bin,
            __global const uint *restrict d_bin_count,
            __global ulong *restrict d_count,
            __global float4 *restrict d_value)
{
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  __local float4 l_value[${work_size}];
  const uint count = min(${bin_size}u, d_bin_count[wid]);
  if (count != 0) {
    float4 value = 0;
    for (uint i = lid; i < count; i += ${work_size}) {
      value += d_bin[i + wid*${bin_size}];
    }
    l_value[lid] = value;
    |local i = work_size
    |while i > 1 do
    |  i = bit.rshift(i, 1)
    barrier(CLK_LOCAL_MEM_FENCE);
    if (lid < ${i}) l_value[lid] += l_value[lid+${i}];
    |end
    if (lid == 0) d_value[wid] += (l_value[0] - d_value[wid]*count) / (d_count[wid] += count);
  }
}
