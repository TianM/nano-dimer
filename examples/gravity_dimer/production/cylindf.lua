------------------------------------------------------------------------------
-- Cylindrical density function.
-- Copyright © 2013–2016 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local hdf5 = require("hdf5")
local ffi = require("ffi")

return function(dom, file, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local nbin, cutoff = args.nbin, args.cutoff
  local initial, final, interval = args.initial, args.final, args.interval

  local program = compute.program(context, "cylindf.cl", {
    dom = dom,
    box = box,
    nbin = nbin,
    cutoff = cutoff,
  })

  local bin = ffi.typeof("struct { cl_uint A, B; }[$][$]", nbin.z, nbin.r)()
  local d_bin = context:create_buffer("use_host_ptr", ffi.sizeof(bin), bin)
  local count = ffi.new("uint32_t[1]")

  local sample do
    local kernel = program:create_kernel("cylindf_bin")
    local work_size = kernel:get_work_group_info(device, "preferred_work_group_size_multiple")
    local glob_size = math.ceil(dom.Ns/work_size) * work_size

    function sample()
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_rs)
      kernel:set_arg(2, dom.d_sps)
      kernel:set_arg(3, d_bin)
      queue:enqueue_write_buffer(dom.d_rd, true, dom.rd)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      count[0] = count[0] + 1
    end
  end

  local group = file:create_group("structure/solvent/cylindrical_density")

  local delta_r = cutoff.r / nbin.r
  local r = ffi.new("float[?]", nbin.r)
  for i = 0, nbin.r-1 do
    r[i] = (i+0.5)*delta_r
  end
  do
    local space_r = hdf5.create_simple_space({1, nbin.r})
    local dset_r = group:create_dataset("radial", hdf5.float, space_r)
    dset_r:write(r, hdf5.float)
    space_r:close()
    dset_r:close()
  end

  local delta_z = (cutoff.z[2]-cutoff.z[1]) / nbin.z
  local z = ffi.new("float[?]", nbin.z)
  for i = 0, nbin.z-1 do
    z[i] = (i+0.5)*delta_z + cutoff.z[1]
  end
  do
    local space_z = hdf5.create_simple_space({nbin.z, 1})
    local dset_z = group:create_dataset("axial", hdf5.float, space_z)
    dset_z:write(z, hdf5.float)
    space_z:close()
    dset_z:close()
  end

  do
    local space_species = hdf5.create_simple_space({2})
    local dtype_species = hdf5.c_s1:copy()
    dtype_species:set_size(2)
    local attr_species = group:create_attribute("species", dtype_species, space_species)
    attr_species:write(ffi.new("char[2][2]", "A", "B"), dtype_species)
    space_species:close()
    dtype_species:close()
    attr_species:close()
  end

  local rho = ffi.typeof("struct { float A, B; }[$][$]", nbin.z, nbin.r)()
  local space_rho = hdf5.create_simple_space({nbin.z, nbin.r, 2})
  local space_count = hdf5.create_space("scalar")
  local dset_rho = group:create_dataset("value", hdf5.float, space_rho)
  local attr_count = group:create_attribute("count", hdf5.uint32, space_count)
  space_rho:close()
  space_count:close()
  dset_rho:close()
  attr_count:close()

  local function write()
    local norm = 2*math.pi * count[0] * delta_z * delta_r
    queue:enqueue_map_buffer(d_bin, true, "read")
    for i = 0, nbin.z-1 do
      for j = 0, nbin.r-1 do
        rho[i][j].A = bin[i][j].A / (r[j]*norm)
        rho[i][j].B = bin[i][j].B / (r[j]*norm)
      end
    end
    queue:enqueue_unmap_mem_object(d_bin, bin)
    dset_rho:write(rho, hdf5.float)
    attr_count:write(count, hdf5.uint32)
  end

  group:close()

  local self = {step = initial}

  function self.observe()
    local group = file:open_group("structure/solvent/cylindrical_density")
    dset_rho = group:open_dataset("value")
    attr_count = group:open_attribute("count")
    group:close()
    while self.step <= final do
      coroutine.yield()
      sample()
      self.step = self.step + interval
    end
    write()
    dset_rho:close()
    attr_count:close()
  end

  function self.snapshot(group)
    local space_bin = hdf5.create_simple_space({nbin.z, nbin.r, 2})
    local space_count = hdf5.create_space("scalar")
    local dset_bin = group:create_dataset("bin", hdf5.uint32, space_bin)
    local attr_count = group:create_attribute("count", hdf5.uint32, space_count)
    queue:enqueue_map_buffer(d_bin, true, "read")
    dset_bin:write(bin, hdf5.uint32, space_bin)
    queue:enqueue_unmap_mem_object(d_bin, bin)
    attr_count:write(count, hdf5.uint32)
    space_bin:close()
    space_count:close()
    dset_bin:close()
    attr_count:close()
  end

  function self.restore(group)
    local space_bin = hdf5.create_simple_space({nbin.z, nbin.r, 2})
    local dset_bin = group:open_dataset("bin")
    local attr_count = group:open_attribute("count")
    queue:enqueue_map_buffer(d_bin, true, "write")
    dset_bin:read(bin, hdf5.uint32, space_bin)
    queue:enqueue_unmap_mem_object(d_bin, bin)
    attr_count:read(count, hdf5.uint32)
    space_bin:close()
    dset_bin:close()
    attr_count:close()
  end

  return self
end
