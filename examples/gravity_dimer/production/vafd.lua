------------------------------------------------------------------------------
-- Velocity autocorrelation function of dimer.
-- Copyright © 2013–2016 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache C types.
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom)
  local self = {
    group = "dynamics/dimer/velocity_autocorrelation",
    dims = {}, -- scalar
  }

  function self.sample()
    local v_cm = vector_n(dom.Nd/2)
    local vd = dom.vd
    local m_C = 1 / (1 + dom.mass.N/dom.mass.C)
    local m_N = 1 / (1 + dom.mass.C/dom.mass.N)
    for i = 0, dom.Nd/2-1 do
      v_cm[i].x = m_C*vd[2*i].x + m_N*vd[2*i+1].x
      v_cm[i].y = m_C*vd[2*i].y + m_N*vd[2*i+1].y
      v_cm[i].z = m_C*vd[2*i].z + m_N*vd[2*i+1].z
    end
    return v_cm
  end

  function self.correlate(v, v0, result)
    local m = 0
    for i = 0, dom.Nd/2-1 do
      local x = v[i].x*v0[i].x
      local y = v[i].y*v0[i].y
      local z = v[i].z*v0[i].z
      m = m + ((x + y + z) - m) / (i+1)
    end
    result(m)
  end

  function self.snapshot(group, v_cm)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:create_dataset("v_cm", hdf5.double, space)
    dset:write(v_cm, hdf5.double, space)
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:open_dataset("v_cm")
    local v_cm = vector_n(dom.Nd/2)
    dset:read(v_cm, hdf5.double, space)
    space:close()
    dset:close()
    return v_cm
  end

  return self
end
