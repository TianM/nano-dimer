#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
# mpl.use("pgf")
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import matplotlib.cm as cm
import numpy as np
import h5py as h5
import json
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("dir1", help="x, y, or z direction")
parser.add_argument("dir2", help="x, y, or z direction")
parser.add_argument("input", nargs="+", help="input filename")
args = parser.parse_args()




with h5.File(args.input[0], "r") as f:
  v = f['/particles/dimer/velocity/value'][:]
  r = f['/particles/dimer/position/value'][:]
  im = f['/particles/dimer/image/value'][:]
  L = f['/particles/dimer/box/edges'][:]
  time = f["/particles/dimer/position/time"][()]*np.arange(r.shape[0]) + f["/particles/dimer/position/time"].attrs["offset"]
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]
  bond = param["bond"]
  for i in range(3):
      r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

nParticles = r.shape[1]
nDimers = int(r.shape[1]/2+0.5)
nTimesteps = r.shape[0]

dir1=dir(args.dir1)
dir2=dir(args.dir2)

zmin1 = 0
zmax1 = L[dir1]
zmin2 = 0
zmax2 = L[dir2]
N1 = 10
N2 = int(np.floor(N1*zmax1/zmax2))
histTheta = np.zeros([N1,N2])
histPhi = np.zeros([N1,N2])
histSamples = np.zeros([N1,N2])

# for bulk
# zmin = min(rz)
# zmax = max(rz)+1
#

vz = np.zeros([nTimesteps,nDimers,3])
rcom = np.zeros([nTimesteps,nDimers,3])
u =  np.zeros([nTimesteps,nDimers,3])
dataTheta = np.zeros([nTimesteps,nDimers])
dataPhi = np.zeros([nTimesteps,nDimers])

for fn in args.input:
  with h5.File(fn, "r") as f:
        v = f['/particles/dimer/velocity/value'][:]
        r = f['/particles/dimer/position/value'][:]
        im = f['/particles/dimer/image/value'][:]
        L = f['/particles/dimer/box/edges'][:]
        time = f["/particles/dimer/position/time"][()]*np.arange(r.shape[0]) + f["/particles/dimer/position/time"].attrs["offset"]
        for i in range(3):
            r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

        for i in range(0,nDimers):
            vz[:,i,:]= (1 / (1 + m["N"]/m["C"])) * v[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * v[:, 2*i+1,:]
            rcom[:,i,:]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1,:]
            u[:,i,:] = mindist(r[:, 2*i,:] - r[:, 2*i+1,:])/bond
            dataTheta[:,i] = np.arccos(u[:,i,dir('z')])
            dataPhi[:,i] = np.arctan2(u[:,i,dir('y')],(u[:,i,dir('x')]))
            rcom=mindist(rcom)
            r1 = rcom[:,:,dir1].reshape(1,-1).T
            r2 = rcom[:,:,dir2].reshape(1,-1).T
            dataTheta = dataTheta.reshape(1,-1).T
            dataPhi = dataPhi.reshape(1,-1).T

# v_norm = np.array([np.linalg.norm(vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
# v_parallel = np.array([np.dot(u[i,j,:],vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
#


        for i in range(0,nTimesteps*nDimers):
            index1 = int(np.floor((r1[i] - zmin1)*N1/(zmax1-zmin1)))
            index2 = int(np.floor((r2[i] - zmin2)*N2/(zmax2-zmin2)))

            if(index1<N1 and index2<N2):
                histTheta[index1,index2] += dataTheta[i]
                histPhi[index1,index2] += dataPhi[i]
                histSamples[index1,index2] += 1
            else:
                print(index1)
                print(index2)

# TODO histSamples != 0
cond=histSamples!=0
histTheta[cond]=histTheta[cond]/histSamples[cond]
histPhi[cond]=histPhi[cond]/histSamples[cond]

cmap=cm.viridis
cmap.set_under(color='black')

fig=plt.figure(1)
ax1 = fig.add_subplot(211)
ax1.imshow((1/np.pi)*histTheta, extent=(zmin1, zmax1, zmin2, zmax2),interpolation='nearest',cmap=cmap, vmin=0.0000001)
plt.xlabel(dir1)
plt.ylabel(dir2)

ax2 = fig.add_subplot(212)
plt.xlabel(dir1)
plt.ylabel(dir2)

ax2.imshow((1/np.pi)*histPhi, extent=(zmin1, zmax1, zmin2, zmax2),interpolation='nearest',cmap=cmap, vmin=0.0000001)


#ax2.yaxis.set_major_formatter(tck.FormatStrFormatter('%g $\pi$'))
#ax2.yaxis.set_major_locator(tck.MultipleLocator(base=0.1))
#plt.plot(np.linspace(zmin,zmax, num=N,endpoint=True),(1/np.pi)*histPhi)
plt.ylabel(r'$\phi$')


if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
