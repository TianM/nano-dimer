#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot trajectory of sphere dimer.
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import argparse
import sys
import json
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser()
parser.add_argument("input", help="input filename")
args = parser.parse_args()


# double3 mindist(double3 d)
# {
#   |for i = 1, 3 do
#   |  if periodic_global[i] then
#   d.s${i-1} -= round(d.s${i-1}*${1/L_global[i]})*${L_global[i]};
#   |  end
#   |end
#   return d;
# }

with h5.File(args.input, "r") as f:
  v = f['/particles/dimer/velocity/value'][:]
  r = f['/particles/dimer/position/value'][:]
  im = f['/particles/dimer/image/value'][:]
  L = f['/particles/dimer/box/edges'][:]
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]
  for i in range(3):
      r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

nParticles = r.shape[1]
nDimers = int(r.shape[1]/2+0.5)
nTimesteps = r.shape[0]

vz = np.zeros([nTimesteps,nDimers,3])
d = np.zeros([nTimesteps,nDimers,3])
rcom = np.zeros([nTimesteps,nDimers,3])
for i in range(0,nDimers):
    vz[:,i,:] = (1 / (1 + m["N"]/m["C"])) * v[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * v[:, 2*i+1,:]
    d[:,i,:] = mindist(r[:, 2*i,:] - r[:, 2*i+1,:])
    rcom[:,i,:]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1,:]

# orientation vector
u=d/param["bond"]

v_norm = np.array([np.linalg.norm(vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
v_parallel = np.array([np.dot(u[i,j,:],vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
v_perp = np.sqrt(abs(v_norm*v_norm-v_parallel*v_parallel))


print("vs_total = " + str(np.mean(v_norm)))
print("vs_parallel = " + str(np.mean(v_parallel)))
print("vs_perp = " + str(np.mean(v_perp)))
