#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from matplotlib.patches import Circle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("dir1", help="x, y, or z direction")
parser.add_argument("dir2", help="x, y, or z direction")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()



def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]
dir1=dir(args.dir1)
dir2=dir(args.dir2)

with h5.File(args.input, "r") as f:
  r = f['/particles/dimer/position/value'][:]
  im = f['/particles/dimer/image/value'][:]
  L = f['/particles/dimer/box/edges'][:]
  for i in range(0,L.shape[0]):
    #   r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]
      r[:,:,i]=r[:,:,i]%L[i]

fig = plt.figure(figsize=(5, 10))
ax = fig.add_subplot(111, aspect='equal')
for x, y in zip(r[-1,:,dir1], r[-1,:,dir2]):
    ax.add_artist(Circle(xy=(x, y),
                  radius=3))

# TODO: set markersize relative to axes scaling

plt.xlim([0,L[dir1]])
plt.ylim([0,L[dir2]])
#plt.axis('equal')

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
