#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("direction", help="x, y, or z direction")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()


def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

with h5.File(args.input, "r") as f:
  r = f['/particles/solvent/position/value'][:]
  im = f['/particles/solvent/image/value'][:]
  L = f['/particles/solvent/box/edges'][:]
  species = f['/particles/solvent/species/value'][:][0]
  print(r.shape)
  print(im.shape)
  for i in range(0,L.shape[0]):
      r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

plt.hist(r[0,:,dir(args.direction)][species==0],bins=200, histtype='step',label="A")
plt.hist(r[0,:,dir(args.direction)][species==1],bins=200, histtype='step',label="B")
plt.legend()

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
