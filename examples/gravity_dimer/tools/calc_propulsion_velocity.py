#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot trajectory of sphere dimer.
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import argparse
import sys
import json
from scipy.optimize import curve_fit


parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
args = parser.parse_args()

with h5.File(args.input[0], "r") as f:
  L = f['/particles/dimer/box/edges'][:]
  r = f['/particles/dimer/position/value'][:]
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]
  nParticles = r.shape[1]
  nDimers = int(r.shape[1]/2+0.5)
  nTimesteps = r.shape[0]


def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

Nbins = 100
count = 0
data = np.zeros([len(args.input),Nbins]);

for fn in args.input:
    with h5.File(fn, "r") as f:
        v = f['/particles/dimer/velocity/value'][:]
        r = f['/particles/dimer/position/value'][:]
        im = f['/particles/dimer/image/value'][:]
        for i in range(3):
            r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]
        vz = np.zeros([nTimesteps,nDimers,3])
        d = np.zeros([nTimesteps,nDimers,3])
        rcom = np.zeros([nTimesteps,nDimers,3])
        for i in range(0,nDimers):
            vz[:,i,:] = (1 / (1 + m["N"]/m["C"])) * v[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * v[:, 2*i+1,:]
            d[:,i,:] = mindist(r[:, 2*i,:] - r[:, 2*i+1,:])
            rcom[:,i,:]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1,:]
        # orientation vector
        u=d/param["bond"]
        v_norm = np.array([np.linalg.norm(vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
        v_parallel = np.array([np.dot(u[i,j,:],vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
        v_perp = np.sqrt(abs(v_norm*v_norm-v_parallel*v_parallel))

        data[count,:], bin_edges = np.histogram(v_parallel.flatten(), density=True, bins=Nbins, range=[-0.1,0.1])

        count += 1


mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);
bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

def fitfunc(x, c, mu, sigma):
    return c*np.exp(-(x-mu)**2/(2*sigma**2))
popt, pcov = curve_fit(fitfunc, bin_centers, mean,p0 = [10, 0.01, 0.1])
print(popt)
print(np.sqrt(np.diag(pcov)))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.plot(bin_centers, mean)
plt.fill_between(bin_centers, mean-error, mean+error,
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)
plt.plot(bin_centers, fitfunc(bin_centers, *popt), '--', label='fit-with-bounds')
ax.axvline(0, linestyle='--', color='k') # vertical lines
ax.axvline(popt[1], linestyle='--', color='k') # vertical lines
plt.xlabel(r'$v_u$')
plt.ylabel(r'$\rho(v_u)$')
print("vs = " + str(popt[1]))
fig.tight_layout(pad=0.1)


if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
