#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot histogram of position in z direction.
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from scipy.optimize import curve_fit
import json

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("direction", help="x, y, or z direction")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filename")
args = parser.parse_args()


with h5.File(args.input[0], "r") as f:
    r = f["particles/dimer/position/value"]
    nParticles = r.shape[1]
    nDimers = int(r.shape[1]/2+0.5)
    nTimesteps = r.shape[0]
    param = json.loads(f.attrs["parameters"].decode('utf-8'))
    m = param["mass"]

Nbins = 50
count = 0
average = 0
nFiles = len(args.input)
rz = np.zeros((nTimesteps,nDimers))
mean = np.zeros(Nbins)
error = np.zeros(Nbins)
r0 = np.zeros([3,1])

for j in range(nFiles):
  fn = args.input[j]
  with h5.File(fn, "r") as f:
    L = f["particles/dimer/box/edges"]
    r = f["particles/dimer/position/value"]
    im = f["particles/dimer/image/value"]
    r = r[:,:, dir(args.direction)] + im[:,:,dir(args.direction)]*L[dir(args.direction)]
    for i in range(0,nDimers):
        rz[:,i]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1]
    value, bin_edges = np.histogram(rz.flatten(), density=True, bins=Nbins, range=[-L[dir(args.direction)], L[dir(args.direction)]])
    count += 1
    delta = value - mean
    mean += delta / count
    error += delta * (value - mean)
    average += np.mean(rz.flatten())

error = np.sqrt(error / (count - 1) / count)
bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])
average = average /(count)

fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
ax.plot(bin_centers, mean)
ax.fill_between(bin_centers, mean-error, mean+error,
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)
ax.set_ylabel(r'$\rho$')
ax.set_xlabel(r'$z$')
ax.legend()

#plt.plot(bin_centers[bin_centers>=walldz],fitfunc(bin_centers[bin_centers>=walldz], popt[0], popt[1]))
#plt.plot(bin_centers[bin_centers>=walldz],fitfunc(bin_centers[bin_centers>=walldz], ((m["N"]+m["C"])*param["gravc"]), 1/((m["N"]+m["C"])*param["gravc"])))

plt.xlabel(args.direction)
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
