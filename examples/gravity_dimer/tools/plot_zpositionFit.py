#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot histogram of position in z direction.
#
import scipy.integrate
import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from scipy.optimize import curve_fit
import json

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("direction", help="x, y, or z direction")
parser.add_argument("show", help="show or save plot")
parser.add_argument("vu", type=float)
parser.add_argument("lower", type=float)
parser.add_argument("upper", type=float)
parser.add_argument("nPlot", type=int)
parser.add_argument("input", nargs="+", help="input filename")
args = parser.parse_args()
vu = args.vu
lower = args.lower
upper = args.upper

with h5.File(args.input[0], "r") as f:
    r = f["particles/dimer/position/value"]
    nParticles = r.shape[1]
    nDimers = int(r.shape[1]/2+0.5)
    nTimesteps = r.shape[0]
    param = json.loads(f.attrs["parameters"].decode('utf-8'))
    m = param["mass"]
    walldz = param["wall"]["dz"]

Nbins = 500
count = 0
average = 0
nFiles = len(args.input)
rz = np.zeros((nTimesteps,nDimers))

data = np.zeros([len(args.input), Nbins]);

for j in range(nFiles):
  fn = args.input[j]
  with h5.File(fn, "r") as f:
    L = f["particles/dimer/box/edges"]
    r = f["particles/dimer/position/value"]
    im = f["particles/dimer/image/value"]
    r = r[:,:, dir(args.direction)] + im[:,:,dir(args.direction)]*L[dir(args.direction)]
    for i in range(0,nDimers):
        rz[:,i]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1]
    data[count,:], bin_edges = np.histogram(rz.flatten(), density=True, bins=Nbins, range=[0, L[dir(args.direction)]])
    count += 1


mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);

bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])


mask = (bin_centers >= lower) & (bin_centers <= upper)
masked_centers = bin_centers[mask]
deltaEff = 4.9*(1+1/6*vu**2/(2.20*2.93*10**(-7)))
integralVal = deltaEff*(np.exp(-masked_centers[0]/deltaEff)-np.exp(-masked_centers[-1]/deltaEff))
norm = scipy.integrate.trapz(mean[mask], x=masked_centers)
print('delta Eff: %.3e' %(deltaEff))

def fitfunc(z):
    return norm/integralVal*np.exp(-z/deltaEff)

mask = ((bin_centers>=lower) & (bin_centers<=upper))
# popt, pconv=curve_fit(fitfunc, bin_centers[mask], mean[mask], p0=[3, vu ])





fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
cm = plt.get_cmap('inferno')
mycolor = cm(1/6*args.nPlot)

p = plt.plot(bin_centers, mean,color=mycolor)


# plt.plot(bin_centers[bin_centers>=walldz+.5],fitfunc(bin_centers[bin_centers>=walldz+.5]),color=p[0].get_color(),linestyle='dashed',label='_nolegend_')
plt.ylabel(r'$\rho(z)$')

plt.xlim([5,30])
# plt.ylim([0,.15*10**0])
plt.ylim([10**-3,.2*10**0])
# ax.set_yscale("log")

fig.tight_layout()

plt.xlabel(args.direction)
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
