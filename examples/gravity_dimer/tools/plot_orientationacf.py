#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot orientation autocorrelation function of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
args = parser.parse_args()

with h5.File(args.input[0], "r") as f:
  g = f["dynamics/dimer/orientation_autocorrelation"]
  time = g["time"][:]
  index = g["count"][:] != 0
  time = np.reshape(time[:, 1:], (-1,))
  index = np.reshape(index[:, 1:], (-1,))

data = np.zeros([len(args.input),len(time)]);

count = 0;
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["dynamics/dimer/orientation_autocorrelation"]
    value = g["value"][:]
    data[count,:] = np.reshape(value[:, 1:], (-1,));
    count += 1

mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);

order = time.argsort()
time = time[order]
index = index[order]
mean = mean[order]
error = error[order]

fitTime=5000
plotTime=5000
def fitfunc(t, D):
    return np.exp(-2*D*t)

popt, pcov=curve_fit(fitfunc, time[time<=fitTime], mean[time<=fitTime])
print('Rotational diffusion constant: %.3e' %popt[0])
print("error: %.3e " %(np.sqrt(pcov)))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.plot(time[time<=plotTime],mean[time<=plotTime])
plt.fill_between(time[time<=plotTime], mean[time<=plotTime]-error[time<=plotTime],\
 mean[time<=plotTime]+error[time<=plotTime],
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)
plt.plot(time[time<=plotTime], fitfunc(time[time<=plotTime],popt[0]),label = str('Rotational diffusion constant: %.3e' %popt[0]))
ax.set_xscale("linear")
# ax.set_xscale("log")
# ax.set_yscale("log")
plt.ylim([0,1])

# plt.ylim([.1,1.1])
plt.xlim([0,5000])

ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r'$\left<\hat{u}(0)^\top\hat{u}(t)\right>$')

plt.tight_layout(pad = 0)

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
