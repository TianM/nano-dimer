#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Calculate hydrodynamic parameters
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import argparse
import json
import sys
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser()
parser.add_argument("input", help="input filename")
parser.add_argument("alpha", help="collision angle in multiples of pi", type=float)
parser.add_argument("T", help="temperature", type=float)
parser.add_argument("a", help="colloid radius", type=float)
parser.add_argument("Dcol", help="diffusion coefficient colloid", type=float)
parser.add_argument("vs", help="propulsion velocity", type=float)
args = parser.parse_args()

f = h5.File(args.input, "r")

param = json.loads(f.attrs["parameters"].decode('utf-8'))
diameterN = param["diameter"]["N"]
diameterC = param["diameter"]["C"]
m = param["mass"]
mass = m["N"]+m["C"]
tcoll=param["timestep"]*param["mpcd"]["interval"]
print(param["mpcd"]["interval"])
T=args.T
gamma=param["rho"]
#g=param["gravc"]
g=10**-4
Dcol=args.Dcol
alpha=args.alpha*np.pi
lambd=tcoll*np.sqrt(T)


fkin=15*gamma/((gamma-1+np.exp(-gamma))*(4-2*np.cos(alpha)-2*np.cos(2*alpha)))-3/2
vkin=fkin*np.sqrt(T)*lambd/3

fcol=(1-np.cos(alpha))*(1-1/gamma+np.exp(-gamma)/gamma)
vcol=fcol*np.sqrt(T)/(18*lambd)

Df=np.sqrt(T)*lambd*(3/(2*(1-np.cos(alpha))*(gamma/(gamma-1) -1./2)))

kinematicViscosity=vkin+vcol
shearViscosity=gamma*kinematicViscosity

# 4 pi for slip
#vs=(m["N"]+m["C"])*g/(4*np.pi*5)
vs = args.vs
print("mass = " + str(mass))
print("Tau_Brownian= " + str(mass*Dcol))
print("Tau_eq >= " + str((mass)/(6*np.pi*shearViscosity*5)))

print("fkin = " + str(fkin))
print("vkin = " + str(vkin))

print("fcol = " + str(fcol))
print("vcol = " + str(vcol))

print("kinematic viscosity v = " + str(kinematicViscosity))
print("shear viscosity eta = " + str(shearViscosity))

print("tcoll = " + str(tcoll))
print("lambda = " + str(tcoll))

print("v_propulsion = " + str(vs))
print("v_sedimentation = " + str(mass*g*Dcol))
print("Stokes time = " + str(args.a /vs))
print("Sc = " + str((kinematicViscosity)/Df))
print("Pe_active = " + str(vs*args.a/(Dcol)))
print("Pe_sedimt = " + str(mass*g*args.a*Dcol/(Dcol)))
print("delta = " + str(1/(mass*g)))
# Pe_gravit = m*g*R/kbT =  m*g*R
# see enculescu 2011
print("Pe_gravit = alpha = " + str(mass*g*args.a))


print("Ma = " + str(vs/np.sqrt(T*5./3)))
print("Re = " + str(vs*args.a /(kinematicViscosity)))
print("Kn = " + str(lambd/args.a))
print("Hydrodynamic radius estimate = " + str(T/(np.pi*6*Dcol*shearViscosity)))
print("m*g/gamma = " + str((mass)*g/(6*np.pi*shearViscosity*args.a)))
