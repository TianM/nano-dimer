#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot mean-square displacement of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

with h5.File(args.input[0], "r") as f:
  g = f["dynamics/dimer/mean_square_displacement"]
  time = g["time"][:]
  index = g["count"][:] != 0
  print(time.shape)
  time = np.reshape(time[:, 1:], (-1,))
  index = np.reshape(index[:, 1:], (-1,))

data = np.zeros([len(args.input),len(time)]);

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["dynamics/dimer/mean_square_displacement"]
    value = g["value"][:]
    data[count,:] = np.reshape(value[:, 1:], (-1,));
    count += 1

mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);

order = time.argsort()
time = time[order]
index = index[order]
mean = mean[order]
error = error[order]
time = time[index]
mean = mean[index]
error = error[index]

def fitfunc(t, D):
    return 6*D*t
mask = (time >100) & (time<1000)
popt, pconv=curve_fit(fitfunc, time[mask], mean[mask])
print('Diffusion constant: %.3e' %popt[0])
print(pconv)
tmax = time[-1]
tPlotmax = time[-1]
def fitfuncLin(t, D):
    return D*t*t


fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
plt.plot(time[index], mean[index])
plt.fill_between(time[index], mean[index]-error[index], mean[index]+error[index],
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)

plt.plot(time[index], fitfunc(time[index],popt[0]),label = str('Diffusion constant: %.3e' %popt[0]))
plt.xlim([time[0],tPlotmax])
# plt.ylim([0,.1])

ax.set_xscale("log")
ax.set_yscale("log")
#ax.set_xscale("linear")
#ax.set_yscale("linear")

ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\Delta L^2(t)$")
fig.tight_layout()

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
