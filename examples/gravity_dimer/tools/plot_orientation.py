#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot trajectory of sphere dimer.
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import argparse
import sys
from scipy.optimize import curve_fit

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]



def autocorr(x):
    # zero padding
    xrenorm=x;
    #if x.ndim==1:
    #    xrenorm = xrenorm-np.ones(x.shape)*np.mean(x);
    xpadd=np.concatenate((xrenorm,np.zeros(xrenorm.shape)), axis=0)
    # print('xpadd.shape: '+ str(xpadd.shape))
    def corrKernel(i):
        # normalise with the number of elements correlated
        norm=x.shape[0]-i
        return np.sum(np.multiply(xpadd,np.roll(xpadd,i,axis=0)))/norm
    result = list(map(corrKernel, np.arange(len(x))))
    return result/result[0]

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("outputang", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()

f = h5.File(args.input, "r")
g = f["particles/dimer"]
position = g["position/value"]
im= g["image/value"]
time = g["position/time"][()]*np.arange(position.shape[0]) + g["position/time"].attrs["offset"]
L = g["box/edges"][:]
boundary = g["box"].attrs["boundary"]
data = position[:,:,:]
# time, particle, (x,y,z)
for i in range(3):
    data[:,:,i]=data[:,:,i]+im[:,:,i]*L[i]

d = data[:,0,:]- data[:,1,:]
# checking the bond vector length
L=np.linalg.norm(d,axis=1)
r=L[0]
# print(L[0], L[1], L[2])
dataTheta=np.arccos(d[:,dir('z')]/r)
dataPhi=np.arctan(d[:,dir('y')]/d[:,dir('x')])

# orientation vector
u=d/r
# autocorrelation function of u
uCorr=autocorr(u)


plt.figure(1)
ax1=plt.subplot(2, 1, 1)
plt.plot(time,dataTheta/np.pi)
plt.ylabel(r'$\theta$')

ax1.yaxis.set_major_formatter(tck.FormatStrFormatter('%g $\pi$'))
#ax1.yaxis.set_major_locator(tck.MultipleLocator(base=0.1))

ax2=plt.subplot(2, 1, 2)
plt.plot(time,dataPhi/np.pi)
plt.ylabel(r'$\phi$')
plt.xlabel('t')
ax2.yaxis.set_major_formatter(tck.FormatStrFormatter('%g $\pi$'))

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.outputang)
else: print('Last argument must be either "show" or "save"!')


# plt.rc('legend',**{'fontsize':10})
# plt.legend()
plt.ylabel(r'$\left<\hat{u}(0)\cdot \hat{u}(t)\right>$')

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
# ADD plt.close()!
