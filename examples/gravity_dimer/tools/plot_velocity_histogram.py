#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
# mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import json
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("dir", help="x, y, or z direction")
parser.add_argument("input", nargs="+", help="input filenames")
args = parser.parse_args()


with h5.File(args.input[0], "r") as f:
  v = f['/particles/dimer/velocity/value'][:]
  r = f['/particles/dimer/position/value'][:]
  im = f['/particles/dimer/image/value'][:]
  L = f['/particles/dimer/box/edges'][:]
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]
  bond = param["bond"]
  for i in range(3):
      r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

nParticles = r.shape[1]
nDimers = int(r.shape[1]/2+0.5)
nTimesteps = r.shape[0]

N = 50
hist = np.zeros([N])
histSamples = np.zeros([N])
zmin = 0
zmax = L[dir(args.dir)]

# for bulk
# zmin = min(rz)
# zmax = max(rz)+1


for fn in args.input:
  with h5.File(fn, "r") as f:
        v = f['/particles/dimer/velocity/value'][:]
        r = f['/particles/dimer/position/value'][:]
        im = f['/particles/dimer/image/value'][:]
        L = f['/particles/dimer/box/edges'][:]
        for i in range(3):
            r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

        vz = np.zeros([nTimesteps,nDimers,3])
        rcom = np.zeros([nTimesteps,nDimers,3])
        u =  np.zeros([nTimesteps,nDimers,3])
        for i in range(0,nDimers):
            vz[:,i,:]= (1 / (1 + m["N"]/m["C"])) * v[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * v[:, 2*i+1,:]
            rcom[:,i,:]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1,:]
            u[:,i,:] = mindist(r[:, 2*i,:] - r[:, 2*i+1,:])/bond

        v_norm = np.array([np.linalg.norm(vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])
        v_parallel = np.array([np.dot(u[i,j,:],vz[i,j,:]) for i in range(nTimesteps) for j in range(nDimers)])

        rz = rcom[:,:,dir(args.dir)].reshape(1,-1).T

        for i in range(0,nTimesteps*nDimers):
            index = int(np.floor((rz[i] - zmin)*N/(zmax-zmin)))
            # total velocity
            hist[index] += v_norm[i]
            # parallel velocity
            #hist[index] += v_parallel[i]
            histSamples[index] += 1

cond=histSamples!= 0
hist[cond]=hist[cond]/histSamples[cond]

plt.plot(np.linspace(zmin,zmax, num=N,endpoint=True),hist)
plt.xlabel(args.dir)
plt.ylabel(r'|v|')

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
