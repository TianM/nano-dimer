#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot velocity autocorrelation function of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import json
import argparse
from scipy.optimize import curve_fit
import scipy.integrate
import sys



parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("direction",  help="direction: parallel, perp or all")
# parser.add_argument("block", type=int, help="block level")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
args = parser.parse_args()

with h5.File(args.input[0], "r") as f:
  if args.direction == "parallel":
    g = f["dynamics/dimer/velocity_autocorrelation_parallel"]
  elif args.direction == "perp":
    g = f["dynamics/dimer/velocity_autocorrelation_perp"]
  else:
    g = f["dynamics/dimer/velocity_autocorrelation"]
  time = g["time"][:]
  index = g["count"][:] != 0
  time = np.reshape(time[:, 1:], (-1,))
  index = np.reshape(index[:, 1:], (-1,))

  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]

data = np.zeros([len(args.input),len(time)]);

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    if args.direction == "parallel":
      g = f["dynamics/dimer/velocity_autocorrelation_parallel"]
    elif args.direction == "perp":
      g = f["dynamics/dimer/velocity_autocorrelation_perp"]
    else:
      g = f["dynamics/dimer/velocity_autocorrelation"]
    value = g["value"][:]
    data[count,:] = np.reshape(value[:, 1:], (-1,));
    count += 1

mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);

order = time.argsort()
time = time[order]
index = index[order]
mean = mean[order]
error = error[order]
time = time[index]
mean = mean[index]
error = error[index]

tmax = 1000;
mean = mean[time<=tmax]
error = error[time<=tmax]
time = time[time<=tmax]

# uncomment to plot vacf directly
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.plot(time, mean/mean[0])
plt.fill_between(time, (mean-error)/mean[0], (mean+error)/mean[0],
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)


# uncomment to plot integral


# for diffusion coefficient
ax.minorticks_on()
ax.set_xlabel(r"$t$")
if args.direction == "parallel":
    ax.set_ylabel(r"$\langle\hat{V}_\parallel(t)^2\rangle/\langle\hat{V}_\parallel(0)^2\rangle$")
elif args.direction == "perp":
    ax.set_ylabel(r"$\langle\hat{V}_\perp(t)^2\rangle/\langle\hat{V}_\perp(0)^2\rangle $")
else:
    ax.set_ylabel(r"$\langle\hat{V}(t)^2\rangle/\langle\hat{V}(0)^2\rangle $")

plt.xlim([0,50])
plt.ylim([10**-2,1])
# ax.set_xscale("log")
ax.set_yscale("log")

plt.tight_layout(pad = 0)
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
