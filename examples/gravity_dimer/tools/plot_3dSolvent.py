import h5py
from mayavi import mlab
a = h5py.File('/media/tina/Tinas_Blaue/nano-dimer/examples/gravity_dimer/production/gravity_dimer_20521395.h5', 'r')

pos = a['/particles/dimer/position/value'][-1]
edges = a['/particles/dimer/box/edges'][:]

posSolvent = a['/particles/solvent/position/value'][-1]



mlab.points3d(pos[:,0], pos[:,1], pos[:,2])
mlab.points3d(posSolvent[:,0], posSolvent[:,1], posSolvent[:,2])



mlab.outline([0, edges[0], 0, edges[1], 0, edges[2]])

mlab.show()
