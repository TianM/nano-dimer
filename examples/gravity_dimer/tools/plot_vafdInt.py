#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot velocity autocorrelation function of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import json
import argparse
from scipy.optimize import curve_fit
import scipy.integrate
import sys



parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("direction",  help="direction: parallel, perp or all")
# parser.add_argument("block", type=int, help="block level")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
args = parser.parse_args()

with h5.File(args.input[0], "r") as f:
  if args.direction == "parallel":
    g = f["dynamics/dimer/velocity_autocorrelation_parallel"]
  elif args.direction == "perp":
    g = f["dynamics/dimer/velocity_autocorrelation_perp"]
  else:
    g = f["dynamics/dimer/velocity_autocorrelation"]
  time = g["time"][:]
  index = g["count"][:] != 0
  time = np.reshape(time[:, 1:], (-1,))
  index = np.reshape(index[:, 1:], (-1,))
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]

data = np.zeros([len(args.input),len(time)]);

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    if args.direction == "parallel":
      g = f["dynamics/dimer/velocity_autocorrelation_parallel"]
    elif args.direction == "perp":
      g = f["dynamics/dimer/velocity_autocorrelation_perp"]
    else:
      g = f["dynamics/dimer/velocity_autocorrelation"]
    value = g["value"][:]
    data[count,:] = np.reshape(value[:, 1:], (-1,));
    count += 1

mean = np.mean(data, axis = 0);
error = np.std(data, axis = 0, ddof=1);

order = time.argsort()
time = time[order]
index = index[order]
mean = mean[order]
error = error[order]
time = time[index]
mean = mean[index]
error = error[index]

tmax = 1000;
mean = mean[time<tmax]
error = error[time<tmax]
time = time[time<tmax]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.plot(time, scipy.integrate.cumtrapz(mean, x=time, initial=0))

plt.fill_between(time, scipy.integrate.cumtrapz(mean-error, x=time, initial=0),
 scipy.integrate.cumtrapz(mean+error, x=time, initial=0),
    alpha=0.2, edgecolor='blue', facecolor='blue',
    linewidth=1, linestyle='-', antialiased=True)

timeInt = 100;
plt.axvline(100)

mean = mean[time<timeInt]
error = error [time<timeInt]
time = time[time<timeInt]

# calculation of diffusion coefficient via integral
D= scipy.integrate.trapz(mean, x=time)
if args.direction == "parallel":
    print('diffusion coefficient parallel  %.3e' %D)
    print('    %.3e' %((D-scipy.integrate.trapz(mean-error, x=time))))
    print('   %.3e' %((D-scipy.integrate.trapz(mean+error, x=time))))
elif args.direction == "perp":
    print('diffusion coefficient perp    %.3e' %(D/2))
    print('    %.3e' %((D-scipy.integrate.trapz(mean-error, x=time))/2))
    print('   %.3e' %((D-scipy.integrate.trapz(mean+error, x=time))/2))
else:
    print('diffusion coefficient total     %.3e' %(D/3))
    print('    %.3e' %((D-scipy.integrate.trapz(mean-error, x=time))/3))
    print('   %.3e' %((D-scipy.integrate.trapz(mean+error, x=time))/3))



# for diffusion coefficient
ax.minorticks_on()
ax.set_xlabel(r"$t$")
if args.direction == "parallel":
    ax.set_ylabel(r"$\int_0^\infty \langle\hat{V}_\parallel(t)^\top\hat{V}_\parallel(0)\rangle dt$ ")
elif args.direction == "perp":
    ax.set_ylabel(r"$\int_0^\infty \langle\hat{V}_\perp(t)^\top\hat{V}_\perp(0)\rangle dt$")
else:
    ax.set_ylabel(r"$\int_0^\infty \langle\hat{V}(t)^\top\hat{V}(0)\rangle dt$")

plt.xlim([0,150])
if args.direction == "parallel":
    plt.ylim([0,.006])
elif args.direction == "perp":
    plt.ylim([0,.008])
else:
    plt.ylim([0,.0125])





plt.tight_layout()
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
