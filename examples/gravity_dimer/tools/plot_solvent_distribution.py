#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("dir1", help="x, y, or z direction")
parser.add_argument("dir2", help="x, y, or z direction")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()



def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]
dir1=dir(args.dir1)
dir2=dir(args.dir2)

with h5.File(args.input, "r") as f:
  r = f['/particles/solvent/position/value'][:]
  im = f['/particles/solvent/image/value'][:]
  L = f['/particles/solvent/box/edges'][:]
  species = f['/particles/solvent/species/value'][:][0]
  print(species.shape)
  for i in range(0,L.shape[0]):
    #   r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]
      r[:,:,i]=r[:,:,i]%L[i]

fig = plt.figure(figsize=(5, 10))
plt.subplot(2, 1, 1)
ax1=plt.hist2d(r[0,:,dir1][species==0],r[0,:,dir2][species==0],bins=50, range=[[0,L[dir1]],[0,L[dir2]]])
plt.subplot(2, 1, 2)
ax2=plt.hist2d(r[0,:,dir1][species==1],r[0,:,dir2][species==1],bins=50, range=[[0,L[dir1]],[0,L[dir2]]])

# fig=plt.figure(figsize=(10, 10))
# ax = plt.subplot(111)
# h1, xedges, yedges=np.histogram2d(r[0,:,dir1][species==0],r[0,:,dir2][species==0],bins=50, range=[[0,L[dir1]],[0,L[dir2]]])
# h2, xedges, yedges=np.histogram2d(r[0,:,dir1][species==1],r[0,:,dir2][species==1],bins=50, range=[[0,L[dir1]],[0,L[dir2]]])
# h = h2 / h1
# pc = ax.pcolorfast(xedges, yedges, h.T)

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
