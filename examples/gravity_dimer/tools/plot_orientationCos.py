#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
# mpl.use("pgf")
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import matplotlib.cm as cm
import numpy as np
import h5py as h5
import json
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("dir", help="x, y, or z direction")
parser.add_argument("input", nargs="+", help="input filename")
args = parser.parse_args()


with h5.File(args.input[0], "r") as f:
  v = f['/particles/dimer/velocity/value'][:]
  r = f['/particles/dimer/position/value'][:]
  im = f['/particles/dimer/image/value'][:]
  L = f['/particles/dimer/box/edges'][:]
  time = f["/particles/dimer/position/time"][()]*np.arange(r.shape[0]) + f["/particles/dimer/position/time"].attrs["offset"]
  param = json.loads(f.attrs["parameters"].decode('utf-8'))
  m = param["mass"]
  m_C,m_N = m["C"], m["N"]
  bond = param["bond"]
  for i in range(3):
      r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

nParticles = r.shape[1]
nDimers = int(r.shape[1]/2+0.5)
nTimesteps = r.shape[0]

zmin = 0
zmax = L[dir(args.dir)]
thetaMin=0
thetaMax=np.pi

N = 50
histTheta = np.zeros(N)
histSamples = np.zeros(N)


# for bulk
# zmin = min(rz)
# zmax = max(rz)+1
#

nFiles=len(args.input)

vz = np.zeros([nTimesteps,nDimers,3])
rcom = np.zeros([nTimesteps,nDimers,3])
u =  np.zeros([nTimesteps,nDimers,3])
dataRcom = np.zeros([nTimesteps,nDimers,nFiles])
dataTheta = np.zeros([nTimesteps,nDimers,nFiles])
dataPhi = np.zeros([nTimesteps,nDimers,nFiles])
count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
        v = f['/particles/dimer/velocity/value'][:]
        r = f['/particles/dimer/position/value'][:]
        im = f['/particles/dimer/image/value'][:]
        L = f['/particles/dimer/box/edges'][:]
        time = f["/particles/dimer/position/time"][()]*np.arange(r.shape[0]) + f["/particles/dimer/position/time"].attrs["offset"]
        for i in range(3):
            r[:,:,i]=r[:,:,i]+im[:,:,i]*L[i]

        for i in range(0,nDimers):
            vz[:,i,:]= (1 / (1 + m["N"]/m["C"])) * v[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * v[:, 2*i+1,:]
            rcom[:,i,:]= (1 / (1 + m["N"]/m["C"])) * r[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * r[:, 2*i+1,:]
            u[:,i,:] = mindist(r[:, 2*i,:] - r[:, 2*i+1,:])/bond
            dataTheta[:,i,count] = u[:,i,dir('z')]
            dataRcom[:,i,count]=rcom[:,i,dir(args.dir)]
        count += 1

rcomFlat = dataRcom.flatten()
dataThetaFlat = dataTheta.flatten()
dataPhiFlat = dataPhi.flatten()
for i in range(0,len(rcomFlat)):
    index = int(np.floor((rcomFlat[i] - zmin)*N/(zmax-zmin)))
    histTheta[index] += dataThetaFlat[i]
    histSamples[index] += 1
cond=histSamples!=0
histTheta[cond]=histTheta[cond]/histSamples[cond]

fig=plt.figure(1)
plt.plot(np.linspace(zmin,zmax,num=N),histTheta)
plt.xlabel(r"z")
plt.ylabel(r"$\left\langle\cos(\theta)\right\rangle$")

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
