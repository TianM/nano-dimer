
import h5py
import matplotlib.pyplot as plt
import sys

a = h5py.File(sys.argv[1], 'r')

pos = a['/particles/dimer/position/value'][-1]
edges = a['/particles/dimer/box/edges'][:]
species = a['/particles/dimer/species/value'][-1]

from mayavi import mlab
mlab.figure(size = (800,1000),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))

mlab.points3d(pos[:,0], pos[:,1], pos[:,2], 1-species, scale_mode='none', scale_factor=6.7, resolution=32)
mlab.outline(extent = [0,64,5,17.5,5,123])
mlab.view(azimuth=90, elevation=90)
mlab.orientation_axes()
mlab.show()
