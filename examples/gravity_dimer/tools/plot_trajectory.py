#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot trajectory of sphere dimer.
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import argparse
import matplotlib.ticker as tck
import json
import sys

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input",help="input filename")

args = parser.parse_args()


print(args.output)
print(args.show)
print(args.input)

def mindist(d):
    d = d-np.floor(d/L+0.5)*L
    return (d)

def dir(x):
    return {
        'x': 0,
        'y': 1,
        'z': 2,
    }[x]

f = h5.File(args.input, "r")
g = f["particles/dimer"]
position = g["position/value"]
im = g["image/value"]
time = g["position/time"][()]*np.arange(position.shape[0]) #+ g["position/time"].attrs["offset"]
L = g["box/edges"][:]
boundary = g["box"].attrs["boundary"]
pos = position[:,:,:]

nParticles = pos.shape[1]
nDimers = int(nParticles/2+.5)
nTimesteps = pos.shape[0]
nDimensions = pos.shape[2]
print(time[-1])
for i in range(nDimensions):
    pos[:,:,i]=pos[:,:,i]+im[:,:,i]*L[i]



param = json.loads(f.attrs["parameters"].decode('utf-8'))
m = param["mass"]


d = np.zeros((nTimesteps,nDimers,nDimensions))
print(d.shape)
for i in range(nDimers):
    d[:,i,:] = mindist(pos[:,2*i,:]- pos[:,2*i+1,:])
# checking the bond vector length
L=np.linalg.norm(d,axis=2)
r=L.flatten()[0]

dataTheta=np.zeros((nTimesteps,nDimers))
dataPhi=np.zeros((nTimesteps,nDimers))
for i in range(nDimers):
    dataTheta[:,i]=np.arccos(d[:,i,dir('z')]/r)
    dataPhi[:,i]=np.arctan(d[:,i,dir('y')]/d[:,i,dir('x')])


 # for all particles
data = np.zeros((nTimesteps,nDimers,nDimensions))
for i in range(nDimers):
    data[:,i,:] = (1 / (1 + m["N"]/m["C"])) * pos[:, 2*i,:] + (1 / (m["C"]/m["N"] + 1)) * pos[:, 2*i+1,:]

axes = [plt.subplot(5, 1, 1)]
axes.extend([plt.subplot(5, 1, i, sharex=axes[0]) for i in [2,3,4,5]])

for i, ax in enumerate(axes):
    if (i<=2):
        print('xyz'[i], 'mean', data[:,:,i].mean(),'std', data[:,:,i].std())
        for j in range(nDimers):
            ax.plot(time[:nTimesteps], data[:,j,i], label = str(j))
        ax.set_ylabel('xyz'[i])

    if (i==3):
        for j in range(nDimers):
            ax.plot(time[:nTimesteps],dataTheta[:,j]/np.pi, label = str(j))
        ax.yaxis.set_major_formatter(tck.FormatStrFormatter('%g $\pi$'))
        ax.set_ylabel(r'$\theta$')

    if (i==4):
        for j in range(nDimers):
            ax.plot(time[:nTimesteps],dataPhi[:,j]/np.pi, label = str(j))
        ax.yaxis.set_major_formatter(tck.FormatStrFormatter('%g $\pi$'))
        ax.set_ylabel(r'$\phi$')

legend = ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2), ncol=nParticles, shadow=True)
plt.subplots_adjust(wspace=0, hspace=0)

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
