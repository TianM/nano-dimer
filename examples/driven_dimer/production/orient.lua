------------------------------------------------------------------------------
-- Orientation autocorrelation of dimer.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local sqrt = math.sqrt
-- Cache C types.
local vector_n = ffi.typeof("struct { double x, y, z; }[?]")

return function(dom)
  local box = dom.box

  local self = {
    group = "dynamics/dimer/orientation_autocorrelation",
    dims = {}, -- scalar
  }

  function self.sample()
    local d = vector_n(dom.Nd/2)
    local rd = dom.rd
    for i = 0, dom.Nd/2-1 do
      local dx = rd[2*i+1].x - rd[2*i].x
      local dy = rd[2*i+1].y - rd[2*i].y
      local dz = rd[2*i+1].z - rd[2*i].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      local dn = 1 / sqrt(dx*dx + dy*dy + dz*dz)
      d[i].x, d[i].y, d[i].z = dx*dn, dy*dn, dz*dn
    end
    return d
  end

  function self.correlate(d, d0, result)
    local m = 0
    for i = 0, dom.Nd/2-1 do
      local x = d[i].x*d0[i].x
      local y = d[i].y*d0[i].y
      local z = d[i].z*d0[i].z
      m = m + ((x + y + z) - m) / (i+1)
    end
    result(m)
  end

  function self.snapshot(group, d)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:create_dataset("d", hdf5.double, space)
    dset:write(d, hdf5.double, space)
    space:close()
    dset:close()
  end

  function self.restore(group)
    local space = hdf5.create_simple_space({dom.Nd/2, 3})
    local dset = group:open_dataset("d")
    local d = vector_n(dom.Nd/2)
    dset:read(d, hdf5.double, space)
    space:close()
    dset:close()
    return d
  end

  return self
end
