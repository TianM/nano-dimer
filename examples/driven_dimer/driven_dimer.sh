#PBS -q gpu
#PBS -l nodes=1:ppn=2:gpus=2
#PBS -l walltime=3:00:00
#PBS -m n
#PBS -V
set -e -o pipefail
PROJECT="${PBS_O_WORKDIR}/nano-dimer"
cd "${PROJECT}" && . examples/env.sh
OUTPUT="${OUTPUT:-${PBS_JOBNAME%%.sh*}_${PBS_JOBID%%[^0-9]*}}"
SEED="$(date +%s)\0${PBS_O_HOST}\0${PBS_JOBID}"
TASKS=()
for TASK in 1 2
do
  if [ ! -e "${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.h5" ]
  then
    mkdir -p "${PBS_O_WORKDIR}/equilibration"
    cat >"${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.lua" <<EOF
-- Edge lengths of simulation domain.
L = {50, 50, 50}
-- Periodic boundary conditions.
periodic = {true, true, true}
-- Number of dimer spheres.
Nd = 2
-- Diameters of dimer spheres.
diameter = {C = 4.0, N = 8.0}
-- Solvent number density.
rho = 9.0
-- Number of solvent particles.
Ns = math.floor(rho*L[1]*L[2]*L[3])
-- Cutoff radius of potential in units of σ.
r_cut = 2^(1/6)
-- Minimum number of steps between solvent particle sorts.
sort = {interval = 500}
-- Solvent temperature.
temp = 1/6
-- Masses of neutrally buoyant dimer spheres.
mass = {C = rho * (math.pi/6) * diameter.C^3, N = rho * (math.pi/6) * diameter.N^3}
-- Distance of dimer spheres.
bond = (diameter.C + diameter.N)/2 * r_cut
-- Potential well depths of dimer spheres with solvent particles.
epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 1.0}
-- Potential diameters.
sigma = {C = diameter.C/2, N = diameter.N/2}
-- Magnitude of external force along dimer bond.
external_force = 2.5
-- Verlet neighbour list skin.
skin = 1.0
-- Number of placeholders per dimer-sphere neighbour list.
nlist_size = {ds = 5000}
-- Number of bins per dimension for dimer spheres.
nbin = {math.floor(L[1]/2), math.floor(L[2]/2), math.floor(L[3]/2)}
-- Number of placeholders per bin.
bin_size = 2
-- Multi-particle collision dynamics.
mpcd = {interval = 50, bin_size = 50, temp = temp}
-- Integration time-step.
timestep = 0.01
-- Number of steps.
nsteps = 200000
-- Thermodynamic variables.
thermo = {initial = 0, final = nsteps, interval = 1000, datatype = "double"}
-- Dimer trajectory.
trajd = {initial = 0, final = nsteps, interval = 1000, datatype = "double", species = {"C", "N"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Solvent trajectory.
trajs = {initial = nsteps, final = nsteps, interval = nsteps, datatype = "double", species = {"A", "B"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Count solvent species.
species = {initial = 0, final = nsteps, interval = 1000, species = {"A", "B"}}
-- OpenCL platforms.
platform = 1
-- OpenCL device per platform.
device = ${TASK}
-- Random number generator seed.
seed = "0x$(echo -ne "${SEED}\0${TASK}\0equilibration" | md5sum | cut -c-16)"
-- H5MD output filename.
output = "${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.h5"
-- H5MD file author.
author = {name = "$(git config user.name)", email = "$(git config user.email)"}
-- H5MD file creator.
creator = {name = "driven_dimer.lua", version = "$(cd "${PROJECT}" && git describe --always)"}
EOF
    cd "${PROJECT}/examples/driven_dimer/equilibration"
    ./driven_dimer.lua "${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.lua" | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >>"${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.log" &
    TASKS+=("${!}")
  fi
done
for TASK in "${TASKS[@]}"
do
  wait "${TASK}"
done
TASKS=()
for TASK in 1 2
do
  if [ ! -e "${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}.h5" ]
  then
    mkdir -p "${PBS_O_WORKDIR}/production"
    cat >"${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}.lua" <<EOF
-- Edge lengths of simulation domain.
L = {50, 50, 50}
-- Periodic boundary conditions.
periodic = {true, true, true}
-- Number of dimer spheres.
Nd = 2
-- Diameters of dimer spheres.
diameter = {C = 4.0, N = 8.0}
-- Solvent number density.
rho = 9.0
-- Number of solvent particles.
Ns = math.floor(rho*L[1]*L[2]*L[3])
-- Cutoff radius of potential in units of σ.
r_cut = 2^(1/6)
-- Minimum number of steps between solvent particle sorts.
sort = {interval = 500}
-- Solvent temperature.
temp = 1/6
-- Masses of neutrally buoyant dimer spheres.
mass = {C = rho * (math.pi/6) * diameter.C^3, N = rho * (math.pi/6) * diameter.N^3}
-- Distance of dimer spheres.
bond = (diameter.C + diameter.N)/2 * r_cut
-- Potential well depths of dimer spheres with solvent particles.
epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 1.0}
-- Potential diameters.
sigma = {C = diameter.C/2, N = diameter.N/2}
-- Magnitude of external force along dimer bond.
external_force = 2.5
-- Verlet neighbour list skin.
skin = 1.0
-- Number of placeholders per dimer-sphere neighbour list.
nlist_size = {ds = 5000}
-- Number of bins per dimension for dimer spheres.
nbin = {math.floor(L[1]/2), math.floor(L[2]/2), math.floor(L[3]/2)}
-- Number of placeholders per bin.
bin_size = 2
-- Multi-particle collision dynamics.
mpcd = {interval = 50, bin_size = 50, temp = temp}
-- Integration time-step.
timestep = 0.01
-- Number of steps.
nsteps = 10000000
-- Snapshot program state.
snapshot = {initial = 1000000, final = nsteps - 1000000, interval = 1000000, output = "${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}-snapshot.h5"}
-- Thermodynamic variables.
thermo = {initial = 0, final = nsteps, interval = 10000, datatype = "double"}
-- Dimer trajectory.
trajd = {initial = 0, final = nsteps, interval = 1000, datatype = "float", species = {"C", "N"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Solvent trajectory.
trajs = {initial = nsteps, final = nsteps, interval = nsteps, datatype = "float", species = {"A", "B"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Cylindrical velocity field.
cylinvf = {initial = 0, final = nsteps, interval = 1000, nbin = {r = 25, z = 50}, bin_size = 2000, cutoff = {r = math.min(L[1], L[2], L[3])/2, z = {-math.min(L[1], L[2], L[3])/2, math.min(L[1], L[2], L[3])/2}}}
-- Mean-square displacement of dimer.
msdd = {initial = 0, final = nsteps, interval = 100, blockcount = 5, blocksize = 100, blockshift = 10, separation = 10000}
-- Velocity autocorrelation function of dimer.
vafd = {initial = 0, final = nsteps, interval = 100, blockcount = 5, blocksize = 100, blockshift = 10, separation = 10000}
-- Orientation autocorrelation of dimer.
orient = {initial = 0, final = nsteps, interval = 100, blockcount = 5, blocksize = 100, blockshift = 10, separation = 10000}
-- OpenCL platforms.
platform = 1
-- OpenCL device per platform.
device = ${TASK}
-- Random number generator seed.
seed = "0x$(echo -ne "${SEED}\0${TASK}\0production" | md5sum | cut -c-16)"
-- H5MD input filename.
input = "${PBS_O_WORKDIR}/equilibration/${OUTPUT}_${TASK}.h5"
-- Read trajectory at step.
step = 200000
-- H5MD output filename.
output = "${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}.h5"
-- H5MD file author.
author = {name = "$(git config user.name)", email = "$(git config user.email)"}
-- H5MD file creator.
creator = {name = "driven_dimer.lua", version = "$(cd "${PROJECT}" && git describe --always)"}
EOF
    cd "${PROJECT}/examples/driven_dimer/production"
    ./driven_dimer.lua "${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}.lua" | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush(); }' >>"${PBS_O_WORKDIR}/production/${OUTPUT}_${TASK}.log" &
    TASKS+=("${!}")
  fi
done
for TASK in "${TASKS[@]}"
do
  wait "${TASK}"
done
