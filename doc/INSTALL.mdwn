---
title: Installing
---


Git
---

Nano-dimer is available from a [git repository]:

~~~
git clone http://git.colberg.org/nano-dimer
~~~

In the repository, checkout the latest release version:

~~~
git checkout 1.0.0
~~~

If you have GnuPG, verify that the tag is signed by the following key:

~~~
git tag -v 1.0.0
~~~

~~~
Primary key fingerprint: 2997 6EC2 2BD1 EBBC CC77  F3A6 183B D5ED 3527 8611
     Subkey fingerprint: 983B F9D0 F02A 6014 0CEC  529F 767E 12A8 C92A 31CB
~~~

After installing the prerequisites as described below, run the test suite:

~~~
make test
~~~

[git repository]: http://git.colberg.org/nano-dimer


Prerequisites
-------------

The following software is required to run the code:

  - [LuaJIT] version 2.0.4 or later.
  - An OpenCL library conforming to OpenCL version 1.1 or later.
  - An OpenCL driver conforming to OpenCL version 1.1 or later.
  - [HDF5] version 1.8.0 or later.
  - [OpenCL for Lua] version 1.2.0 or later.
  - [HDF5 for Lua] version 2.0.0 or later.
  - [Templet for Lua] version 1.0.0 or later.
  - [ljsyscall] version 0.10 or later.
  - [Lua CJSON] version 2.1.0 or later.

[LuaJIT]: http://luajit.org/download.html
[HDF5]: http://www.hdfgroup.org/HDF5/release/obtain5.html
[OpenCL for Lua]: http://colberg.org/lua-opencl/
[HDF5 for Lua]: http://colberg.org/lua-hdf5/
[Templet for Lua]: http://colberg.org/lua-templet/
[ljsyscall]: https://github.com/justincormack/ljsyscall
[Lua CJSON]: http://www.kyne.com.au/~mark/software/lua-cjson.php

While not required, it is recommended to install the Lua modules above using

  - [LuaRocks] version 2.2.2 or later.

[LuaRocks]: http://luarocks.org/


Installation guide
------------------

The following provides a step-by-step guide to installing all of the above.

If you intend to run the code on a machine that is maintained by someone
else, please ask them to install the following software:

  - [LuaJIT](#luajit)
  - [LuaRocks](#luarocks)
  - [OpenCL](#opencl)
  - [HDF5](#hdf5)

You may then install the Lua modules using LuaRocks:

  - [OpenCL for Lua](#opencl-for-lua)
  - [HDF5 for Lua](#hdf5-for-lua)
  - [Templet for Lua](#templet-for-lua)
  - [ljsyscall](#ljsyscall)
  - [Lua CJSON](#lua-cjson)

Most of the software packages may be installed with the [packages makefile]:

~~~
git clone http://git.colberg.org/packages
~~~

The makefile supports the automated installation of various scientific
software packages into a user’s home directory or a system-wide directory.
Each software is downloaded, verified against a fingerprint, compiled, and
installed into its own subdirectory. The installed software is intended—but
not required—to be used together with [environment modules].

[packages makefile]: http://git.colberg.org/packages
[environment modules]: http://modules.sourceforge.net/


### LuaJIT

GCC 4 is required to compile LuaJIT.
Please refer to the [LuaJIT installation] manual for details.

LuaJIT may be downloaded and installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-luajit
~~~

LuaJIT may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-luajit >> ~/.bashrc
~~~

After restarting the shell, verify that the LuaJIT interpreter works:

~~~
luajit
~~~

You may exit the interpreter using `Ctrl` + `d`.

[LuaJIT installation]: http://luajit.org/install.html


### LuaRocks

LuaRocks is a package manager for Lua modules.
Please refer to the [LuaRocks documentation] for details.

LuaRocks must be installed using LuaJIT, since we will be installing
modules that require LuaJIT to run tests.

LuaRocks may be downloaded and installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-luarocks
~~~

LuaRocks may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-luarocks >> ~/.bashrc
~~~

Append the following line *in verbatim* to your `~/.bashrc`

~~~
eval $(luarocks path)
~~~

which sets the environment variable `LUA_PATH`.

After restarting the shell, verify that LuaRocks works:

~~~
luarocks help
~~~

[LuaRocks documentation]: https://github.com/keplerproject/luarocks/wiki/Documentation


### OpenCL

An OpenCL installation consists of two parts: an OpenCL library and one or
multiple OpenCL drivers. Programs link against the OpenCL library to use
OpenCL. The OpenCL library loads an OpenCL driver to access the devices of
the corresponding platform.

All vendors ship both the library and a driver for their supported devices.
Due to the vendor-independent [OpenCL ICD extension], any OpenCL library
may be used with any OpenCL driver, as long as the OpenCL version of the
library is equal or greater than the OpenCL version of the driver.

[OpenCL ICD extension]: https://www.khronos.org/registry/cl/extensions/khr/cl_khr_icd.txt


#### OpenCL library

Separate installation of an OpenCL library is not needed in most cases.

Installing the free/libre OpenCL library [ocl-icd] may be useful when
setting up OpenCL on a machine without root privileges. By default, OpenCL
drivers are configured by creating a file `/etc/OpenCL/<driver>.icd` that
contains the library name of the driver, e.g., `libnvidia-opencl.so.1`.
Using ocl-icd, the OpenCL drivers directory may be overriden by setting
the environment variable `OCL_ICD_VENDORS`, to a path in the user’s home
directory, or directly to the library name of the driver.

[ocl-icd] may be installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-opencl-icd
~~~

ocl-icd may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-opencl-icd >> ~/.bashrc
~~~

[ocl-icd]: https://forge.imag.fr/projects/ocl-icd/


#### OpenCL drivers

All currently available drivers that comply with OpenCL version 1.1 and
later are non-free software. The [pocl] project is working on a free/libre
OpenCL driver for CPUs, but so far it does not provide the features and
performance necessary to run the code.

[pocl]: http://portablecl.org/

For AMD GPUs, you need to install the AMD GPU driver. The driver installs
the tool `clinfo` that displays information about all available OpenCL
devices. A headless server may require running—but not using—the X server
to access AMD GPU devices.

For NVIDIA GPUs, you need to install the NVIDIA GPU driver. The driver
installs the tool `nvidia-smi` that displays information about available
NVIDIA GPUs. The NVIDIA Visual Profiler may be used to interactively
profile OpenCL kernels on NVIDIA GPUs; note that OpenCL profiling
requires NVIDIA CUDA driver version 4.2 or earlier, which does not
work with [NVIDIA Tesla GPUs] newer than the Fermi microarchitecture.

[NVIDIA Tesla GPUs]: https://en.wikipedia.org/wiki/Nvidia_Tesla#Specifications_and_configurations

For Intel Core and Intel Xeon CPUs, the Intel OpenCL driver may be installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-intel-opencl-icd
~~~

The Intel OpenCL driver may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-intel-opencl-icd >> ~/.bashrc
~~~

For x86-64 CPUs, the AMD OpenCL driver may be installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-amd-opencl-icd
~~~

The AMD OpenCL driver may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-amd-opencl-icd >> ~/.bashrc
~~~

Both drivers are configured for use with ocl-icd installed above.


### HDF5

The HDF5 library may be installed with

~~~
make -f packages.mk PREFIX=$HOME/opt install-hdf5
~~~

If you wish to compile HDF5 including the MPIO driver for parallel HDF5 use

~~~
make -f packages.mk PREFIX=$HOME/opt install-hdf5 USE_MPI=1
~~~

This requires the same compiler version that was used to compile the MPI library.

The HDF5 library may be included in your shell environment with

~~~
make -f packages.mk PREFIX=$HOME/opt env-hdf5 >> ~/.bashrc
~~~

After restarting the shell, verify that HDF5 works:

~~~
h5dump
~~~


### OpenCL for Lua

An OpenCL device is required to run tests during installation.

OpenCL for Lua may be installed with

~~~
luarocks install --local opencl
~~~


### HDF5 for Lua

GCC 4.5 or later is required to parse the HDF5 library headers.
Please refer to [HDF5 for Lua installation] manual for details.

HDF5 for Lua may be installed with

~~~
luarocks install --local hdf5
~~~

[HDF5 for Lua installation]: http://colberg.org/lua-hdf5/INSTALL.html


### Templet for Lua

Templet for Lua may be installed with

~~~
luarocks install --local templet
~~~


### ljsyscall

ljsyscall may be installed with

~~~
luarocks install --local ljsyscall
~~~


### Lua CJSON

Lua CJSON may be installed with

~~~
luarocks install --local lua-cjson
~~~
