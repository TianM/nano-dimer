---
title: About nano-dimer
---

[Nano-dimer] is a molecular modelling software to study the dynamics of
chemically powered self-propelled nanomotors [@Kapral2013].
The software combines molecular dynamics and multi-particle collision
dynamics [@Malevanets1999; @Malevanets2000; @Ihle2001] to simulate the
molecular motion of a single to hundreds of sphere-dimer motors
[@Rueckner2007] and their long-range hydrodynamic interaction in a
solvent comprising several million particles.

Nano-dimer relies on novel computational techniques to scale from a
multi-core processor to a parallel accelerator with thousands of scalar
cores. The simulation algorithms are composed in the [OpenCL C] language
using [run-time code generation], which allows running simulations with
optimal efficiency across a variety of devices, such as AMD GPUs, NVIDIA
GPUs, and Intel CPUs.

A simulation is orchestrated using [Lua] scripts run with [LuaJIT], an
interpreter and [tracing just-in-time compiler] for the Lua language that
provides native C data structures through its foreign function interface
([FFI]). Particle trajectories of nanomotors and solvent, and a versatile
set of structural and dynamical observables are stored in an [H5MD] file,
a file format for molecular data [@Buyl2014] based on the hierarchical
data format ([HDF5]).

[Nano-dimer]: http://colberg.org/nano-dimer/
[OpenCL C]: https://www.khronos.org/registry/cl/sdk/1.2/docs/man/xhtml/
[run-time code generation]: http://colberg.org/lua-templet/
[Lua]: http://www.lua.org/about.html
[LuaJIT]: http://luajit.org/luajit.html
[tracing just-in-time compiler]: https://en.wikipedia.org/wiki/Tracing_just-in-time_compilation
[FFI]: http://luajit.org/ext_ffi.html
[H5MD]: http://nongnu.org/h5md/
[HDF5]: http://www.hdfgroup.org/HDF5/


References
----------
