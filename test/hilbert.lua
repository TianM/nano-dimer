------------------------------------------------------------------------------
-- Test Hilbert space-filling curve for domain with unequal side lengths.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local compute = require("nanomotor.compute")
local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Test 2-dimensional Hilbert curve.
do
  local M = {2, 5}
  local program = compute.program(context, "hilbert.cl", {M = M})

  local N = 2^(M[1]+M[2])
  local d_bin = context:create_buffer(N*ffi.sizeof("cl_uint3"))
  local kernel = program:create_kernel("test_hilbert")
  kernel:set_arg(0, d_bin)
  queue:enqueue_ndrange_kernel(kernel, nil, {N})

  local file = hdf5.open_file("hilbert_2x5.h5")
  local dset = file:open_dataset("value")
  local buf = ffi.new("int[?][2]", N)
  local space = hdf5.create_simple_space({N, 2})
  dset:read(buf, hdf5.int, space)
  space:close()
  dset:close()
  file:close()

  local bin = ffi.cast("cl_uint2 *", queue:enqueue_map_buffer(d_bin, true, "read"))
  for i = 0, N-1 do
    assert(bin[i].s0 == buf[i][0])
    assert(bin[i].s1 == buf[i][1])
  end
  queue:enqueue_unmap_mem_object(d_bin, bin)
end

-- Test 3-dimensional Hilbert curve.
do
  local M = {4, 2, 3}
  local program = compute.program(context, "hilbert.cl", {M = M})

  local N = 2^(M[1]+M[2]+M[3])
  local d_bin = context:create_buffer(N*ffi.sizeof("cl_uint3"))
  local kernel = program:create_kernel("test_hilbert")
  kernel:set_arg(0, d_bin)
  queue:enqueue_ndrange_kernel(kernel, nil, {N})

  local file = hdf5.open_file("hilbert_4x2x3.h5")
  local dset = file:open_dataset("value")
  local buf = ffi.new("int[?][3]", N)
  local space = hdf5.create_simple_space({N, 3})
  dset:read(buf, hdf5.int, space)
  space:close()
  dset:close()
  file:close()

  local bin = ffi.cast("cl_uint3 *", queue:enqueue_map_buffer(d_bin, true, "read"))
  for i = 0, N-1 do
    assert(bin[i].s0 == buf[i][0])
    assert(bin[i].s1 == buf[i][1])
    assert(bin[i].s2 == buf[i][2])
  end
  queue:enqueue_unmap_mem_object(d_bin, bin)
end

-- Test 4-dimensional Hilbert curve.
do
  local M = {1, 3, 2, 4}
  local program = compute.program(context, "hilbert.cl", {M = M})

  local N = 2^(M[1]+M[2]+M[3]+M[4])
  local d_bin = context:create_buffer(N*ffi.sizeof("cl_uint3"))
  local kernel = program:create_kernel("test_hilbert")
  kernel:set_arg(0, d_bin)
  queue:enqueue_ndrange_kernel(kernel, nil, {N})

  local file = hdf5.open_file("hilbert_1x3x2x4.h5")
  local dset = file:open_dataset("value")
  local buf = ffi.new("int[?][4]", N)
  local space = hdf5.create_simple_space({N, 4})
  dset:read(buf, hdf5.int, space)
  space:close()
  dset:close()
  file:close()

  local bin = ffi.cast("cl_uint4 *", queue:enqueue_map_buffer(d_bin, true, "read"))
  for i = 0, N-1 do
    assert(bin[i].s0 == buf[i][0])
    assert(bin[i].s1 == buf[i][1])
    assert(bin[i].s2 == buf[i][2])
    assert(bin[i].s3 == buf[i][3])
  end
  queue:enqueue_unmap_mem_object(d_bin, bin)
end
