------------------------------------------------------------------------------
-- Test solvent particle sort.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local box = require("nanomotor.box")
local domain = require("nanomotor.domain")
local sort = require("nanomotor.sort")
local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

math.randomseed(42)

local file = hdf5.open_file("sort.h5")
local dset = file:open_dataset("value")

local attr = dset:open_attribute("N")
local buf = ffi.new("int[1]")
attr:read(buf, hdf5.int)
attr:close()
local N = buf[0]

local attr = dset:open_attribute("L")
local buf = ffi.new("int[3]")
attr:read(buf, hdf5.int)
attr:close()
local L = {buf[0], buf[1], buf[2]}

local ims = ffi.new("cl_short3[?]", N)
local memspace = hdf5.create_simple_space({N, 4})
local filespace = dset:get_space()
memspace:select_hyperslab("set", {0, 0}, nil, {N, 3})
filespace:select_hyperslab("set", {0, 0, 0}, nil, {1, N, 3})
dset:read(ims, hdf5.int16, memspace, filespace)

local rs = ffi.new("cl_double3[?]", N)
local vs = ffi.new("cl_double3[?]", N)
local sps = ffi.new("cl_char4[?]", N)
local ids = ffi.new("cl_int[?]", N)
for i = 0, N-1 do
  rs[i].x = ims[i].x + math.random()
  rs[i].y = ims[i].y + math.random()
  rs[i].z = ims[i].z + math.random()
  vs[i].x = math.random()
  vs[i].y = math.random()
  vs[i].z = math.random()
  sps[i].s0 = math.random(0, 127)
  sps[i].s1 = math.random(0, 127)
  sps[i].s2 = math.random(0, 127)
  sps[i].s3 = math.random(0, 127)
  ids[i] = i
end

local Nd = 2
local box = box({L = L, periodic = {true, true, true}})
local dom = domain(queue, box, {Nd = Nd, Ns = N})
local sort = sort(dom)
queue:enqueue_write_buffer(dom.d_rs, true, rs)
queue:enqueue_write_buffer(dom.d_ims, true, ims)
queue:enqueue_write_buffer(dom.d_vs, true, vs)
queue:enqueue_write_buffer(dom.d_sps, true, sps)
queue:enqueue_write_buffer(dom.d_ids, true, ids)
sort.update()
queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)
queue:enqueue_read_buffer(dom.d_sps, true, dom.sps)
queue:enqueue_read_buffer(dom.d_ids, true, dom.ids)

local im2 = ffi.new("cl_short3[?]", N)
filespace:offset_simple({1, 0, 0})
dset:read(im2, hdf5.int16, memspace, filespace)
memspace:close()
filespace:close()
dset:close()
file:close()

local count = ffi.new("int[?]", N)
for i = 0, N-1 do
  assert(dom.ims[i].x == im2[i].x)
  assert(dom.ims[i].y == im2[i].y)
  assert(dom.ims[i].z == im2[i].z)
  local id = dom.ids[i]
  assert(dom.rs[i].x == rs[id].x)
  assert(dom.rs[i].y == rs[id].y)
  assert(dom.rs[i].z == rs[id].z)
  assert(dom.vs[i].x == vs[id].x)
  assert(dom.vs[i].y == vs[id].y)
  assert(dom.vs[i].z == vs[id].z)
  assert(dom.sps[i].s0 == sps[id].s0)
  assert(dom.sps[i].s1 == sps[id].s1)
  assert(dom.sps[i].s2 == sps[id].s2)
  assert(dom.sps[i].s3 == sps[id].s3)
  count[id] = count[id] + 1
end
for i = 0, N-1 do
  assert(count[i] == 1)
end
