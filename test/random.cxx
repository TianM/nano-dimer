/*
 * Test random distributions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#include <Random123/threefry.h>
#include <hdf5.h>
#include <stdint.h>
#include <stdlib.h>

/**
Generates test data using the [Random123] library by D. E. Shaw Research.

[Random123]: http://www.deshawresearch.com/resources_random123.html
**/
int main(int argc, char **argv)
{
  if (argc != 7) {
    fprintf(stderr, "Usage: %s OUTPUT N C0 C1 K0 K1\n", argv[0]);
    return 1;
  }
  int N = atoi(argv[2]);
  uint64_t seed[4];
  for (int i = 0; i < 4; ++i) seed[i] = strtoul(argv[i+3], NULL, 0);
  threefry2x64_ctr_t c = {seed[0], seed[1]};
  threefry2x64_key_t k = {seed[2], seed[3]};
  threefry2x64_ctr_t *buf = (threefry2x64_ctr_t *)malloc(N*sizeof(threefry2x64_ctr_t));
  for (int i = 0; i < N; ++i) {
    buf[i] = threefry2x64(c, k);
    c.v[0]++;
  }
  hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_libver_bounds(fapl, H5F_LIBVER_18, H5F_LIBVER_LATEST);
  hid_t file = H5Fcreate(argv[1], H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
  hsize_t dims[2] = {N, 2};
  hid_t space = H5Screate_simple(2, dims, NULL);
  hid_t dset = H5Dcreate(file, "value", H5T_NATIVE_UINT64, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(dset, H5T_NATIVE_UINT64, space, H5S_ALL, H5P_DEFAULT, buf);
  hsize_t adims[2] = {2, 2};
  H5Sset_extent_simple(space, 2, adims, NULL);
  hid_t attr = H5Acreate(dset, "seed", H5T_NATIVE_UINT64, space, H5P_DEFAULT, H5P_DEFAULT);
  H5Awrite(attr, H5T_NATIVE_UINT64, seed);
  H5Pclose(fapl);
  H5Sclose(space);
  H5Dclose(dset);
  H5Aclose(attr);
  H5Fclose(file);
}
