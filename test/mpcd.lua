------------------------------------------------------------------------------
-- Test multi-particle collision dynamics.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local nm = {
  compute = require("nanomotor.compute"),
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
  mpcd = require("nanomotor.mpcd"),
}

local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- MPI may be enabled by passing -lmpi to the interpreter.
local mpi = package.loaded["mpi"]
local rank = mpi and mpi.comm_world:rank() or 0
local nranks = mpi and mpi.comm_world:size() or 1

-- Number of subdomains per dimension for MPI tests.
local ndom = {1, 1, 1}
for i = 1, 3 do
  if mpi then ndom[i] = assert(tonumber(arg[i])) end
end

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[rank%#platform:get_devices() + 1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

local file = hdf5.open_file("mpcd_10x8x6.h5")

local Ns do
  local attr = file:open_attribute("N")
  Ns = ffi.new("int[1]")
  attr:read(Ns, hdf5.int)
  attr:close()
  Ns = Ns[0]
end

local L do
  local attr = file:open_attribute("L")
  L = ffi.new("int[3]")
  attr:read(L, hdf5.int)
  attr:close()
  L = {L[0], L[1], L[2]}
end

local d_rs = context:create_buffer(Ns*ffi.sizeof("cl_double3"))
local d_vs = context:create_buffer(Ns*ffi.sizeof("cl_double3"))
local program = nm.compute.program(context, "mpcd.cl", {L = L})
local kernel = program:create_kernel("test_mpcd_init")
kernel:set_arg(0, d_rs)
kernel:set_arg(1, d_vs)
queue:enqueue_ndrange_kernel(kernel, nil, {Ns})

local dset = file:open_dataset("value")
local filespace = dset:get_space()
filespace:select_hyperslab("set", {0, 0, 0}, nil, {1, Ns, 3})

local memspace = hdf5.create_simple_space({Ns, 4})
memspace:select_hyperslab("set", {0, 0}, nil, {Ns, 3})

local boundary = {
  {true,  true,  true },
  {false, true,  true },
  {true,  false, true },
  {true,  true,  false},
  {false, false, true },
  {false, true,  false},
  {true,  false, false},
  {false, false, false},
}

for offset, periodic in ipairs(boundary) do
  math.randomseed(42)

  local box = nm.box({L = L, periodic = periodic, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 0, Ns_max = Ns})
  local mpcd = nm.mpcd(dom, {mpcd = {bin_size = 50}, timestep = 0.01, skin = 1.0})
  local comm = box.comm

  if not comm or comm:rank() == 0 then
    dom.Ns = Ns
    queue:enqueue_read_buffer(d_rs, true, dom.rs)
    queue:enqueue_read_buffer(d_vs, true, dom.vs)
  end

  for i = 0, dom.Ns-1 do
    dom.ids[i] = i
  end

  if comm then dom.alltoalls() end

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)

  mpcd.post_neigh()
  mpcd.collide()

  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  local vs = ffi.new("cl_double3[?]", Ns)
  filespace:offset_simple({offset-1, 0, 0})
  dset:read(vs, hdf5.double, memspace, filespace)

  for i = 0, dom.Ns-1 do
    local j = dom.ids[i]
    assert(math.abs(dom.vs[i].x - vs[j].x) < 1e-14)
    assert(math.abs(dom.vs[i].y - vs[j].y) < 1e-14)
    assert(math.abs(dom.vs[i].z - vs[j].z) < 1e-14)
  end
end

filespace:close()
memspace:close()
dset:close()
file:close()

if mpi then mpi.finalize() end
