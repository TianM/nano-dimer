/*
 * Generate test data for solvent particle sort.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#include <Hilbert.hpp>
#include <hdf5.h>
#include <random>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdint>
#include <array>

/**
This program randomly places N particles on a cubic lattice with
L1×L2×L3 points, where multiple particles may lie on the same point.
The particles are then ordered according to a space-filling curve that
traverses the lattice points according to the Hilbert curve, which is
generated using [libhilbert] by Chris Hamilton. The unordered and the
ordered configuration are written to an HDF5 file that serves as test
data for the solvent particle sort.

[libhilbert]: https://web.cs.dal.ca/~chamilto/hilbert/
**/

typedef std::array<CFixBitVec, 4> CFixBitVec4;

int main(int argc, char **argv)
{
  if (argc != 6) {
    fprintf(stderr, "Usage: %s OUTPUT N L1 L2 L3\n", argv[0]);
    return 1;
  }
  int N = std::atoi(argv[2]);
  int Lx = std::atoi(argv[3]);
  int Ly = std::atoi(argv[4]);
  int Lz = std::atoi(argv[5]);
  int L[3] = {Lx, Ly, Lz};
  int M[3] = {1, 1, 1};
  while ((1<<M[0]) < Lx) M[0]++;
  while ((1<<M[1]) < Ly) M[1]++;
  while ((1<<M[2]) < Lz) M[2]++;

  std::vector<CFixBitVec4> value(N);
  std::vector<int> count(Lx*Ly*Lz, 0);
  std::mt19937_64 rd(42);
  std::uniform_real_distribution<double> uniform;
  for (int i = 0; i < N; ++i) {
    int x = Lx*uniform(rd);
    int y = Ly*uniform(rd);
    int z = Lz*uniform(rd);
    int n = count[x + y*Lx + z*Lx*Ly]++;
    value[i][0] = x;
    value[i][1] = y;
    value[i][2] = z;
    value[i][3] = n;
  }

  hsize_t dims[3] = {2, N, 4};
  hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_libver_bounds(fapl, H5F_LIBVER_18, H5F_LIBVER_LATEST);
  hid_t file = H5Fcreate(argv[1], H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
  H5Pclose(fapl);
  hid_t file_space = H5Screate_simple(3, dims, NULL);
  hid_t dcpl = H5Pcreate(H5P_DATASET_CREATE);
  hsize_t chunk[3] = {1, N, 4};
  H5Pset_chunk(dcpl, 3, chunk);
  H5Pset_shuffle(dcpl);
  H5Pset_deflate(dcpl, 9);
  hid_t dset = H5Dcreate(file, "value", H5T_NATIVE_UINT8, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);
  H5Pclose(dcpl);
  hsize_t start[3] = {0, 0, 0};
  H5Sselect_hyperslab(file_space, H5S_SELECT_SET, start, NULL, chunk, NULL);
  hid_t mem_space = H5Screate_simple(3, chunk, NULL);
  H5Dwrite(dset, H5T_NATIVE_UINT64, mem_space, file_space, H5P_DEFAULT, value.data());

  std::stable_sort(value.begin(), value.end(), [&](CFixBitVec4 const& p1, CFixBitVec4 const& p2) {
    CFixBitVec h1, h2;
    Hilbert::coordsToCompactIndex(p1.data(), M, 3, h1);
    Hilbert::coordsToCompactIndex(p2.data(), M, 3, h2);
    return h1.rack() < h2.rack();
  });

  hssize_t offset[3] = {1, 0, 0};
  H5Soffset_simple(file_space, offset);
  H5Dwrite(dset, H5T_NATIVE_UINT64, mem_space, file_space, H5P_DEFAULT, value.data());
  H5Sset_extent_simple(file_space, 0, dims, NULL);
  hid_t attr = H5Acreate(dset, "N", H5T_NATIVE_UINT32, file_space, H5P_DEFAULT, H5P_DEFAULT);
  H5Awrite(attr, H5T_NATIVE_INT, &N);
  H5Aclose(attr);
  dims[0] = 3;
  H5Sset_extent_simple(file_space, 1, dims, NULL);
  attr = H5Acreate(dset, "M", H5T_NATIVE_UINT8, file_space, H5P_DEFAULT, H5P_DEFAULT);
  H5Awrite(attr, H5T_NATIVE_INT, M);
  H5Aclose(attr);
  attr = H5Acreate(dset, "L", H5T_NATIVE_UINT8, file_space, H5P_DEFAULT, H5P_DEFAULT);
  H5Awrite(attr, H5T_NATIVE_INT, L);
  H5Aclose(attr);
  H5Sclose(mem_space);
  H5Sclose(file_space);
  H5Dclose(dset);
  H5Fclose(file);
}
