------------------------------------------------------------------------------
-- Test random solvent positions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local box = require("nanomotor.box")
local domain = require("nanomotor.domain")
local position = require("nanomotor.position")
local cl = require("opencl")
local ffi = require("ffi")

-- Cache library functions.
local abs, floor, sqrt = math.abs, math.floor, math.sqrt

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

math.randomseed(42)

-- Test uniform distribution of solvent particles.
do
  local Nd = 100
  local Ns = 1000000
  local L = {100, 100, 100}
  local sigma = {C = 1, N = 2}

  local box = box({L = L, periodic = {true, true, true}})
  local dom = domain(queue, box, {Nd = Nd, Ns = Ns})
  local pos = position.random(dom, {sigma = sigma})
  local rd, rs, spd, ids = dom.rd, dom.rs, dom.spd, dom.ids
  for i = 0, Nd-1 do
    rd[i].x = math.random()*L[1]
    rd[i].y = math.random()*L[2]
    rd[i].z = math.random()*L[3]
    spd[i].s0 = i%2
  end
  pos()
  queue:enqueue_read_buffer(dom.d_rs, true, rs)
  queue:enqueue_read_buffer(dom.d_ids, true, ids)

  local nbin = 100
  local bins = ffi.new("struct { uint32_t x, y, z; }[?]", nbin)
  for i = 0, Ns-1 do
    assert(rs[i].x >= 0 and rs[i].x < L[1])
    assert(rs[i].y >= 0 and rs[i].y < L[2])
    assert(rs[i].z >= 0 and rs[i].z < L[3])
    assert(ids[i] == i)

    for j = 0, Nd-1 do
      local dx = rs[i].x - rd[j].x
      local dy = rs[i].y - rd[j].y
      local dz = rs[i].z - rd[j].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      local d2 = dx*dx + dy*dy + dz*dz
      assert(d2 > (j%2 == 0 and sigma.C^2 or sigma.N^2))
    end

    local bx = floor(rs[i].x * (nbin/L[1]))
    local by = floor(rs[i].y * (nbin/L[2]))
    local bz = floor(rs[i].z * (nbin/L[3]))
    if bx >= 0 and bx < nbin then bins[bx].x = bins[bx].x + 1 end
    if by >= 0 and by < nbin then bins[by].y = bins[by].y + 1 end
    if bz >= 0 and bz < nbin then bins[bz].z = bins[bz].z + 1 end
  end

  local norm = nbin / Ns
  local err = sqrt(norm)
  for i = 0, nbin-1 do
    assert(abs(bins[i].x*norm - 1.0) < 4.0*err)
    assert(abs(bins[i].y*norm - 1.0) < 4.0*err)
    assert(abs(bins[i].z*norm - 1.0) < 4.0*err)
  end
end
