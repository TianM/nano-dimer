------------------------------------------------------------------------------
-- Test statistical moments.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local stats = require("nanomotor.statistics")
local hdf5 = require("hdf5")

local abs, log, random, sqrt = math.abs, math.log, math.random, math.sqrt

-- https://en.wikipedia.org/wiki/Exponential_distribution
local lambda = 1.5
local function exponential(u)
  local u = random()
  return (-1/lambda) * log(u)
end

local mean = lambda^-1
local var = lambda^-2
local skew = 2.0
local kurt = 9.0

local file = hdf5.create_file("statistics.h5")

do
  local N = 6
  local dtype = stats.accum2:datatype()
  local filespace = hdf5.create_simple_space({N})
  local dset = file:create_dataset("acc2", dtype, filespace)
  local memspace = hdf5.create_space("scalar")
  filespace:select_hyperslab("set", {0}, nil, {1})
  math.randomseed(42)
  for i = 1, N do
    local acc = stats.accum2()
    for j = 1, 10^i do
      local v = exponential()
      acc(v)
    end
    filespace:offset_simple({i-1})
    dset:write(acc, dtype, memspace, filespace)
  end
  for i = 1, N do
    local count = 10^i
    local err = sqrt(1/count)
    local acc = stats.accum2()
    filespace:offset_simple({i-1})
    dset:read(acc, dtype, memspace, filespace)
    assert(stats.count(acc) == count)
    assert(abs(stats.mean(acc) - mean) < 4.0*err*mean)
    assert(stats.sem(acc) < 4.0*err*mean)
    assert(abs(stats.var(acc) - var) < 4.0*err*var)
  end
  dtype:close()
  memspace:close()
  filespace:close()
  dset:close()
end

do
  local N = 6
  local dtype = stats.accum4:datatype()
  local filespace = hdf5.create_simple_space({N})
  local dset = file:create_dataset("acc4", dtype, filespace)
  local memspace = hdf5.create_space("scalar")
  filespace:select_hyperslab("set", {0}, nil, {1})
  math.randomseed(42)
  for i = 1, N do
    local acc = stats.accum4()
    for j = 1, 10^i do
      local v = exponential()
      acc(v)
    end
    filespace:offset_simple({i-1})
    dset:write(acc, dtype, memspace, filespace)
  end
  for i = 1, N do
    local count = 10^i
    local err = sqrt(1/count)
    local acc = stats.accum4()
    filespace:offset_simple({i-1})
    dset:read(acc, dtype, memspace, filespace)
    assert(stats.count(acc) == count)
    assert(abs(stats.mean(acc) - mean) < 4.0*err*mean)
    assert(stats.sem(acc) < 4.0*err*mean)
    assert(abs(stats.var(acc) - var) < 4.0*err*var)
    assert(abs(stats.skew(acc) - skew) < 4.0*err*skew)
    assert(abs(stats.kurt(acc) - kurt) < 7.5*err*kurt)
  end
  dtype:close()
  memspace:close()
  filespace:close()
  dset:close()
end

file:close()
