------------------------------------------------------------------------------
-- Test simulation domain.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local nm = {
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
}

local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- MPI may be enabled by passing -lmpi to the interpreter.
local mpi = package.loaded["mpi"]
local rank = mpi and mpi.comm_world:rank() or 0
local nranks = mpi and mpi.comm_world:size() or 1

-- Number of subdomains per dimension for MPI tests.
local ndom = {1, 1, 1}
for i = 1, 3 do
  if mpi then ndom[i] = assert(tonumber(arg[i])) end
end

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[rank%#platform:get_devices() + 1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Test initialisation.
do
  local box = nm.box({L = {20, 20, 20}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 100, Nd_max = 200, Ns = 1000, Ns_max = 1100})

  assert(dom.Nd == 100)
  assert(dom.Nd_max == 200)

  for i = 0, dom.Nd-1 do
    assert(dom.rd[i].x == 0)
    assert(dom.rd[i].y == 0)
    assert(dom.rd[i].z == 0)
    assert(dom.imd[i].x == 0)
    assert(dom.imd[i].y == 0)
    assert(dom.imd[i].z == 0)
    assert(dom.vd[i].x == 0)
    assert(dom.vd[i].y == 0)
    assert(dom.vd[i].z == 0)
    assert(dom.fd[i].x == 0)
    assert(dom.fd[i].y == 0)
    assert(dom.fd[i].z == 0)
    assert(dom.spd[i].s0 == 0)
    assert(dom.spd[i].s1 == 0)
    assert(dom.spd[i].s2 == 0)
    assert(dom.spd[i].s3 == 0)
    assert(dom.idd[i] == -1)
  end

  dom.queue:enqueue_read_buffer(dom.d_rs, false, dom.rs)
  dom.queue:enqueue_read_buffer(dom.d_ims, false, dom.ims)
  dom.queue:enqueue_read_buffer(dom.d_vs, false, dom.vs)
  dom.queue:enqueue_read_buffer(dom.d_fs, false, dom.fs)
  dom.queue:enqueue_read_buffer(dom.d_sps, false, dom.sps)
  dom.queue:enqueue_read_buffer(dom.d_ids, true, dom.ids)

  assert(dom.Ns == 1000)
  assert(dom.Ns_max == 1100)

  for i = 0, dom.Ns-1 do
    assert(dom.rs[i].x == 0)
    assert(dom.rs[i].y == 0)
    assert(dom.rs[i].z == 0)
    assert(dom.ims[i].x == 0)
    assert(dom.ims[i].y == 0)
    assert(dom.ims[i].z == 0)
    assert(dom.vs[i].x == 0)
    assert(dom.vs[i].y == 0)
    assert(dom.vs[i].z == 0)
    assert(dom.fs[i].x == 0)
    assert(dom.fs[i].y == 0)
    assert(dom.fs[i].z == 0)
    assert(dom.sps[i].s0 == 0)
    assert(dom.sps[i].s1 == 0)
    assert(dom.sps[i].s2 == 0)
    assert(dom.sps[i].s3 == 0)
    assert(dom.ids[i] == -1)
  end
end

-- Test all-to-all scatter/gather of dimer spheres.
if mpi then
  local box = nm.box({L = {20, 20, 20}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 100, Nd_max = 200, Ns = 0, Ns_max = 1})
  local comm = box.comm
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global
  local Nd, Nd_global = dom.Nd, dom.Nd*nranks

  math.randomseed(rank)

  for i = 0, dom.Nd-1 do
    dom.rd[i].x = math.random()*L_global[1]
    dom.rd[i].y = math.random()*L_global[2]
    dom.rd[i].z = math.random()*L_global[3]
    dom.imd[i].x = math.random(-32768, 32767)
    dom.imd[i].y = math.random(-32768, 32767)
    dom.imd[i].z = math.random(-32768, 32767)
    dom.vd[i].x = math.random()
    dom.vd[i].y = math.random()
    dom.vd[i].z = math.random()
    dom.spd[i].s0 = math.random(0, 127)
    dom.spd[i].s1 = math.random(0, 127)
    dom.spd[i].s2 = math.random(0, 127)
    dom.spd[i].s3 = math.random(0, 127)
    dom.idd[i] = i + Nd*rank
  end

  local rd = ffi.new("cl_double3[?]", Nd_global)
  local imd = ffi.new("cl_short3[?]", Nd_global)
  local vd = ffi.new("cl_double3[?]", Nd_global)
  local spd = ffi.new("cl_char4[?]", Nd_global)
  local idd = ffi.new("cl_int[?]", Nd_global)

  mpi.allgather(dom.rd, dom.Nd, dom.type_rd, rd, dom.Nd, dom.type_rd, comm)
  mpi.allgather(dom.imd, dom.Nd, dom.type_imd, imd, dom.Nd, dom.type_imd, comm)
  mpi.allgather(dom.vd, dom.Nd, dom.type_vd, vd, dom.Nd, dom.type_vd, comm)
  mpi.allgather(dom.spd, dom.Nd, dom.type_spd, spd, dom.Nd, dom.type_spd, comm)
  mpi.allgather(dom.idd, dom.Nd, dom.type_idd, idd, dom.Nd, dom.type_idd, comm)

  dom.alltoalld()

  do
    local count = ffi.new("int[1]", dom.Nd)
    mpi.allreduce(mpi.in_place, count, 1, mpi.int, mpi.sum, comm)
    assert(count[0] == Nd_global)
  end

  for i = 0, dom.Nd-1, 2 do
    assert(dom.rd[i].x >= O_local[1] and dom.rd[i].x < O_local[1] + L_local[1])
    assert(dom.rd[i].y >= O_local[2] and dom.rd[i].y < O_local[2] + L_local[2])
    assert(dom.rd[i].z >= O_local[3] and dom.rd[i].z < O_local[3] + L_local[3])
    assert(dom.idd[i+1] == dom.idd[i]+1)
  end

  for i = 0, dom.Nd-1 do
    local j = dom.idd[i]
    assert(dom.rd[i].x == rd[j].x)
    assert(dom.rd[i].y == rd[j].y)
    assert(dom.rd[i].z == rd[j].z)
    assert(dom.imd[i].x == imd[j].x)
    assert(dom.imd[i].y == imd[j].y)
    assert(dom.imd[i].z == imd[j].z)
    assert(dom.vd[i].x == vd[j].x)
    assert(dom.vd[i].y == vd[j].y)
    assert(dom.vd[i].z == vd[j].z)
    assert(dom.spd[i].s0 == spd[j].s0)
    assert(dom.spd[i].s1 == spd[j].s1)
    assert(dom.spd[i].s2 == spd[j].s2)
    assert(dom.spd[i].s3 == spd[j].s3)
    assert(dom.idd[i] == idd[j])
  end

  do
    local sum = ffi.new("int[1]")
    for i = 0, dom.Nd-1 do
      sum[0] = sum[0] + dom.idd[i]
    end
    mpi.allreduce(mpi.in_place, sum, 1, mpi.int, mpi.sum, comm)
    assert(sum[0] == (Nd_global-1)*Nd_global/2)
  end
end

-- Test all-to-all scatter/gather of solvent particles.
if mpi then
  local box = nm.box({L = {20, 20, 20}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 1000, Ns_max = 1100})
  local comm = box.comm
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global
  local Ns, Ns_global = dom.Ns, dom.Ns*nranks

  math.randomseed(rank)

  for i = 0, dom.Ns-1 do
    dom.rs[i].x = math.random()*L_global[1]
    dom.rs[i].y = math.random()*L_global[2]
    dom.rs[i].z = math.random()*L_global[3]
    dom.ims[i].x = math.random(-32768, 32767)
    dom.ims[i].y = math.random(-32768, 32767)
    dom.ims[i].z = math.random(-32768, 32767)
    dom.vs[i].x = math.random()
    dom.vs[i].y = math.random()
    dom.vs[i].z = math.random()
    dom.sps[i].s0 = math.random(0, 127)
    dom.sps[i].s1 = math.random(0, 127)
    dom.sps[i].s2 = math.random(0, 127)
    dom.sps[i].s3 = math.random(0, 127)
    dom.ids[i] = i + Ns*rank
  end

  local rs = ffi.new("cl_double3[?]", Ns_global)
  local ims = ffi.new("cl_short3[?]", Ns_global)
  local vs = ffi.new("cl_double3[?]", Ns_global)
  local sps = ffi.new("cl_char4[?]", Ns_global)
  local ids = ffi.new("cl_int[?]", Ns_global)

  mpi.allgather(dom.rs, dom.Ns, dom.type_rs, rs, dom.Ns, dom.type_rs, comm)
  mpi.allgather(dom.ims, dom.Ns, dom.type_ims, ims, dom.Ns, dom.type_ims, comm)
  mpi.allgather(dom.vs, dom.Ns, dom.type_vs, vs, dom.Ns, dom.type_vs, comm)
  mpi.allgather(dom.sps, dom.Ns, dom.type_sps, sps, dom.Ns, dom.type_sps, comm)
  mpi.allgather(dom.ids, dom.Ns, dom.type_ids, ids, dom.Ns, dom.type_ids, comm)

  dom.alltoalls()

  do
    local count = ffi.new("int[1]", dom.Ns)
    mpi.allreduce(mpi.in_place, count, 1, mpi.int, mpi.sum, comm)
    assert(count[0] == Ns_global)
  end

  for i = 0, dom.Ns-1 do
    assert(dom.rs[i].x >= O_local[1] and dom.rs[i].x < O_local[1] + L_local[1])
    assert(dom.rs[i].y >= O_local[2] and dom.rs[i].y < O_local[2] + L_local[2])
    assert(dom.rs[i].z >= O_local[3] and dom.rs[i].z < O_local[3] + L_local[3])
  end

  for i = 0, dom.Ns-1 do
    local j = dom.ids[i]
    assert(dom.rs[i].x == rs[j].x)
    assert(dom.rs[i].y == rs[j].y)
    assert(dom.rs[i].z == rs[j].z)
    assert(dom.ims[i].x == ims[j].x)
    assert(dom.ims[i].y == ims[j].y)
    assert(dom.ims[i].z == ims[j].z)
    assert(dom.vs[i].x == vs[j].x)
    assert(dom.vs[i].y == vs[j].y)
    assert(dom.vs[i].z == vs[j].z)
    assert(dom.sps[i].s0 == sps[j].s0)
    assert(dom.sps[i].s1 == sps[j].s1)
    assert(dom.sps[i].s2 == sps[j].s2)
    assert(dom.sps[i].s3 == sps[j].s3)
    assert(dom.ids[i] == ids[j])
  end

  do
    local sum = ffi.new("int[1]")
    for i = 0, dom.Ns-1 do
      sum[0] = sum[0] + dom.ids[i]
    end
    mpi.allreduce(mpi.in_place, sum, 1, mpi.int, mpi.sum, comm)
    assert(sum[0] == (Ns_global-1)*Ns_global/2)
  end
end

-- Test snapshot to HDF5 file.
do
  local box = nm.box({L = {20, 20, 20}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 200, Ns = 1000, Ns_max = 1100})
  local comm = box.comm
  local L_local, O_local = box.L_local, box.O_local

  math.randomseed(rank)

  dom.Nd = 2*math.random(40, 60)

  for i = 0, dom.Nd-1 do
    dom.rd[i].x = math.random()*L_local[1] + O_local[1]
    dom.rd[i].y = math.random()*L_local[2] + O_local[2]
    dom.rd[i].z = math.random()*L_local[3] + O_local[3]
    dom.imd[i].x = math.random(-32768, 32767)
    dom.imd[i].y = math.random(-32768, 32767)
    dom.imd[i].z = math.random(-32768, 32767)
    dom.vd[i].x = math.random()
    dom.vd[i].y = math.random()
    dom.vd[i].z = math.random()
    dom.spd[i].s0 = math.random(0, 127)
    dom.spd[i].s1 = math.random(0, 127)
    dom.spd[i].s2 = math.random(0, 127)
    dom.spd[i].s3 = math.random(0, 127)
    dom.idd[i] = i + dom.Nd_max*rank
  end

  dom.Ns = math.random(980, 1020)

  for i = 0, dom.Ns-1 do
    dom.rs[i].x = math.random()*L_local[1] + O_local[1]
    dom.rs[i].y = math.random()*L_local[2] + O_local[2]
    dom.rs[i].z = math.random()*L_local[3] + O_local[3]
    dom.ims[i].x = math.random(-32768, 32767)
    dom.ims[i].y = math.random(-32768, 32767)
    dom.ims[i].z = math.random(-32768, 32767)
    dom.vs[i].x = math.random()
    dom.vs[i].y = math.random()
    dom.vs[i].z = math.random()
    dom.sps[i].s0 = math.random(0, 127)
    dom.sps[i].s1 = math.random(0, 127)
    dom.sps[i].s2 = math.random(0, 127)
    dom.sps[i].s3 = math.random(0, 127)
    dom.ids[i] = i + dom.Ns_max*rank
  end

  dom.queue:enqueue_write_buffer(dom.d_rs, false, dom.rs)
  dom.queue:enqueue_write_buffer(dom.d_ims, false, dom.ims)
  dom.queue:enqueue_write_buffer(dom.d_vs, false, dom.vs)
  dom.queue:enqueue_write_buffer(dom.d_sps, false, dom.sps)
  dom.queue:enqueue_write_buffer(dom.d_ids, true, dom.ids)

  local fapl = hdf5.create_plist("file_access")
  if comm then fapl:set_fapl_mpio(comm) end
  local file = hdf5.create_file("domain.h5", nil, nil, fapl)
  fapl:close()
  dom.snapshot(file)
  file:close()
end

-- Test resume from HDF5 file.
do
  local box = nm.box({L = {20, 20, 20}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 200, Ns = 0, Ns_max = 1100})
  local comm = box.comm
  local L_local, O_local = box.L_local, box.O_local

  local fapl = hdf5.create_plist("file_access")
  if comm then fapl:set_fapl_mpio(comm) end
  local file = hdf5.open_file("domain.h5", nil, fapl)
  fapl:close()
  dom.restore(file)
  file:close()

  math.randomseed(rank)

  assert(dom.Nd == 2*math.random(40, 60))
  assert(dom.Nd_max == 200)

  for i = 0, dom.Nd-1 do
    assert(dom.rd[i].x == math.random()*L_local[1] + O_local[1])
    assert(dom.rd[i].y == math.random()*L_local[2] + O_local[2])
    assert(dom.rd[i].z == math.random()*L_local[3] + O_local[3])
    assert(dom.imd[i].x == math.random(-32768, 32767))
    assert(dom.imd[i].y == math.random(-32768, 32767))
    assert(dom.imd[i].z == math.random(-32768, 32767))
    assert(dom.vd[i].x == math.random())
    assert(dom.vd[i].y == math.random())
    assert(dom.vd[i].z == math.random())
    assert(dom.spd[i].s0 == math.random(0, 127))
    assert(dom.spd[i].s1 == math.random(0, 127))
    assert(dom.spd[i].s2 == math.random(0, 127))
    assert(dom.spd[i].s3 == math.random(0, 127))
    assert(dom.idd[i] == i + dom.Nd_max*rank)
  end

  dom.queue:enqueue_read_buffer(dom.d_rs, false, dom.rs)
  dom.queue:enqueue_read_buffer(dom.d_ims, false, dom.ims)
  dom.queue:enqueue_read_buffer(dom.d_vs, false, dom.vs)
  dom.queue:enqueue_read_buffer(dom.d_sps, false, dom.sps)
  dom.queue:enqueue_read_buffer(dom.d_ids, true, dom.ids)

  assert(dom.Ns == math.random(980, 1020))
  assert(dom.Ns_max == 1100)

  for i = 0, dom.Ns-1 do
    assert(dom.rs[i].x == math.random()*L_local[1] + O_local[1])
    assert(dom.rs[i].y == math.random()*L_local[2] + O_local[2])
    assert(dom.rs[i].z == math.random()*L_local[3] + O_local[3])
    assert(dom.ims[i].x == math.random(-32768, 32767))
    assert(dom.ims[i].y == math.random(-32768, 32767))
    assert(dom.ims[i].z == math.random(-32768, 32767))
    assert(dom.vs[i].x == math.random())
    assert(dom.vs[i].y == math.random())
    assert(dom.vs[i].z == math.random())
    assert(dom.sps[i].s0 == math.random(0, 127))
    assert(dom.sps[i].s1 == math.random(0, 127))
    assert(dom.sps[i].s2 == math.random(0, 127))
    assert(dom.sps[i].s3 == math.random(0, 127))
    assert(dom.ids[i] == i + dom.Ns_max*rank)
  end
end

if mpi then mpi.finalize() end
