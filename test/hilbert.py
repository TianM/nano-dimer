#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot Hilbert space-filling curve for domain with unequal side lengths.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("--cmap", default="rainbow", help="line colormap")
args = parser.parse_args()

with h5.File(args.input, "r") as f:
  xyz = f["value"][:]

fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
segments = np.swapaxes((xyz[:-1], xyz[1:]), 0, 1)
lines = Line3DCollection(segments, cmap=args.cmap)
lines.set_array(np.linspace(0., 1., num=segments.shape[0]))
lines.set_linewidth(2.)
ax.add_collection(lines)
ax.set_xlim(np.min(xyz[:, 0]), np.max(xyz[:, 0]))
ax.set_ylim(np.min(xyz[:, 1]), np.max(xyz[:, 1]))
ax.set_zlim(np.min(xyz[:, 2]), np.max(xyz[:, 2]))
ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$y$")
ax.set_zlabel(r"$z$")
fig.savefig(args.output)
