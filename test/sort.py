#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot Hilbert space-filling curve for domain with unequal side lengths.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
args = parser.parse_args()

pdf = PdfPages(args.output)
f = h5.File(args.input, "r")
xyz = f["value"][:]
f.close()
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection="3d")
ax.plot(xyz[0, :, 0], xyz[0, :, 1], xyz[0, :, 2], color="#0072b2", lw=0.1)
pdf.savefig()
ax = fig.add_subplot(1, 1, 1, projection="3d")
ax.plot(xyz[1, :, 0], xyz[1, :, 1], xyz[1, :, 2], color="#0072b2", lw=0.5)
pdf.savefig()
pdf.close()
