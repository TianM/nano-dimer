/*
 * Test random distributions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/random.cl"}

__kernel void
test_prng(__global ulong2 *d_count,
          const ulong4 seed)
{
  const uint gid = get_global_id(0);
  const ulong2 c = seed.s01 + (ulong2)(gid, 0);
  const ulong2 k = seed.s23;
  d_count[gid] = random_prng(c, k);
}

__kernel void
test_uniform(__global uint *d_bins,
             const uint N,
             const uint nbin)
{
  const uint gid = get_global_id(0);
  const uint gsize = get_global_size(0);
  ulong4 c = (ulong4)(0, gid, 0x7a1c5ed47d83d152UL, 0x2858177f076bc363UL);
  for (uint i = gid; i < N; i += 2*gsize) {
    const double2 u = random_uniform(&c);
    const uint2 i = clamp(convert_uint2_rtz(u*nbin), 0U, nbin-1);
    atomic_inc(&d_bins[i.s0]);
    atomic_inc(&d_bins[i.s1]);
  }
}

__kernel void
test_normal(__global uint *d_bins,
            const uint N,
            const uint nbin,
            const double cutoff)
{
  const uint gid = get_global_id(0);
  const uint gsize = get_global_size(0);
  ulong4 c = (ulong4)(0, gid, 0x509a671f366cb4f2UL, 0x907aff47550a233dUL);
  for (uint i = gid; i < N; i += 2*gsize) {
    const double2 u = random_normal(&c);
    const int2 i = convert_int2_rtn((u/cutoff + 1.0) * (nbin/2));
    if (i.s0 >= 0 && i.s0 < nbin) atomic_inc(&d_bins[i.s0]);
    if (i.s1 >= 0 && i.s1 < nbin) atomic_inc(&d_bins[i.s1]);
  }
}

__kernel void
test_circle(__global uint *d_bins,
            __global uint *d_err,
            const uint N,
            const uint nbin)
{
  const uint gid = get_global_id(0);
  const uint gsize = get_global_size(0);
  ulong4 c = (ulong4)(0, gid, 0x304588b56a4bff4aUL, 0x0e352cbe4c0cff62UL);
  for (uint i = gid; i < N; i += gsize) {
    const double2 v = random_circle(&c);
    const double r = length(v);
    if (fabs(r - 1.0) > 1e-15) atomic_inc(d_err);
    const double phi = atan2(v.y, v.x);
    const uint i = clamp(convert_uint_rtz((phi*0.5*M_1_PI + 0.5) * nbin), 0U, nbin-1);
    atomic_inc(&d_bins[i]);
  }
}

__kernel void
test_sphere(__global uint *d_bins,
            __global uint *d_err,
            const uint N,
            const uint nbin_theta,
            const uint nbin_phi)
{
  const uint gid = get_global_id(0);
  const uint gsize = get_global_size(0);
  ulong4 c = (ulong4)(0, gid, 0xe29df451196e7e42UL, 0x142e7682fd69115cUL);
  for (uint i = gid; i < N; i += gsize) {
    const double3 v = random_sphere(&c);
    const double r = length(v);
    if (fabs(r - 1.0) > 1e-15) atomic_inc(d_err);
    const double theta = acos(v.z/r);
    const double phi = atan2(v.y, v.x);
    const uint i = clamp(convert_uint_rtz(theta*M_1_PI * nbin_theta), 0U, nbin_theta-1);
    const uint j = clamp(convert_uint_rtz((phi*0.5*M_1_PI + 0.5) * nbin_phi), 0U, nbin_phi-1);
    atomic_inc(&d_bins[i*nbin_phi+j]);
  }
}
