------------------------------------------------------------------------------
-- Test velocity-Verlet algorithm for solvent particles.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local nm = {
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
  verlet = require("nanomotor.verlet"),
}

local cl = require("opencl")
local ffi = require("ffi")

-- MPI may be enabled by passing -lmpi to the interpreter.
local mpi = package.loaded["mpi"]
local rank = mpi and mpi.comm_world:rank() or 0
local nranks = mpi and mpi.comm_world:size() or 1

-- Number of subdomains per dimension for MPI tests.
local ndom = {1, 1, 1}
for i = 1, 3 do
  if mpi then ndom[i] = assert(tonumber(arg[i])) end
end

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[rank%#platform:get_devices() + 1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Test bounce-back boundary conditions.
if ndom[1] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {false, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 0, Ns_max = 2})
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global

  local verlet = nm.verlet(dom, {timestep = 0.01, skin = 1.0, Ns_send_max = 0})

  dom.Ns = 2

  -- Bounce first solvent particle off left wall.
  dom.rs[0].x = O_local[1] + 1.002
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.vs[0].x = -1
  dom.vs[0].y = -2
  dom.vs[0].z = -4

  -- Bounce second solvent particle off right wall.
  dom.rs[1].x = O_local[1] + L_local[1] - 1.009
  dom.rs[1].y = O_local[2] + L_local[2]
  dom.rs[1].z = O_local[3] + L_local[3]
  dom.vs[1].x = 1
  dom.vs[1].y = 2
  dom.vs[1].z = 4

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)

  for i = 1, 100 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == -1)
    assert(dom.vs[0].y == -2)
    assert(dom.vs[0].z == -4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 1.002 + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == 1)
    assert(dom.vs[1].y == 2)
    assert(dom.vs[1].z == 4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] - 1.009 + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] + 0.04*i)) < 1e-11)
  end

  for i = 100, 1, -1 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == 1)
    assert(dom.vs[0].y == 2)
    assert(dom.vs[0].z == 4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 1.008 + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.012 + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 0.024 + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == -1)
    assert(dom.vs[1].y == -2)
    assert(dom.vs[1].z == -4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] - 1.001 + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] + 0.016 + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] + 0.032 + 0.04*i)) < 1e-11)
  end
end

-- Test bounce-back boundary conditions.
if ndom[2] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {true, false, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 0, Ns_max = 2})
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global

  local verlet = nm.verlet(dom, {timestep = 0.01, skin = 1.0, Ns_send_max = 0})

  dom.Ns = 2

  -- Bounce first solvent particle off back wall.
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + 2.004
  dom.rs[0].z = O_local[3]
  dom.vs[0].x = -1
  dom.vs[0].y = -2
  dom.vs[0].z = -4

  -- Bounce second solvent particle off front wall.
  dom.rs[1].x = O_local[1] + L_local[1]
  dom.rs[1].y = O_local[2] + L_local[2] - 2.018
  dom.rs[1].z = O_local[3] + L_local[3]
  dom.vs[1].x = 1
  dom.vs[1].y = 2
  dom.vs[1].z = 4

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)

  for i = 1, 100 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == -1)
    assert(dom.vs[0].y == -2)
    assert(dom.vs[0].z == -4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 2.004 + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == 1)
    assert(dom.vs[1].y == 2)
    assert(dom.vs[1].z == 4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] - 2.018 + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] + 0.04*i)) < 1e-11)
  end

  for i = 100, 1, -1 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == 1)
    assert(dom.vs[0].y == 2)
    assert(dom.vs[0].z == 4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.006 + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 2.016 + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 0.024 + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == -1)
    assert(dom.vs[1].y == -2)
    assert(dom.vs[1].z == -4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] + 0.008 + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] - 2.002 + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] + 0.032 + 0.04*i)) < 1e-11)
  end
end

-- Test bounce-back boundary conditions.
if ndom[3] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {true, true, false}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 0, Ns_max = 2})
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global

  local verlet = nm.verlet(dom, {timestep = 0.01, skin = 1.0, Ns_send_max = 0})

  dom.Ns = 2

  -- Bounce first solvent particle off below wall.
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + 4.008
  dom.vs[0].x = -1
  dom.vs[0].y = -2
  dom.vs[0].z = -4

  -- Bounce second solvent particle off above wall.
  dom.rs[1].x = O_local[1] + L_local[1]
  dom.rs[1].y = O_local[2] + L_local[2]
  dom.rs[1].z = O_local[3] + L_local[3] - 4.036
  dom.vs[1].x = 1
  dom.vs[1].y = 2
  dom.vs[1].z = 4

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)

  for i = 1, 100 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == -1)
    assert(dom.vs[0].y == -2)
    assert(dom.vs[0].z == -4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 4.008 + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == 1)
    assert(dom.vs[1].y == 2)
    assert(dom.vs[1].z == 4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] - 4.036 + 0.04*i)) < 1e-11)
  end

  for i = 100, 1, -1 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == 1)
    assert(dom.vs[0].y == 2)
    assert(dom.vs[0].z == 4)
    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.006 + -0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.012 + -0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 4.032 + -0.04*i)) < 1e-11)

    assert(dom.vs[1].x == -1)
    assert(dom.vs[1].y == -2)
    assert(dom.vs[1].z == -4)
    assert(math.abs((dom.rs[1].x + L_global[1]*dom.ims[1].x) - (O_local[1] + L_local[1] + 0.008 + 0.01*i)) < 1e-11)
    assert(math.abs((dom.rs[1].y + L_global[2]*dom.ims[1].y) - (O_local[2] + L_local[2] + 0.016 + 0.02*i)) < 1e-11)
    assert(math.abs((dom.rs[1].z + L_global[3]*dom.ims[1].z) - (O_local[3] + L_local[3] - 4.004 + 0.04*i)) < 1e-11)
  end
end

-- Test bounce-back boundary conditions with two non-periodic dimensions.
if ndom[1] == 1 and ndom[2] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {false, false, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 1, Ns = 0, Ns_max = 2})
  local L_local, O_local = box.L_local, box.O_local
  local L_global = box.L_global

  local verlet = nm.verlet(dom, {timestep = 0.01, skin = 1.0, Ns_send_max = 0})

  dom.Ns = 1

  -- Bounce first solvent particle off left and back walls.
  dom.rs[0].x = O_local[1] + 0.0051
  dom.rs[0].y = O_local[2] + 0.0098
  dom.rs[0].z = O_local[3] + 0.0004
  dom.vs[0].x = 1
  dom.vs[0].y = -2
  dom.vs[0].z = 4

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)

  for i = 1, 10 do
    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == -1)
    assert(dom.vs[0].y == 2)
    assert(dom.vs[0].z == -4)

    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0049)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0102)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] - 0.0004)) < 1e-11)

    verlet.integrate()
    verlet.stream()

    queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
    queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
    queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

    assert(dom.vs[0].x == 1)
    assert(dom.vs[0].y == -2)
    assert(dom.vs[0].z == 4)

    assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0051)) < 1e-11)
    assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0098)) < 1e-11)
    assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 0.0004)) < 1e-11)
  end

  for i = 1, 2 do
    verlet.integrate()
  end
  verlet.stream()

  queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  assert(dom.vs[0].x == 1)
  assert(dom.vs[0].y == -2)
  assert(dom.vs[0].z == 4)

  assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0051)) < 1e-11)
  assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0098)) < 1e-11)
  assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 0.0004)) < 1e-11)

  for i = 1, 3 do
    verlet.integrate()
  end
  verlet.stream()

  queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  assert(dom.vs[0].x == -1)
  assert(dom.vs[0].y == 2)
  assert(dom.vs[0].z == -4)

  assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0049)) < 1e-11)
  assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0102)) < 1e-11)
  assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] - 0.0004)) < 1e-11)

  for i = 1, 50 do
    verlet.integrate()
  end
  verlet.stream()

  queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  assert(dom.vs[0].x == -1)
  assert(dom.vs[0].y == 2)
  assert(dom.vs[0].z == -4)

  assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0049)) < 1e-11)
  assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0102)) < 1e-11)
  assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] - 0.0004)) < 1e-11)

  for i = 1, 99 do
    verlet.integrate()
  end
  verlet.stream()

  queue:enqueue_read_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_read_buffer(dom.d_ims, true, dom.ims)
  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  assert(dom.vs[0].x == 1)
  assert(dom.vs[0].y == -2)
  assert(dom.vs[0].z == 4)

  assert(math.abs((dom.rs[0].x + L_global[1]*dom.ims[0].x) - (O_local[1] + 0.0051)) < 1e-11)
  assert(math.abs((dom.rs[0].y + L_global[2]*dom.ims[0].y) - (O_local[2] + 0.0098)) < 1e-11)
  assert(math.abs((dom.rs[0].z + L_global[3]*dom.ims[0].z) - (O_local[3] + 0.0004)) < 1e-11)
end

if mpi then mpi.finalize() end
