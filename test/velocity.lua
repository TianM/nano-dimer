------------------------------------------------------------------------------
-- Test Maxwell-Boltzmann velocity distribution.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local box = require("nanomotor.box")
local domain = require("nanomotor.domain")
local velocity = require("nanomotor.velocity")
local stats = require("nanomotor.statistics")
local cl = require("opencl")
local ffi = require("ffi")

-- Cache library functions.
local abs, exp, floor, pi, sqrt = math.abs, math.exp, math.floor, math.pi, math.sqrt

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

math.randomseed(42)

-- Test Gaussian distribution.
do
  local Nd = 2
  local Ns = 1000000
  local L = {50, 50, 50}
  local temp = 1/6

  local box = box({L = L, periodic = {true, true, true}})
  local dom = domain(queue, box, {Nd = Nd, Ns = Ns})
  local vel = velocity.gaussian(dom, {temp = temp})
  vel()
  queue:enqueue_read_buffer(dom.d_vs, true, dom.vs)

  local nbin = 100
  local bins = ffi.new("struct { uint32_t x, y, z; }[?]", nbin)
  local sigma2 = temp
  local cutoff = 3.0*sqrt(sigma2)
  local vx, vy, vz = stats.accum2(), stats.accum2(), stats.accum2()
  local vs = dom.vs
  for i = 0, Ns-1 do
    vx(vs[i].x)
    vy(vs[i].y)
    vz(vs[i].z)
    local bx = floor((vs[i].x/cutoff + 1.0) * nbin/2)
    local by = floor((vs[i].y/cutoff + 1.0) * nbin/2)
    local bz = floor((vs[i].z/cutoff + 1.0) * nbin/2)
    if bx >= 0 and bx < nbin then bins[bx].x = bins[bx].x + 1 end
    if by >= 0 and by < nbin then bins[by].y = bins[by].y + 1 end
    if bz >= 0 and bz < nbin then bins[bz].z = bins[bz].z + 1 end
  end

  local norm = 1/sqrt(sigma2)
  assert(abs(norm*stats.mean(vx)) < 1e-16)
  assert(abs(norm*stats.mean(vy)) < 1e-16)
  assert(abs(norm*stats.mean(vz)) < 1e-16)

  local norm = 1/sigma2
  local err = 1/sqrt(Ns)
  assert(abs(norm*stats.var(vx) - 1.0) < 4.0*err)
  assert(abs(norm*stats.var(vy) - 1.0) < 4.0*err)
  assert(abs(norm*stats.var(vz) - 1.0) < 4.0*err)

  for i = 0, nbin-1 do
    local v = ((i+0.5)/(nbin/2) - 1.0)*cutoff
    local norm = (nbin/2)*sqrt(2*pi*sigma2) / (Ns*cutoff*exp(-(0.5/sigma2)*(v*v)))
    local err = sqrt(norm)
    assert(abs(bins[i].x*norm - 1.0) < 4.0*err)
    assert(abs(bins[i].y*norm - 1.0) < 4.0*err)
    assert(abs(bins[i].x*norm - 1.0) < 4.0*err)
  end
end
