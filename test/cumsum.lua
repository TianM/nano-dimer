------------------------------------------------------------------------------
-- Test cumulative sum.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local cl = require("opencl")
local ffi = require("ffi")
local cumsum = require("nanomotor.cumsum")

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

local N = 31991 -- < 2^16
local count = ffi.new("cl_uint[?]", N)
for i = 0, N-1 do
  count[i] = i+1
end
local d_sum = context:create_buffer("copy_host_ptr", N*ffi.sizeof("cl_uint"), count)
local cumsum = cumsum(queue, N, d_sum)
assert(cumsum() == N*(N+1)/2)
local sum = ffi.cast("cl_uint *", queue:enqueue_map_buffer(d_sum, true, "read"))
for i = 0, N-1 do
  assert(sum[i] == i*(i+1)/2)
end
queue:enqueue_unmap_mem_object(d_sum, sum)
