------------------------------------------------------------------------------
-- Test random distributions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local compute = require("nanomotor.compute")
local random = require("nanomotor.random")
local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")

local acos, atan2, sin, pi = math.acos, math.atan2, math.sin, math.pi
local abs, floor, min, max = math.abs, math.floor, math.min, math.max
local exp, sqrt = math.exp, math.sqrt

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)
local program = compute.program(context, "random.cl")

-- Test pseudo-random number generator.
do
  local file = hdf5.open_file("random.h5")
  local dset = file:open_dataset("value")
  local space = dset:get_space()
  local N = space:get_simple_extent_dims()[1]
  space:close()
  local buf = ffi.new("uint64_t[?][2]", N)
  local space = hdf5.create_simple_space({N, 2})
  dset:read(buf, hdf5.uint64, space)
  space:close()
  local attr = dset:open_attribute("seed")
  local seed = ffi.new("cl_ulong4")
  attr:read(seed, hdf5.uint64)
  attr:close()
  dset:close()
  file:close()

  local d_count = context:create_buffer(N*ffi.sizeof("cl_ulong2"))
  local kernel = program:create_kernel("test_prng")
  kernel:set_arg(0, d_count)
  kernel:set_arg(1, ffi.sizeof(seed), seed)
  queue:enqueue_ndrange_kernel(kernel, nil, {N})

  local count = ffi.cast("cl_ulong2 *", queue:enqueue_map_buffer(d_count, true, "read"))
  for i = 0, N-1 do
    assert(count[i].s0 == buf[i][0])
    assert(count[i].s1 == buf[i][1])
  end
  queue:enqueue_unmap_mem_object(d_count, count)
end

-- Test uniform distribution.
do
  local N = 1048576
  local nbin = 100
  local bins = ffi.new("cl_uint[?]", nbin)
  local d_bins = context:create_buffer("use_host_ptr", nbin*ffi.sizeof("cl_uint"), bins)
  local kernel = program:create_kernel("test_uniform")
  kernel:set_arg(0, d_bins)
  kernel:set_arg(1, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", N))
  kernel:set_arg(2, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", nbin))
  queue:enqueue_ndrange_kernel(kernel, nil, {4096})
  queue:enqueue_map_buffer(d_bins, true, "read")
  local norm = nbin / N
  local err = sqrt(norm)
  for i = 0, nbin-1 do
    local p = bins[i]*norm
    assert(abs(p - 1.0) < 4.0*err)
  end
  queue:enqueue_unmap_mem_object(d_bins, bins)
end

-- Test normal distribution.
do
  local N = 1048576
  local nbin = 100
  local cutoff = 3.0
  local bins = ffi.new("uint32_t[?]", nbin)
  math.randomseed(42)
  for i = 1, N, 2 do
    local v, w = random.normal()
    local i = floor((v/cutoff + 1.0) * nbin/2)
    local j = floor((w/cutoff + 1.0) * nbin/2)
    if i >= 0 and i < nbin then bins[i] = bins[i] + 1 end
    if j >= 0 and j < nbin then bins[j] = bins[j] + 1 end
  end
  for i = 0, nbin-1 do
    local x = ((i+0.5)/(nbin/2) - 1.0)*cutoff
    local norm = (nbin/2)*sqrt(2*pi) / (N*cutoff*exp(-0.5*x*x))
    local err = sqrt(norm)
    local p = bins[i]*norm
    assert(abs(p - 1.0) < 4.0*err)
  end
end

do
  local N = 8388608
  local nbin = 100
  local cutoff = 3.0
  local bins = ffi.new("cl_uint[?]", nbin)
  local d_bins = context:create_buffer("use_host_ptr", nbin*ffi.sizeof("cl_uint"), bins)
  local kernel = program:create_kernel("test_normal")
  kernel:set_arg(0, d_bins)
  kernel:set_arg(1, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", N))
  kernel:set_arg(2, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", nbin))
  kernel:set_arg(3, ffi.sizeof("cl_double"), ffi.new("cl_double[1]", cutoff))
  queue:enqueue_ndrange_kernel(kernel, nil, {4096})
  queue:enqueue_map_buffer(d_bins, true, "read")
  for i = 0, nbin-1 do
    local x = ((i+0.5)/(nbin/2) - 1.0)*cutoff
    local norm = (nbin/2)*sqrt(2*pi) / (N*cutoff*exp(-0.5*x*x))
    local err = sqrt(norm)
    local p = bins[i]*norm
    assert(abs(p - 1.0) < 4.0*err)
  end
  queue:enqueue_unmap_mem_object(d_bins, bins)
end

-- Test circular distribution.
do
  local N = 1048576
  local nbin = 100
  local bins = ffi.new("uint32_t[?]", nbin)
  math.randomseed(42)
  for i = 1, N do
    local x, y = random.circle()
    local r = sqrt(x*x+y*y)
    assert(abs(r - 1.0) < 1e-15)
    local phi = atan2(y, x)
    local i = max(0, min(nbin-1, floor((phi/(2*pi) + 0.5) * nbin)))
    bins[i] = bins[i] + 1
  end
  local norm = nbin / N
  local err = sqrt(norm)
  for i = 0, nbin-1 do
    local p = bins[i]*norm
    assert(abs(p - 1.0) < 4.0*err)
  end
end

do
  local N = 1048576
  local nbin = 100
  local bins = ffi.new("cl_uint[?]", nbin)
  local d_bins = context:create_buffer("use_host_ptr", nbin*ffi.sizeof("cl_uint"), bins)
  local err = ffi.new("cl_uint[1]")
  local d_err = context:create_buffer("use_host_ptr", ffi.sizeof("cl_uint"), err)
  local kernel = program:create_kernel("test_circle")
  kernel:set_arg(0, d_bins)
  kernel:set_arg(1, d_err)
  kernel:set_arg(2, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", N))
  kernel:set_arg(3, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", nbin))
  queue:enqueue_ndrange_kernel(kernel, nil, {4096})
  queue:enqueue_map_buffer(d_err, true, "read")
  assert(err[0] == 0)
  queue:enqueue_unmap_mem_object(d_err, err)
  queue:enqueue_map_buffer(d_bins, true, "read")
  local norm = nbin / N
  local err = sqrt(norm)
  for i = 0, nbin-1 do
    local p = bins[i]*norm
    assert(abs(p - 1.0) < 4.0*err)
  end
  queue:enqueue_unmap_mem_object(d_bins, bins)
end

-- Test spherical distribution.
do
  local N = 8388608
  local nbin_theta, nbin_phi = 100, 10
  local bins = ffi.typeof("uint32_t[$][$]", nbin_theta, nbin_phi)()
  math.randomseed(42)
  for i = 1, N do
    local x, y, z = random.sphere()
    local r = sqrt(x*x+y*y+z*z)
    assert(abs(r - 1.0) < 1e-15)
    local theta, phi = acos(z/r), atan2(y, x)
    local i = max(0, min(nbin_theta-1, floor(theta/pi * nbin_theta)))
    local j = max(0, min(nbin_phi-1, floor((phi/(2*pi) + 0.5) * nbin_phi)))
    bins[i][j] = bins[i][j] + 1
  end
  for i = 0, nbin_theta-1 do
    local theta = (i+0.5) * (pi/nbin_theta)
    local norm = (nbin_theta*nbin_phi) / (N*pi/2*sin(theta))
    local err = sqrt(norm)
    for j = 0, nbin_phi-1 do
      local p = bins[i][j]*norm
      assert(abs(p - 1.0) < 4.0*err)
    end
  end
end

do
  local N = 33554432
  local nbin_theta, nbin_phi = 100, 10
  local bins = ffi.typeof("uint32_t[$][$]", nbin_theta, nbin_phi)()
  local d_bins = context:create_buffer("use_host_ptr", nbin_theta*nbin_phi*ffi.sizeof("cl_uint"), bins)
  local err = ffi.new("cl_uint[1]")
  local d_err = context:create_buffer("use_host_ptr", ffi.sizeof("cl_uint"), err)
  local kernel = program:create_kernel("test_sphere")
  kernel:set_arg(0, d_bins)
  kernel:set_arg(1, d_err)
  kernel:set_arg(2, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", N))
  kernel:set_arg(3, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", nbin_theta))
  kernel:set_arg(4, ffi.sizeof("cl_uint"), ffi.new("cl_uint[1]", nbin_phi))
  queue:enqueue_ndrange_kernel(kernel, nil, {65536})
  queue:enqueue_map_buffer(d_err, true, "read")
  assert(err[0] == 0)
  queue:enqueue_unmap_mem_object(d_err, err)
  queue:enqueue_map_buffer(d_bins, true, "read")
  for i = 0, nbin_theta-1 do
    local theta = (i+0.5) * (pi/nbin_theta)
    local norm = (nbin_theta*nbin_phi) / (N*pi/2*sin(theta))
    local err = sqrt(norm)
    for j = 0, nbin_phi-1 do
      local p = bins[i][j]*norm
      assert(abs(p - 1.0) < 4.0*err)
    end
  end
  queue:enqueue_unmap_mem_object(d_bins, bins)
end
