/*
 * Test multi-particle collision dynamics.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#include <Random123/threefry.h>
#include <hdf5.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct { double x, y, z; } vec3;
typedef struct { double xx, xy, xz, yx, yy, yz, zx, zy, zz; } mat3;

const bool boundary[][3] = {
  {true,  true,  true },
  {false, true,  true },
  {true,  false, true },
  {true,  true,  false},
  {false, false, true },
  {false, true,  false},
  {true,  false, false},
  {false, false, false},
};

const uint64_t seed = 0xf4c19cb9024aec14UL;

/**
Generates test data using the [Random123] library by D. E. Shaw Research.

~~~
cc -IRandom123-1.08/include -O2 -std=c99 -Wall -o mpcd mpcd.c -lhdf5
~~~

[Random123]: http://www.deshawresearch.com/resources_random123.html
**/
int main(int argc, char **argv)
{
  if (argc != 6) {
    fprintf(stderr, "Usage: %s OUTPUT L1 L2 L3 N\n", argv[0]);
    return 1;
  }

  const int Lx = atoi(argv[2]);
  const int Ly = atoi(argv[3]);
  const int Lz = atoi(argv[4]);
  const int Ns = atoi(argv[5]);

  hid_t file;
  {
    hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_libver_bounds(fapl, H5F_LIBVER_18, H5F_LIBVER_LATEST);
    file = H5Fcreate(argv[1], H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
    H5Pclose(fapl);
  }

  hid_t filespace = H5Screate(H5S_SIMPLE);

  hid_t dset;
  {
    hsize_t dims[3] = {8, Ns, 3};
    H5Sset_extent_simple(filespace, 3, dims, NULL);
    hid_t dcpl = H5Pcreate(H5P_DATASET_CREATE);
    hsize_t chunk[3] = {8, Ns, 3};
    H5Pset_chunk(dcpl, 3, chunk);
    H5Pset_shuffle(dcpl);
    H5Pset_deflate(dcpl, 9);
    dset = H5Dcreate(file, "value", H5T_NATIVE_DOUBLE, filespace, H5P_DEFAULT, dcpl, H5P_DEFAULT);
    H5Pclose(dcpl);
  }

  hid_t memspace;
  {
    hsize_t start[3] = {0, 0, 0};
    hsize_t block[3] = {1, Ns, 3};
    H5Sselect_hyperslab(filespace, H5S_SELECT_SET, start, NULL, block, NULL);
    memspace = H5Screate_simple(3, block, NULL);
  }

  vec3 *rs = (vec3 *)malloc(Ns*sizeof(vec3));
  vec3 *vs = (vec3 *)malloc(Ns*sizeof(vec3));

  const int nx = Lx+1;
  const int ny = Ly+1;
  const int nz = Lz+1;

  int *count = (int *)malloc(nx*ny*nz*sizeof(int));
  vec3 *v_cm = (vec3 *)malloc(nx*ny*nz*sizeof(vec3));
  mat3 *R = (mat3 *)malloc(nx*ny*nz*sizeof(mat3));

  for (int b = 0; b < 8; ++b) {
    for (int i = 0; i < Ns; ++i) {
      threefry2x64_ctr_t c = {{0, i}};
      threefry2x64_key_t k = {{0, 0}};
      const threefry2x64_ctr_t v1 = threefry2x64(c, k);
      c.v[0]++;
      const threefry2x64_ctr_t v2 = threefry2x64(c, k);
      c.v[0]++;
      const threefry2x64_ctr_t v3 = threefry2x64(c, k);
      c.v[0]++;

      /* Assign uniform positions in [0.0, L). */
      rs[i].x = (v1.v[0]*0x1p-64) * Lx;
      rs[i].y = (v2.v[0]*0x1p-64) * Ly;
      rs[i].z = (v3.v[0]*0x1p-64) * Lz;

      /* Assign uniform velocities in [-1.0, 1.0). */
      vs[i].x = ((int64_t)v1.v[1])*0x1p-63;
      vs[i].y = ((int64_t)v2.v[1])*0x1p-63;
      vs[i].z = ((int64_t)v3.v[1])*0x1p-63;
    }

    memset(count, 0, nx*ny*nz*sizeof(int));
    memset(v_cm, 0, nx*ny*nz*sizeof(vec3));
    memset(R, 0, nx*ny*nz*sizeof(mat3));

    threefry2x64_ctr_t c = {{0, -1UL}};
    threefry2x64_key_t k = {{0, seed}};
    const threefry2x64_ctr_t v1 = threefry2x64(c, k);
    c.v[0]++;
    const threefry2x64_ctr_t v2 = threefry2x64(c, k);
    c.v[0]++;

    for (int i = 0; i < Ns; ++i) {
      /* Shift grid by uniform vector in [0.0, 1.0). */
      int x = rs[i].x + v1.v[0]*0x1p-64;
      int y = rs[i].y + v1.v[1]*0x1p-64;
      int z = rs[i].z + v2.v[0]*0x1p-64;

      /* Consider periodic boundary conditions. */
      if (boundary[b][0]) x %= Lx;
      if (boundary[b][1]) y %= Ly;
      if (boundary[b][2]) z %= Lz;

      const int j = x + nx*y + (nx*ny)*z;
      count[j]++;
      v_cm[j].x += (vs[i].x - v_cm[j].x) / count[j];
      v_cm[j].y += (vs[i].y - v_cm[j].y) / count[j];
      v_cm[j].z += (vs[i].z - v_cm[j].z) / count[j];
    }

    for (int x = 0; x < nx; ++x) {
      for (int y = 0; y < ny; ++y) {
        for (int z = 0; z < nz; ++z) {
          const int j = x + nx*y + (nx*ny)*z;
          threefry2x64_ctr_t c = {{0, j}};
          threefry2x64_key_t k = {{0, seed}};
          threefry2x64_ctr_t v;

          double v1, v2, s;
          do {
            v = threefry2x64(c, k);
            c.v[0]++;
            v1 = ((int64_t)v.v[0])*0x1p-63;
            v2 = ((int64_t)v.v[1])*0x1p-63;
            s = v1*v1 + v2*v2;
          } while (s >= 1);

          const double ux = 2*v1*sqrt(1-s);
          const double uy = 2*v2*sqrt(1-s);
          const double uz = 1-2*s;

          R[j].xx = ux*ux; R[j].xy = ux*uy-uz; R[j].xz = ux*uz+uy;
          R[j].yx = uy*ux+uz; R[j].yy = uy*uy; R[j].yz = uy*uz-ux;
          R[j].zx = uz*ux-uy; R[j].zy = uz*uy+ux; R[j].zz = uz*uz;
        }
      }
    }

    for (int i = 0; i < Ns; ++i) {
      /* Shift grid by uniform vector in [0.0, 1.0). */
      int x = rs[i].x + v1.v[0]*0x1p-64;
      int y = rs[i].y + v1.v[1]*0x1p-64;
      int z = rs[i].z + v2.v[0]*0x1p-64;

      /* Consider periodic boundary conditions. */
      if (boundary[b][0]) x %= Lx;
      if (boundary[b][1]) y %= Ly;
      if (boundary[b][2]) z %= Lz;

      const int j = x + nx*y + (nx*ny)*z;
      const double vx = vs[i].x - v_cm[j].x;
      const double vy = vs[i].y - v_cm[j].y;
      const double vz = vs[i].z - v_cm[j].z;
      vs[i].x = v_cm[j].x + R[j].xx*vx + R[j].xy*vy + R[j].xz*vz;
      vs[i].y = v_cm[j].y + R[j].yx*vx + R[j].yy*vy + R[j].yz*vz;
      vs[i].z = v_cm[j].z + R[j].zx*vx + R[j].zy*vy + R[j].zz*vz;
    }

    {
      const hssize_t offset[3] = {b, 0, 0};
      H5Soffset_simple(filespace, offset);
      H5Dwrite(dset, H5T_NATIVE_DOUBLE, memspace, filespace, H5P_DEFAULT, vs);
    }
  }

  {
    H5Sset_extent_simple(filespace, 0, NULL, NULL);
    hid_t attr = H5Acreate(file, "N", H5T_NATIVE_UINT32, filespace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_INT, &Ns);
    H5Aclose(attr);
  }

  {
    const hsize_t dims[1] = {3};
    H5Sset_extent_simple(filespace, 1, dims, NULL);
    hid_t attr = H5Acreate(file, "L", H5T_NATIVE_UINT8, filespace, H5P_DEFAULT, H5P_DEFAULT);
    const int L[3] = {Lx, Ly, Lz};
    H5Awrite(attr, H5T_NATIVE_INT, L);
    H5Aclose(attr);
  }

  H5Sclose(memspace);
  H5Sclose(filespace);
  H5Dclose(dset);
  H5Fclose(file);

  free(rs);
  free(vs);
  free(count);
  free(v_cm);
  free(R);
}
