/*
 * Test multi-particle collision dynamics.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/random.cl"}

__kernel void
test_mpcd_init(__global double3 *restrict d_rs,
               __global double3 *restrict d_vs)
{
  const uint gid = get_global_id(0);
  ulong2 c = (ulong2)(0, gid);
  ulong2 k = (ulong2)(0, 0);
  |for i = 1, 3 do
  const ulong2 v${i} = random_prng(c, k);
  c.s0++;
  |end

  /* Assign uniform positions in [0.0, L). */
  const double rx = (v1.s0*0x1p-64) * ${L[1]};
  const double ry = (v2.s0*0x1p-64) * ${L[2]};
  const double rz = (v3.s0*0x1p-64) * ${L[3]};

  /* Assign uniform velocities in [-1.0, 1.0). */
  const double vx = ((long)v1.s1)*0x1p-63;
  const double vy = ((long)v2.s1)*0x1p-63;
  const double vz = ((long)v3.s1)*0x1p-63;

  d_rs[gid] = (double3)(rx, ry, rz);
  d_vs[gid] = (double3)(vx, vy, vz);
}
