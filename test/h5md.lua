------------------------------------------------------------------------------
-- Test writing and reading H5MD files.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

local file = hdf5.create_file("h5md.h5")

-- Test H5MD metadata.
do
  local author = {name = "B.J. Alder", email = "bjalder@ucdavis.example"}
  local creator = {name = "vaf.lua", version = "1.0"}
  h5md.write_meta(file, {author = author, creator = creator})

  local t = h5md.read_meta(file)
  assert(t.version[1] == 1)
  assert(t.version[2] == 1)
  assert(t.author.name == author.name)
  assert(t.author.email == author.email)
  assert(t.creator.name == creator.name)
  assert(t.creator.version == creator.version)
end

-- Test simulation box.
do
  local L = {50, 60, 70}
  local periodic = {true, true, false}
  local group = file:create_group("particles/dimer")
  h5md.write_box(group, {L = L, periodic = periodic})

  local t = h5md.read_box(group)
  group:close()
  assert(t.dimension == 3)
  assert(t.periodic[1] == periodic[1])
  assert(t.periodic[2] == periodic[2])
  assert(t.periodic[3] == periodic[3])
  assert(t.L[1] == L[1])
  assert(t.L[2] == L[2])
  assert(t.L[3] == L[3])
end

-- Test scalar observable.
do
  local group = file:create_group("observables/solvent")
  local space = hdf5.create_space("scalar")
  do
    local step = {interval = 10, offset = 100}
    local time = {interval = 0.25, offset = 2.5}
    local species = h5md.create_observable(group, "species", step, time, hdf5.double)
    for i = 10, 50 do
      local filespace = species:append(1)
      filespace:select_hyperslab("set", {0}, nil, {1})
      species.value:write(ffi.new("double[1]", 100*i), hdf5.double, space, filespace)
      filespace:close()
    end
    species:close()
  end
  do
    local species = h5md.open_observable(group, "species")
    for i = 51, 100 do
      local filespace = species:append(1)
      filespace:select_hyperslab("set", {0}, nil, {1})
      species.value:write(ffi.new("double[1]", 100*i), hdf5.double, space, filespace)
      filespace:close()
    end
    species:close()
  end
  do
    local species = h5md.open_observable(group, "species")
    local value = ffi.new("double[1]")
    for i = 10, 100 do
      local filespace, time = species:seek(10*i)
      filespace:select_hyperslab("set", {0}, nil, {1})
      species.value:read(value, hdf5.double, space, filespace)
      filespace:close()
      assert(time == 0.25*i)
      assert(value[0] == 100*i)
    end
    species:close()
  end
  space:close()
  group:close()
end

-- Test 1-dimensional observable.
do
  local N = 1000
  local group = file:create_group("particles/solvent")
  local memspace = hdf5.create_simple_space({N})
  do
    local step = {interval = 10, offset = 10}
    local time = {interval = 0.25, offset = 0.25}
    local dtype = hdf5.uint8:enum_create()
    dtype:enum_insert("A", ffi.new("uint8_t[1]", 0))
    dtype:enum_insert("B", ffi.new("uint8_t[1]", 1))
    local position = h5md.create_observable(group, "position", step, time, dtype, {N})
    local array = ffi.new("uint8_t[?]", N)
    math.randomseed(42)
    for i = 1, 10 do
      local filespace = position:append(1)
      filespace:select_hyperslab("set", {0, 0}, nil, {1, N})
      for i = 0, N-1 do
        array[i] = math.random(0, 1)
      end
      position.value:write(array, dtype, memspace, filespace)
      filespace:close()
    end
    dtype:close()
    position:close()
  end
  do
    local position = h5md.open_observable(group, "position")
    local array = ffi.new("int[?]", N)
    local dtype = hdf5.int:enum_create()
    dtype:enum_insert("A", ffi.new("int[1]", 0))
    dtype:enum_insert("B", ffi.new("int[1]", 1))
    math.randomseed(42)
    for i = 1, 10 do
      local filespace, time = position:seek(10*i)
      filespace:select_hyperslab("set", {0, 0}, nil, {1, N})
      position.value:read(array, dtype, memspace, filespace)
      filespace:close()
      assert(time == 0.25*i)
      for i = 0, N-1 do
        assert(array[i] == math.random(0, 1))
      end
    end
    dtype:close()
    position:close()
  end
  memspace:close()
  group:close()
end

-- Test 2-dimensional observable.
do
  local N = 2
  local group = file:open_group("particles/dimer")
  local memspace = hdf5.create_simple_space({N, 4})
  memspace:select_hyperslab("set", {0, 0}, nil, {N, 3})
  do
    local step = {interval = 10, offset = 10}
    local time = {interval = 0.25, offset = 0.25}
    local position = h5md.create_observable(group, "position", step, time, hdf5.double, {N, 3})
    local array = ffi.new("struct { double x, y, z, w; }[?]", N)
    math.randomseed(42)
    for i = 1, 100 do
      local filespace = position:append(1)
      filespace:select_hyperslab("set", {0, 0, 0}, nil, {1, N, 3})
      for i = 0, N-1 do
        array[i].x = math.random()
        array[i].y = math.random()
        array[i].z = math.random()
      end
      position.value:write(array, hdf5.double, memspace, filespace)
      filespace:close()
    end
    position:close()
  end
  do
    local position = h5md.open_observable(group, "position")
    local array = ffi.new("double[?][4]", N)
    math.randomseed(42)
    for i = 1, 100 do
      local filespace, time = position:seek(10*i)
      filespace:select_hyperslab("set", {0, 0, 0}, nil, {1, N, 3})
      position.value:read(array, hdf5.double, memspace, filespace)
      filespace:close()
      assert(time == 0.25*i)
      for i = 0, N-1 do
        assert(array[i][0] == math.random())
        assert(array[i][1] == math.random())
        assert(array[i][2] == math.random())
      end
    end
    position:close()
  end
  memspace:close()
  group:close()
end

file:close()
