/*
 * Test Hilbert space-filling curve for domain with unequal side lengths.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

${include "nanomotor/hilbert.cl"}

|local bit = require("bit")

|-- Number of spatial dimensions.
|local D = #M

__kernel void
test_hilbert(__global uint${D} *restrict d_bin)
{
  const uint gid = get_global_id(0);
  uint${D} bin;
  |local s = 0
  |for i = 0, D-1 do
  bin.s${i} = (gid>>${s}) & ${bit.lshift(1, M[i+1])-1};
  |  s = s + M[i+1]
  |end
  const uint index = hilbert_index(bin);
  d_bin[index] = bin;
}
