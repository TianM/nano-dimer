------------------------------------------------------------------------------
-- Test Lennard-Jones potential.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

require("strict")

local nm = {
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
  lj = require("nanomotor.lj"),
  neigh = require("nanomotor.neigh"),
}

local cl = require("opencl")
local ffi = require("ffi")

-- MPI may be enabled by passing -lmpi to the interpreter.
local mpi = package.loaded["mpi"]
local rank = mpi and mpi.comm_world:rank() or 0
local nranks = mpi and mpi.comm_world:size() or 1

-- Number of subdomains per dimension for MPI tests.
local ndom = {1, 1, 1}
for i = 1, 3 do
  if mpi then ndom[i] = assert(tonumber(arg[i])) end
end

local platform = cl.get_platforms()[1]
local device = platform:get_devices()[rank%#platform:get_devices() + 1]
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Test solvent-dimer interaction.
do
  local box = nm.box({L = {100, 80, 90}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 8, Ns = 0, Ns_max = 3})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1, A = 0, B = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
  local sigma = {C = diameter.C/2, N = diameter.N/2}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local nlist_size = {ds = dom.Ns_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut})

  -- Place sphere at a corner.
  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.C

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle right of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == -12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle left of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle behind sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + L_local[2] - sigma.C
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle below sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + L_local[3] - sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 12)

  -- Place solvent particles left of, behind and below sphere.
  dom.Ns = 3
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.rs[1].x = O_local[1]
  dom.rs[1].y = O_local[2] + L_local[2] - sigma.C
  dom.rs[1].z = O_local[3]
  dom.rs[2].x = O_local[1]
  dom.rs[2].y = O_local[2]
  dom.rs[2].z = O_local[3] + L_local[3] - sigma.C
  dom.sps[0].s0 = species.A
  dom.sps[1].s0 = species.A
  dom.sps[2].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 12)
  assert(dom.fd[0].y == 12)
  assert(dom.fd[0].z == 12)
end

-- Test dimer-dimer interaction.
do
  local box = nm.box({L = {100, 80, 90}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 6, Ns = 0, Ns_max = 1})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1, CC = 10.0, CN = 10.0, NN = 10.0}
  local sigma = {C = diameter.C/2, N = diameter.N/2, CC = diameter.C, CN = (diameter.C+diameter.N)/2, NN = diameter.N}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local nlist_size = {ds = dom.Ns_max, dd = dom.Nd_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut})

  dom.Nd = 2
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2] + L_local[2]/2
  dom.rd[0].z = O_local[3] + L_local[3]/2
  dom.rd[1].x = O_local[1] - sigma.CN
  dom.rd[1].y = dom.rd[0].y
  dom.rd[1].z = dom.rd[0].z
  dom.spd[0].s0 = species.C
  dom.spd[1].s0 = species.N
  dom.idd[0] = 0
  dom.idd[1] = 1

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)
  assert(dom.fd[1].x == 0)
  assert(dom.fd[1].y == 0)
  assert(dom.fd[1].z == 0)

  dom.Nd = 4
  dom.rd[2].x = O_local[1] + L_local[1] - sigma.NN - 2*sigma.CN
  dom.rd[2].y = O_local[2] + L_local[2]/2
  dom.rd[2].z = O_local[3] + L_local[3]/2
  dom.rd[3].x = O_local[1] + L_local[1] - sigma.NN - sigma.CN
  dom.rd[3].y = dom.rd[0].y
  dom.rd[3].z = dom.rd[0].z
  dom.spd[2].s0 = species.C
  dom.spd[3].s0 = species.N
  dom.idd[2] = 2
  dom.idd[3] = 3

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)
  assert(dom.fd[1].x == 30)
  assert(dom.fd[1].y == 0)
  assert(dom.fd[1].z == 0)
  assert(dom.fd[2].x == 0)
  assert(dom.fd[2].y == 0)
  assert(dom.fd[2].z == 0)
  assert(dom.fd[3].x == -30)
  assert(dom.fd[3].y == 0)
  assert(dom.fd[3].z == 0)
end

-- Test sphere surrounded by ring of solvent particles.
do
  local box = nm.box({L = {100, 80, 90}, periodic = {true, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 8, Ns = 0, Ns_max = 360})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1, A = 0, B = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
  local sigma = {C = diameter.C/2, N = diameter.N/2}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local nlist_size = {ds = dom.Ns_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut})

  dom.Nd = 1
  dom.rd[0].x = O_local[1] + L_local[1]/2
  dom.rd[0].y = O_local[2] + L_local[2]/2
  dom.rd[0].z = O_local[3] + L_local[3]/2
  dom.spd[0].s0 = species.N

  dom.Ns = 0
  for i = 0, 359 do
    dom.Ns = dom.Ns + 1
    dom.rs[i].x = O_local[1] + L_local[1]/2 + sigma.N*math.cos(i*(math.pi/180))
    dom.rs[i].y = O_local[2] + L_local[2]/2 + sigma.N*math.sin(i*(math.pi/180))
    dom.rs[i].z = O_local[3] + L_local[3]/2
    dom.sps[i].s0 = species.A
  end
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x) < 1e-11)
  assert(math.abs(dom.fd[0].y) < 1e-11)
  assert(math.abs(dom.fd[0].z) < 1e-11)

  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.N

  dom.Ns = 0
  for i = 0, 89 do
    dom.Ns = dom.Ns + 1
    dom.rs[i].x = O_local[1] + sigma.N*math.cos(i*(math.pi/180))
    dom.rs[i].y = O_local[2] + sigma.N*math.sin(i*(math.pi/180))
    dom.rs[i].z = O_local[3]
    dom.sps[i].s0 = species.A
  end
  for i = 90, 179 do
    dom.Ns = dom.Ns + 1
    dom.rs[i].x = O_local[1] + L_local[1] + sigma.N*math.cos(i*(math.pi/180))
    dom.rs[i].y = O_local[2] + sigma.N*math.sin(i*(math.pi/180))
    dom.rs[i].z = O_local[3]
    dom.sps[i].s0 = species.A
  end
  for i = 180, 269 do
    dom.Ns = dom.Ns + 1
    dom.rs[i].x = O_local[1] + L_local[1] + sigma.N*math.cos(i*(math.pi/180))
    dom.rs[i].y = O_local[2] + L_local[2] + sigma.N*math.sin(i*(math.pi/180))
    dom.rs[i].z = O_local[3]
    dom.sps[i].s0 = species.A
  end
  for i = 270, 359 do
    dom.Ns = dom.Ns + 1
    dom.rs[i].x = O_local[1] + sigma.N*math.cos(i*(math.pi/180))
    dom.rs[i].y = O_local[2] + L_local[2] + sigma.N*math.sin(i*(math.pi/180))
    dom.rs[i].z = O_local[3]
    dom.sps[i].s0 = species.A
  end

  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x) < 1e-11)
  assert(math.abs(dom.fd[0].y) < 1e-11)
  assert(math.abs(dom.fd[0].z) < 1e-11)
end

-- Test wall-dimer interaction.
if ndom[1] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {false, true, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 4, Ns = 0, Ns_max = 1})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1, A = 0, B = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
  local sigma = {C = diameter.C/2, N = diameter.N/2}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local wall = {r_cut = 3^(1/6), epsilon = 5.0, sigma = sigma}
  local nlist_size = {ds = dom.Ns_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut, wall = wall})

  -- Place sphere right of left wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1] + sigma.C
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.C

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle right of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + 2*sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 26.97114317029974) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle in front of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + sigma.C
  dom.rs[0].y = O_local[2] + sigma.C
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].y == -12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle above sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == -12)

  -- Place sphere left of right wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1] + L_local[1] - sigma.N
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.N

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - -19.48557158514987) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle left of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - 2*sigma.N
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - -18.88557158514987) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle behind sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.N
  dom.rs[0].y = O_local[2] + L_local[2] - sigma.N
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - -19.48557158514987) < 1e-12)
  assert(math.abs(dom.fd[0].y - 0.6) < 1e-15)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle below sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.N
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + L_local[3] - sigma.N
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - -19.48557158514987) < 1e-12)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - 0.6) < 1e-15)
end

-- Test wall-dimer interaction.
if ndom[2] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {true, false, true}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 4, Ns = 0, Ns_max = 1})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1, A = 0, B = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
  local sigma = {C = diameter.C/2, N = diameter.N/2}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local wall = {r_cut = 3^(1/6), epsilon = 5.0, sigma = sigma}
  local nlist_size = {ds = dom.Ns_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut, wall = wall})

  -- Place sphere in front of back wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2] + sigma.C
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.C

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle right of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + sigma.C
  dom.rs[0].y = O_local[2] + sigma.C
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == -12)
  assert(math.abs(dom.fd[0].y - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle in front of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + 2*sigma.C
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - 26.97114317029974) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle above sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + sigma.C
  dom.rs[0].z = O_local[3] + sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - 38.97114317029974) < 1e-12)
  assert(dom.fd[0].z == -12)

  -- Place sphere behind front wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2] + L_local[2] - sigma.N
  dom.rd[0].z = O_local[3]
  dom.spd[0].s0 = species.N

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - -19.48557158514987) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle left of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.N
  dom.rs[0].y = O_local[2] + L_local[2] - sigma.N
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 0.6) < 1e-15)
  assert(math.abs(dom.fd[0].y - -19.48557158514987) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle behind sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + L_local[2] - 2*sigma.N
  dom.rs[0].z = O_local[3]
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - -18.88557158514987) < 1e-12)
  assert(dom.fd[0].z == 0)

  -- Place solvent particle below sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + L_local[2] - sigma.N
  dom.rs[0].z = O_local[3] + L_local[3] - sigma.N
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - -19.48557158514987) < 1e-12)
  assert(math.abs(dom.fd[0].z - 0.6) < 1e-15)
end

-- Test wall-dimer interaction.
if ndom[3] == 1 then
  local box = nm.box({L = {100, 80, 90}, periodic = {true, true, false}, ndom = mpi and ndom})
  local dom = nm.domain(queue, box, {Nd = 0, Nd_max = 4, Ns = 0, Ns_max = 1})
  local L_local, O_local = box.L_local, box.O_local
  local species = {C = 0, N = 1, A = 0, B = 1}

  local diameter = {C = 4.0, N = 8.0}
  local epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.1}
  local sigma = {C = diameter.C/2, N = diameter.N/2}
  local skin = 1.0
  local r_cut = 2^(1/6)
  local wall = {r_cut = 3^(1/6), epsilon = 5.0, sigma = sigma}
  local nlist_size = {ds = dom.Ns_max}
  local nbin = {math.floor(L_local[1]/2), math.floor(L_local[2]/2), math.floor(L_local[3]/2)}
  local bin_size = 10

  local neigh = nm.neigh(dom, {sigma = sigma, skin = skin, r_cut = r_cut, nlist_size = nlist_size, nbin = nbin, bin_size = bin_size})
  local lj = nm.lj(dom, neigh, {epsilon = epsilon, sigma = sigma, r_cut = r_cut, wall = wall})

  -- Place sphere above wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3] + sigma.C
  dom.spd[0].s0 = species.C

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - 38.97114317029974) < 1e-12)

  -- Place solvent particle right of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + sigma.C
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == -12)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - 38.97114317029974) < 1e-12)

  -- Place solvent particle in front of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + sigma.C
  dom.rs[0].z = O_local[3] + sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == -12)
  assert(math.abs(dom.fd[0].z - 38.97114317029974) < 1e-12)

  -- Place solvent particle above sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + 2*sigma.C
  dom.sps[0].s0 = species.A
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - 26.97114317029974) < 1e-12)

  -- Place sphere below wall.
  dom.Nd = 1
  dom.rd[0].x = O_local[1]
  dom.rd[0].y = O_local[2]
  dom.rd[0].z = O_local[3] + L_local[3] - sigma.N
  dom.spd[0].s0 = species.N

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - -19.48557158514987) < 1e-12)

  -- Place solvent particle left of sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1] + L_local[1] - sigma.N
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + L_local[3] - sigma.N
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(math.abs(dom.fd[0].x - 0.6) < 1e-15)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - -19.48557158514987) < 1e-12)

  -- Place solvent particle behind sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2] + L_local[2] - sigma.N
  dom.rs[0].z = O_local[3] + L_local[3] - sigma.N
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(math.abs(dom.fd[0].y - 0.6) < 1e-15)
  assert(math.abs(dom.fd[0].z - -19.48557158514987) < 1e-12)

  -- Place solvent particle below sphere.
  dom.Ns = 1
  dom.rs[0].x = O_local[1]
  dom.rs[0].y = O_local[2]
  dom.rs[0].z = O_local[3] + L_local[3] - 2*sigma.N
  dom.sps[0].s0 = species.B
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)

  neigh.pre_update()
  neigh.update()
  lj.update()
  neigh.post_update()
  lj.post_update()

  assert(dom.fd[0].x == 0)
  assert(dom.fd[0].y == 0)
  assert(math.abs(dom.fd[0].z - -18.88557158514987) < 1e-12)
end

if mpi then mpi.finalize() end
