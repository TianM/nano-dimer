#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot mean-square displacement of dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
#mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("show", help="show or save plot")
parser.add_argument("input", nargs="+", help="input filenames")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
args = parser.parse_args()

mpl.rc("pgf", texsystem="xelatex", preamble=(r"\usepackage{amsmath,bm,txfonts}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input[0], "r") as f:
  g = f["dynamics/dimer/mean_square_displacement"]
  time = g["time"][:]
  index = g["count"][:] != 0
  mean = np.zeros(shape=g["value"].shape)
  error = np.zeros(shape=g["value"].shape)

count = 0
for fn in args.input:
  with h5.File(fn, "r") as f:
    g = f["dynamics/dimer/mean_square_displacement"]
    assert np.all(time == g["time"])
    value = g["value"][:]
    count += 1
    delta = value - mean
    mean += delta / count
    error += delta * (value - mean)

error = np.sqrt(error / (count - 1) / count)

time = np.reshape(time[:, 1:], (-1,))
index = np.reshape(index[:, 1:], (-1,))
mean = np.reshape(mean[:, 1:], (-1,))
error = np.reshape(error[:, 1:], (-1,))

order = time.argsort()
time = time[order]
index = index[order]
mean = mean[order]
error = error[order]

def fitfunc(t, D):
    return 6*D*t
popt, pconv=curve_fit(fitfunc, time[index], mean[index])
print('Diffusion constant: %.3e' %popt[0])

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.errorbar(time[index], mean[index], error[index], errorevery=10)
plt.plot(time[index], fitfunc(time[index],popt[0]),label = str('Diffusion constant: %.3e' %popt[0]))
# ax.set_xscale("log")
# ax.set_yscale("log")
ax.set_xscale("linear")
ax.set_yscale("linear")

ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\Delta L^2(t)$")
fig.tight_layout(pad=0.1)

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
