#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot center-of-mass velocity.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse
from itertools import cycle

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()

mpl.rc("pgf", preamble=(r"\usepackage{amsmath,amssymb,bm}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input, "r") as f:
  g = f["observables/center_of_mass_velocity"]
  value = g["value"][:]
  time = g["time"][()]*np.arange(value.shape[0]) + g["time"].attrs["offset"]

fig, axes = plt.subplots(3, 1, sharex=True, figsize=args.figsize)
color = cycle(["b", "g", "r"])
label = cycle(["x", "y", "z"])

for i, ax in enumerate(axes):
  ax.plot(time, value[:, i], c=next(color))
  ax.minorticks_on()
  ax.set_ylabel(r"$\bm{V}_%s$" % next(label))

ax.set_xlabel(r"$t$")
fig.tight_layout(pad=0.1)
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
