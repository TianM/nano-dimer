#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot trajectory of sphere dimer.
#

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()

f = h5.File(args.input, "r")
g = f["particles/dimer"]
force = g["force/value"]
time = g["position/time"][()]*np.arange(force.shape[0]) + g["position/time"].attrs["offset"]
data = force[:,:,:]
data = data.mean(axis=1)
axes = [plt.subplot(3, 1, 1)]
axes.extend([plt.subplot(3, 1, i, sharex=axes[0]) for i in [2,3]])
for i, ax in enumerate(axes):
    print('xyz'[i], 'mean', data[:,i].mean(),
    'std', data[:,i].std())
    ax.plot(time, data[:,i])
    ax.set_ylabel('xyz'[i])

if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
