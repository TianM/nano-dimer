#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plot potential energy per degree of freedom.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("output", help="plot filename")
parser.add_argument("input", help="input filename")
parser.add_argument("--figsize", type=float, nargs=2, default=(6., 4.), help="figure size")
parser.add_argument("show", help="show or save plot")
args = parser.parse_args()

mpl.rc("pgf", preamble=(r"\usepackage{amsmath,amssymb,bm}",))
mpl.rc("font", family="serif", serif=(), size=11)
mpl.rc("legend", fontsize=11, numpoints=1)

with h5.File(args.input, "r") as f:
  g = f["observables/potential_energy"]
  value = g["value"][:]
  time = g["time"][()]*np.arange(value.shape[0]) + g["time"].attrs["offset"]
  mean = np.mean(value)

fig = plt.figure(figsize=args.figsize)
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, value)
ax.axhline(mean, dashes=(4, 4), c="k")
ax.minorticks_on()
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$V/\nu$")
fig.tight_layout(pad=0.1)
if args.show =="show":
    plt.show()
elif args.show=="save":
    plt.savefig(args.output)
else: print('Last argument must be either "show" or "save"!')
