#
# Makefile for nano-dimer.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

all: test

SUBDIRS = test doc

.PHONY: $(SUBDIRS)

$(SUBDIRS):
	@$(MAKE) -C $@
