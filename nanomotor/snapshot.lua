------------------------------------------------------------------------------
-- Snapshot program state.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")
local syscall = require("syscall")

-- Cache C types.
local int_ptr = ffi.typeof("int *")
local uint64_t_1 = ffi.typeof("uint64_t[1]")

--[[
Snapshots the state of an object to an HDF5 group.
--]]
local function snapshot(object, group, name)
  group = group:create_group(name)
  local space = hdf5.create_space("scalar")
  local attr = group:create_attribute("step", hdf5.uint64, space)
  space:close()
  attr:write(uint64_t_1(object.step), hdf5.uint64)
  attr:close()
  local snapshot = object.snapshot
  if snapshot then snapshot(group) end
  group:close()
end

--[[
Restores the state of an object from an HDF5 group.
--]]
local function restore(object, group, name)
  group = group:open_group(name)
  local attr = group:open_attribute("step")
  local buf = uint64_t_1()
  attr:read(buf, hdf5.uint64)
  attr:close()
  object.step = tonumber(buf[0])
  local restore = object.restore
  if restore then restore(group) end
  group:close()
end

--[[
Restores and snapshots program state.

If a previous snapshot file exists, the program state is restored
before resuming the simulation. The program state is periodically
written to a snapshot file during the simulation. A snapshot is
atomic: a previous snapshot file is only overwritten once the
superseding snapshot file have been synchronized to disk. This
ensures that the previous snapshot is preserved if the current
snapshot is interrupted.
]]
return function(dom, observables, args)
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank() or 0
  local output = args.output
  local initial, final, interval = args.initial, args.final, args.interval
  local self = {step = initial}

  function self.observe()
    if syscall.access(output, "r") then
      local fapl = hdf5.create_plist("file_access")
      if comm then fapl:set_fapl_mpio(comm) end
      local file = hdf5.open_file(output, nil, fapl)
      fapl:close()
      restore(dom, file, "domain")
      for i, observable in ipairs(observables) do
        restore(observable, file, "observables/"..i)
      end
      file:close()
    end
    while self.step <= final do
      coroutine.yield()
      self.step = self.step + interval
      local fapl = hdf5.create_plist("file_access")
      if comm then fapl:set_fapl_mpio(comm) end
      local file = hdf5.create_file(output..".tmp", nil, nil, fapl)
      fapl:close()
      snapshot(dom, file, "domain")
      for i, observable in ipairs(observables) do
        snapshot(observable, file, "observables/"..i)
      end
      file:flush()
      if not comm then assert((syscall.fsync(ffi.cast(int_ptr, file:get_vfd_handle())[0]))) end
      file:close()
      if rank == 0 then assert((syscall.rename(output..".tmp", output))) end
    end
  end

  return self
end
