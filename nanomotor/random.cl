/*
 * Random distributions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

|local bit = require("bit")

|-- Rotation constants
|local R = {16, 42, 12, 31, 16, 32, 24, 21}
|-- Number of rounds
|local N = 20
|-- Number of state words
|local W = 2
|-- Key schedule constant
|local K = "0x1BD11BDAA9FC1A22"

/**
Returns 128-bit random variate given a counter and a key.

This function implements Threefry-2×64, a counter-based pseudo-random
number generator [@Salmon2011] that produces a high-quality pseudo-random
sequence from an arbitrarily correlated sequence, e.g., 1, 2, 3, ….

The algorithm is based on the Threefish block cipher that is part of the
Skein hash function family [@Ferguson2010]. Compared with Threefish-256,
the state size of the PRNG is reduced from 256 to 128 bits, the number of
rounds is reduced from 72 to 20, and the key schedule tweak is removed.
For the reference implementation of Threefish refer to `skein_block.c`
in the [NIST submission CD].

[NIST submission CD]: http://www.skein-hash.info/sites/default/files/NIST_CD_102610.zip
**/
ulong2 random_prng(ulong2 c, ulong2 k)
{
  ulong ks${W} = ${K}UL;
  |for i = 0, W-1 do
  ulong ks${i} = k.s${i};
  ks${W} ^= k.s${i};
  |end
  |for i = 0, W-1 do
  c.s${i} += ks${i};
  |end
  |for r = 0, N-1 do
  c.s0 += c.s1; c.s1 = rotate(c.s1, ${R[r%#R+1]}UL); c.s1 ^= c.s0;
  |  if r%4 == 3 then
  |    local r = bit.rshift(r+1, 2)
  |    for i = 0, W-1 do
  c.s${i} += ks${(r+i)%(W+1)};
  |    end
  c.s${W-1} += ${r};
  |  end
  |end
  return c;
}

/**
Returns two variates from a uniform distribution in [0.0, 1.0).
**/
double2 random_uniform(ulong4 *c)
{
  const ulong2 u = random_prng((*c).s01, (*c).s23);
  (*c).s0++;
  return convert_double2(u)*0x1p-64;
}

/**
Returns two variates from a standard normal distribution.

See @Marsaglia1964.
**/
double2 random_normal(ulong4 *c)
{
  double2 v;
  double s;
  do {
    const ulong2 u = random_prng((*c).s01, (*c).s23);
    (*c).s0++;
    v = convert_double2(as_long2(u))*0x1p-63;
    s = dot(v, v);
  } while (s >= 1);
  return v*sqrt(-2*log(s)/s);
}

/**
Returns uniformly chosen point on a unit circle.

See @Neumann1951 or @Cook1957, and [Circle Point Picking].

[Circle Point Picking]: http://mathworld.wolfram.com/CirclePointPicking.html
**/
double2 random_circle(ulong4 *c)
{
  double2 v;
  double s;
  do {
    const ulong2 u = random_prng((*c).s01, (*c).s23);
    (*c).s0++;
    v = convert_double2(as_long2(u))*0x1p-63;
    s = dot(v, v);
  } while (s >= 1);
  const double t = 1/s;
  return (double2)((v.x*v.x-v.y*v.y)*t, 2*v.x*v.y*t);
}

/**
Returns uniformly chosen point on a unit sphere.

See @Marsaglia1972 and [Sphere Point Picking].

[Sphere Point Picking]: http://mathworld.wolfram.com/SpherePointPicking.html
**/
double3 random_sphere(ulong4 *c)
{
  double2 v;
  double s;
  do {
    const ulong2 u = random_prng((*c).s01, (*c).s23);
    (*c).s0++;
    v = convert_double2(as_long2(u))*0x1p-63;
    s = dot(v, v);
  } while (s >= 1);
  return (double3)(v*(2*sqrt(1-s)), 1-2*s);
}
