------------------------------------------------------------------------------
-- Simulation domain.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local array = require("nanomotor.array")
local cl = require("opencl")
local ffi = require("ffi")
local hdf5 = require("hdf5")

-- Cache library functions.
local ceil, floor = math.ceil, math.floor
-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double3 = ffi.typeof("cl_double3")
local cl_int = ffi.typeof("cl_int")
local cl_short3 = ffi.typeof("cl_short3")
local int_1 = ffi.typeof("int[1]")
local int_n = ffi.typeof("int[?]")

return function(queue, box, args)
  local context = queue:get_info("context")
  local device = queue:get_info("device")
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank()
  local nranks = comm and comm:size()
  local Nd, Nd_max = args.Nd, args.Nd_max or args.Nd
  local Ns, Ns_max = args.Ns, args.Ns_max or args.Ns
  local mass = args.mass

  local dom = {
    context = context, device = device, queue = queue,
    box = box,
    Nd = Nd, Nd_max = Nd_max,
    Ns = Ns, Ns_max = Ns_max, Ns_neigh = 0,
    mass = mass,
    step = 0,
  }

  dom.h_rd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_double3"))
  dom.h_imd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_short3"))
  dom.h_vd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_double3"))
  dom.h_fd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_double3"))
  dom.h_spd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_char4"))
  dom.h_idd = context:create_buffer("alloc_host_ptr", Nd_max*ffi.sizeof("cl_int"))

  dom.rd = ffi.cast("cl_double3 *", queue:enqueue_map_buffer(dom.h_rd, true, "read"))
  dom.imd = ffi.cast("cl_short3 *", queue:enqueue_map_buffer(dom.h_imd, true, "read"))
  dom.vd = ffi.cast("cl_double3 *", queue:enqueue_map_buffer(dom.h_vd, true, "read"))
  dom.fd = ffi.cast("cl_double3 *", queue:enqueue_map_buffer(dom.h_fd, true, "read"))
  dom.spd = ffi.cast("cl_char4 *", queue:enqueue_map_buffer(dom.h_spd, true, "read"))
  dom.idd = ffi.cast("cl_int *", queue:enqueue_map_buffer(dom.h_idd, true, "read"))

  dom.rs = array.new("cl_double3", Ns_max)
  dom.ims = array.new("cl_short3", Ns_max)
  dom.vs = array.new("cl_double3", Ns_max)
  dom.fs = array.new("cl_double3", Ns_max)
  dom.sps = array.new("cl_char4", Ns_max)
  dom.ids = array.new("cl_int", Ns_max)

  ffi.fill(dom.rd, Nd_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.imd, Nd_max*ffi.sizeof("cl_short3"))
  ffi.fill(dom.vd, Nd_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.fd, Nd_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.spd, Nd_max*ffi.sizeof("cl_char4"))
  ffi.fill(dom.idd, Nd_max*ffi.sizeof("cl_int"), 0xff)

  ffi.fill(dom.rs, Ns_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.ims, Ns_max*ffi.sizeof("cl_short3"))
  ffi.fill(dom.vs, Ns_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.fs, Ns_max*ffi.sizeof("cl_double3"))
  ffi.fill(dom.sps, Ns_max*ffi.sizeof("cl_char4"))
  ffi.fill(dom.ids, Ns_max*ffi.sizeof("cl_int"), 0xff)

  dom.d_rd = context:create_buffer(Nd_max*ffi.sizeof("cl_double3"))
  dom.d_imd = context:create_buffer(Nd_max*ffi.sizeof("cl_short3"))
  dom.d_vd = context:create_buffer(Nd_max*ffi.sizeof("cl_double3"))
  dom.d_fd = context:create_buffer(Nd_max*ffi.sizeof("cl_double3"))
  dom.d_spd = context:create_buffer(Nd_max*ffi.sizeof("cl_char4"))
  dom.d_idd = context:create_buffer(Nd_max*ffi.sizeof("cl_int"))

  dom.d_rs = context:create_buffer(Ns_max*ffi.sizeof("cl_double3"))
  dom.d_ims = context:create_buffer(Ns_max*ffi.sizeof("cl_short3"))
  dom.d_vs = context:create_buffer(Ns_max*ffi.sizeof("cl_double3"))
  dom.d_fs = context:create_buffer(Ns_max*ffi.sizeof("cl_double3"))
  dom.d_sps = context:create_buffer(Ns_max*ffi.sizeof("cl_char4"))
  dom.d_ids = context:create_buffer(Ns_max*ffi.sizeof("cl_int"))

  queue:enqueue_write_buffer(dom.d_rs, false, dom.rs)
  queue:enqueue_write_buffer(dom.d_ims, false, dom.ims)
  queue:enqueue_write_buffer(dom.d_vs, false, dom.vs)
  queue:enqueue_write_buffer(dom.d_fs, false, dom.fs)
  queue:enqueue_write_buffer(dom.d_sps, false, dom.sps)
  queue:enqueue_write_buffer(dom.d_ids, true, dom.ids)

  if comm then
    dom.type_rd = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_rd:commit()
    dom.type_imd = mpi.type_create_resized(mpi.type_contiguous(3, mpi.int16), 0, ffi.sizeof("cl_short3"))
    dom.type_imd:commit()
    dom.type_vd = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_vd:commit()
    dom.type_fd = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_fd:commit()
    dom.type_spd = mpi.type_contiguous(4, mpi.int8)
    dom.type_spd:commit()
    dom.type_idd = mpi.int32

    --[[
    Redistributes dimer spheres among all domains.

    For each dimer, the local box is determined from the position of its first
    sphere, and the particle data of the dimer is sent to the corresponding
    domain. The function reads from and writes to the buffers in host memory.
    --]]
    function dom.alltoalld()
      local ndom = mpi.cart_get(comm)
      local count = int_n(nranks)
      local displ = int_n(nranks)
      for i = 0, dom.Nd-1, 2 do
        local x, y, z = box.mincoords(dom.rd[i].x, dom.rd[i].y, dom.rd[i].z)
        local n = x + y*ndom[1] + z*(ndom[1]*ndom[2])
        count[n] = count[n] + 2
      end
      for i = 1, nranks-1 do
        displ[i] = displ[i-1] + count[i-1]
      end
      local sendcount = int_n(nranks)
      local senddispl = int_n(nranks)
      for i = 0, nranks-1 do
        local coords = mpi.cart_coords(comm, i)
        local n = coords[1] + coords[2]*ndom[1] + coords[3]*(ndom[1]*ndom[2])
        sendcount[i] = count[n]
        senddispl[i] = displ[n]
      end
      local rd = ffi.new("cl_double3[?]", Nd_max)
      local imd = ffi.new("cl_short3[?]", Nd_max)
      local vd = ffi.new("cl_double3[?]", Nd_max)
      local spd = ffi.new("cl_char4[?]", Nd_max)
      local idd = ffi.new("cl_int[?]", Nd_max)
      for i = 0, dom.Nd-1, 2 do
        local x, y, z = box.mincoords(dom.rd[i].x, dom.rd[i].y, dom.rd[i].z)
        local n = x + y*ndom[1] + z*(ndom[1]*ndom[2])
        local j = displ[n]
        rd[j], rd[j+1] = dom.rd[i], dom.rd[i+1]
        imd[j], imd[j+1] = dom.imd[i], dom.imd[i+1]
        vd[j], vd[j+1] = dom.vd[i], dom.vd[i+1]
        spd[j], spd[j+1] = dom.spd[i], dom.spd[i+1]
        idd[j], idd[j+1] = dom.idd[i], dom.idd[i+1]
        displ[n] = j + 2
      end
      local recvcount = int_n(nranks)
      local recvdispl = int_n(nranks)
      mpi.alltoall(sendcount, 1, mpi.int, recvcount, 1, mpi.int, comm)
      local Nd = 0
      for i = 0, nranks-1 do
        recvdispl[i] = Nd
        Nd = Nd + recvcount[i]
      end
      if Nd > Nd_max then error("receive buffer overflow of "..Nd.." dimer spheres") end
      dom.Nd = Nd
      mpi.alltoallv(rd, sendcount, senddispl, dom.type_rd, dom.rd, recvcount, recvdispl, dom.type_rd, comm)
      mpi.alltoallv(imd, sendcount, senddispl, dom.type_imd, dom.imd, recvcount, recvdispl, dom.type_imd, comm)
      mpi.alltoallv(vd, sendcount, senddispl, dom.type_vd, dom.vd, recvcount, recvdispl, dom.type_vd, comm)
      mpi.alltoallv(spd, sendcount, senddispl, dom.type_spd, dom.spd, recvcount, recvdispl, dom.type_spd, comm)
      mpi.alltoallv(idd, sendcount, senddispl, dom.type_idd, dom.idd, recvcount, recvdispl, dom.type_idd, comm)
    end

    dom.type_rs = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_rs:commit()
    dom.type_ims = mpi.type_create_resized(mpi.type_contiguous(3, mpi.int16), 0, ffi.sizeof("cl_short3"))
    dom.type_ims:commit()
    dom.type_vs = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_vs:commit()
    dom.type_fs = mpi.type_create_resized(mpi.type_contiguous(3, mpi.double), 0, ffi.sizeof("cl_double3"))
    dom.type_fs:commit()
    dom.type_sps = mpi.type_contiguous(4, mpi.int8)
    dom.type_sps:commit()
    dom.type_ids = mpi.int32

    --[[
    Redistributes solvent particles among all domains.

    For each solvent particle, the local box is determined from its position,
    and the particle data of the solvent particle is sent to the corresponding
    domain. The function reads from and writes to the buffers in host memory.
    --]]
    function dom.alltoalls()
      local ndom = mpi.cart_get(comm)
      local count = int_n(nranks)
      local displ = int_n(nranks)
      for i = 0, dom.Ns-1 do
        local x, y, z = box.mincoords(dom.rs[i].x, dom.rs[i].y, dom.rs[i].z)
        local n = x + y*ndom[1] + z*(ndom[1]*ndom[2])
        count[n] = count[n] + 1
      end
      for i = 1, nranks-1 do
        displ[i] = displ[i-1] + count[i-1]
      end
      local sendcount = int_n(nranks)
      local senddispl = int_n(nranks)
      for i = 0, nranks-1 do
        local coords = mpi.cart_coords(comm, i)
        local n = coords[1] + coords[2]*ndom[1] + coords[3]*(ndom[1]*ndom[2])
        sendcount[i] = count[n]
        senddispl[i] = displ[n]
      end
      local rs = array.new("cl_double3", Ns_max)
      local ims = array.new("cl_short3", Ns_max)
      local vs = array.new("cl_double3", Ns_max)
      local sps = array.new("cl_char4", Ns_max)
      local ids = array.new("cl_int", Ns_max)
      for i = 0, dom.Ns-1 do
        local x, y, z = box.mincoords(dom.rs[i].x, dom.rs[i].y, dom.rs[i].z)
        local n = x + y*ndom[1] + z*(ndom[1]*ndom[2])
        local j = displ[n]
        rs[j] = dom.rs[i]
        ims[j] = dom.ims[i]
        vs[j] = dom.vs[i]
        sps[j] = dom.sps[i]
        ids[j] = dom.ids[i]
        displ[n] = j + 1
      end
      local recvcount = int_n(nranks)
      local recvdispl = int_n(nranks)
      mpi.alltoall(sendcount, 1, mpi.int, recvcount, 1, mpi.int, comm)
      local Ns = 0
      for i = 0, nranks-1 do
        recvdispl[i] = Ns
        Ns = Ns + recvcount[i]
      end
      if Ns > Ns_max then error("receive buffer overflow of "..Ns.." solvent particles") end
      dom.Ns = Ns
      mpi.alltoallv(rs, sendcount, senddispl, dom.type_rs, dom.rs, recvcount, recvdispl, dom.type_rs, comm)
      mpi.alltoallv(ims, sendcount, senddispl, dom.type_ims, dom.ims, recvcount, recvdispl, dom.type_ims, comm)
      mpi.alltoallv(vs, sendcount, senddispl, dom.type_vs, dom.vs, recvcount, recvdispl, dom.type_vs, comm)
      mpi.alltoallv(sps, sendcount, senddispl, dom.type_sps, dom.sps, recvcount, recvdispl, dom.type_sps, comm)
      mpi.alltoallv(ids, sendcount, senddispl, dom.type_ids, dom.ids, recvcount, recvdispl, dom.type_ids, comm)
      array.free(rs)
      array.free(ims)
      array.free(vs)
      array.free(sps)
      array.free(ids)
    end
  end

  function dom.snapshot(group)
    local dxpl = hdf5.create_plist("dataset_xfer")
    if comm then dxpl:set_dxpl_mpio("collective") end
    do
      local Nd = int_1(dom.Nd)
      if comm then mpi.allreduce(mpi.in_place, Nd, 1, mpi.int, mpi.sum, comm) end
      Nd = Nd[0]
      local subgroup = group:create_group("dimer")
      local filespace_rd = hdf5.create_simple_space({Nd, 3})
      local filespace_imd = hdf5.create_simple_space({Nd, 3})
      local filespace_vd = hdf5.create_simple_space({Nd, 3})
      local filespace_spd = hdf5.create_simple_space({Nd, 4})
      local filespace_idd = hdf5.create_simple_space({Nd})
      local dset_rd = subgroup:create_dataset("position", hdf5.double, filespace_rd)
      local dset_imd = subgroup:create_dataset("image", hdf5.int16, filespace_imd)
      local dset_vd = subgroup:create_dataset("velocity", hdf5.double, filespace_vd)
      local dset_spd = subgroup:create_dataset("species", hdf5.int8, filespace_spd)
      local dset_idd = subgroup:create_dataset("id", hdf5.int32, filespace_idd)
      subgroup:close()
      local offset = int_1()
      if comm then mpi.exscan(int_1(dom.Nd), offset, 1, mpi.int, mpi.sum, comm) end
      offset = offset[0]
      filespace_rd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_imd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_vd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_spd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 4})
      filespace_idd:select_hyperslab("set", {offset}, nil, {dom.Nd})
      local memspace_rd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_imd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_vd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_spd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_idd = hdf5.create_simple_space({Nd_max})
      memspace_rd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_imd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_vd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_spd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 4})
      memspace_idd:select_hyperslab("set", {0}, nil, {dom.Nd})
      dset_rd:write(dom.rd, hdf5.double, memspace_rd, filespace_rd, dxpl)
      dset_imd:write(dom.imd, hdf5.int16, memspace_imd, filespace_imd, dxpl)
      dset_vd:write(dom.vd, hdf5.double, memspace_vd, filespace_vd, dxpl)
      dset_spd:write(dom.spd, hdf5.int8, memspace_spd, filespace_spd, dxpl)
      dset_idd:write(dom.idd, hdf5.int32, memspace_idd, filespace_idd, dxpl)
      filespace_rd:close()
      filespace_imd:close()
      filespace_vd:close()
      filespace_spd:close()
      filespace_idd:close()
      memspace_rd:close()
      memspace_imd:close()
      memspace_vd:close()
      memspace_spd:close()
      memspace_idd:close()
      dset_rd:close()
      dset_imd:close()
      dset_vd:close()
      dset_spd:close()
      dset_idd:close()
    end
    do
      local Ns = int_1(dom.Ns)
      if comm then mpi.allreduce(mpi.in_place, Ns, 1, mpi.int, mpi.sum, comm) end
      Ns = Ns[0]
      local subgroup = group:create_group("solvent")
      local filespace_rs = hdf5.create_simple_space({Ns, 3})
      local filespace_ims = hdf5.create_simple_space({Ns, 3})
      local filespace_vs = hdf5.create_simple_space({Ns, 3})
      local filespace_sps = hdf5.create_simple_space({Ns, 4})
      local filespace_ids = hdf5.create_simple_space({Ns})
      local dset_rs = subgroup:create_dataset("position", hdf5.double, filespace_rs)
      local dset_ims = subgroup:create_dataset("image", hdf5.int16, filespace_ims)
      local dset_vs = subgroup:create_dataset("velocity", hdf5.double, filespace_vs)
      local dset_sps = subgroup:create_dataset("species", hdf5.int8, filespace_sps)
      local dset_ids = subgroup:create_dataset("id", hdf5.int32, filespace_ids)
      subgroup:close()
      queue:enqueue_read_buffer(dom.d_rs, false, 0, dom.Ns*ffi.sizeof(cl_double3), dom.rs)
      queue:enqueue_read_buffer(dom.d_ims, false, 0, dom.Ns*ffi.sizeof(cl_short3), dom.ims)
      queue:enqueue_read_buffer(dom.d_vs, false, 0, dom.Ns*ffi.sizeof(cl_double3), dom.vs)
      queue:enqueue_read_buffer(dom.d_sps, false, 0, dom.Ns*ffi.sizeof(cl_char4), dom.sps)
      queue:enqueue_read_buffer(dom.d_ids, true, 0, dom.Ns*ffi.sizeof(cl_int), dom.ids)
      local offset = int_1()
      if comm then mpi.exscan(int_1(dom.Ns), offset, 1, mpi.int, mpi.sum, comm) end
      offset = offset[0]
      filespace_rs:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_ims:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_vs:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_sps:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 4})
      filespace_ids:select_hyperslab("set", {offset}, nil, {dom.Ns})
      local memspace_rs = hdf5.create_simple_space({Ns_max, 4})
      local memspace_ims = hdf5.create_simple_space({Ns_max, 4})
      local memspace_vs = hdf5.create_simple_space({Ns_max, 4})
      local memspace_sps = hdf5.create_simple_space({Ns_max, 4})
      local memspace_ids = hdf5.create_simple_space({Ns_max})
      memspace_rs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_ims:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_vs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_sps:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 4})
      memspace_ids:select_hyperslab("set", {0}, nil, {dom.Ns})
      dset_rs:write(dom.rs, hdf5.double, memspace_rs, filespace_rs, dxpl)
      dset_ims:write(dom.ims, hdf5.int16, memspace_ims, filespace_ims, dxpl)
      dset_vs:write(dom.vs, hdf5.double, memspace_vs, filespace_vs, dxpl)
      dset_sps:write(dom.sps, hdf5.int8, memspace_sps, filespace_sps, dxpl)
      dset_ids:write(dom.ids, hdf5.int32, memspace_ids, filespace_ids, dxpl)
      filespace_rs:close()
      filespace_ims:close()
      filespace_vs:close()
      filespace_sps:close()
      filespace_ids:close()
      memspace_rs:close()
      memspace_ims:close()
      memspace_vs:close()
      memspace_sps:close()
      memspace_ids:close()
      dset_rs:close()
      dset_ims:close()
      dset_vs:close()
      dset_sps:close()
      dset_ids:close()
    end
    dxpl:close()
  end

  function dom.restore(group)
    local dxpl = hdf5.create_plist("dataset_xfer")
    if comm then dxpl:set_dxpl_mpio("collective") end
    do
      local subgroup = group:open_group("dimer")
      local dset_rd = subgroup:open_dataset("position")
      local dset_imd = subgroup:open_dataset("image")
      local dset_vd = subgroup:open_dataset("velocity")
      local dset_spd = subgroup:open_dataset("species")
      local dset_idd = subgroup:open_dataset("id")
      subgroup:close()
      local filespace_rd = dset_rd:get_space()
      local filespace_imd = dset_imd:get_space()
      local filespace_vd = dset_vd:get_space()
      local filespace_spd = dset_spd:get_space()
      local filespace_idd = dset_idd:get_space()
      dom.Nd = filespace_idd:get_simple_extent_dims()[1]
      if comm then dom.Nd = 2*((rank < (dom.Nd/2)%nranks) and ceil or floor)((dom.Nd/2)/nranks) end
      local offset = int_1()
      if comm then mpi.exscan(int_1(dom.Nd), offset, 1, mpi.int, mpi.sum, comm) end
      offset = offset[0]
      filespace_rd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_imd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_vd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 3})
      filespace_spd:select_hyperslab("set", {offset, 0}, nil, {dom.Nd, 4})
      filespace_idd:select_hyperslab("set", {offset}, nil, {dom.Nd})
      local memspace_rd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_imd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_vd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_spd = hdf5.create_simple_space({Nd_max, 4})
      local memspace_idd = hdf5.create_simple_space({Nd_max})
      memspace_rd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_imd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_vd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
      memspace_spd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 4})
      memspace_idd:select_hyperslab("set", {0}, nil, {dom.Nd})
      dset_rd:read(dom.rd, hdf5.double, memspace_rd, filespace_rd, dxpl)
      dset_imd:read(dom.imd, hdf5.int16, memspace_imd, filespace_imd, dxpl)
      dset_vd:read(dom.vd, hdf5.double, memspace_vd, filespace_vd, dxpl)
      dset_spd:read(dom.spd, hdf5.int8, memspace_spd, filespace_spd, dxpl)
      dset_idd:read(dom.idd, hdf5.int32, memspace_idd, filespace_idd, dxpl)
      filespace_rd:close()
      filespace_imd:close()
      filespace_vd:close()
      filespace_spd:close()
      filespace_idd:close()
      memspace_rd:close()
      memspace_imd:close()
      memspace_vd:close()
      memspace_spd:close()
      memspace_idd:close()
      dset_rd:close()
      dset_imd:close()
      dset_vd:close()
      dset_spd:close()
      dset_idd:close()
      if comm then dom.alltoalld() end
    end
    do
      local subgroup = group:open_group("solvent")
      local dset_rs = subgroup:open_dataset("position")
      local dset_ims = subgroup:open_dataset("image")
      local dset_vs = subgroup:open_dataset("velocity")
      local dset_sps = subgroup:open_dataset("species")
      local dset_ids = subgroup:open_dataset("id")
      subgroup:close()
      local filespace_rs = dset_rs:get_space()
      local filespace_ims = dset_ims:get_space()
      local filespace_vs = dset_vs:get_space()
      local filespace_sps = dset_sps:get_space()
      local filespace_ids = dset_ids:get_space()
      dom.Ns = filespace_ids:get_simple_extent_dims()[1]
      if comm then dom.Ns = ((rank < dom.Ns%nranks) and ceil or floor)(dom.Ns/nranks) end
      local offset = int_1()
      if comm then mpi.exscan(int_1(dom.Ns), offset, 1, mpi.int, mpi.sum, comm) end
      offset = offset[0]
      filespace_rs:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_ims:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_vs:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 3})
      filespace_sps:select_hyperslab("set", {offset, 0}, nil, {dom.Ns, 4})
      filespace_ids:select_hyperslab("set", {offset}, nil, {dom.Ns})
      local memspace_rs = hdf5.create_simple_space({Ns_max, 4})
      local memspace_ims = hdf5.create_simple_space({Ns_max, 4})
      local memspace_vs = hdf5.create_simple_space({Ns_max, 4})
      local memspace_sps = hdf5.create_simple_space({Ns_max, 4})
      local memspace_ids = hdf5.create_simple_space({Ns_max})
      memspace_rs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_ims:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_vs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
      memspace_sps:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 4})
      memspace_ids:select_hyperslab("set", {0}, nil, {dom.Ns})
      dset_rs:read(dom.rs, hdf5.double, memspace_rs, filespace_rs, dxpl)
      dset_ims:read(dom.ims, hdf5.int16, memspace_ims, filespace_ims, dxpl)
      dset_vs:read(dom.vs, hdf5.double, memspace_vs, filespace_vs, dxpl)
      dset_sps:read(dom.sps, hdf5.int8, memspace_sps, filespace_sps, dxpl)
      dset_ids:read(dom.ids, hdf5.int32, memspace_ids, filespace_ids, dxpl)
      filespace_rs:close()
      filespace_ims:close()
      filespace_vs:close()
      filespace_sps:close()
      filespace_ids:close()
      memspace_rs:close()
      memspace_ims:close()
      memspace_vs:close()
      memspace_sps:close()
      memspace_ids:close()
      dset_rs:close()
      dset_ims:close()
      dset_vs:close()
      dset_sps:close()
      dset_ids:close()
      if comm then dom.alltoalls() end
      queue:enqueue_write_buffer(dom.d_rs, false, 0, dom.Ns*ffi.sizeof(cl_double3), dom.rs)
      queue:enqueue_write_buffer(dom.d_ims, false, 0, dom.Ns*ffi.sizeof(cl_short3), dom.ims)
      queue:enqueue_write_buffer(dom.d_vs, false, 0, dom.Ns*ffi.sizeof(cl_double3), dom.vs)
      queue:enqueue_write_buffer(dom.d_sps, false, 0, dom.Ns*ffi.sizeof(cl_char4), dom.sps)
      queue:enqueue_write_buffer(dom.d_ids, true, 0, dom.Ns*ffi.sizeof(cl_int), dom.ids)
    end
    dxpl:close()
  end

  return dom
end
