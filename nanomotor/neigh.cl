/*
 * Verlet neighbour lists for dimer-solvent interaction.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

|local comm = box.comm
|local mpi = comm and require("mpi")

|local L_local, O_local, periodic_local = box.L_local, box.O_local, box.periodic_local

__kernel void
neigh_fill(__global uint *restrict d_bin_count)
{
  const uint gid = get_global_id(0);
  if (gid < ${nbins}) d_bin_count[gid] = 0;
}

|local work_size = 64

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
neigh_bin(__global const double3 *restrict d_rd,
          __global const char4 *restrict d_spd,
          __global const int *restrict d_idd,
          __global uint *restrict d_bin,
          __global uint *restrict d_bin_count,
          __global uint *restrict d_bin_err,
          __global uint *restrict d_nlist_count_ds,
          __global uint *restrict d_nlist_dd,
          __global uint *restrict d_nlist_count_dd,
          __global uint *restrict d_nlist_err_dd)
{
  const uint wid = get_group_id(0);
  const uint lid = get_local_id(0);
  const uint Nd = get_num_groups(0);
  const double3 rd = d_rd[wid];
  const char spd = d_spd[wid].s0;
  {
    const int3 nbin = (int3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
    const double3 nbin_frac = (double3)(${nbin[1]/L_local[1]}, ${nbin[2]/L_local[2]}, ${nbin[3]/L_local[3]});
    const double3 edges = (double3)(${L_local[1]/nbin[1]}, ${L_local[2]/nbin[2]}, ${L_local[3]/nbin[3]});
    const double r_cut = select((double)(${r_cut*sigma.C + skin}), (double)(${r_cut*sigma.N + skin}), (long)(spd));
    const double3 O_local = (double3)(${O_local[1]}, ${O_local[2]}, ${O_local[3]});
    const double3 r = minlocal(rd);
    const int3 p = convert_int3_rtn((r - r_cut)*nbin_frac);
    const int3 q = convert_int3_rtp((r + r_cut)*nbin_frac);
    const int3 periods = -(int3)(${periodic_local[1]}, ${periodic_local[2]}, ${periodic_local[3]});
    const int3 m = select(max(0, p), p + nbin, periods);
    const int3 n = select(min(nbin, q), q + nbin, periods);
    const double dist = r_cut + ${0.5*math.sqrt((L_local[1]/nbin[1])^2 + (L_local[2]/nbin[2])^2 + (L_local[3]/nbin[3])^2)};
    const double dist2 = dist*dist;
    for (int z = m.z; z < n.z; z++) {
      for (int y = m.y + lid/8; y < n.y; y += ${work_size/8}) {
        for (int x = m.x + lid%8; x < n.x; x += 8) {
          const uint3 cell = (uint3)(x%${nbin[1]}, y%${nbin[2]}, z%${nbin[3]});
          const double3 mid = O_local + (convert_double3(cell) + 0.5)*edges;
          const double3 d = mindist(mid - rd);
          if (dot(d, d) < dist2) {
            const uint bin = cell.x + cell.y*${nbin[1]} + cell.z*${nbin[1]*nbin[2]};
            const uint count = atomic_inc(&d_bin_count[bin]);
            if (count < ${bin_size}) d_bin[count + bin*${bin_size}] = wid;
            else atomic_max(d_bin_err, count+1);
          }
        }
      }
    }
  }
  if (lid == 0) d_nlist_count_ds[wid] = 0;
  |if nlist_size.dd then
  const int idd = d_idd[wid];
  {
    __local uint l_count[1];
    if (lid == 0) *l_count = 0;
    barrier(CLK_LOCAL_MEM_FENCE);
    |if comm then
    |  local ndom, periods, coords = mpi.cart_get(comm)
    const uint3 coords = (uint3)(${coords[1]}, ${coords[2]}, ${coords[3]});
    if (all(mincoords(rd) == coords))
    |end
    {
      const double2 r_cut21 = select((double2)(${(r_cut*sigma.CC + skin)^2}, ${(r_cut*sigma.CN + skin)^2}), (double2)(${(r_cut*sigma.CN + skin)^2}, ${(r_cut*sigma.NN + skin)^2}), (long2)(-spd));
      for (uint i = lid; i < Nd; i += ${work_size}) {
        if (d_idd[i]>>1 == idd>>1) continue;
        const char spd = d_spd[i].s0;
        const double r_cut2 = select(r_cut21.s0, r_cut21.s1, (long)(spd));
        const double3 d = mindist(rd - d_rd[i]);
        if (dot(d, d) < r_cut2) {
          const uint count = atomic_inc(l_count);
          if (count < ${nlist_size.dd}) d_nlist_dd[count + wid*${nlist_size.dd}] = i;
          else atomic_max(d_nlist_err_dd, count+1);
        }
      }
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (lid == 0) d_nlist_count_dd[wid] = *l_count;
  }
  |end
}

|for _, kernel in ipairs({"count", "update"}) do
__kernel void
neigh_${kernel}(__global const double3 *restrict d_rd,
                __global const double3 *restrict d_rs,
                __global const uint *restrict d_bin,
                __global const uint *restrict d_bin_count,
                __global const char4 *restrict d_spd,
                |if kernel == "count" then
                __global char4 *restrict d_sps,
                __global uint *restrict d_count_neigh,
                |else
                __global uint *restrict d_nlist_ds,
                __global uint *restrict d_nlist_count_ds,
                __global uint *restrict d_nlist_err_ds,
                |end
                const uint Ns)
{
  const uint gid = get_global_id(0);
  const uint3 nbin = (uint3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
  const double3 nbin_frac = (double3)(${nbin[1]/L_local[1]}, ${nbin[2]/L_local[2]}, ${nbin[3]/L_local[3]});
  |if kernel == "count" then
  const uint lid = get_local_id(0);
  __local uint l_count[1];
  if (lid == 0) *l_count = 0;
  barrier(CLK_LOCAL_MEM_FENCE);
  |end
  if (gid < Ns) {
    const double3 r = d_rs[gid];
    |if kernel == "count" then
    char4 sps = d_sps[gid];
    sps.s12 = 0;
    |end
    const uint3 cell = min(convert_uint3_rtz(minlocal(r)*nbin_frac), nbin-1);
    const uint bin = cell.x + cell.y*${nbin[1]} + cell.z*${nbin[1]*nbin[2]};
    const uint count = min(d_bin_count[bin], (uint)(${bin_size}));
    for (uint i = 0; i < count; ++i) {
      const uint j = d_bin[i + bin*${bin_size}];
      const char spd = d_spd[j].s0;
      const double r_cut2 = select((double)(${(r_cut*sigma.C + skin)^2}), (double)(${(r_cut*sigma.N + skin)^2}), (long)(spd));
      const double3 d = mindist(r - d_rd[j]);
      if (dot(d, d) < r_cut2) {
        |if kernel == "count" then
        sps.s2 = 1;
        break;
        |else
        const uint count = atomic_inc(&d_nlist_count_ds[j]);
        if (count < ${nlist_size.ds}) d_nlist_ds[count + j*${nlist_size.ds}] = gid;
        else atomic_max(d_nlist_err_ds, count+1);
        |end
      }
    }
    |if kernel == "count" then
    if (sps.s2 == 1) atomic_inc(l_count);
    d_sps[gid] = sps;
    |end
  }
  |if kernel == "count" then
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid == 0) atomic_add(d_count_neigh, *l_count);
  |end
}
|end

__kernel void
neigh_offset(__global const char4 *restrict d_sps,
             __global uint *restrict d_offset_neigh,
             __global uint *restrict d_count_neigh,
             const uint Ns,
             const uint Ns_neigh)
{
  const uint gid = get_global_id(0);
  if (gid >= Ns_neigh && gid < Ns) {
    const char4 sp = d_sps[gid];
    if (sp.s2 == 1) {
      const uint index = atomic_inc(d_count_neigh);
      d_offset_neigh[index] = gid;
    }
  }
}

__kernel void
neigh_permute(__global double3 *restrict d_rs,
              __global double3 *restrict d_vs,
              __global short3 *restrict d_ims,
              __global char4 *restrict d_sps,
              __global int *restrict d_ids,
              __global const uint *restrict d_offset_neigh,
              __global uint *restrict d_count_neigh,
              const uint Ns_neigh)
{
  const uint gid = get_global_id(0);
  if (gid < Ns_neigh) {
    const char4 sp = d_sps[gid];
    if (sp.s2 == 0) {
      const uint index = atomic_inc(d_count_neigh);
      const uint offset = d_offset_neigh[index];
      const double3 r = d_rs[gid];
      const double3 v = d_vs[gid];
      const short3 im = d_ims[gid];
      const int id = d_ids[gid];
      d_rs[gid] = d_rs[offset];
      d_vs[gid] = d_vs[offset];
      d_ims[gid] = d_ims[offset];
      d_sps[gid] = d_sps[offset];
      d_ids[gid] = d_ids[offset];
      d_rs[offset] = r;
      d_vs[offset] = v;
      d_ims[offset] = im;
      d_sps[offset] = sp;
      d_ids[offset] = id;
    }
  }
}
