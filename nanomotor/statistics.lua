------------------------------------------------------------------------------
-- Statistical moments.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local ffi = require("ffi")
local hdf5 = require("hdf5")

local _M = {}

local sqrt = math.sqrt

-- Timothy B. Terriberry, Computing Higher-Order Moments Online
-- http://people.xiph.org/~tterribe/notes/homs.html

_M.count = function(acc) return acc.n end
_M.mean = function(acc) return acc.m1 end
_M.sem = function(acc) return sqrt(acc.m2 / ((acc.n-1)*acc.n)) end
_M.var = function(acc) return acc.m2 / (acc.n-1) end
_M.skew = function(acc) return sqrt(acc.n) * acc.m3 * acc.m2^-1.5 end
_M.kurt = function(acc) return acc.n*acc.m4 / (acc.m2*acc.m2) end

_M.accum2 = ffi.metatype("struct { double n, m1, m2; }", {
  __call = function(acc, x)
    acc.n = acc.n + 1
    local d = x - acc.m1
    local d_n = d / acc.n
    acc.m2 = acc.m2 + d_n*d*(acc.n-1)
    acc.m1 = acc.m1 + d_n
  end,

  __index = {
    datatype = function(acc)
      local dtype = hdf5.create_type("compound", ffi.sizeof(acc))
      dtype:insert("n", ffi.offsetof(acc, "n"), hdf5.double)
      dtype:insert("m1", ffi.offsetof(acc, "m1"), hdf5.double)
      dtype:insert("m2", ffi.offsetof(acc, "m2"), hdf5.double)
      return dtype
    end,
  },
})

-- https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Higher-order_statistics

_M.accum4 = ffi.metatype("struct { double n, m1, m2, m3, m4; }", {
  __call = function(acc, x)
    acc.n = acc.n + 1
    local d = x - acc.m1
    local d_n = d / acc.n
    acc.m4 = acc.m4 + d_n*d*(acc.n-1)*(d_n*d_n)*(acc.n*acc.n - 3*acc.n + 3) + 6*(d_n*d_n)*acc.m2 - 4*d_n*acc.m3
    acc.m3 = acc.m3 + d_n*d*(acc.n-1)*d_n*(acc.n-2) - 3*d_n*acc.m2
    acc.m2 = acc.m2 + d_n*d*(acc.n-1)
    acc.m1 = acc.m1 + d_n
  end,

  __index = {
    datatype = function(acc)
      local dtype = hdf5.create_type("compound", ffi.sizeof(acc))
      dtype:insert("n", ffi.offsetof(acc, "n"), hdf5.double)
      dtype:insert("m1", ffi.offsetof(acc, "m1"), hdf5.double)
      dtype:insert("m2", ffi.offsetof(acc, "m2"), hdf5.double)
      dtype:insert("m3", ffi.offsetof(acc, "m3"), hdf5.double)
      dtype:insert("m4", ffi.offsetof(acc, "m4"), hdf5.double)
      return dtype
    end,
  },
})

return _M
