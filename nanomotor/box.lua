------------------------------------------------------------------------------
-- Boundary conditions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

-- Cache library functions.
local floor, max, min = math.floor, math.max, math.min

return function(args)
  local L, periodic = args.L, args.periodic
  local ndom = args.ndom

  local comm, coords
  if ndom then
    local mpi = require("mpi")
    local periods = {}
    for i = 1, 3 do
      periods[i] = periodic[i] and ndom[i] ~= 1
    end
    comm = mpi.cart_create(mpi.comm_world, ndom, periods, false)
    ndom, periods, coords = mpi.cart_get(comm)
  end

  local L_global = {}
  for i = 1, 3 do
    L_global[i] = L[i]
  end

  local L_local = {}
  for i = 1, 3 do
    L_local[i] = comm and L[i]/ndom[i] or L[i]
  end

  local O_local = {}
  for i = 1, 3 do
    O_local[i] = comm and (L[i]*coords[i])/ndom[i] or 0
  end

  local U_local = {}
  for i = 1, 3 do
    U_local[i] = comm and (L[i]*(coords[i]+1))/ndom[i] or L[i]
  end

  local periodic_global = {}
  for i = 1, 3 do
    periodic_global[i] = periodic[i]
  end

  local periodic_local = {}
  for i = 1, 3 do
    periodic_local[i] = periodic[i] and (not ndom or ndom[i] == 1)
  end

  local self = {
    L_global = L_global,
    L_local = L_local,
    O_local = O_local,
    U_local = U_local,
    periodic_global = periodic_global,
    periodic_local = periodic_local,
    comm = comm,
  }

  do
    local px, py, pz = periodic_global[1], periodic_global[2], periodic_global[3]
    local Lx, Ly, Lz = L_global[1], L_global[2], L_global[3]
    local Ix, Iy, Iz = 1/Lx, 1/Ly, 1/Lz

    --[[
    Returns minimum image vector.
    ]]
    function self.mindist(x, y, z)
      if x and px then x = x - floor(x*Ix + 0.5)*Lx end
      if y and py then y = y - floor(y*Iy + 0.5)*Ly end
      if z and pz then z = z - floor(z*Iz + 0.5)*Lz end
      return x, y, z
    end

    --[[
    Returns minimum-image position vector.
    ]]
    function self.minimage(x, y, z)
      local imx, imy, imz
      if x and px then imx = floor(x*Ix); x = x - imx*Lx end
      if y and py then imy = floor(y*Iy); y = y - imy*Ly end
      if z and pz then imz = floor(z*Iz); z = z - imz*Lz end
      return x, y, z, imx, imy, imz
    end
  end

  if comm then
    local nx, ny, nz = ndom[1], ndom[2], ndom[3]
    local Lx, Ly, Lz = L_local[1], L_local[2], L_local[3]
    local Ix, Iy, Iz = 1/Lx, 1/Ly, 1/Lz

    --[[
    Returns Cartesian coordinates of domain that position lies in.
    --]]
    function self.mincoords(x, y, z)
      x, y, z = self.minimage(x, y, z)
      if x then x = min(max(floor(x*Ix), 0), nx-1) end
      if y then y = min(max(floor(y*Iy), 0), ny-1) end
      if z then z = min(max(floor(z*Iz), 0), nz-1) end
      return x, y, z
    end
  end

  return self
end
