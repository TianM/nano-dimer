------------------------------------------------------------------------------
-- Observe observables.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local coroutine = require("coroutine")

--Cache library functions.
local assert, error, ipairs = assert, error, ipairs

--[[
Resumes execution of a suspended coroutine.

If the coroutine raises an error, this function appends a stack trace of
the coroutine to the error message and propagates the error to the caller.
--]]
local function resume(co, ...)
  local status, err = coroutine.resume(co, ...)
  if not status then return error(debug.traceback(co, err)) end
end

--[[
Runs the simulation for the given observables.

An observable provides a method `observe()` that samples the system state
at a monotonically increasing sequence of steps. Each observable contains
a separate simulation loop; but all observables observe the same system.
This is accomplished by running each observable in its own [coroutine].

A function that is executed as a coroutine may suspend its execution,
thereby returning control to the caller. The caller may continue
execution of the function where it left off by resuming the coroutine.
Coroutines provide a simple means of concurrently executing functions;
however, only one coroutine is actually running at any point in time.

At the beginning of the simulation, each observable is initialised by
creating and starting a coroutine. When an observable has initialised
itself, it suspends execution to allow initialisation of the next
observable. After all observables have been initialised, the system
is initialised by updating the integrator.

An observable provides an attribute `step` that indicates the next
integration step at which the observable wishes to sample the system
state. The current integration step is given by the attribute `step`
of the simulation domain.

During the simulation, each suspended observable is queried for its
step, and, if it matches the current integration step, execution of the
observable is resumed for sampling. After an observable has finished
sampling the current system state, it increases its step and suspends
execution to continue sampling; otherwise it terminates execution.

When all observables have been queried, the system is propagated to
the next integration step, which is given by the minimum of the steps
of all suspended observables.

The simulation ends when all observables have terminated.

[coroutine]: http://www.lua.org/pil/9.html
--]]
return function(integrate, observables)
  local dom = integrate.dom
  local observe = {}
  for i, observable in ipairs(observables) do
    observe[i] = coroutine.create(observable.observe)
    resume(observe[i])
  end
  integrate.update()
  while true do
    local step
    for i, observable in ipairs(observables) do
      if coroutine.status(observe[i]) == "dead" then
        goto continue
      end
      if observable.step == dom.step then
        resume(observe[i])
      end
      if observable.step <= dom.step then
        return error("non-monotonic time step for observable #"..i)
      end
      if coroutine.status(observe[i]) == "dead" then
        goto continue
      end
      if step == nil or observable.step < step then
        step = observable.step
      end
      ::continue::
    end
    if step == nil then break end
    integrate.integrate(step)
    assert(dom.step == step)
  end
end
