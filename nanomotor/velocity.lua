------------------------------------------------------------------------------
-- Initialise solvent velocities.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

local _M = {}

-- Cache library functions.
local sqrt = math.sqrt
-- Cache C types.
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local cl_ulong_1 = ffi.typeof("cl_ulong[1]")

--[[
Assigns solvent velocities from Maxwell-Boltzmann distribution.

The velocity components of each solvent particle are chosen from a
Gaussian distribution with mean equal to zero and variance equal to
the given temperature. To account for the finite number of random
samples, the velocities are subsequently shifted and rescaled, which
results in a center of mass velocity of exactly zero and the exact
temperature.
]]
function _M.gaussian(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local temp = args.temp

  local work_size = 64
  local num_groups = 512
  local glob_size = num_groups*work_size

  local program = compute.program(context, "nanomotor/velocity.cl", {
    dom = dom,
    box = box,
    work_size = work_size,
    num_groups = num_groups,
    temp = temp,
  })

  local d_en = context:create_buffer(num_groups*ffi.sizeof("cl_double4"))

  local gaussian do
    local kernel = program:create_kernel("velocity_gaussian")

    function gaussian()
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, d_en)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      kernel:set_arg(3, cl_ulong_1(dom.step))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local shiftscale do
    local kernel = program:create_kernel("velocity_shiftscale")

    function shiftscale()
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, d_en)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  return function()
    gaussian()
    shiftscale()
  end
end

return _M
