------------------------------------------------------------------------------
-- Read and write H5MD files.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local hdf5 = require("hdf5")
local ffi = require("ffi")
local bit = require("bit")

local _M = {}

-- Cache library functions.
local error, setmetatable = error, setmetatable
local floor, min = math.floor, math.min
local rshift = bit.rshift
-- Cache C types.
local char_n = ffi.typeof("char[?]")
local char_3_9 = ffi.typeof("char[3][9]")
local double_1 = ffi.typeof("double[1]")
local double_3 = ffi.typeof("double[3]")
local uint8_t_1 = ffi.typeof("uint8_t[1]")
local uint8_t_2 = ffi.typeof("uint8_t[2]")
local uint64_t_1 = ffi.typeof("uint64_t[1]")

--[[
Writes string attribute.
--]]
local function write_string(group, name, value)
  local space = hdf5.create_space("scalar")
  local dtype = hdf5.c_s1:copy()
  dtype:set_size(#value+1)
  local attr = group:create_attribute(name, dtype, space)
  attr:write(value, dtype)
  space:close()
  dtype:close()
  attr:close()
end

--[[
Reads string attribute.
--]]
local function read_string(group, name)
  local attr = group:open_attribute(name)
  local dtype = attr:get_type()
  local size = dtype:get_size()
  dtype:close()
  local buf = char_n(size)
  local dtype = hdf5.c_s1:copy()
  dtype:set_size(size)
  attr:read(buf, dtype)
  dtype:close()
  attr:close()
  return ffi.string(buf)
end

--[[
Writes [H5MD metadata] to file or group.

[H5MD metadata]: http://nongnu.org/h5md/h5md.html#h5md-metadata
]]
function _M.write_meta(group, h5md)
  group = group:create_group("h5md")
  do
    local subgroup = group:create_group("author")
    write_string(subgroup, "name", h5md.author.name)
    write_string(subgroup, "email", h5md.author.email)
    subgroup:close()
  end
  do
    local subgroup = group:create_group("creator")
    write_string(subgroup, "name", h5md.creator.name)
    write_string(subgroup, "version", h5md.creator.version)
    subgroup:close()
  end
  do
    local space = hdf5.create_simple_space({2})
    local attr = group:create_attribute("version", hdf5.uint8, space)
    attr:write(uint8_t_2(1, 1), hdf5.uint8)
    space:close()
    attr:close()
  end
  group:close()
end

--[[
Reads [H5MD metadata] from file or group.

[H5MD metadata]: http://nongnu.org/h5md/h5md.html#h5md-metadata
]]
function _M.read_meta(group)
  group = group:open_group("h5md")
  local h5md = {}
  do
    local subgroup = group:open_group("author")
    local name = read_string(subgroup, "name")
    local email = read_string(subgroup, "email")
    subgroup:close()
    h5md.author = {name = name, email = email}
  end
  do
    local subgroup = group:open_group("creator")
    local name = read_string(subgroup, "name")
    local version = read_string(subgroup, "version")
    subgroup:close()
    h5md.creator = {name = name, version = version}
  end
  do
    local attr = group:open_attribute("version")
    local buf = uint8_t_2()
    attr:read(buf, hdf5.uint8)
    attr:close()
    h5md.version = {buf[0], buf[1]}
  end
  group:close()
  return h5md
end

--[[
Writes [simulation box] to particle group.

[simulation box]: http://h5md.nongnu.org/h5md.html#simulation-box
]]
function _M.write_box(group, box)
  group = group:create_group("box")
  local space = hdf5.create_simple_space({3})
  do
    local dcpl = hdf5.create_plist("dataset_create")
    dcpl:set_layout("compact")
    local dset = group:create_dataset("edges", hdf5.double, space, nil, dcpl)
    dcpl:close()
    dset:write(double_3(box.L), hdf5.double)
    dset:close()
  end
  do
    local dtype = hdf5.c_s1:copy()
    dtype:set_size(9)
    local attr = group:create_attribute("boundary", dtype, space)
    local buf = char_3_9()
    for i = 1, 3 do
      buf[i-1] = box.periodic[i] and "periodic" or "none"
    end
    attr:write(char_3_9(buf), dtype)
    dtype:close()
    attr:close()
  end
  do
    local space = hdf5.create_space("scalar")
    local attr = group:create_attribute("dimension", hdf5.uint8, space)
    attr:write(uint8_t_1(3), hdf5.uint8)
    space:close()
    attr:close()
  end
  space:close()
  group:close()
end

--[[
Reads [simulation box] from file or group.

[simulation box]: http://h5md.nongnu.org/h5md.html#simulation-box
]]
function _M.read_box(group)
  group = group:open_group("box")
  local box = {}
  do
    local dset = group:open_dataset("edges")
    local space = hdf5.create_simple_space({3})
    local buf = double_3()
    dset:read(buf, hdf5.double, space)
    space:close()
    dset:close()
    box.L = {buf[0], buf[1], buf[2]}
  end
  do
    local attr = group:open_attribute("boundary")
    local dtype = hdf5.c_s1:copy()
    dtype:set_size(9)
    local buf = char_3_9()
    attr:read(buf, dtype)
    dtype:close()
    attr:close()
    local t = {}
    for i = 1, 3 do
      t[i] = ffi.string(buf[i-1]) == "periodic"
    end
    box.periodic = t
  end
  do
    local attr = group:open_attribute("dimension")
    local buf = uint8_t_1()
    attr:read(buf, hdf5.uint8)
    attr:close()
    box.dimension = buf[0]
  end
  group:close()
  return box
end

local observable = {}

--[[
Creates [time-dependent observable] of given datatype and dimensions.

The "value" dataset is created with a variable-size first dimension and
fixed-size higher dimensions. The chunk size is chosen between 8k and
1 MiB, which yields optimal read/write performance with the default
dataset chunk cache size of 1 MiB.

[time-dependent observable]: http://nongnu.org/h5md/h5md.html#time-dependent-data
]]
function _M.create_observable(group, name, step, time, dtype, shape)
  group = group:create_group(name)
  local self = {}
  local space = hdf5.create_space("scalar")
  local dcpl = hdf5.create_plist("dataset_create")
  dcpl:set_layout("compact")
  self.step = group:create_dataset("step", hdf5.uint64, space, nil, dcpl)
  self.time = group:create_dataset("time", hdf5.double, space, nil, dcpl)
  self.step:write(uint64_t_1(step.interval), hdf5.uint64)
  self.time:write(double_1(time.interval), hdf5.double)
  local attr_step = self.step:create_attribute("offset", hdf5.uint64, space)
  local attr_time = self.time:create_attribute("offset", hdf5.double, space)
  attr_step:write(uint64_t_1(step.offset), hdf5.uint64)
  attr_time:write(double_1(time.offset), hdf5.double)
  attr_step:close()
  attr_time:close()
  local rank
  if shape == nil then rank = 1 else rank = #shape+1 end
  local dims, maxdims = {0}, {nil}
  for i = 2, rank do dims[i], maxdims[i] = shape[i-1], shape[i-1] end
  local filespace = hdf5.create_simple_space(dims, maxdims)
  local size = dtype:get_size()
  maxdims[1] = 1
  while size*maxdims[1] < 8092 do maxdims[1] = maxdims[1]*2 end
  for i = rank, 1, -1 do
    dims[i] = 1
    while dims[i] < maxdims[i] and size*dims[i] <= 524288 do
      dims[i] = min(maxdims[i], dims[i]*2)
    end
    size = size*dims[i]
  end
  dcpl:set_chunk(dims)
  self.value = group:create_dataset("value", dtype, filespace, nil, dcpl)
  filespace:close()
  space:close()
  dcpl:close()
  group:close()
  return setmetatable(self, {__index = observable})
end

--[[
Opens time-dependent observable.
]]
function _M.open_observable(group, name)
  group = group:open_group(name)
  local self = {}
  self.value = group:open_dataset("value")
  self.step = group:open_dataset("step")
  self.time = group:open_dataset("time")
  group:close()
  return setmetatable(self, {__index = observable})
end

--[[
Closes datasets of time-dependent observable.
]]
function observable:close()
  self.value:close()
  self.step:close()
  self.time:close()
end

--[[
Appends given number of values and returns file dataspace.
]]
function observable:append(count)
  local filespace = self.value:get_space()
  local dims = filespace:get_simple_extent_dims()
  local offset = {dims[1]}
  for i = 2, #dims do offset[i] = 0 end
  dims[1] = dims[1] + count
  filespace:set_extent_simple(dims)
  filespace:offset_simple(offset)
  self.value:set_extent(dims)
  return filespace
end

--[[
Seeks to given step and returns file dataspace and time.
]]
function observable:seek(step)
  local step_interval = uint64_t_1()
  local time_interval = double_1()
  local space = hdf5.create_space("scalar")
  self.step:read(step_interval, hdf5.uint64, space)
  self.time:read(time_interval, hdf5.double, space)
  space:close()
  local step_offset = uint64_t_1()
  local time_offset = double_1()
  if self.step:exists_attribute("offset") then
    local attr_step = self.step:open_attribute("offset")
    attr_step:read(step_offset, hdf5.uint64)
    attr_step:close()
  end
  if self.time:exists_attribute("offset") then
    local attr_time = self.time:open_attribute("offset")
    attr_time:read(time_offset, hdf5.double)
    attr_time:close()
  end
  local offset = {tonumber(step - step_offset[0]) / tonumber(step_interval[0])}
  if offset[1] ~= floor(offset[1]) then
    return error("time step out of range")
  end
  local filespace = self.value:get_space()
  local rank = filespace:get_simple_extent_ndims()
  for i = 2, rank do offset[i] = 0 end
  filespace:offset_simple(offset)
  local time = time_offset[0] + offset[1]*time_interval[0]
  return filespace, time
end

return _M
