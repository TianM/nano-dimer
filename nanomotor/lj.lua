------------------------------------------------------------------------------
-- Lennard-Jones potential for dimer-solvent and dimer-dimer interactions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

-- Cache library functions.
local sqrt = math.sqrt
-- Cache C types.
local cl_double3 = ffi.typeof("cl_double3")
local cl_uint_1 = ffi.typeof("cl_uint[1]")

return function(dom, neigh, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local external_force = args.external_force
  local gravc = args.gravc

  local program = compute.program(context, "nanomotor/lj.cl", {
    dom = dom,
    box = box,
    nlist_size = neigh.nlist_size,
    epsilon = args.epsilon,
    sigma = args.sigma,
    r_cut = args.r_cut,
    reaction = args.reaction,
    wall = args.wall,
  })

  local self = {}

  local mem_queue = context:create_command_queue(device)
  local en = ffi.new("cl_double2[?]", dom.Nd_max)
  local d_en = context:create_buffer("use_host_ptr", ffi.sizeof(en), en)
  local err = ffi.new("cl_uint[1]")
  local d_err = context:create_buffer("use_host_ptr", ffi.sizeof(err), err)

  do
    local kernel = program:create_kernel("lj_update")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]

    function self.update()
      local glob_size = neigh.Nd*work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_rs)
      kernel:set_arg(2, dom.d_fd)
      kernel:set_arg(3, dom.d_fs)
      kernel:set_arg(4, dom.d_spd)
      kernel:set_arg(5, dom.d_sps)
      kernel:set_arg(6, neigh.d_nlist_count_ds)
      kernel:set_arg(7, neigh.d_nlist_ds)
      kernel:set_arg(8, neigh.d_nlist_count_dd)
      kernel:set_arg(9, neigh.d_nlist_dd)
      kernel:set_arg(10, d_en)
      kernel:set_arg(11, d_err)
      kernel:set_arg(12, cl_uint_1(dom.Nd))
      local event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      mem_queue:enqueue_wait_for_events({event})
    end
  end

  local send_positions, send_forces
  if comm then
    local rank_left, rank_right = mpi.cart_shift(comm, 0, 1)
    local rank_back, rank_front = mpi.cart_shift(comm, 1, 1)
    local rank_down, rank_up = mpi.cart_shift(comm, 2, 1)

    function send_positions()
      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_right, rank_right, 0, dom.rd, 1, neigh.recvtype_rd_left, rank_left, 0, comm)
      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_left, rank_left, 0, dom.rd, 1, neigh.recvtype_rd_right, rank_right, 0, comm)

      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_front, rank_front, 0, dom.rd, 1, neigh.recvtype_rd_back, rank_back, 0, comm)
      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_back, rank_back, 0, dom.rd, 1, neigh.recvtype_rd_front, rank_front, 0, comm)

      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_up, rank_up, 0, dom.rd, 1, neigh.recvtype_rd_down, rank_down, 0, comm)
      mpi.sendrecv(dom.rd, 1, neigh.sendtype_rd_down, rank_down, 0, dom.rd, 1, neigh.recvtype_rd_up, rank_up, 0, comm)
    end

    local fd = ffi.new("cl_double3[?]", dom.Nd_max)

    function send_forces()
      ffi.fill(fd, ffi.sizeof(fd))
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_down, rank_down, 0, fd, 1, neigh.sendtype_rd_up, rank_up, 0, comm)
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_up, rank_up, 0, fd, 1, neigh.sendtype_rd_down, rank_down, 0, comm)
      for i = 0, neigh.Nd-1 do
        dom.fd[i].x = dom.fd[i].x + fd[i].x
        dom.fd[i].y = dom.fd[i].y + fd[i].y
        dom.fd[i].z = dom.fd[i].z + fd[i].z
      end

      ffi.fill(fd, ffi.sizeof(fd))
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_back, rank_back, 0, fd, 1, neigh.sendtype_rd_front, rank_front, 0, comm)
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_front, rank_front, 0, fd, 1, neigh.sendtype_rd_back, rank_back, 0, comm)
      for i = 0, neigh.Nd-1 do
        dom.fd[i].x = dom.fd[i].x + fd[i].x
        dom.fd[i].y = dom.fd[i].y + fd[i].y
        dom.fd[i].z = dom.fd[i].z + fd[i].z
      end

      ffi.fill(fd, ffi.sizeof(fd))
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_left, rank_left, 0, fd, 1, neigh.sendtype_rd_right, rank_right, 0, comm)
      mpi.sendrecv(dom.fd, 1, neigh.recvtype_rd_right, rank_right, 0, fd, 1, neigh.sendtype_rd_left, rank_left, 0, comm)
      for i = 0, neigh.Nd-1 do
        dom.fd[i].x = dom.fd[i].x + fd[i].x
        dom.fd[i].y = dom.fd[i].y + fd[i].y
        dom.fd[i].z = dom.fd[i].z + fd[i].z
      end
    end
  end

  --[[
  Adds a constant force of the given magnitude along the bond of each dimer.

  Since the force vector is parallel to the bond vector, the torque is zero
  regardless at which point along the bond the force is applied. Therefore
  the constant force may be added to either dimer sphere.
  --]]
  local function add_external_force()
    local rd, fd = dom.rd, dom.fd
    for i = 0, dom.Nd-1, 2 do
      local zx = rd[i].x - rd[i+1].x
      local zy = rd[i].y - rd[i+1].y
      local zz = rd[i].z - rd[i+1].z
      zx, zy, zz = box.mindist(zx, zy, zz)
      local zn = 1 / sqrt(zx*zx + zy*zy + zz*zz)
      zx, zy, zz = zx*zn, zy*zn, zz*zn
      fd[i].x = fd[i].x + external_force*zx
      fd[i].y = fd[i].y + external_force*zy
      fd[i].z = fd[i].z + external_force*zz
    end
  end

  local function add_gravitational_force()
    local fd = dom.fd
    for i = 0, dom.Nd-1, 2 do
      fd[i].z = fd[i].z - gravc*dom.mass.C
      fd[i+1].z = fd[i+1].z - gravc*dom.mass.N
    end
  end


  function self.post_integrate()
    if comm then send_positions() end
    local event = mem_queue:enqueue_write_buffer(dom.d_rd, true, 0, neigh.Nd*ffi.sizeof(cl_double3), dom.rd)
    queue:enqueue_wait_for_events({event})
  end

  function self.post_update()
    mem_queue:enqueue_read_buffer(dom.d_fd, false, 0, neigh.Nd*ffi.sizeof(cl_double3), dom.fd)
    mem_queue:enqueue_map_buffer(d_err, true, "read")
    if err[0] ~= 0 then error("solvent particle interacts with multiple dimer spheres") end
    queue:enqueue_unmap_mem_object(d_err, err)
    if comm then send_forces() end
    if external_force then add_external_force() end
    if gravc then add_gravitational_force() end
  end

  function self.compute_en()
    local en_pot, virial = 0, 0
    queue:enqueue_map_buffer(d_en, true, "read")
    for i = 0, neigh.Nd-1 do
      en_pot, virial = en_pot+en[i].s0, virial+en[i].s1
    end
    queue:enqueue_unmap_mem_object(d_en, en)
    return en_pot, virial
  end

  return self
end
