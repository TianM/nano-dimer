/*
 * Velocity-Verlet algorithm for solvent particles.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}
${include "nanomotor/random.cl"}

|local bit = require("bit")
|local ffi = require("ffi")

|local comm = box.comm
|local mpi = comm and require("mpi")
|local rank = comm and comm:rank() or 0
|local ndom, periods, coords
|if comm then ndom, periods, coords = mpi.cart_get(comm) end

|local seed = ffi.new("uint32_t[2]")
|if rank == 0 then
|  for i = 0, 1 do
|    seed[i] = math.random(0, 0xffffffff)
|  end
|end
|if comm then mpi.bcast(seed, 2, mpi.uint32, 0, comm) end
|seed = "0x"..bit.tohex(seed[0])..bit.tohex(seed[1])

|local L_global, periodic_global = box.L_global, box.periodic_global

|for i = 1, 3 do
|  if not periodic_global[i] then
/**
Returns time elapsed since bounce-back reflection.

This function returns the time elapsed since a particle traversed a
non-periodic boundary to the outside of the box; otherwise, if the
particle is inside the box, the function returns zero.

For a box with multiple non-periodic dimensions, the function returns
the maximum of the elapsed time for each dimension, which corresponds
to the non-periodic boundary that the particle was closest to before
having been propagated from the inside to the outside of the box.

**/
double bounceback_time(double3 r, double3 v)
{
  double t = 0;
  |for i = 1, 3 do
  |  if not periodic_global[i] then
  if (r.s${i-1} < 0) {
    t = max(t, r.s${i-1}/v.s${i-1});
  }
  else if (r.s${i-1} > ${L_global[i]}) {
    t = max(t, (r.s${i-1} - ${L_global[i]})/v.s${i-1});
  }
  |  end
  |end
  return t;
}

/**
Solvent bounce-back boundary condition.

This function applies the bounce-back boundary condition to a solvent
particle [@Malevanets1999; @Whitmer2010], which at the macroscopic scale
is equivalent to the stick boundary condition.

The function is invoked after the particle position has been propagated
by a given time-step. If the particle has traversed a non-periodic
boundary, its velocity vector is reversed and its position is corrected
according to the time elapsed since traversing the boundary.

For the case where the particle is close to an edge or a corner in a box
with multiple non-periodic dimensions, the particle is reflected at most
twice; further reflections due to bouncing back and forth between two
boundaries, which has no effect on the particle position, are discarded.
**/
void bounceback(double3 *r, double3 *v)
{
  double t1 = bounceback_time(*r, *v);
  if (t1 == 0) return; /* no reflection */
  (*v) = -(*v);
  (*r) += (*v)*(2*t1);
  const double t2 = bounceback_time(*r, *v);
  if (t2 == 0) return; /* one reflection */
  (*r) -= (*v)*t1;
  const double tau = 2*(t1-t2);
  t1 -= round(t1/tau)*tau;
  if (t1 < 0) (*v) = -(*v);
  (*r) += (*v)*t1;
}
|    break
|  end
|end

|for kernel, flag in pairs({stream = 0, integrate = 1}) do
__kernel void
verlet_${kernel}(__global double3 *restrict d_rs,
                 __global double3 *restrict d_vs,
                 __global short3 *restrict d_ims,
                 __global char4 *restrict d_sps,
                 |if flag == 1 then
                 __global const double3 *restrict d_fs,
                 __global const double3 *restrict d_rs0,
                 __global uint *restrict d_count,
                 |end
                 |if reaction and reaction.radius and flag == 0 then
                 __constant const double3 *restrict d_rd,
                 |end
                 |if reaction and reaction.rate then
                 const ulong seed,
                 |end
                 const uint Ns,
                 const uint offset,
                 const double delta_t)
{
  const uint gid = get_global_id(0);
  if (gid >= offset && gid < Ns) {
    char4 sp = d_sps[gid];
    double3 r = d_rs[gid];
    double3 v = d_vs[gid];
    const double3 v0 = v;
    |if flag == 1 then
    if (sp.s1 == 1) v += d_fs[gid]*delta_t;
    r += v*${timestep};
    |else
    r += v*delta_t;
    |end
    |for i = 1, 3 do
    |  if not periodic_global[i] then
    bounceback(&r, &v);
    |    break
    |  end
    |end
    const short3 im = minimage(&r);
    |if flag == 1 then
    const double3 d = mindist(r - d_rs0[gid]);
    if (dot(d, d) > ${(skin/2)^2}) atomic_inc(d_count);
    |end
    |if reaction and ((reaction.radius and flag == 0) or reaction.rate) then
    |  if reaction.radius and flag == 0 then
    const double3 dd = mindist(r - d_rd[0]);
    |  end
    |  if reaction.rate then
    ulong4 c = (ulong4)(0, gid + ${rank*dom.Ns_max}UL, seed, ${seed}UL);
    const double2 u = random_uniform(&c);
    |  end
    |  if reaction.wall then
    bool in_boundary = true;
    |  end
    |  if reaction.wall and flag == 1 then
    |    for i = 1, 3 do
    |      if (reaction.wall.reactive[i] and (not periodic_global[i])) then
    |        local dz = reaction.wall.dz
    {
      if ((r[${i-1}] < ${L_global[i] - dz}) && (r[${i-1}] > ${dz})) in_boundary = false;
    }
    |      end
    |    end
    |  end
    if (sp.s0 == 1 && sp.s1 == 0)
    |  if reaction.radius and flag == 0 then
    if (dot(dd, dd) > ${reaction.radius^2})
    |  end
    |  if reaction.wall then
    |    if flag == 1 then
    if ((u.s0 < ${reaction.rate*timestep}) && in_boundary)
    |    else
    if ((u.s0 < ${reaction.rate}*delta_t) && in_boundary)
    |    end
    |  end
    |  if (reaction.rate and (not reaction.wall)) then
    |    if flag == 1 then
    if (u.s0 < ${reaction.rate*timestep})
    |    else
    if (u.s0 < ${reaction.rate}*delta_t)
    |    end
    |  end
    sp.s0 = 0;
    |end
    |if flag == 1 then
    sp.s1 = 0;
    |end
    |if comm then
    const uint3 coords = (uint3)(${coords[1]}, ${coords[2]}, ${coords[3]});
    sp.s3 = any(mincoords(r) != coords);
    |end
    d_sps[gid] = sp;
    d_rs[gid] = r;
    if (any(v != v0)) d_vs[gid] = v;
    if (d_ims && any(im != (short3)(0))) d_ims[gid] += im;
  }
}
|end

__kernel void
verlet_finalize(__global double3 *restrict d_vs,
                __global double3 *restrict d_fs,
                __global const char4 *restrict d_sps,
                const uint Ns)
{
  const uint gid = get_global_id(0);
  if (gid < Ns) {
    const char4 sp = d_sps[gid];
    if (sp.s1 == 1) d_vs[gid] += d_fs[gid]*${timestep/2};
    else d_fs[gid] = 0;
  }
}

|local work_size = 128

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
verlet_sum(__global const double3 *restrict d_vs,
           __global double4 *restrict d_en,
           const uint Ns)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  const uint gsize = get_global_size(0);
  __local double4 l_en[${work_size}];
  double en = 0;
  double3 vcm = 0;
  for (uint i = gid; i < Ns; i += gsize) {
    const double3 v = d_vs[i];
    en += dot(v, v);
    vcm += v;
  }
  l_en[lid] = (double4)(en, vcm);
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_en[lid] += l_en[lid+${i}];
  |end
  if (lid == 0) d_en[wid] = l_en[0];
}

|if comm then
__kernel void
verlet_count(__global const char4 *restrict d_sps,
             __global uint *restrict d_count_send,
             const uint Ns)
{
  const uint gid = get_global_id(0);
  if (gid < Ns) {
    const char4 sp = d_sps[gid];
    if (sp.s3 == 1) atomic_inc(d_count_send);
  }
}

__kernel void
verlet_offset(__global const char4 *restrict d_sps,
              __global uint *restrict d_offset_send,
              __global uint *restrict d_count_send,
              const uint Ns_local)
{
  const uint gid = get_global_id(0);
  if (gid < Ns_local) {
    const char4 sp = d_sps[gid];
    if (sp.s3 == 1) {
      const uint index = atomic_inc(d_count_send);
      d_offset_send[index] = gid;
    }
  }
}

__kernel void
verlet_permute(__global double3 *restrict d_rs,
               __global double3 *restrict d_vs,
               __global double3 *restrict d_fs,
               __global short3 *restrict d_ims,
               __global char4 *restrict d_sps,
               __global int *restrict d_ids,
               __global const uint *restrict d_offset_send,
               __global uint *restrict d_count_send,
               const uint Ns,
               const uint Ns_local)
{
  const uint gid = get_global_id(0);
  if (gid >= Ns_local && gid < Ns) {
    const char4 sp = d_sps[gid];
    if (sp.s3 == 0) {
      const uint index = atomic_inc(d_count_send);
      const uint offset = d_offset_send[index];
      const double3 r = d_rs[gid];
      const double3 v = d_vs[gid];
      const double3 f = d_fs[gid];
      const short3 im = d_ims[gid];
      const int id = d_ids[gid];
      d_rs[gid] = d_rs[offset];
      d_vs[gid] = d_vs[offset];
      d_fs[gid] = d_fs[offset];
      d_ims[gid] = d_ims[offset];
      d_sps[gid] = d_sps[offset];
      d_ids[gid] = d_ids[offset];
      d_rs[offset] = r;
      d_vs[offset] = v;
      d_fs[offset] = f;
      d_ims[offset] = im;
      d_sps[offset] = sp;
      d_ids[offset] = id;
    }
  }
}
|end
