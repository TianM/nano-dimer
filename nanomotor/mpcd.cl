/*
 * Multi-particle collision dynamics.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}
${include "nanomotor/random.cl"}

|local bit = require("bit")
|local ffi = require("ffi")

|local comm = box.comm
|local mpi = comm and require("mpi")
|local rank = comm and comm:rank() or 0
|local ndom, periods, coords
|if comm then ndom, periods, coords = mpi.cart_get(comm) end

|local seed = ffi.new("uint32_t[2]")
|if rank == 0 then
|  for i = 0, 1 do
|    seed[i] = math.random(0, 0xffffffff)
|  end
|end
|if comm then mpi.bcast(seed, 2, mpi.uint32, 0, comm) end
|seed = "0x"..bit.tohex(seed[0])..bit.tohex(seed[1])

|local O_local, periodic_local = box.O_local, box.periodic_local
|local L_global = box.L_global

|if nbins.outer ~= 0 then

uint convert_index(uint3 cell)
{
  if (cell.x <= 1) {
    return ${offset.left} + cell.x + cell.y*2 + cell.z*${2*nbin[2]};
  }
  else if (cell.x >= ${nbin[1]-2}) {
    return ${offset.right} + (cell.x-${nbin[1]-2}) + cell.y*2 + cell.z*${2*nbin[2]};
  }
  else if (cell.y <= 1) {
    return ${offset.back} + (cell.x-2) + cell.y*${nbin[1]-4} + cell.z*${2*(nbin[1]-4)};
  }
  else if (cell.y >= ${nbin[2]-2}) {
    return ${offset.front} + (cell.x-2) + (cell.y-${nbin[2]-2})*${nbin[1]-4} + cell.z*${2*(nbin[1]-4)};
  }
  else if (cell.z <= 1) {
    return ${offset.down} + (cell.x-2) + (cell.y-2)*${nbin[1]-4} + cell.z*${(nbin[1]-4)*(nbin[2]-4)};
  }
  else if (cell.z >= ${nbin[3]-2}) {
    return ${offset.up} + (cell.x-2) + (cell.y-2)*${nbin[1]-4} + (cell.z-${nbin[3]-2})*${(nbin[1]-4)*(nbin[2]-4)};
  }
  else {
    return ${offset.inner} + (cell.x-2) + (cell.y-2)*${nbin[1]-4} + (cell.z-2)*${(nbin[1]-4)*(nbin[2]-4)};
  }
}

uint3 convert_cell(uint index)
{
  uint3 cell;
  if (index == clamp(index, ${offset.left}U, ${offset.left + nbins.left - 1}U)) {
    index -= ${offset.left};
    cell.x = index%2;
    cell.y = index/2%${nbin[2]};
    cell.z = index/${2*nbin[2]};
  }
  else if (index == clamp(index, ${offset.right}U, ${offset.right + nbins.right - 1}U)) {
    index -= ${offset.right};
    cell.x = index%2 + ${nbin[1]-2};
    cell.y = index/2%${nbin[2]};
    cell.z = index/${2*nbin[2]};
  }
  else if (index == clamp(index, ${offset.back}U, ${offset.back + nbins.back - 1}U)) {
    index -= ${offset.back};
    cell.x = index%${nbin[1]-4} + 2;
    cell.y = index/${nbin[1]-4}%2;
    cell.z = index/${2*(nbin[1]-4)};
  }
  else if (index == clamp(index, ${offset.front}U, ${offset.front + nbins.front - 1}U)) {
    index -= ${offset.front};
    cell.x = index%${nbin[1]-4} + 2;
    cell.y = index/${nbin[1]-4}%2 + ${nbin[2]-2};
    cell.z = index/${2*(nbin[1]-4)};
  }
  else if (index == clamp(index, ${offset.down}U, ${offset.down + nbins.down - 1}U)) {
    index -= ${offset.down};
    cell.x = index%${nbin[1]-4} + 2;
    cell.y = index/${nbin[1]-4}%${nbin[2]-4} + 2;
    cell.z = index/${(nbin[1]-4)*(nbin[2]-4)};
  }
  else if (index == clamp(index, ${offset.up}U, ${offset.up + nbins.up - 1}U)) {
    index -= ${offset.up};
    cell.x = index%${nbin[1]-4} + 2;
    cell.y = index/${nbin[1]-4}%${nbin[2]-4} + 2;
    cell.z = index/${(nbin[1]-4)*(nbin[2]-4)} + ${nbin[3]-2};
  }
  else {
    index -= ${offset.inner};
    cell.x = index%${nbin[1]-4} + 2;
    cell.y = index/${nbin[1]-4}%${nbin[2]-4} + 2;
    cell.z = index/${(nbin[1]-4)*(nbin[2]-4)} + 2;
  }
  return cell;
}

|else

uint convert_index(uint3 cell)
{
  return cell.x + cell.y*${nbin[1]} + cell.z*${nbin[1]*nbin[2]};
}

uint3 convert_cell(uint index)
{
  uint3 cell;
  cell.x = index%${nbin[1]};
  cell.y = index/${nbin[1]}%${nbin[2]};
  cell.z = index/${nbin[1]*nbin[2]};
  return cell;
}

|end

__kernel void
mpcd_fill(__global uint *restrict d_bin_count)
{
  const uint gid = get_global_id(0);
  if (gid < ${nbins.all}) d_bin_count[gid] = 0;
}

|-- Minimum global cell coordinates for subdomain.
|local mincell = {}
|for i = 1, 3 do
|  mincell[i] = math.floor(O_local[i])
|end

|-- Maximum global cell coordinates for subdomain.
|local maxcell = {}
|for i = 1, 3 do
|  maxcell[i] = math.min(L_global[i], mincell[i] + nbin[i] - 1)
|end

__kernel void
mpcd_bin(__global const double3 *restrict d_rs,
         __global uint *restrict d_bin,
         __global uint *restrict d_bin_count,
         __global uint *restrict d_err,
         const uint Ns,
         const ulong seed)
{
  const uint gid = get_global_id(0);
  const uint3 mincell = (uint3)(${mincell[1]}, ${mincell[2]}, ${mincell[3]});
  const uint3 maxcell = (uint3)(${maxcell[1]}, ${maxcell[2]}, ${maxcell[3]});
  if (gid < Ns) {
    double3 r = d_rs[gid];
    ulong4 c = (ulong4)(0, -1UL, seed, ${seed}UL);
    const double2 v1 = random_uniform(&c);
    const double2 v2 = random_uniform(&c);
    r += (double3)(v1, v2.s0);
    uint3 cell = clamp(convert_uint3_rtz(r), mincell, maxcell);
    cell -= mincell;
    |for i = 1, 3 do
    |  if periodic_local[i] then
    cell.s${i-1} %= ${L_global[i]};
    |  end
    |end
    const uint index = convert_index(cell);
    const uint count = atomic_inc(&d_bin_count[index]);
    if (count < ${bin_size}) d_bin[index + count*${nbins.all}] = gid;
    else atomic_max(d_err, count+1);
  }
}

double3
sum_cell(__global double3 *restrict d_vs,
         __global const uint *restrict d_bin,
         const uint gid,
         const uint count,
         const ulong seed)
{
  |if temp then
  ulong4 c = (ulong4)(0, gid + ${rank*nbins.all}, seed, ${seed}UL);
  |end
  double3 v_cm = 0;
  for (uint i = 0; i < count; ++i) {
    const uint j = d_bin[gid + i*${nbins.all}];
    v_cm += d_vs[j];
    |if temp then
    double2 v1 = random_normal(&c);
    double2 v2 = random_normal(&c);
    double3 v = (double3)(v1, v2.s0);
    v *= ${math.sqrt(temp)};
    v_cm -= v;
    d_vs[j] = v;
    |end
  }
  return v_cm;
}

void
collide_cell(__global double3 *restrict d_vs,
             __global const uint *restrict d_bin,
             const uint gid,
             const uint count,
             const ulong seed,
             const double3 v_cm)
{
  |if not temp then
  const uint3 mincell = (uint3)(${mincell[1]}, ${mincell[2]}, ${mincell[3]});
  uint3 cell = convert_cell(gid);
  cell += mincell;
  |  for i = 1, 3 do
  |    if comm and periods[i] then
  cell.s${i-1} %= ${L_global[i]};
  |    end
  |  end
  const uint index = cell.x + cell.y*${L_global[1]+1} + cell.z*${(L_global[1]+1)*(L_global[2]+1)};
  ulong4 c = (ulong4)(0, index, seed, ${seed}UL);
  const double3 u = random_sphere(&c);
  const double3 Rx = (double3)(u.x*u.x, u.x*u.y-u.z, u.x*u.z+u.y);
  const double3 Ry = (double3)(u.y*u.x+u.z, u.y*u.y, u.y*u.z-u.x);
  const double3 Rz = (double3)(u.z*u.x-u.y, u.z*u.y+u.x, u.z*u.z);
  |end
  for (uint i = 0; i < count; ++i) {
    const uint j = d_bin[gid + i*${nbins.all}];
    |if temp then
    d_vs[j] += v_cm;
    |else
    const double3 dv = d_vs[j] - v_cm;
    d_vs[j] = v_cm + (double3)(dot(Rx, dv), dot(Ry, dv), dot(Rz, dv));
    |end
  }
}

|if nbins.inner ~= 0 then
__kernel void
mpcd_collide_inner(__global double3 *restrict d_vs,
                   __global const uint *restrict d_bin,
                   __global const uint *restrict d_bin_count,
                   const ulong seed)
{
  const uint gid = get_global_id(0);
  if (gid == clamp(gid, ${offset.inner}U, ${offset.inner + nbins.inner - 1}U)) {
    const uint count = min(d_bin_count[gid], (uint)(${bin_size}));
    double3 v_cm = sum_cell(d_vs, d_bin, gid, count, seed);
    v_cm /= count;
    collide_cell(d_vs, d_bin, gid, count, seed, v_cm);
  }
}
|end

|if nbins.outer ~= 0 then
__kernel void
mpcd_sum_outer(__global double3 *restrict d_vs,
               __global const uint *restrict d_bin,
               __global const uint *restrict d_bin_count,
               __global double4 *restrict d_v_cm,
               const ulong seed)
{
  const uint gid = get_global_id(0);
  if (gid == clamp(gid, ${offset.outer}U, ${offset.outer + nbins.outer - 1}U)) {
    const uint count = min(d_bin_count[gid], (uint)(${bin_size}));
    double3 v_cm = sum_cell(d_vs, d_bin, gid, count, seed);
    d_v_cm[gid] = (double4)(v_cm, count);
  }
}

__kernel void
mpcd_collide_outer(__global double3 *restrict d_vs,
                   __global const uint *restrict d_bin,
                   __global const uint *restrict d_bin_count,
                   __global const double4 *restrict d_v_cm,
                   const ulong seed)
{
  const uint gid = get_global_id(0);
  if (gid == clamp(gid, ${offset.outer}U, ${offset.outer + nbins.outer - 1}U)) {
    const uint count = min(d_bin_count[gid], (uint)(${bin_size}));
    double3 v_cm = d_v_cm[gid].xyz / d_v_cm[gid].w;
    collide_cell(d_vs, d_bin, gid, count, seed, v_cm);
  }
}
|end

|local work_size = 64

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
mpcd_v_max(__global const double3 *restrict d_vs,
           __global double *restrict d_vs_max,
           const uint Ns,
           const uint Ns_neigh)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  const uint gsize = get_global_size(0);
  __local double l_v[${work_size}];
  double v2 = 0;
  for (uint i = gid; i < Ns; i += gsize) {
    if (i >= Ns_neigh) {
      const double3 v = d_vs[i];
      v2 = max(v2, dot(v, v));
    }
  }
  l_v[lid] = sqrt(v2);
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_v[lid] = max(l_v[lid], l_v[lid+${i}]);
  |end
  if (lid == 0) d_vs_max[wid] = l_v[0];
}
