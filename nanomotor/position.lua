------------------------------------------------------------------------------
-- Initialise solvent positions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

local _M = {}

-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double3 = ffi.typeof("cl_double3")
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local cl_ulong_1 = ffi.typeof("cl_ulong[1]")
local int_1 = ffi.typeof("int[1]")

--[[
Places solvent particles at random positions within the box.

The coordinates of each solvent particle are chosen from a uniform
distribution. If the position lies within the excluded volume of the
dimer spheres, the position is discarded and the placement repeated.
--]]
function _M.random(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local nbin = {}
  for i = 1, 3 do
    nbin[i] = math.floor(box.L_local[i])
  end
  local nbins = nbin[1]*nbin[2]*nbin[3]
  local sigma = args.sigma

  local program = compute.program(context, "nanomotor/position.cl", {
    dom = dom,
    box = box,
    nbin = nbin,
    nbins = nbins,
    sigma = sigma,
  })

  local d_count = context:create_buffer(nbins*ffi.sizeof("cl_uint"))

  local fill do
    local kernel = program:create_kernel("position_fill")
    local work_size = 64
    local glob_size = math.ceil(nbins/work_size) * work_size

    function fill()
      kernel:set_arg(0, d_count)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local Nd

  local bin do
    local kernel = program:create_kernel("position_bin")
    local work_size = kernel:get_work_group_info(dom.device, "compile_work_group_size")[1]

    function bin()
      local glob_size = Nd*work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_spd)
      kernel:set_arg(2, d_count)
      queue:enqueue_write_buffer(dom.d_rd, false, 0, Nd*ffi.sizeof(cl_double3), dom.rd)
      queue:enqueue_write_buffer(dom.d_spd, true, 0, Nd*ffi.sizeof(cl_char4), dom.spd)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local random do
    local kernel = program:create_kernel("position_random")
    local work_size = 64

    function random()
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      local offset = cl_uint_1()
      if comm then mpi.exscan(cl_uint_1(dom.Ns), offset, 1, mpi.uint32, mpi.sum, comm) end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, dom.d_ids)
      kernel:set_arg(2, d_count)
      kernel:set_arg(3, cl_uint_1(dom.Ns))
      kernel:set_arg(4, offset)
      kernel:set_arg(5, cl_ulong_1(dom.step))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local virtual
  if comm then
    local rank_left, rank_right = mpi.cart_shift(comm, 0, 1)
    local rank_back, rank_front = mpi.cart_shift(comm, 1, 1)
    local rank_down, rank_up = mpi.cart_shift(comm, 2, 1)

    local mid_x = box.O_local[1] + box.L_local[1]/2
    local mid_y = box.O_local[2] + box.L_local[2]/2
    local mid_z = box.O_local[3] + box.L_local[3]/2

    local r_cut_C = sigma.C
    local r_cut_C_x = box.L_local[1]/2 - r_cut_C
    local r_cut_C_y = box.L_local[2]/2 - r_cut_C
    local r_cut_C_z = box.L_local[3]/2 - r_cut_C

    local r_cut_N = sigma.N
    local r_cut_N_x = box.L_local[1]/2 - r_cut_N
    local r_cut_N_y = box.L_local[2]/2 - r_cut_N
    local r_cut_N_z = box.L_local[3]/2 - r_cut_N

    local sendoffset_right = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_left = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_front = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_back = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_up = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_down = ffi.new("int[?]", dom.Nd_max)

    function virtual()
      local sendcount_right = 0
      local sendcount_left = 0
      for i = 0, Nd-1 do
        local x = dom.rd[i].x - mid_x
        local y, z
        x, y, z = box.mindist(x, y, z)
        local r_cut_x = dom.spd[i].s0 == 0 and r_cut_C_x or r_cut_N_x
        if rank_right and x > r_cut_x then
          if sendcount_right >= dom.Nd_max then error("right send buffer overflow") end
          sendoffset_right[sendcount_right] = i
          sendcount_right = sendcount_right + 1
        elseif rank_left and x < -r_cut_x then
          if sendcount_left >= dom.Nd_max then error("left send buffer overflow") end
          sendoffset_left[sendcount_left] = i
          sendcount_left = sendcount_left + 1
        end
      end

      local recvoffset_left = int_1(Nd)
      local recvcount_left = int_1()
      mpi.sendrecv(int_1(sendcount_right), 1, mpi.int, rank_right, 0, recvcount_left, 1, mpi.int, rank_left, 0, comm)
      recvcount_left = recvcount_left[0]
      Nd = Nd + recvcount_left
      if Nd > dom.Nd_max then error("left receive buffer overflow") end

      local sendtype_rd_right = mpi.type_create_indexed_block(sendcount_right, 1, sendoffset_right, dom.type_rd)
      sendtype_rd_right:commit()
      local sendtype_spd_right = mpi.type_create_indexed_block(sendcount_right, 1, sendoffset_right, dom.type_spd)
      sendtype_spd_right:commit()

      local recvtype_rd_left = mpi.type_create_indexed_block(1, recvcount_left, recvoffset_left, dom.type_rd)
      recvtype_rd_left:commit()
      local recvtype_spd_left = mpi.type_create_indexed_block(1, recvcount_left, recvoffset_left, dom.type_spd)
      recvtype_spd_left:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_right, rank_right, 0, dom.rd, 1, recvtype_rd_left, rank_left, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_right, rank_right, 0, dom.spd, 1, recvtype_spd_left, rank_left, 0, comm)

      local recvoffset_right = int_1(Nd)
      local recvcount_right = int_1()
      mpi.sendrecv(int_1(sendcount_left), 1, mpi.int, rank_left, 0, recvcount_right, 1, mpi.int, rank_right, 0, comm)
      recvcount_right = recvcount_right[0]
      Nd = Nd + recvcount_right
      if Nd > dom.Nd_max then error("right receive buffer overflow") end

      local sendtype_rd_left = mpi.type_create_indexed_block(sendcount_left, 1, sendoffset_left, dom.type_rd)
      sendtype_rd_left:commit()
      local sendtype_spd_left = mpi.type_create_indexed_block(sendcount_left, 1, sendoffset_left, dom.type_spd)
      sendtype_spd_left:commit()

      local recvtype_rd_right = mpi.type_create_indexed_block(1, recvcount_right, recvoffset_right, dom.type_rd)
      recvtype_rd_right:commit()
      local recvtype_spd_right = mpi.type_create_indexed_block(1, recvcount_right, recvoffset_right, dom.type_spd)
      recvtype_spd_right:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_left, rank_left, 0, dom.rd, 1, recvtype_rd_right, rank_right, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_left, rank_left, 0, dom.spd, 1, recvtype_spd_right, rank_right, 0, comm)

      local sendcount_front = 0
      local sendcount_back = 0
      for i = 0, Nd-1 do
        local y = dom.rd[i].y - mid_y
        local x, z
        x, y, z = box.mindist(x, y, z)
        local r_cut_y = dom.spd[i].s0 == 0 and r_cut_C_y or r_cut_N_y
        if rank_front and y > r_cut_y then
          if sendcount_front >= dom.Nd_max then error("front send buffer overflow") end
          sendoffset_front[sendcount_front] = i
          sendcount_front = sendcount_front + 1
        elseif rank_back and y < -r_cut_y then
          if sendcount_back >= dom.Nd_max then error("back send buffer overflow") end
          sendoffset_back[sendcount_back] = i
          sendcount_back = sendcount_back + 1
        end
      end

      local recvoffset_back = int_1(Nd)
      local recvcount_back = int_1()
      mpi.sendrecv(int_1(sendcount_front), 1, mpi.int, rank_front, 0, recvcount_back, 1, mpi.int, rank_back, 0, comm)
      recvcount_back = recvcount_back[0]
      Nd = Nd + recvcount_back
      if Nd > dom.Nd_max then error("back receive buffer overflow") end

      local sendtype_rd_front = mpi.type_create_indexed_block(sendcount_front, 1, sendoffset_front, dom.type_rd)
      sendtype_rd_front:commit()
      local sendtype_spd_front = mpi.type_create_indexed_block(sendcount_front, 1, sendoffset_front, dom.type_spd)
      sendtype_spd_front:commit()

      local recvtype_rd_back = mpi.type_create_indexed_block(1, recvcount_back, recvoffset_back, dom.type_rd)
      recvtype_rd_back:commit()
      local recvtype_spd_back = mpi.type_create_indexed_block(1, recvcount_back, recvoffset_back, dom.type_spd)
      recvtype_spd_back:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_front, rank_front, 0, dom.rd, 1, recvtype_rd_back, rank_back, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_front, rank_front, 0, dom.spd, 1, recvtype_spd_back, rank_back, 0, comm)

      local recvoffset_front = int_1(Nd)
      local recvcount_front = int_1()
      mpi.sendrecv(int_1(sendcount_back), 1, mpi.int, rank_back, 0, recvcount_front, 1, mpi.int, rank_front, 0, comm)
      recvcount_front = recvcount_front[0]
      Nd = Nd + recvcount_front
      if Nd > dom.Nd_max then error("front receive buffer overflow") end

      local sendtype_rd_back = mpi.type_create_indexed_block(sendcount_back, 1, sendoffset_back, dom.type_rd)
      sendtype_rd_back:commit()
      local sendtype_spd_back = mpi.type_create_indexed_block(sendcount_back, 1, sendoffset_back, dom.type_spd)
      sendtype_spd_back:commit()

      local recvtype_rd_front = mpi.type_create_indexed_block(1, recvcount_front, recvoffset_front, dom.type_rd)
      recvtype_rd_front:commit()
      local recvtype_spd_front = mpi.type_create_indexed_block(1, recvcount_front, recvoffset_front, dom.type_spd)
      recvtype_spd_front:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_back, rank_back, 0, dom.rd, 1, recvtype_rd_front, rank_front, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_back, rank_back, 0, dom.spd, 1, recvtype_spd_front, rank_front, 0, comm)

      local sendcount_up = 0
      local sendcount_down = 0
      for i = 0, Nd-1 do
        local z = dom.rd[i].z - mid_z
        local x, y
        x, y, z = box.mindist(x, y, z)
        local r_cut_z = dom.spd[i].s0 == 0 and r_cut_C_z or r_cut_N_z
        if rank_up and z > r_cut_z then
          if sendcount_up >= dom.Nd_max then error("up send buffer overflow") end
          sendoffset_up[sendcount_up] = i
          sendcount_up = sendcount_up + 1
        elseif rank_down and z < -r_cut_z then
          if sendcount_down >= dom.Nd_max then error("down send buffer overflow") end
          sendoffset_down[sendcount_down] = i
          sendcount_down = sendcount_down + 1
        end
      end

      local recvoffset_down = int_1(Nd)
      local recvcount_down = int_1()
      mpi.sendrecv(int_1(sendcount_up), 1, mpi.int, rank_up, 0, recvcount_down, 1, mpi.int, rank_down, 0, comm)
      recvcount_down = recvcount_down[0]
      Nd = Nd + recvcount_down
      if Nd > dom.Nd_max then error("down receive buffer overflow") end

      local sendtype_rd_up = mpi.type_create_indexed_block(sendcount_up, 1, sendoffset_up, dom.type_rd)
      sendtype_rd_up:commit()
      local sendtype_spd_up = mpi.type_create_indexed_block(sendcount_up, 1, sendoffset_up, dom.type_spd)
      sendtype_spd_up:commit()

      local recvtype_rd_down = mpi.type_create_indexed_block(1, recvcount_down, recvoffset_down, dom.type_rd)
      recvtype_rd_down:commit()
      local recvtype_spd_down = mpi.type_create_indexed_block(1, recvcount_down, recvoffset_down, dom.type_spd)
      recvtype_spd_down:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_up, rank_up, 0, dom.rd, 1, recvtype_rd_down, rank_down, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_up, rank_up, 0, dom.spd, 1, recvtype_spd_down, rank_down, 0, comm)

      local recvoffset_up = int_1(Nd)
      local recvcount_up = int_1()
      mpi.sendrecv(int_1(sendcount_down), 1, mpi.int, rank_down, 0, recvcount_up, 1, mpi.int, rank_up, 0, comm)
      recvcount_up = recvcount_up[0]
      Nd = Nd + recvcount_up
      if Nd > dom.Nd_max then error("up receive buffer overflow") end

      local sendtype_rd_down = mpi.type_create_indexed_block(sendcount_down, 1, sendoffset_down, dom.type_rd)
      sendtype_rd_down:commit()
      local sendtype_spd_down = mpi.type_create_indexed_block(sendcount_down, 1, sendoffset_down, dom.type_spd)
      sendtype_spd_down:commit()

      local recvtype_rd_up = mpi.type_create_indexed_block(1, recvcount_up, recvoffset_up, dom.type_rd)
      recvtype_rd_up:commit()
      local recvtype_spd_up = mpi.type_create_indexed_block(1, recvcount_up, recvoffset_up, dom.type_spd)
      recvtype_spd_up:commit()

      mpi.sendrecv(dom.rd, 1, sendtype_rd_down, rank_down, 0, dom.rd, 1, recvtype_rd_up, rank_up, 0, comm)
      mpi.sendrecv(dom.spd, 1, sendtype_spd_down, rank_down, 0, dom.spd, 1, recvtype_spd_up, rank_up, 0, comm)
    end
  end

  return function()
    Nd = dom.Nd
    if comm then virtual() end
    fill()
    bin()
    random()
  end
end

return _M
