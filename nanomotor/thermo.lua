------------------------------------------------------------------------------
-- Thermodynamic variables.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local ceil, max = math.ceil, math.max

return function(integrate, file, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local rank = comm and comm:rank() or 0
  local timestep = integrate.timestep
  local initial, final, interval = args.initial, args.final, args.interval
  local dtype = hdf5[args.datatype]

  local step = {interval = interval, offset = initial}
  local time = {interval = interval*timestep, offset = initial*timestep}
  local obs_en_tot = h5md.create_observable(file, "observables/total_energy", step, time, dtype)
  local obs_en_pot = h5md.create_observable(file, "observables/potential_energy", step, time, dtype)
  local obs_en_kin = h5md.create_observable(file, "observables/kinetic_energy", step, time, dtype)
  local obs_temp = h5md.create_observable(file, "observables/temperature", step, time, dtype)
  local obs_press = h5md.create_observable(file, "observables/pressure", step, time, dtype)
  local obs_v_cm = h5md.create_observable(file, "observables/center_of_mass_velocity", step, time, dtype, {3})
  obs_en_tot:close()
  obs_en_pot:close()
  obs_en_kin:close()
  obs_temp:close()
  obs_press:close()
  obs_v_cm:close()

  local size = max(0, ceil((final - initial + 1)/interval))
  local en_tot = ffi.new("double[?]", size)
  local en_pot = ffi.new("double[?]", size)
  local en_kin = ffi.new("double[?]", size)
  local temp = ffi.new("double[?]", size)
  local press = ffi.new("double[?]", size)
  local v_cm = ffi.new("double[?][3]", size)

  local count = 0

  local function write()
    if count == 0 then return end
    local filespace_en_tot = obs_en_tot:append(count)
    local filespace_en_pot = obs_en_pot:append(count)
    local filespace_en_kin = obs_en_kin:append(count)
    local filespace_temp = obs_temp:append(count)
    local filespace_press = obs_press:append(count)
    local filespace_v_cm = obs_v_cm:append(count)
    filespace_en_tot:select_hyperslab("set", {0}, nil, {count})
    filespace_en_pot:select_hyperslab("set", {0}, nil, {count})
    filespace_en_kin:select_hyperslab("set", {0}, nil, {count})
    filespace_temp:select_hyperslab("set", {0}, nil, {count})
    filespace_press:select_hyperslab("set", {0}, nil, {count})
    filespace_v_cm:select_hyperslab("set", {0, 0}, nil, {count, 3})
    local memspace_scalar = hdf5.create_simple_space({size})
    local memspace_vector = hdf5.create_simple_space({size, 3})
    memspace_scalar:select_hyperslab("set", {0}, nil, {count})
    memspace_vector:select_hyperslab("set", {0, 0}, nil, {count, 3})
    if rank == 0 then
      obs_en_tot.value:write(en_tot, hdf5.double, memspace_scalar, filespace_en_tot)
      obs_en_pot.value:write(en_pot, hdf5.double, memspace_scalar, filespace_en_pot)
      obs_en_kin.value:write(en_kin, hdf5.double, memspace_scalar, filespace_en_kin)
      obs_temp.value:write(temp, hdf5.double, memspace_scalar, filespace_temp)
      obs_press.value:write(press, hdf5.double, memspace_scalar, filespace_press)
      obs_v_cm.value:write(v_cm, hdf5.double, memspace_vector, filespace_v_cm)
    end
    filespace_en_tot:close()
    filespace_en_pot:close()
    filespace_en_kin:close()
    filespace_temp:close()
    filespace_press:close()
    filespace_v_cm:close()
    memspace_scalar:close()
    memspace_vector:close()
    count = 0
  end

  local self = {step = initial}

  function self.observe()
    obs_en_tot = h5md.open_observable(file, "observables/total_energy")
    obs_en_pot = h5md.open_observable(file, "observables/potential_energy")
    obs_en_kin = h5md.open_observable(file, "observables/kinetic_energy")
    obs_temp = h5md.open_observable(file, "observables/temperature")
    obs_press = h5md.open_observable(file, "observables/pressure")
    obs_v_cm = h5md.open_observable(file, "observables/center_of_mass_velocity")
    while self.step <= final do
      coroutine.yield()
      local en = integrate.compute_en()
      en_tot[count] = en.en_tot
      en_pot[count] = en.en_pot
      en_kin[count] = en.en_kin
      temp[count] = en.temp
      press[count] = en.press
      v_cm[count] = en.v_cm
      count = count + 1
      self.step = self.step + interval
    end
    write()
    obs_en_tot:close()
    obs_en_pot:close()
    obs_en_kin:close()
    obs_temp:close()
    obs_press:close()
    obs_v_cm:close()
  end

  function self.snapshot(group)
    local filespace_scalar = hdf5.create_simple_space({count})
    local filespace_vector = hdf5.create_simple_space({count, 3})
    local dset_en_tot = group:create_dataset("total_energy", hdf5.double, filespace_scalar)
    local dset_en_pot = group:create_dataset("potential_energy", hdf5.double, filespace_scalar)
    local dset_en_kin = group:create_dataset("kinetic_energy", hdf5.double, filespace_scalar)
    local dset_temp = group:create_dataset("temperature", hdf5.double, filespace_scalar)
    local dset_press = group:create_dataset("pressure", hdf5.double, filespace_scalar)
    local dset_v_cm = group:create_dataset("center_of_mass_velocity", hdf5.double, filespace_vector)
    local memspace_scalar = hdf5.create_simple_space({size})
    local memspace_vector = hdf5.create_simple_space({size, 3})
    memspace_scalar:select_hyperslab("set", {0}, nil, {count})
    memspace_vector:select_hyperslab("set", {0, 0}, nil, {count, 3})
    if rank == 0 then
      dset_en_tot:write(en_tot, hdf5.double, memspace_scalar, filespace_scalar)
      dset_en_pot:write(en_pot, hdf5.double, memspace_scalar, filespace_scalar)
      dset_en_kin:write(en_kin, hdf5.double, memspace_scalar, filespace_scalar)
      dset_temp:write(temp, hdf5.double, memspace_scalar, filespace_scalar)
      dset_press:write(press, hdf5.double, memspace_scalar, filespace_scalar)
      dset_v_cm:write(v_cm, hdf5.double, memspace_vector, filespace_vector)
    end
    filespace_scalar:close()
    filespace_vector:close()
    memspace_scalar:close()
    memspace_vector:close()
    dset_en_tot:close()
    dset_en_pot:close()
    dset_en_kin:close()
    dset_temp:close()
    dset_press:close()
    dset_v_cm:close()
  end

  function self.restore(group)
    local dset_en_tot = group:open_dataset("total_energy")
    local dset_en_pot = group:open_dataset("potential_energy")
    local dset_en_kin = group:open_dataset("kinetic_energy")
    local dset_temp = group:open_dataset("temperature")
    local dset_press = group:open_dataset("pressure")
    local dset_v_cm = group:open_dataset("center_of_mass_velocity")
    local filespace_scalar = dset_en_tot:get_space()
    local filespace_vector = dset_v_cm:get_space()
    count = filespace_scalar:get_simple_extent_dims()[1]
    local memspace_scalar = hdf5.create_simple_space({size})
    local memspace_vector = hdf5.create_simple_space({size, 3})
    memspace_scalar:select_hyperslab("set", {0}, nil, {count})
    memspace_vector:select_hyperslab("set", {0, 0}, nil, {count, 3})
    if rank == 0 then
      dset_en_tot:read(en_tot, hdf5.double, memspace_scalar, filespace_scalar)
      dset_en_pot:read(en_pot, hdf5.double, memspace_scalar, filespace_scalar)
      dset_en_kin:read(en_kin, hdf5.double, memspace_scalar, filespace_scalar)
      dset_temp:read(temp, hdf5.double, memspace_scalar, filespace_scalar)
      dset_press:read(press, hdf5.double, memspace_scalar, filespace_scalar)
      dset_v_cm:read(v_cm, hdf5.double, memspace_vector, filespace_vector)
    end
    filespace_scalar:close()
    filespace_vector:close()
    memspace_scalar:close()
    memspace_vector:close()
    dset_en_tot:close()
    dset_en_pot:close()
    dset_en_kin:close()
    dset_temp:close()
    dset_press:close()
    dset_v_cm:close()
  end

  return self
end
