------------------------------------------------------------------------------
-- Multi-particle collision dynamics.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

-- Cache C types.
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local cl_ulong_1 = ffi.typeof("cl_ulong[1]")

--[[
Multi-particle collision dynamics is a mesoscopic simulation method to
model hydrodynamic flows in a solvent [@Malevanets1999, @Malevanets2000].
The solvent is modelled using point-like particles that interact via local
multi-particle collisions. Between collisions the solvent particles are
streamed freely according to their velocities.

This function implements the collision step. The solvent is partitioned
into a grid of cubic cells of unit volume. The particles within each cell
interact via stochastic rotations, where the velocities relative to the
local center of mass velocity are rotated around a randomly chosen axis
of unit length. The rotation angle, which defines the transport properties
of the solvent, is chosen as 90°.

To obtain Galilean invariance independent of the ratio of the mean free
path of the solvent to the edge length of the cells, the grid is shifted
along all dimensions by random fractions of the edge length [@Ihle2001].

For non-equilibrium simulations with viscous heating of the solvent, an
Andersen thermostat may be substituted for the rotations [@Gompper2009].
At the collision step, the solvent velocities are assigned from a Gaussian
distribution at the given temperature, and, for each cell, its center of
mass velocity after the thermostatting is replaced with its center of mass
velocity before the thermostatting, thus conserving the cell’s momentum.
--]]
return function(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local bin_size = args.mpcd.bin_size
  local temp = args.mpcd.temp
  local interval = args.mpcd.interval

  local nbin = {}
  for i = 1, 3 do
    nbin[i] = math.floor(box.U_local[i]) - math.floor(box.O_local[i])
    if comm then
      nbin[i] = nbin[i] + 2
      assert(nbin[i] >= 4)
    else
      nbin[i] = nbin[i] + 1
      assert(nbin[i] >= 2)
    end
  end

  local nbins = {}
  local offset = {}

  nbins.all = nbin[1]*nbin[2]*nbin[3]

  if comm then
    nbins.inner = (nbin[1]-4)*(nbin[2]-4)*(nbin[3]-4)
    nbins.outer = nbins.all - nbins.inner
  else
    nbins.inner = nbins.all
    nbins.outer = 0
  end

  offset.outer = 0
  offset.inner = nbins.outer

  if comm then
    nbins.left = 2*nbin[2]*nbin[3]
    nbins.right = nbins.left
    nbins.back = 2*(nbin[1]-4)*nbin[3]
    nbins.front = nbins.back
    nbins.down = 2*(nbin[1]-4)*(nbin[2]-4)
    nbins.up = nbins.down

    offset.left = 0
    offset.right = offset.left + nbins.left
    offset.back = offset.right + nbins.right
    offset.front = offset.back + nbins.back
    offset.down = offset.front + nbins.front
    offset.up = offset.down + nbins.down
  end

  local program = compute.program(context, "nanomotor/mpcd.cl", {
    dom = dom,
    box = box,
    nbin = nbin,
    nbins = nbins,
    offset = offset,
    bin_size = bin_size,
    temp = temp,
  })

  local self = {}

  local mem_queue = context:create_command_queue(device)
  local d_bin = context:create_buffer(nbins.all*bin_size*ffi.sizeof("cl_uint"))
  local d_bin_count = context:create_buffer(nbins.all*ffi.sizeof("cl_uint"))
  local err = ffi.new("cl_uint[1]")
  local d_err = context:create_buffer("use_host_ptr", ffi.sizeof(err), err)

  local fill do
    local kernel = program:create_kernel("mpcd_fill")
    local work_size = 64
    local glob_size = math.ceil(nbins.all/work_size) * work_size

    function fill()
      kernel:set_arg(0, d_bin_count)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local bin do
    local kernel = program:create_kernel("mpcd_bin")
    local work_size = 64

    function bin()
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, d_bin)
      kernel:set_arg(2, d_bin_count)
      kernel:set_arg(3, d_err)
      kernel:set_arg(4, cl_uint_1(dom.Ns))
      kernel:set_arg(5, cl_ulong_1(dom.step))
      local event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      mem_queue:enqueue_wait_for_events({event})
    end
  end

  local function check_bin()
    mem_queue:enqueue_map_buffer(d_err, true, "read")
    if err[0] ~= 0 then error("bin overflow of "..err[0].." solvent particles") end
    queue:enqueue_unmap_mem_object(d_err, err)
  end

  local collide_inner
  if nbins.inner ~= 0 then
    local kernel = program:create_kernel("mpcd_collide_inner")
    local work_size = 64
    local glob_offset = math.floor(nbins.outer/work_size) * work_size
    local glob_size = math.ceil(nbins.all/work_size) * work_size - glob_offset

    function collide_inner()
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, d_bin)
      kernel:set_arg(2, d_bin_count)
      kernel:set_arg(3, cl_ulong_1(dom.step))
      queue:enqueue_ndrange_kernel(kernel, {glob_offset}, {glob_size}, {work_size})
    end
  end

  local sum_outer, collide_outer, send_outer
  if nbins.outer ~= 0 then
    local v_cm = ffi.new("cl_double4[?]", nbins.outer)
    local d_v_cm = context:create_buffer("use_host_ptr", ffi.sizeof(v_cm), v_cm)

    do
      local kernel = program:create_kernel("mpcd_sum_outer")
      local work_size = 64
      local glob_size = math.ceil(nbins.outer/work_size) * work_size

      function sum_outer()
        kernel:set_arg(0, dom.d_vs)
        kernel:set_arg(1, d_bin)
        kernel:set_arg(2, d_bin_count)
        kernel:set_arg(3, d_v_cm)
        kernel:set_arg(4, cl_ulong_1(dom.step))
        local event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
        mem_queue:enqueue_wait_for_events({event})
      end
    end

    do
      local kernel = program:create_kernel("mpcd_collide_outer")
      local work_size = 64
      local glob_size = math.ceil(nbins.outer/work_size) * work_size

      function collide_outer()
        kernel:set_arg(0, dom.d_vs)
        kernel:set_arg(1, d_bin)
        kernel:set_arg(2, d_bin_count)
        kernel:set_arg(3, d_v_cm)
        kernel:set_arg(4, cl_ulong_1(dom.step))
        queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      end
    end

    local neigh_left, neigh_right = mpi.cart_shift(comm, 0, 1)
    local neigh_back, neigh_front = mpi.cart_shift(comm, 1, 1)
    local neigh_down, neigh_up = mpi.cart_shift(comm, 2, 1)

    local type_v_cm = mpi.type_contiguous(4, mpi.double)

    local type_v_cm_left do
      local blocks = 1
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      displs[0] = offset.left
      counts[0] = nbins.left
      type_v_cm_left = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_left:commit()
    end

    local type_v_cm_right do
      local blocks = 1
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      displs[0] = offset.right
      counts[0] = nbins.right
      type_v_cm_right = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_right:commit()
    end

    local type_v_cm_back do
      local blocks = 2*nbin[3] + 1
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      local i = 0
      for z = 0, nbin[3]-1 do
        displs[i] = offset.left + z*(2*nbin[2])
        counts[i] = 4
        i = i + 1
      end
      for z = 0, nbin[3]-1 do
        displs[i] = offset.right + z*(2*nbin[2])
        counts[i] = 4
        i = i + 1
      end
      displs[i] = offset.back
      counts[i] = nbins.back
      type_v_cm_back = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_back:commit()
    end

    local type_v_cm_front do
      local blocks = 2*nbin[3] + 1
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      local i = 0
      for z = 0, nbin[3]-1 do
        displs[i] = offset.left + z*(2*nbin[2]) + (2*nbin[2]-4)
        counts[i] = 4
        i = i + 1
      end
      for z = 0, nbin[3]-1 do
        displs[i] = offset.right + z*(2*nbin[2]) + (2*nbin[2]-4)
        counts[i] = 4
        i = i + 1
      end
      displs[i] = offset.front
      counts[i] = nbins.front
      type_v_cm_front = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_front:commit()
    end

    local type_v_cm_down do
      local blocks = 5
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      displs[0] = offset.left
      counts[0] = 2*nbin[2]
      displs[1] = offset.right
      counts[1] = 2*nbin[2]
      displs[2] = offset.back
      counts[2] = 2*(nbin[1]-4)
      displs[3] = offset.front
      counts[3] = 2*(nbin[1]-4)
      displs[4] = offset.down
      counts[4] = nbins.down
      type_v_cm_down = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_down:commit()
    end

    local type_v_cm_up do
      local blocks = 5
      local displs = ffi.new("int[?]", blocks)
      local counts = ffi.new("int[?]", blocks)
      displs[0] = offset.left + 2*nbin[2]*(nbin[3]-2)
      counts[0] = 2*nbin[2]
      displs[1] = offset.right + 2*nbin[2]*(nbin[3]-2)
      counts[1] = 2*nbin[2]
      displs[2] = offset.back + 2*(nbin[1]-4)*(nbin[3]-2)
      counts[2] = 2*(nbin[1]-4)
      displs[3] = offset.front + 2*(nbin[1]-4)*(nbin[3]-2)
      counts[3] = 2*(nbin[1]-4)
      displs[4] = offset.up
      counts[4] = nbins.up
      type_v_cm_up = mpi.type_indexed(blocks, counts, displs, type_v_cm)
      type_v_cm_up:commit()
    end

    local v_cm_buf = ffi.new("cl_double4[?]", nbins.outer)

    function send_outer()
      mem_queue:enqueue_map_buffer(d_v_cm, true, "write")

      ffi.fill(v_cm_buf, ffi.sizeof(v_cm_buf))
      mpi.sendrecv(v_cm, 1, type_v_cm_right, neigh_right, 0, v_cm_buf, 1, type_v_cm_left, neigh_left, 0, comm)
      for i = 0, nbins.outer-1 do
        v_cm[i].x = v_cm[i].x + v_cm_buf[i].x
        v_cm[i].y = v_cm[i].y + v_cm_buf[i].y
        v_cm[i].z = v_cm[i].z + v_cm_buf[i].z
        v_cm[i].w = v_cm[i].w + v_cm_buf[i].w
      end

      ffi.fill(v_cm_buf, ffi.sizeof(v_cm_buf))
      mpi.sendrecv(v_cm, 1, type_v_cm_front, neigh_front, 0, v_cm_buf, 1, type_v_cm_back, neigh_back, 0, comm)
      for i = 0, nbins.outer-1 do
        v_cm[i].x = v_cm[i].x + v_cm_buf[i].x
        v_cm[i].y = v_cm[i].y + v_cm_buf[i].y
        v_cm[i].z = v_cm[i].z + v_cm_buf[i].z
        v_cm[i].w = v_cm[i].w + v_cm_buf[i].w
      end

      ffi.fill(v_cm_buf, ffi.sizeof(v_cm_buf))
      mpi.sendrecv(v_cm, 1, type_v_cm_up, neigh_up, 0, v_cm_buf, 1, type_v_cm_down, neigh_down, 0, comm)
      for i = 0, nbins.outer-1 do
        v_cm[i].x = v_cm[i].x + v_cm_buf[i].x
        v_cm[i].y = v_cm[i].y + v_cm_buf[i].y
        v_cm[i].z = v_cm[i].z + v_cm_buf[i].z
        v_cm[i].w = v_cm[i].w + v_cm_buf[i].w
      end

      mpi.sendrecv(v_cm, 1, type_v_cm_down, neigh_down, 0, v_cm, 1, type_v_cm_up, neigh_up, 0, comm)
      mpi.sendrecv(v_cm, 1, type_v_cm_back, neigh_back, 0, v_cm, 1, type_v_cm_front, neigh_front, 0, comm)
      mpi.sendrecv(v_cm, 1, type_v_cm_left, neigh_left, 0, v_cm, 1, type_v_cm_right, neigh_right, 0, comm)

      local event = mem_queue:enqueue_unmap_mem_object(d_v_cm, v_cm)
      queue:enqueue_wait_for_events({event})
    end
  end

  local delta_t = args.timestep
  local delta_r = args.skin/2
  local r_max, v_max, step

  local update_v_max do
    local kernel = program:create_kernel("mpcd_v_max")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]
    local num_groups = 512
    local glob_size = num_groups * work_size
    local vs_max = ffi.new("cl_double[?]", num_groups)
    local d_vs_max = context:create_buffer("use_host_ptr", ffi.sizeof(vs_max), vs_max)

    function update_v_max()
      local glob_offset = math.floor(dom.Ns_neigh/work_size) * work_size
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, d_vs_max)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      kernel:set_arg(3, cl_uint_1(dom.Ns_neigh))
      queue:enqueue_ndrange_kernel(kernel, {glob_offset}, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_vs_max, true, "read")
      v_max = 0
      for i = 0, num_groups-1 do
        v_max = math.max(v_max, vs_max[i])
      end
      queue:enqueue_unmap_mem_object(d_vs_max, vs_max)
      step = dom.step
    end
  end

  function self.collide()
    fill()
    bin()
    if collide_outer then
      sum_outer()
    end
    if collide_inner then
      collide_inner()
    end
    if collide_outer then
      send_outer()
      collide_outer()
    end
    check_bin()
    r_max = r_max + v_max*delta_t*(dom.step-step)
    update_v_max()
  end

  function self.check_collide()
    return dom.step % interval == 0
  end

  do
    local buf = ffi.new("double[1]")

    function self.check_neigh()
      buf[0] = r_max + v_max*delta_t*(dom.step-step)
      if comm then mpi.allreduce(mpi.in_place, buf, 1, mpi.double, mpi.max, comm) end
      return buf[0] > delta_r
    end
  end

  function self.post_neigh()
    r_max = 0
    update_v_max()
  end

  return self
end
