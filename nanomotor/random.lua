------------------------------------------------------------------------------
-- Random distributions.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local random, log, sqrt = math.random, math.log, math.sqrt

local _M = {}

--[[
Returns two variates from a standard normal distribution.

See @Marsaglia1964.
]]
function _M.normal()
  local v, w, s
  repeat
    v = 2*random()-1
    w = 2*random()-1
    s = v*v + w*w
  until s < 1
  local t = sqrt(-2*log(s)/s)
  return v*t, w*t
end

--[[
Returns uniformly chosen point on a unit circle.

See @Neumann1951 or @Cook1957, and [Circle Point Picking].

[Circle Point Picking]: http://mathworld.wolfram.com/CirclePointPicking.html
]]
function _M.circle()
  local v, w, s
  repeat
    v = 2*random()-1
    w = 2*random()-1
    s = v*v + w*w
  until s < 1
  local t = 1/s
  return (v*v-w*w)*t, 2*v*w*t
end

--[[
Returns uniformly chosen point on a unit sphere.

See @Marsaglia1972 and [Sphere Point Picking].

[Sphere Point Picking]: http://mathworld.wolfram.com/SpherePointPicking.html
]]
function _M.sphere()
  local v, w, s
  repeat
    v = 2*random()-1
    w = 2*random()-1
    s = v*v + w*w
  until s < 1
  local t = sqrt(1-s)
  return 2*v*t, 2*w*t, 1-2*s
end

return _M
