------------------------------------------------------------------------------
-- MPCDMD integration.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local nm = {
  sort = require("nanomotor.sort"),
  neigh = require("nanomotor.neigh"),
  lj = require("nanomotor.lj"),
  rattle = require("nanomotor.rattle"),
  verlet = require("nanomotor.verlet"),
  mpcd = require("nanomotor.mpcd"),
}

local ffi = require("ffi")

-- Cache C types.
local energy = ffi.typeof[[
struct {
  double Nd, Ns, V, en_pot, en_kin, en_tot, temp, press, v_cm[3];
}
]]

return function(dom, args)
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")

  local sort = nm.sort(dom, args)
  local neigh = nm.neigh(dom, args)
  local lj = nm.lj(dom, neigh, args)
  local rattle = nm.rattle(dom, args)
  local verlet = nm.verlet(dom, args)
  local mpcd = nm.mpcd(dom, args)

  local self = {
    dom = dom,
    timestep = args.timestep,
  }

  function self.update()
    neigh.pre_update()
    sort.update()
    neigh.update()
    lj.update()
    neigh.post_update()
    verlet.post_neigh()
    rattle.post_neigh()
    mpcd.post_neigh()
    lj.post_update()
  end

  function self.integrate(step)
    dom.step = dom.step + 1
    verlet.integrate()
    rattle.integrate()
    while true do
      lj.post_integrate()
      if mpcd.check_collide() then
        verlet.stream()
        if comm then verlet.send() end
      end
      if mpcd.check_neigh() or rattle.check_neigh() or verlet.check_neigh() then
        if comm then rattle.send() end
        if verlet.check_stream() then
          verlet.stream()
          if comm then verlet.send() end
        end
        neigh.pre_update()
        if sort.check_update() then
          sort.update()
        end
        neigh.update()
        neigh.post_update()
        verlet.post_neigh()
        rattle.post_neigh()
        mpcd.post_neigh()
      end
      lj.update()
      if mpcd.check_collide() then
        verlet.finalize()
        mpcd.collide()
      end
      if dom.step == step then break end
      dom.step = dom.step + 1
      verlet.integrate()
      lj.post_update()
      rattle.finalize()
      rattle.integrate()
    end
    if verlet.check_stream() then
      verlet.stream()
    end
    if verlet.check_finalize() then
      verlet.finalize()
    end
    lj.post_update()
    rattle.finalize()
  end

  function self.compute_en()
    local en = energy()
    do
      local en_pot, vir = lj.compute_en()
      local en_kin_s, p_cm_s = verlet.compute_en()
      local en_kin_d, p_cm_d = rattle.compute_en()
      local L_local = box.L_local
      en.Nd = dom.Nd
      en.Ns = dom.Ns
      en.V = L_local[1]*L_local[2]*L_local[3]
      en.en_pot = en_pot
      en.en_kin = en_kin_s + en_kin_d
      en.en_tot = en.en_pot + en.en_kin
      en.temp = 2*en.en_kin / 3
      en.press = (2*en.en_kin + vir) / 3
      en.v_cm[0] = p_cm_s[1] + p_cm_d[1]
      en.v_cm[1] = p_cm_s[2] + p_cm_d[2]
      en.v_cm[2] = p_cm_s[3] + p_cm_d[3]
    end
    if comm then mpi.allreduce(mpi.in_place, en, 11, mpi.double, mpi.sum, comm) end
    do
      local dof_d = 5*(en.Nd/2)
      local dof_s = 3*en.Ns
      local N = (dof_d + dof_s) / 3
      local V = en.V
      local mass_d = (en.Nd/2) * (dom.mass.C + dom.mass.N)
      local mass_s = en.Ns
      local mass = mass_d + mass_s
      en.en_pot = en.en_pot / N
      en.en_kin = en.en_kin / N
      en.en_tot = en.en_tot / N
      en.temp = en.temp / N
      en.press = en.press / V
      en.v_cm[0] = en.v_cm[0] / mass
      en.v_cm[1] = en.v_cm[1] / mass
      en.v_cm[2] = en.v_cm[2] / mass
    end
    return en
  end

  return self
end
