------------------------------------------------------------------------------
-- Write dimer trajectory.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local array = require("nanomotor.array")
local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local ceil, floor, max, min = math.ceil, math.floor, math.max, math.min
local ipairs = ipairs
-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double3 = ffi.typeof("cl_double3")
local cl_int = ffi.typeof("cl_int")
local cl_short3 = ffi.typeof("cl_short3")
local int8_t_1 = ffi.typeof("int8_t[1]")
local int_1 = ffi.typeof("int[1]")

return function(integrate, file, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank() or 0
  local nranks = comm and comm:size() or 1
  local timestep = integrate.timestep
  local initial, final, interval = args.initial, args.final, args.interval
  local dtype = hdf5[args.datatype]
  local species = args.species

  local Nd = int_1(dom.Nd)
  if comm then mpi.allreduce(mpi.in_place, Nd, 1, mpi.int, mpi.sum, comm) end
  Nd = Nd[0]

  local size = max(0, ceil((final - initial + 1)/interval))
  if comm then size = ceil(size/nranks) end
  local offset = size*rank

  local create = {
    position = function(group, step, time)
      local rd = array.new(cl_double3, Nd*size)
      local obs = h5md.create_observable(group, "position", step, time, dtype, {Nd, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "position")
        end,

        close = function()
          obs:close()
        end,

        append = function(count)
          ffi.copy(rd + Nd*count, dom.rd, dom.Nd*ffi.sizeof(cl_double3))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.rd, dom.Nd, dom.type_rd, rd, recvcount, recvdispl, dom.type_rd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(rd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd, 3})
          local dset = group:create_dataset("position", hdf5.double, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(rd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("position")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(rd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,

    image = function(group, step, time)
      local imd = array.new(cl_short3, Nd*size)
      local obs = h5md.create_observable(group, "image", step, time, hdf5.int16, {Nd, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "image")
        end,

        close = function()
          obs:close()
        end,

        append = function(count)
          ffi.copy(imd + Nd*count, dom.imd, dom.Nd*ffi.sizeof(cl_short3))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.imd, dom.Nd, dom.type_imd, imd, recvcount, recvdispl, dom.type_imd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(imd, hdf5.int16, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd, 3})
          local dset = group:create_dataset("image", hdf5.int16, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(imd, hdf5.int16, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("image")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(imd, hdf5.int16, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,

    velocity = function(group, step, time)
      local vd = array.new(cl_double3, Nd*size)
      local obs = h5md.create_observable(group, "velocity", step, time, dtype, {Nd, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "velocity")
        end,

        close = function()
          obs:close()
        end,

        append = function(count)
          ffi.copy(vd + Nd*count, dom.vd, dom.Nd*ffi.sizeof(cl_double3))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.vd, dom.Nd, dom.type_vd, vd, recvcount, recvdispl, dom.type_vd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(vd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd, 3})
          local dset = group:create_dataset("velocity", hdf5.double, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(vd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("velocity")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(vd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,

    force = function(group, step, time)
      local fd = array.new(cl_double3, Nd*size)
      local obs = h5md.create_observable(group, "force", step, time, dtype, {Nd, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "force")
        end,

        close = function()
          obs:close()
        end,

        append = function(count)
          ffi.copy(fd + Nd*count, dom.fd, dom.Nd*ffi.sizeof(cl_double3))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.fd, dom.Nd, dom.type_fd, fd, recvcount, recvdispl, dom.type_fd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(fd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd, 3})
          local dset = group:create_dataset("force", hdf5.double, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(fd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("force")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0, 0}, nil, {count, Nd, 3})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(fd, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,

    species = function(group, step, time)
      local spd = array.new(cl_char4, Nd*size)
      local dtype = hdf5.int8:enum_create()
      for i = 0, #species-1 do
        dtype:enum_insert(species[i+1], int8_t_1(i))
      end
      local obs = h5md.create_observable(group, "species", step, time, dtype, {Nd})
      dtype:close()
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "species")
          dtype = obs.value:get_type()
        end,

        close = function()
          dtype:close()
          obs:close()
        end,

        append = function(count)
          ffi.copy(spd + Nd*count, dom.spd, dom.Nd*ffi.sizeof(cl_char4))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.spd, dom.Nd, dom.type_spd, spd, recvcount, recvdispl, dom.type_spd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 1})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(spd, dtype, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd})
          local dset = group:create_dataset("species", hdf5.int8, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 1})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(spd, hdf5.int8, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("species")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd, 4})
          memspace:select_hyperslab("set", {0, 0, 0}, nil, {count, Nd, 1})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(spd, hdf5.int8, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,

    id = function(group, step, time)
      local idd = array.new(cl_int, Nd*size)
      local obs = h5md.create_observable(group, "id", step, time, hdf5.int32, {Nd})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "id")
        end,

        close = function()
          obs:close()
        end,

        append = function(count)
          ffi.copy(idd + Nd*count, dom.idd, dom.Nd*ffi.sizeof(cl_int))
        end,

        gather = function(recvcount, recvdispl, root)
          mpi.gatherv(dom.idd, dom.Nd, dom.type_idd, idd, recvcount, recvdispl, dom.type_idd, root, comm)
        end,

        write = function(count)
          local filespace = obs:append(count)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd})
          memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          obs.value:write(idd, hdf5.int32, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,

        snapshot = function(group, count)
          local filespace = hdf5.create_simple_space({count, Nd})
          local dset = group:create_dataset("id", hdf5.int32, filespace)
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd})
          memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:write(idd, hdf5.int32, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,

        restore = function(group, count)
          local dset = group:open_dataset("id")
          local filespace = dset:get_space()
          count = max(0, min(size, count - offset))
          filespace:select_hyperslab("set", {offset, 0}, nil, {count, Nd})
          local memspace = hdf5.create_simple_space({size, Nd})
          memspace:select_hyperslab("set", {0, 0}, nil, {count, Nd})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dset:read(idd, hdf5.int32, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
          dset:close()
        end,
      }
    end,
  }

  local group = file:create_group("particles/dimer")

  h5md.write_box(group, {L = box.L_global, periodic = box.periodic_global})

  local step = {interval = interval, offset = initial}
  local time = {interval = interval*timestep, offset = initial*timestep}

  local elements = {}
  for i, name in ipairs(args.elements) do
    elements[i] = create[name](group, step, time)
  end

  group:close()

  local recvdispl = comm and ffi.new("int[?]", nranks)
  local recvcount = comm and ffi.new("int[?]", nranks)

  local self = {step = initial}

  function self.observe()
    local group = file:open_group("particles/dimer")
    for _, element in ipairs(elements) do
      element.open(group)
    end
    group:close()
    local count = (self.step - initial) / interval
    while self.step <= final do
      coroutine.yield()
      if comm then
        local root = floor(count/size)
        mpi.gather(int_1(dom.Nd), 1, mpi.int, recvcount, 1, mpi.int, root, comm)
        if rank == root then
          recvdispl[0] = Nd * (count - size*rank)
          for i = 1, comm:size()-1 do
            recvdispl[i] = recvdispl[i-1] + recvcount[i-1]
          end
        end
        for _, element in ipairs(elements) do
          element.gather(recvcount, recvdispl, root)
        end
      else
        for _, element in ipairs(elements) do
          element.append(count)
        end
      end
      count = count + 1
      self.step = self.step + interval
    end
    for _, element in ipairs(elements) do
      element.write(count)
    end
    for _, element in ipairs(elements) do
      element.close()
    end
  end

  function self.snapshot(group)
    local count = (self.step - initial) / interval
    for _, element in ipairs(elements) do
      element.snapshot(group, count)
    end
  end

  function self.restore(group)
    local count = (self.step - initial) / interval
    for _, element in ipairs(elements) do
      element.restore(group, count)
    end
  end

  return self
end
