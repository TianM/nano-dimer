------------------------------------------------------------------------------
-- Count solvent species.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local ceil, max = math.ceil, math.max
-- Cache C types.
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local uint32_t_8 = ffi.typeof("uint32_t[8]")

return function(integrate, file, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local rank = comm and comm:rank() or 0
  local mpi = comm and require("mpi")
  local context, device, queue = dom.context, dom.device, dom.queue
  local timestep = integrate.timestep
  local species = args.species
  local initial, final, interval = args.initial, args.final, args.interval

  local program = compute.program(context, "nanomotor/species.cl", {
    dom = dom,
    species = species,
  })

  local sum do
    local kernel = program:create_kernel("species_sum")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]
    local num_groups = 128
    local glob_size = num_groups * work_size
    local count = ffi.new("cl_uint8[?]", num_groups)
    local d_count = context:create_buffer("use_host_ptr", ffi.sizeof(count), count)

    function sum()
      kernel:set_arg(0, dom.d_sps)
      kernel:set_arg(1, d_count)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_count, true, "read")
      local s = uint32_t_8()
      for i = 0, num_groups-1 do
        s[0] = s[0] + count[i].s0
        s[1] = s[1] + count[i].s1
        s[2] = s[2] + count[i].s2
        s[3] = s[3] + count[i].s3
        s[4] = s[4] + count[i].s4
        s[5] = s[5] + count[i].s5
        s[6] = s[6] + count[i].s6
        s[7] = s[7] + count[i].s7
      end
      queue:enqueue_unmap_mem_object(d_count, count)
      if comm then mpi.allreduce(mpi.in_place, s, 8, mpi.uint32, mpi.sum, comm) end
      return s
    end
  end

  local step = {interval = interval, offset = initial}
  local time = {interval = interval*timestep, offset = initial*timestep}
  local obs_species = h5md.create_observable(file, "observables/solvent/species", step, time, hdf5.uint32, {#species})
  obs_species:close()

  do
    local group = file:open_group("observables/solvent/species")
    local space_species = hdf5.create_simple_space({#species})
    local dtype_species = hdf5.c_s1:copy()
    dtype_species:set_size(2)
    local attr_species = group:create_attribute("species", dtype_species, space_species)
    attr_species:write(ffi.new("char[?][2]", #species, species), dtype_species)
    space_species:close()
    dtype_species:close()
    attr_species:close()
    group:close()
  end

  local size = max(0, ceil((final - initial + 1)/interval))
  local sum_species = ffi.new("uint32_t[?][8]", size)

  local count = 0

  local function write()
    if count == 0 then return end
    local filespace_species = obs_species:append(count)
    filespace_species:select_hyperslab("set", {0, 0}, nil, {count, #species})
    local memspace_species = hdf5.create_simple_space({size, 8})
    memspace_species:select_hyperslab("set", {0, 0}, nil, {count, #species})
    if rank == 0 then
      obs_species.value:write(sum_species, hdf5.uint32, memspace_species, filespace_species)
    end
    filespace_species:close()
    memspace_species:close()
    count = 0
  end

  local self = {step = initial}

  function self.observe()
    obs_species = h5md.open_observable(file, "observables/solvent/species")
    while self.step <= final do
      coroutine.yield()
      local value = sum()
      sum_species[count] = value
      count = count + 1
      self.step = self.step + interval
    end
    write()
    obs_species:close()
  end

  function self.snapshot(group)
    local filespace_species = hdf5.create_simple_space({count, #species})
    local dset_species = group:create_dataset("species", hdf5.uint32, filespace_species)
    local memspace_species = hdf5.create_simple_space({size, 8})
    memspace_species:select_hyperslab("set", {0, 0}, nil, {count, #species})
    if rank == 0 then
      dset_species:write(sum_species, hdf5.uint32, memspace_species, filespace_species)
    end
    filespace_species:close()
    memspace_species:close()
    dset_species:close()
  end

  function self.restore(group)
    local dset_species = group:open_dataset("species")
    local filespace_species = dset_species:get_space()
    count = filespace_species:get_simple_extent_dims()[1]
    local memspace_species = hdf5.create_simple_space({size, 8})
    memspace_species:select_hyperslab("set", {0, 0}, nil, {count, #species})
    if rank == 0 then
      dset_species:read(sum_species, hdf5.uint32, memspace_species, filespace_species)
    end
    filespace_species:close()
    memspace_species:close()
    dset_species:close()
  end

  return self
end
