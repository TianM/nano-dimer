------------------------------------------------------------------------------
-- Cumulative sum.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

-- Cache library functions.
local max, min = math.max, math.min

return function(queue, N, d_sum)
  local context = queue:get_info("context")
  local device = queue:get_info("device")
  local max_work_size = device:get_info("max_work_group_size")

  local w1, w2 = 1, 1
  while w1*w2 < N do
    w1 = w1*2
    w1, w2 = w2, w1
  end
  w1, w2 = max(w1, w2), min(w1, w2)

  local n1, n2 = 1, 1
  repeat
    n1, w1 = n1*2, w1/2
  until w1 <= max_work_size
  repeat
    n2, w2 = n2*2, w2/2
  until w2 <= max_work_size

  local program = compute.program(context, "nanomotor/cumsum.cl", {
    N = N,
    n = {n1, n2},
    w = {w1, w2},
  })

  local num_groups = w2*n2
  local d_work_sum = context:create_buffer(num_groups*ffi.sizeof("cl_uint"))
  local glob_sum = ffi.new("cl_uint[1]")
  local d_glob_sum = context:create_buffer("use_host_ptr", ffi.sizeof(glob_sum), glob_sum)
  local mem_queue = context:create_command_queue(device)
  local kernel1 = program:create_kernel("cumsum1")
  local kernel2 = program:create_kernel("cumsum2")
  local kernel3 = program:create_kernel("cumsum3")
  local work_size1 = {w1}
  local glob_size1 = {w1*num_groups}
  local work_size2 = {w2}
  local glob_size2 = {w2}

  return function()
    kernel1:set_arg(0, d_sum)
    kernel1:set_arg(1, d_work_sum)
    queue:enqueue_ndrange_kernel(kernel1, nil, glob_size1, work_size1)
    kernel2:set_arg(0, d_work_sum)
    kernel2:set_arg(1, d_glob_sum)
    local event = queue:enqueue_ndrange_kernel(kernel2, nil, glob_size2, work_size2)
    kernel3:set_arg(0, d_sum)
    kernel3:set_arg(1, d_work_sum)
    queue:enqueue_ndrange_kernel(kernel3, nil, glob_size1, work_size1)
    mem_queue:enqueue_map_buffer(d_glob_sum, true, "read", 0, ffi.sizeof(glob_sum), {event})
    local sum = glob_sum[0]
    mem_queue:enqueue_unmap_mem_object(d_glob_sum, glob_sum)
    return sum
  end
end
