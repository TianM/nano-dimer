/*
 * Count solvent species.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

|local bit = require("bit")
|local rshift = bit.rshift

|local work_size = 128

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
species_sum(__global const char4 *restrict d_sps,
            __global uint8 *restrict d_count,
            const uint Ns)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  const uint gsize = get_global_size(0);
  __local uint8 l_count[${work_size}];
  uint8 count = 0;
  for (uint i = gid; i < Ns; i += gsize) {
    const char sp = d_sps[i].s0;
    |for i = 0, #species-1 do
    if (sp == ${i}) count.s${i}++;
    |end
  }
  l_count[lid] = count;
  |local i = work_size
  |while i ~= 1 do
  |  i = rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_count[lid] += l_count[lid+${i}];
  |end
  if (lid == 0) d_count[wid] = l_count[0];
}
