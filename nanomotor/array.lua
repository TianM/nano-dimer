------------------------------------------------------------------------------
-- Allocate large-size C array.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local ffi = require("ffi")

ffi.cdef[[
void *calloc(size_t nmemb, size_t size);
void free(void *ptr);
]]

local _M = {}

function _M.new(ctype, nmemb)
  ctype = ffi.typeof(ctype)
  local ptr = ffi.C.calloc(nmemb, ffi.sizeof(ctype))
  if ptr == nil then return error("out of memory") end
  ptr = ffi.cast(ffi.typeof("$ *", ctype), ptr)
  return ffi.gc(ptr, ffi.C.free)
end

function _M.free(ptr)
  ffi.C.free(ffi.gc(ptr, nil))
end

return _M
