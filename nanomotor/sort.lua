------------------------------------------------------------------------------
-- Solvent particle sort.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local cumsum = require("nanomotor.cumsum")
local ffi = require("ffi")

-- Cache C types.
local cl_uint_1 = ffi.typeof("cl_uint[1]")

--[[
Sorts solvent particle after Hilbert space-filling curve.
]]
return function(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local interval = args and args.sort.interval

  -- Derive Hilbert edge lengths 2^M[i] not smaller than domain lengths.
  local L_local = box.L_local
  local M = {1, 1, 1}
  local nbin = {2, 2, 2}
  for i = 1, 3 do
    while nbin[i] < L_local[i] do M[i], nbin[i] = M[i]+1, nbin[i]*2 end
  end
  local nbins = nbin[1]*nbin[2]*nbin[3]

  local program = compute.program(context, "nanomotor/sort.cl", {
    dom = dom,
    box = box,
    M = M,
    nbin = nbin,
    nbins = nbins,
  })

  local self = {}

  local d_bin_offset = context:create_buffer(2*nbins*ffi.sizeof("cl_uint"))

  local fill do
    local kernel = program:create_kernel("sort_fill")
    local work_size = 64
    local glob_size = 2*nbins

    function fill()
      kernel:set_arg(0, d_bin_offset)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local count do
    local kernel = program:create_kernel("sort_count")
    local work_size = 256

    function count()
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, d_bin_offset)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      kernel:set_arg(3, cl_uint_1(dom.Ns_neigh))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local sum = cumsum(queue, 2*nbins, d_bin_offset)

  local permute do
    local kernel = program:create_kernel("sort_permute")
    local work_size = 32
    local d_rs = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_double3"))
    local d_vs = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_double3"))
    local d_sps = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_char4"))
    local d_ims = dom.d_ims and context:create_buffer(dom.Ns_max*ffi.sizeof("cl_short3"))
    local d_ids = dom.d_ids and context:create_buffer(dom.Ns_max*ffi.sizeof("cl_int"))

    function permute()
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, d_bin_offset)
      kernel:set_arg(1, dom.d_rs)
      kernel:set_arg(2, dom.d_vs)
      kernel:set_arg(3, dom.d_sps)
      kernel:set_arg(4, dom.d_ims)
      kernel:set_arg(5, dom.d_ids)
      kernel:set_arg(6, d_rs)
      kernel:set_arg(7, d_vs)
      kernel:set_arg(8, d_sps)
      kernel:set_arg(9, d_ims)
      kernel:set_arg(10, d_ids)
      kernel:set_arg(11, cl_uint_1(dom.Ns))
      kernel:set_arg(12, cl_uint_1(dom.Ns_neigh))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      dom.d_rs, d_rs = d_rs, dom.d_rs
      dom.d_vs, d_vs = d_vs, dom.d_vs
      dom.d_sps, d_sps = d_sps, dom.d_sps
      dom.d_ims, d_ims = d_ims, dom.d_ims
      dom.d_ids, d_ids = d_ids, dom.d_ids
    end
  end

  local step

  function self.update()
    fill()
    count()
    sum()
    permute()
    step = dom.step
  end

  function self.check_update()
    return dom.step >= step + interval
  end

  return self
end
