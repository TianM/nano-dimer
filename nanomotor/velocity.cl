/*
 * Initialise solvent velocities.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/random.cl"}

|local bit = require("bit")
|local ffi = require("ffi")

|local comm = box.comm
|local mpi = comm and require("mpi")
|local rank = comm and comm:rank() or 0

|local seed = ffi.new("uint32_t[2]")
|if rank == 0 then
|  for i = 0, 1 do
|    seed[i] = math.random(0, 0xffffffff)
|  end
|end
|if comm then mpi.bcast(seed, 2, mpi.uint32, 0, comm) end
|seed = "0x"..bit.tohex(seed[0])..bit.tohex(seed[1])

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
velocity_gaussian(__global double3 *restrict d_vs,
                  __global double4 *restrict d_en,
                  const uint Ns,
                  const ulong seed)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  __local double4 l_en[${work_size}];
  double en = 0;
  double3 vcm = 0;
  for (uint i = gid; i < Ns; i += ${num_groups*work_size}) {
    ulong4 c = (ulong4)(0, i + ${rank*dom.Ns_max}UL, seed, ${seed}UL);
    double2 v1 = random_normal(&c);
    double2 v2 = random_normal(&c);
    double3 v = (double3)(v1, v2.s0);
    v *= ${math.sqrt(temp)};
    en += dot(v, v);
    vcm += v;
    d_vs[i] = v;
  }
  l_en[lid] = (double4)(en, vcm);
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_en[lid] += l_en[lid+${i}];
  |end
  if (lid == 0) d_en[wid] = l_en[0];
}

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
velocity_shiftscale(__global double3 *restrict d_vs,
                    __global const double4 *restrict d_en,
                    const uint Ns)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  __local double4 l_en[${work_size}];
  double4 en = 0;
  for (uint i = lid; i < ${num_groups}; i += ${work_size}) {
    en += d_en[i];
  }
  l_en[lid] = en;
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_en[lid] += l_en[lid+${i}];
  |end
  barrier(CLK_LOCAL_MEM_FENCE);
  en = l_en[0]/Ns;
  for (uint i = gid; i < Ns; i += ${num_groups*work_size}) {
    double3 v = d_vs[i];
    double3 vcm = en.s123;
    v -= vcm;
    v *= ${math.sqrt(3*temp)} * rsqrt(en.s0 - dot(vcm, vcm));
    d_vs[i] = v;
  }
}
