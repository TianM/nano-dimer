/*
 * Cumulative sum.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

|local bit = require("bit")
|local lshift, rshift = bit.lshift, bit.rshift

/**
Computes exclusive cumulative sum of integer sequence.

See @Blelloch1990 and @Sengupta2007.
**/
|for k = 1, 2 do
__kernel void __attribute__((reqd_work_group_size(${w[k]}, 1, 1)))
cumsum${k}(__global uint *restrict d_sum,
           __global uint *restrict d_work_sum)
{
  const uint wid = get_group_id(0);
  const uint lid = get_local_id(0);
  __local uint l_sum[${n[k]*w[k]}];
  uint i;

  |for i = 0, n[k]-1 do
  l_sum[lid + ${i*w[k]}] = ((i = lid + wid*${n[k]*w[k]} + ${i*w[k]}) < ${N}) ? d_sum[i] : 0;
  |end
  barrier(CLK_LOCAL_MEM_FENCE);

  |local l, r = 1, n[k]*(w[k]/2)
  |while r > 0 do
  for (i = lid; i < ${r}; i += ${w[k]}) {
    l_sum[i*${2*l} + ${2*l-1}] += l_sum[i*${2*l} + ${l-1}];
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  |  l, r = lshift(l, 1), rshift(r, 1)
  |end

  if (lid == 0) {
    if (d_work_sum) d_work_sum[wid] = l_sum[${n[k]*w[k]-1}];
    l_sum[${n[k]*w[k]-1}] = 0;
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  |local l, r = 1, n[k]*(w[k]/2)
  |while r > 0 do
  for (i = lid; i < ${l}; i += ${w[k]}) {
    const uint s = l_sum[i*${2*r} + ${r-1}];
    const uint t = l_sum[i*${2*r} + ${2*r-1}];
    l_sum[i*${2*r} + ${r-1}] = t;
    l_sum[i*${2*r} + ${2*r-1}] = s + t;
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  |  l, r = lshift(l, 1), rshift(r, 1)
  |end

  |for i = 0, n[k]-1 do
  if ((i = lid + wid*${n[k]*w[k]} + ${i*w[k]}) < ${N}) d_sum[i] = l_sum[lid + ${i*w[k]}];
  |end
}
|end

__kernel void __attribute__((reqd_work_group_size(${w[1]}, 1, 1)))
cumsum3(__global uint *restrict d_sum,
        __global const uint *restrict d_work_sum)
{
  const uint wid = get_group_id(0);
  const uint lid = get_local_id(0);
  __local uint l_sum[1];
  uint i;

  if (lid == 0) l_sum[0] = d_work_sum[wid];
  barrier(CLK_LOCAL_MEM_FENCE);
  |for i = 0, n[1]-1 do
  if ((i = lid + wid*${n[1]*w[1]} + ${i*w[1]}) < ${N}) d_sum[i] += l_sum[0];
  |end
}
