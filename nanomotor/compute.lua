------------------------------------------------------------------------------
-- Load and build OpenCL programs.
-- Copyright © 2013–2015 Peter Colberg.
-- For conditions of distribution and use, see copyright notice in LICENSE.
------------------------------------------------------------------------------

local templet = require("templet")

local _M = {}

-- Derive template path from package path.
local path
for p in string.gmatch(package.path, "%f[^%z;]([^;]+%?)%.lua%f[%z;]") do
  if path then path = path..";"..p else path = p end
end

local mt = {__index = function(t, k) return rawget(_G, k) end}

--[[
Loads and expands the OpenCL source file in the given environment.
]]
local function loadfile(filename, env)
  local t
  local function include(filename)
    local filename, err = package.searchpath(filename, path, "/")
    if not filename then return error(err) end
    local status, template = pcall(templet.loadfile, filename)
    if not status then return error(template) end
    return template(t)
  end
  t = setmetatable({include = include}, mt)
  if env then t = setmetatable(env, {__index = t}) end
  return include(filename)
end

--[[
Builds the OpenCL program and prints warnings to standard error.
]]
local function build(program)
  local devices = program:get_info("devices")
  local status, err = pcall(program.build, program)
  for _, device in ipairs(devices) do
    io.stderr:write(program:get_build_info(device, "log"))
  end
  if not status then return error(err) end
  return program
end

--[[
Loads and builds an OpenCL program.
]]
function _M.program(context, filename, env)
  local status, source = pcall(loadfile, filename, env)
  if not status then return error(source) end
  local program = context:create_program_with_source(source)
  return build(program)
end

return _M
