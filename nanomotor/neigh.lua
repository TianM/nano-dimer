------------------------------------------------------------------------------
-- Verlet neighbour lists for dimer-solvent interaction.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double3 = ffi.typeof("cl_double3")
local cl_int = ffi.typeof("cl_int")
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local int_1 = ffi.typeof("int[1]")

return function(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local nbin = args.nbin
  local nbins = nbin[1]*nbin[2]*nbin[3]
  local bin_size, nlist_size = args.bin_size, args.nlist_size
  local sigma, r_cut, skin = args.sigma, args.r_cut, args.skin

  local program = compute.program(context, "nanomotor/neigh.cl", {
    dom = dom,
    box = box,
    nbin = nbin,
    nbins = nbins,
    bin_size = bin_size,
    nlist_size = nlist_size,
    sigma = sigma,
    r_cut = r_cut,
    skin = skin,
  })

  local self = {
    d_nlist_ds = context:create_buffer(dom.Nd_max*nlist_size.ds*ffi.sizeof("cl_uint")),
    d_nlist_count_ds = context:create_buffer(dom.Nd_max*ffi.sizeof("cl_uint")),
    d_nlist_dd = nlist_size.dd and context:create_buffer(dom.Nd_max*nlist_size.dd*ffi.sizeof("cl_uint")),
    d_nlist_count_dd = nlist_size.dd and context:create_buffer(dom.Nd_max*ffi.sizeof("cl_uint")),
    nlist_size = nlist_size,
  }

  local mem_queue = context:create_command_queue(device)
  local d_bin = context:create_buffer(nbins*bin_size*ffi.sizeof("cl_uint"))
  local d_bin_count = context:create_buffer(nbins*ffi.sizeof("cl_uint"))
  local bin_err = ffi.new("cl_uint[1]")
  local d_bin_err = context:create_buffer("use_host_ptr", ffi.sizeof(bin_err), bin_err)
  local nlist_err_ds = ffi.new("cl_uint[1]")
  local d_nlist_err_ds = context:create_buffer("use_host_ptr", ffi.sizeof(nlist_err_ds), nlist_err_ds)
  local nlist_err_dd = ffi.new("cl_uint[1]")
  local d_nlist_err_dd = context:create_buffer("use_host_ptr", ffi.sizeof(nlist_err_dd), nlist_err_dd)

  local fill do
    local kernel = program:create_kernel("neigh_fill")
    local work_size = 64
    local glob_size = math.ceil(nbins/work_size) * work_size

    function fill()
      kernel:set_arg(0, d_bin_count)
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  local bin do
    local kernel = program:create_kernel("neigh_bin")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]

    function bin()
      local glob_size = self.Nd*work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_spd)
      kernel:set_arg(2, dom.d_idd)
      kernel:set_arg(3, d_bin)
      kernel:set_arg(4, d_bin_count)
      kernel:set_arg(5, d_bin_err)
      kernel:set_arg(6, self.d_nlist_count_ds)
      kernel:set_arg(7, self.d_nlist_dd)
      kernel:set_arg(8, self.d_nlist_count_dd)
      kernel:set_arg(9, d_nlist_err_dd)
      mem_queue:enqueue_write_buffer(dom.d_rd, false, 0, self.Nd*ffi.sizeof(cl_double3), dom.rd)
      mem_queue:enqueue_write_buffer(dom.d_spd, false, 0, self.Nd*ffi.sizeof(cl_char4), dom.spd)
      local event = mem_queue:enqueue_write_buffer(dom.d_idd, true, 0, self.Nd*ffi.sizeof(cl_int), dom.idd)
      event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size}, {event})
      mem_queue:enqueue_wait_for_events({event})
    end
  end

  local count_neigh = ffi.new("cl_uint[1]")
  local d_count_neigh = context:create_buffer("use_host_ptr", ffi.sizeof(count_neigh), count_neigh)

  local count do
    local kernel = program:create_kernel("neigh_count")
    local work_size = 128

    function count()
      dom.Ns_neigh = 0
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_rs)
      kernel:set_arg(2, d_bin)
      kernel:set_arg(3, d_bin_count)
      kernel:set_arg(4, dom.d_spd)
      kernel:set_arg(5, dom.d_sps)
      kernel:set_arg(6, d_count_neigh)
      kernel:set_arg(7, cl_uint_1(dom.Ns))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_count_neigh, true, "write")
      dom.Ns_neigh = count_neigh[0]
      count_neigh[0] = 0
      queue:enqueue_unmap_mem_object(d_count_neigh, count_neigh)
    end
  end

  local d_offset_neigh = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_uint"))

  local offset do
    local kernel = program:create_kernel("neigh_offset")
    local work_size = 128

    function offset()
      local glob_offset = math.floor(dom.Ns_neigh/work_size) * work_size
      local glob_size = math.ceil(dom.Ns/work_size) * work_size - glob_offset
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_sps)
      kernel:set_arg(1, d_offset_neigh)
      kernel:set_arg(2, d_count_neigh)
      kernel:set_arg(3, cl_uint_1(dom.Ns))
      kernel:set_arg(4, cl_uint_1(dom.Ns_neigh))
      queue:enqueue_ndrange_kernel(kernel, {glob_offset}, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_count_neigh, true, "write")
      count_neigh[0] = 0
      queue:enqueue_unmap_mem_object(d_count_neigh, count_neigh)
    end
  end

  local permute do
    local kernel = program:create_kernel("neigh_permute")
    local work_size = 128

    function permute()
      local glob_size = math.ceil(dom.Ns_neigh/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, dom.d_vs)
      kernel:set_arg(2, dom.d_ims)
      kernel:set_arg(3, dom.d_sps)
      kernel:set_arg(4, dom.d_ids)
      kernel:set_arg(5, d_offset_neigh)
      kernel:set_arg(6, d_count_neigh)
      kernel:set_arg(7, cl_uint_1(dom.Ns_neigh))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_count_neigh, true, "write")
      count_neigh[0] = 0
      queue:enqueue_unmap_mem_object(d_count_neigh, count_neigh)
    end
  end

  local virtual

  function self.pre_update()
    self.Nd = dom.Nd
    if comm then virtual() end
    fill()
    bin()
    count()
    offset()
    permute()
  end

  do
    local kernel = program:create_kernel("neigh_update")
    local work_size = 128

    function self.update()
      local glob_size = math.ceil(dom.Ns_neigh/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rd)
      kernel:set_arg(1, dom.d_rs)
      kernel:set_arg(2, d_bin)
      kernel:set_arg(3, d_bin_count)
      kernel:set_arg(4, dom.d_spd)
      kernel:set_arg(5, self.d_nlist_ds)
      kernel:set_arg(6, self.d_nlist_count_ds)
      kernel:set_arg(7, d_nlist_err_ds)
      kernel:set_arg(8, cl_uint_1(dom.Ns_neigh))
      local event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      mem_queue:enqueue_map_buffer(d_bin_err, true, "read")
      if bin_err[0] ~= 0 then error("bin overflow of "..bin_err[0].." dimer spheres") end
      queue:enqueue_unmap_mem_object(d_bin_err, bin_err)
      mem_queue:enqueue_map_buffer(d_nlist_err_dd, true, "read")
      if nlist_err_dd[0] ~= 0 then error("neighbour list overflow of "..nlist_err_dd[0].." dimer spheres") end
      queue:enqueue_unmap_mem_object(d_nlist_err_dd, nlist_err_dd)
      mem_queue:enqueue_wait_for_events({event})
    end
  end

  function self.post_update()
    mem_queue:enqueue_map_buffer(d_nlist_err_ds, true, "read")
    if nlist_err_ds[0] ~= 0 then error("neighbour list overflow of "..nlist_err_ds[0].." solvent particles") end
    queue:enqueue_unmap_mem_object(d_nlist_err_ds, nlist_err_ds)
  end

  if comm then
    local rank_left, rank_right = mpi.cart_shift(comm, 0, 1)
    local rank_back, rank_front = mpi.cart_shift(comm, 1, 1)
    local rank_down, rank_up = mpi.cart_shift(comm, 2, 1)

    local mid_x = box.O_local[1] + box.L_local[1]/2
    local mid_y = box.O_local[2] + box.L_local[2]/2
    local mid_z = box.O_local[3] + box.L_local[3]/2

    local sigma_C = nlist_size.dd and math.max(sigma.CC, sigma.CN) or sigma.C
    local sigma_N = nlist_size.dd and math.max(sigma.NN, sigma.CN) or sigma.N

    local r_cut_C = r_cut*sigma_C + skin
    local r_cut_C_x = box.L_local[1]/2 - r_cut_C
    local r_cut_C_y = box.L_local[2]/2 - r_cut_C
    local r_cut_C_z = box.L_local[3]/2 - r_cut_C

    if rank_left or rank_right then assert(r_cut_C_x > 0) end
    if rank_back or rank_front then assert(r_cut_C_y > 0) end
    if rank_down or rank_up then assert(r_cut_C_z > 0) end

    local r_cut_N = r_cut*sigma_N + skin
    local r_cut_N_x = box.L_local[1]/2 - r_cut_N
    local r_cut_N_y = box.L_local[2]/2 - r_cut_N
    local r_cut_N_z = box.L_local[3]/2 - r_cut_N

    if rank_left or rank_right then assert(r_cut_N_x > 0) end
    if rank_back or rank_front then assert(r_cut_N_y > 0) end
    if rank_down or rank_up then assert(r_cut_N_z > 0) end

    local sendoffset_right = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_left = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_front = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_back = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_up = ffi.new("int[?]", dom.Nd_max)
    local sendoffset_down = ffi.new("int[?]", dom.Nd_max)

    function virtual()
      local sendcount_right = 0
      local sendcount_left = 0
      for i = 0, self.Nd-1 do
        local x = dom.rd[i].x - mid_x
        local y, z
        x, y, z = box.mindist(x, y, z)
        local r_cut_x = dom.spd[i].s0 == 0 and r_cut_C_x or r_cut_N_x
        if rank_right and x > r_cut_x then
          if sendcount_right >= dom.Nd_max then error("right send buffer overflow") end
          sendoffset_right[sendcount_right] = i
          sendcount_right = sendcount_right + 1
        elseif rank_left and x < -r_cut_x then
          if sendcount_left >= dom.Nd_max then error("left send buffer overflow") end
          sendoffset_left[sendcount_left] = i
          sendcount_left = sendcount_left + 1
        end
      end

      local recvoffset_left = int_1(self.Nd)
      local recvcount_left = int_1()
      mpi.sendrecv(int_1(sendcount_right), 1, mpi.int, rank_right, 0, recvcount_left, 1, mpi.int, rank_left, 0, comm)
      recvcount_left = recvcount_left[0]
      self.Nd = self.Nd + recvcount_left
      if self.Nd > dom.Nd_max then error("left receive buffer overflow") end

      self.sendtype_rd_right = mpi.type_create_indexed_block(sendcount_right, 1, sendoffset_right, dom.type_rd)
      self.sendtype_rd_right:commit()
      self.sendtype_spd_right = mpi.type_create_indexed_block(sendcount_right, 1, sendoffset_right, dom.type_spd)
      self.sendtype_spd_right:commit()
      self.sendtype_idd_right = mpi.type_create_indexed_block(sendcount_right, 1, sendoffset_right, dom.type_idd)
      self.sendtype_idd_right:commit()

      self.recvtype_rd_left = mpi.type_create_indexed_block(1, recvcount_left, recvoffset_left, dom.type_rd)
      self.recvtype_rd_left:commit()
      self.recvtype_spd_left = mpi.type_create_indexed_block(1, recvcount_left, recvoffset_left, dom.type_spd)
      self.recvtype_spd_left:commit()
      self.recvtype_idd_left = mpi.type_create_indexed_block(1, recvcount_left, recvoffset_left, dom.type_idd)
      self.recvtype_idd_left:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_right, rank_right, 0, dom.rd, 1, self.recvtype_rd_left, rank_left, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_right, rank_right, 0, dom.spd, 1, self.recvtype_spd_left, rank_left, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_right, rank_right, 0, dom.idd, 1, self.recvtype_idd_left, rank_left, 0, comm)

      local recvoffset_right = int_1(self.Nd)
      local recvcount_right = int_1()
      mpi.sendrecv(int_1(sendcount_left), 1, mpi.int, rank_left, 0, recvcount_right, 1, mpi.int, rank_right, 0, comm)
      recvcount_right = recvcount_right[0]
      self.Nd = self.Nd + recvcount_right
      if self.Nd > dom.Nd_max then error("right receive buffer overflow") end

      self.sendtype_rd_left = mpi.type_create_indexed_block(sendcount_left, 1, sendoffset_left, dom.type_rd)
      self.sendtype_rd_left:commit()
      self.sendtype_spd_left = mpi.type_create_indexed_block(sendcount_left, 1, sendoffset_left, dom.type_spd)
      self.sendtype_spd_left:commit()
      self.sendtype_idd_left = mpi.type_create_indexed_block(sendcount_left, 1, sendoffset_left, dom.type_idd)
      self.sendtype_idd_left:commit()

      self.recvtype_rd_right = mpi.type_create_indexed_block(1, recvcount_right, recvoffset_right, dom.type_rd)
      self.recvtype_rd_right:commit()
      self.recvtype_spd_right = mpi.type_create_indexed_block(1, recvcount_right, recvoffset_right, dom.type_spd)
      self.recvtype_spd_right:commit()
      self.recvtype_idd_right = mpi.type_create_indexed_block(1, recvcount_right, recvoffset_right, dom.type_idd)
      self.recvtype_idd_right:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_left, rank_left, 0, dom.rd, 1, self.recvtype_rd_right, rank_right, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_left, rank_left, 0, dom.spd, 1, self.recvtype_spd_right, rank_right, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_left, rank_left, 0, dom.idd, 1, self.recvtype_idd_right, rank_right, 0, comm)

      local sendcount_front = 0
      local sendcount_back = 0
      for i = 0, self.Nd-1 do
        local y = dom.rd[i].y - mid_y
        local x, z
        x, y, z = box.mindist(x, y, z)
        local r_cut_y = dom.spd[i].s0 == 0 and r_cut_C_y or r_cut_N_y
        if rank_front and y > r_cut_y then
          if sendcount_front >= dom.Nd_max then error("front send buffer overflow") end
          sendoffset_front[sendcount_front] = i
          sendcount_front = sendcount_front + 1
        elseif rank_back and y < -r_cut_y then
          if sendcount_back >= dom.Nd_max then error("back send buffer overflow") end
          sendoffset_back[sendcount_back] = i
          sendcount_back = sendcount_back + 1
        end
      end

      local recvoffset_back = int_1(self.Nd)
      local recvcount_back = int_1()
      mpi.sendrecv(int_1(sendcount_front), 1, mpi.int, rank_front, 0, recvcount_back, 1, mpi.int, rank_back, 0, comm)
      recvcount_back = recvcount_back[0]
      self.Nd = self.Nd + recvcount_back
      if self.Nd > dom.Nd_max then error("back receive buffer overflow") end

      self.sendtype_rd_front = mpi.type_create_indexed_block(sendcount_front, 1, sendoffset_front, dom.type_rd)
      self.sendtype_rd_front:commit()
      self.sendtype_spd_front = mpi.type_create_indexed_block(sendcount_front, 1, sendoffset_front, dom.type_spd)
      self.sendtype_spd_front:commit()
      self.sendtype_idd_front = mpi.type_create_indexed_block(sendcount_front, 1, sendoffset_front, dom.type_idd)
      self.sendtype_idd_front:commit()

      self.recvtype_rd_back = mpi.type_create_indexed_block(1, recvcount_back, recvoffset_back, dom.type_rd)
      self.recvtype_rd_back:commit()
      self.recvtype_spd_back = mpi.type_create_indexed_block(1, recvcount_back, recvoffset_back, dom.type_spd)
      self.recvtype_spd_back:commit()
      self.recvtype_idd_back = mpi.type_create_indexed_block(1, recvcount_back, recvoffset_back, dom.type_idd)
      self.recvtype_idd_back:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_front, rank_front, 0, dom.rd, 1, self.recvtype_rd_back, rank_back, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_front, rank_front, 0, dom.spd, 1, self.recvtype_spd_back, rank_back, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_front, rank_front, 0, dom.idd, 1, self.recvtype_idd_back, rank_back, 0, comm)

      local recvoffset_front = int_1(self.Nd)
      local recvcount_front = int_1()
      mpi.sendrecv(int_1(sendcount_back), 1, mpi.int, rank_back, 0, recvcount_front, 1, mpi.int, rank_front, 0, comm)
      recvcount_front = recvcount_front[0]
      self.Nd = self.Nd + recvcount_front
      if self.Nd > dom.Nd_max then error("front receive buffer overflow") end

      self.sendtype_rd_back = mpi.type_create_indexed_block(sendcount_back, 1, sendoffset_back, dom.type_rd)
      self.sendtype_rd_back:commit()
      self.sendtype_spd_back = mpi.type_create_indexed_block(sendcount_back, 1, sendoffset_back, dom.type_spd)
      self.sendtype_spd_back:commit()
      self.sendtype_idd_back = mpi.type_create_indexed_block(sendcount_back, 1, sendoffset_back, dom.type_idd)
      self.sendtype_idd_back:commit()

      self.recvtype_rd_front = mpi.type_create_indexed_block(1, recvcount_front, recvoffset_front, dom.type_rd)
      self.recvtype_rd_front:commit()
      self.recvtype_spd_front = mpi.type_create_indexed_block(1, recvcount_front, recvoffset_front, dom.type_spd)
      self.recvtype_spd_front:commit()
      self.recvtype_idd_front = mpi.type_create_indexed_block(1, recvcount_front, recvoffset_front, dom.type_idd)
      self.recvtype_idd_front:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_back, rank_back, 0, dom.rd, 1, self.recvtype_rd_front, rank_front, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_back, rank_back, 0, dom.spd, 1, self.recvtype_spd_front, rank_front, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_back, rank_back, 0, dom.idd, 1, self.recvtype_idd_front, rank_front, 0, comm)

      local sendcount_up = 0
      local sendcount_down = 0
      for i = 0, self.Nd-1 do
        local z = dom.rd[i].z - mid_z
        local x, y
        x, y, z = box.mindist(x, y, z)
        local r_cut_z = dom.spd[i].s0 == 0 and r_cut_C_z or r_cut_N_z
        if rank_up and z > r_cut_z then
          if sendcount_up >= dom.Nd_max then error("up send buffer overflow") end
          sendoffset_up[sendcount_up] = i
          sendcount_up = sendcount_up + 1
        elseif rank_down and z < -r_cut_z then
          if sendcount_down >= dom.Nd_max then error("down send buffer overflow") end
          sendoffset_down[sendcount_down] = i
          sendcount_down = sendcount_down + 1
        end
      end

      local recvoffset_down = int_1(self.Nd)
      local recvcount_down = int_1()
      mpi.sendrecv(int_1(sendcount_up), 1, mpi.int, rank_up, 0, recvcount_down, 1, mpi.int, rank_down, 0, comm)
      recvcount_down = recvcount_down[0]
      self.Nd = self.Nd + recvcount_down
      if self.Nd > dom.Nd_max then error("down receive buffer overflow") end

      self.sendtype_rd_up = mpi.type_create_indexed_block(sendcount_up, 1, sendoffset_up, dom.type_rd)
      self.sendtype_rd_up:commit()
      self.sendtype_spd_up = mpi.type_create_indexed_block(sendcount_up, 1, sendoffset_up, dom.type_spd)
      self.sendtype_spd_up:commit()
      self.sendtype_idd_up = mpi.type_create_indexed_block(sendcount_up, 1, sendoffset_up, dom.type_idd)
      self.sendtype_idd_up:commit()

      self.recvtype_rd_down = mpi.type_create_indexed_block(1, recvcount_down, recvoffset_down, dom.type_rd)
      self.recvtype_rd_down:commit()
      self.recvtype_spd_down = mpi.type_create_indexed_block(1, recvcount_down, recvoffset_down, dom.type_spd)
      self.recvtype_spd_down:commit()
      self.recvtype_idd_down = mpi.type_create_indexed_block(1, recvcount_down, recvoffset_down, dom.type_idd)
      self.recvtype_idd_down:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_up, rank_up, 0, dom.rd, 1, self.recvtype_rd_down, rank_down, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_up, rank_up, 0, dom.spd, 1, self.recvtype_spd_down, rank_down, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_up, rank_up, 0, dom.idd, 1, self.recvtype_idd_down, rank_down, 0, comm)

      local recvoffset_up = int_1(self.Nd)
      local recvcount_up = int_1()
      mpi.sendrecv(int_1(sendcount_down), 1, mpi.int, rank_down, 0, recvcount_up, 1, mpi.int, rank_up, 0, comm)
      recvcount_up = recvcount_up[0]
      self.Nd = self.Nd + recvcount_up
      if self.Nd > dom.Nd_max then error("up receive buffer overflow") end

      self.sendtype_rd_down = mpi.type_create_indexed_block(sendcount_down, 1, sendoffset_down, dom.type_rd)
      self.sendtype_rd_down:commit()
      self.sendtype_spd_down = mpi.type_create_indexed_block(sendcount_down, 1, sendoffset_down, dom.type_spd)
      self.sendtype_spd_down:commit()
      self.sendtype_idd_down = mpi.type_create_indexed_block(sendcount_down, 1, sendoffset_down, dom.type_idd)
      self.sendtype_idd_down:commit()

      self.recvtype_rd_up = mpi.type_create_indexed_block(1, recvcount_up, recvoffset_up, dom.type_rd)
      self.recvtype_rd_up:commit()
      self.recvtype_spd_up = mpi.type_create_indexed_block(1, recvcount_up, recvoffset_up, dom.type_spd)
      self.recvtype_spd_up:commit()
      self.recvtype_idd_up = mpi.type_create_indexed_block(1, recvcount_up, recvoffset_up, dom.type_idd)
      self.recvtype_idd_up:commit()

      mpi.sendrecv(dom.rd, 1, self.sendtype_rd_down, rank_down, 0, dom.rd, 1, self.recvtype_rd_up, rank_up, 0, comm)
      mpi.sendrecv(dom.spd, 1, self.sendtype_spd_down, rank_down, 0, dom.spd, 1, self.recvtype_spd_up, rank_up, 0, comm)
      mpi.sendrecv(dom.idd, 1, self.sendtype_idd_down, rank_down, 0, dom.idd, 1, self.recvtype_idd_up, rank_up, 0, comm)
    end
  end

  return self
end
