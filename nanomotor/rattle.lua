------------------------------------------------------------------------------
-- RATTLE algorithm for dimer spheres.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local ffi = require("ffi")

-- Cache C types.
local cl_double3 = ffi.typeof("cl_double3")
local int_1 = ffi.typeof("int[1]")
local uint32_t_1 = ffi.typeof("uint32_t[1]")

local particle = ffi.typeof[[
struct {
  cl_double3 r[2], v[2], f[2];
  cl_short3 im[2];
  cl_char4 sp[2];
  cl_int id[2];
}
]]
local particle_n = ffi.typeof("$[?]", particle)

--[[
Integrates equations of motion with bond-length constraint.

See @Andersen1983.
--]]
return function(dom, args)
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local bond = args.bond
  local timestep = args.timestep
  local skin = args.skin

  local self = {}

  do
    local h_C = timestep / (2*dom.mass.C)
    local h_N = timestep / (2*dom.mass.N)
    local m_C = 1 / (2*timestep * (1 + dom.mass.C/dom.mass.N))
    local m_N = 1 / (2*timestep * (1 + dom.mass.N/dom.mass.C))

    function self.integrate()
      local rd, imd, vd, fd, spd = dom.rd, dom.imd, dom.vd, dom.fd, dom.spd
      for i = 0, dom.Nd-1 do
        local h = spd[i].s0 == 0 and h_C or h_N
        vd[i].x = vd[i].x + fd[i].x*h
        vd[i].y = vd[i].y + fd[i].y*h
        vd[i].z = vd[i].z + fd[i].z*h
      end
      for i = 0, dom.Nd-1, 2 do
        local m1 = spd[i].s0 == 0 and m_C or m_N
        local m2 = spd[i+1].s0 == 0 and m_C or m_N
        local zx = rd[i].x - rd[i+1].x
        local zy = rd[i].y - rd[i+1].y
        local zz = rd[i].z - rd[i+1].z
        zx, zy, zz = box.mindist(zx, zy, zz)
        local sx = zx + timestep*(vd[i].x - vd[i+1].x)
        local sy = zy + timestep*(vd[i].y - vd[i+1].y)
        local sz = zz + timestep*(vd[i].z - vd[i+1].z)
        local g = (sx*sx + sy*sy + sz*sz - bond^2) / (sx*zx + sy*zy + sz*zz)
        vd[i].x = vd[i].x - (g*m1)*zx
        vd[i].y = vd[i].y - (g*m1)*zy
        vd[i].z = vd[i].z - (g*m1)*zz
        vd[i+1].x = vd[i+1].x + (g*m2)*zx
        vd[i+1].y = vd[i+1].y + (g*m2)*zy
        vd[i+1].z = vd[i+1].z + (g*m2)*zz
      end
      for i = 0, dom.Nd-1 do
        local x, y, z
        rd[i].x = rd[i].x + vd[i].x*timestep
        rd[i].y = rd[i].y + vd[i].y*timestep
        rd[i].z = rd[i].z + vd[i].z*timestep
        rd[i].x, rd[i].y, rd[i].z, x, y, z = box.minimage(rd[i].x, rd[i].y, rd[i].z)
        if x then imd[i].x = imd[i].x + x end
        if y then imd[i].y = imd[i].y + y end
        if z then imd[i].z = imd[i].z + z end
      end
    end
  end

  do
    local h_C = timestep / (2*dom.mass.C)
    local h_N = timestep / (2*dom.mass.N)
    local m_C = 1 / (bond^2 * (1 + dom.mass.C/dom.mass.N))
    local m_N = 1 / (bond^2 * (1 + dom.mass.N/dom.mass.C))

    function self.finalize()
      local rd, vd, fd, spd = dom.rd, dom.vd, dom.fd, dom.spd
      for i = 0, dom.Nd-1 do
        local h = spd[i].s0 == 0 and h_C or h_N
        vd[i].x = vd[i].x + fd[i].x*h
        vd[i].y = vd[i].y + fd[i].y*h
        vd[i].z = vd[i].z + fd[i].z*h
      end
      for i = 0, dom.Nd-1, 2 do
        local m1 = spd[i].s0 == 0 and m_C or m_N
        local m2 = spd[i+1].s0 == 0 and m_C or m_N
        local zx = rd[i].x - rd[i+1].x
        local zy = rd[i].y - rd[i+1].y
        local zz = rd[i].z - rd[i+1].z
        zx, zy, zz = box.mindist(zx, zy, zz)
        local wx = vd[i].x - vd[i+1].x
        local wy = vd[i].y - vd[i+1].y
        local wz = vd[i].z - vd[i+1].z
        local k = zx*wx + zy*wy + zz*wz
        vd[i].x = vd[i].x - (k*m1)*zx
        vd[i].y = vd[i].y - (k*m1)*zy
        vd[i].z = vd[i].z - (k*m1)*zz
        vd[i+1].x = vd[i+1].x + (k*m2)*zx
        vd[i+1].y = vd[i+1].y + (k*m2)*zy
        vd[i+1].z = vd[i+1].z + (k*m2)*zz
      end
    end
  end

  local neigh = false

  do
    local rd0 = ffi.new("cl_double3[?]", dom.Nd_max)

    function self.check_neigh()
      if neigh then return true end
      local rd = dom.rd
      local count = uint32_t_1()
      for i = 0, dom.Nd-1 do
        local dx = rd[i].x - rd0[i].x
        local dy = rd[i].y - rd0[i].y
        local dz = rd[i].z - rd0[i].z
        dx, dy, dz = box.mindist(dx, dy, dz)
        if dx*dx + dy*dy + dz*dz > (skin/2)^2 then
          count[0] = 1
          break
        end
      end
      if comm then mpi.allreduce(mpi.in_place, count, 1, mpi.uint32, mpi.max, comm) end
      return count[0] ~= 0
    end

    function self.post_neigh()
      ffi.copy(rd0, dom.rd, dom.Nd*ffi.sizeof(cl_double3))
      neigh = false
    end
  end

  function self.compute_en()
    local m_C, m_N = dom.mass.C, dom.mass.N
    local vd = dom.vd
    local en_kin = 0
    local px_cm, py_cm, pz_cm = 0, 0, 0
    for i = 0, dom.Nd-1, 2 do
      en_kin = en_kin + m_C*vd[i].x*vd[i].x + m_N*vd[i+1].x*vd[i+1].x
      en_kin = en_kin + m_C*vd[i].y*vd[i].y + m_N*vd[i+1].y*vd[i+1].y
      en_kin = en_kin + m_C*vd[i].z*vd[i].z + m_N*vd[i+1].z*vd[i+1].z
      px_cm = px_cm + m_C*vd[i].x + m_N*vd[i+1].x
      py_cm = py_cm + m_C*vd[i].y + m_N*vd[i+1].y
      pz_cm = pz_cm + m_C*vd[i].z + m_N*vd[i+1].z
    end
    en_kin = 0.5*en_kin
    return en_kin, {px_cm, py_cm, pz_cm}
  end

  if comm then
    local lengths = ffi.new("int[6]", 2)
    local displs = ffi.new("MPI_Aint[6]", ffi.offsetof(particle, "r"), ffi.offsetof(particle, "v"), ffi.offsetof(particle, "f"), ffi.offsetof(particle, "im"), ffi.offsetof(particle, "sp"), ffi.offsetof(particle, "id"))
    local types = ffi.new("MPI_Datatype[6]", dom.type_rd, dom.type_vd, dom.type_fd, dom.type_imd, dom.type_spd, dom.type_idd)
    local type_part = mpi.type_create_struct(6, lengths, displs, types)
    type_part = mpi.type_create_resized(type_part, 0, ffi.sizeof(particle))
    type_part:commit()

    local rank_left, rank_right = mpi.cart_shift(comm, 0, 1)
    local rank_back, rank_front = mpi.cart_shift(comm, 1, 1)
    local rank_down, rank_up = mpi.cart_shift(comm, 2, 1)

    local coords_right = rank_right and mpi.cart_coords(comm, rank_right)
    local coords_left = rank_left and mpi.cart_coords(comm, rank_left)
    local coords_front = rank_front and mpi.cart_coords(comm, rank_front)
    local coords_back = rank_back and mpi.cart_coords(comm, rank_back)
    local coords_up = rank_up and mpi.cart_coords(comm, rank_up)
    local coords_down = rank_down and mpi.cart_coords(comm, rank_down)

    local sendpart_right = rank_right and particle_n(dom.Nd_max)
    local sendpart_left = rank_left and particle_n(dom.Nd_max)
    local sendpart_front = rank_front and particle_n(dom.Nd_max)
    local sendpart_back = rank_back and particle_n(dom.Nd_max)
    local sendpart_up = rank_up and particle_n(dom.Nd_max)
    local sendpart_down = rank_down and particle_n(dom.Nd_max)
    local recvpart = particle_n(dom.Nd_max)

    local sendcount_right = int_1()
    local sendcount_left = int_1()
    local sendcount_front = int_1()
    local sendcount_back = int_1()
    local sendcount_up = int_1()
    local sendcount_down = int_1()
    local recvcount = int_1()

    local function append_dom(i)
      local j = dom.Nd
      if j + 2 > dom.Nd_max then error("receive buffer overflow") end
      dom.rd[j], dom.rd[j+1] = recvpart[i].r[0], recvpart[i].r[1]
      dom.vd[j], dom.vd[j+1] = recvpart[i].v[0], recvpart[i].v[1]
      dom.fd[j], dom.fd[j+1] = recvpart[i].f[0], recvpart[i].f[1]
      dom.imd[j], dom.imd[j+1] = recvpart[i].im[0], recvpart[i].im[1]
      dom.spd[j], dom.spd[j+1] = recvpart[i].sp[0], recvpart[i].sp[1]
      dom.idd[j], dom.idd[j+1] = recvpart[i].id[0], recvpart[i].id[1]
      dom.Nd = j + 2
    end

    local function append_z(i)
      local z = recvpart[i].r[0].z
      local x, y
      x, y, z = box.mincoords(x, y, z)
      if rank_up and z == coords_up[3] then
        local j = sendcount_up[0]
        if j>= dom.Nd_max then return error("up send buffer overflow") end
        sendpart_up[j] = recvpart[i]
        sendcount_up[0] = j + 1
      elseif rank_down and z == coords_down[3] then
        local j = sendcount_down[0]
        if j>= dom.Nd_max then return error("down send buffer overflow") end
        sendpart_down[j] = recvpart[i]
        sendcount_down[0] = j + 1
      else
        return append_dom(i)
      end
    end

    local function append_yz(i)
      local y = recvpart[i].r[0].y
      local x, z
      x, y, z = box.mincoords(x, y, z)
      if rank_front and y == coords_front[2] then
        local j = sendcount_front[0]
        if j>= dom.Nd_max then return error("front send buffer overflow") end
        sendpart_front[j] = recvpart[i]
        sendcount_front[0] = j + 1
      elseif rank_back and y == coords_back[2] then
        local j = sendcount_back[0]
        if j>= dom.Nd_max then return error("back send buffer overflow") end
        sendpart_back[j] = recvpart[i]
        sendcount_back[0] = j + 1
      else
        return append_z(i)
      end
    end

    local function append_xyz(i)
      local x = recvpart[i].r[0].x
      local y, z
      x, y, z = box.mincoords(x, y, z)
      if rank_right and x == coords_right[1] then
        local j = sendcount_right[0]
        if j>= dom.Nd_max then return error("right send buffer overflow") end
        sendpart_right[j] = recvpart[i]
        sendcount_right[0] = j + 1
      elseif rank_left and x == coords_left[1] then
        local j = sendcount_left[0]
        if j>= dom.Nd_max then return error("left send buffer overflow") end
        sendpart_left[j] = recvpart[i]
        sendcount_left[0] = j + 1
      else
        return append_yz(i)
      end
    end

    function self.send()
      sendcount_left[0] = 0
      sendcount_right[0] = 0
      sendcount_back[0] = 0
      sendcount_front[0] = 0
      sendcount_down[0] = 0
      sendcount_up[0] = 0
      recvcount[0] = dom.Nd/2

      for i = 0, recvcount[0]-1 do
        recvpart[i].r[0], recvpart[i].r[1] = dom.rd[2*i], dom.rd[2*i+1]
        recvpart[i].v[0], recvpart[i].v[1] = dom.vd[2*i], dom.vd[2*i+1]
        recvpart[i].f[0], recvpart[i].f[1] = dom.fd[2*i], dom.fd[2*i+1]
        recvpart[i].im[0], recvpart[i].im[1] = dom.imd[2*i], dom.imd[2*i+1]
        recvpart[i].sp[0], recvpart[i].sp[1] = dom.spd[2*i], dom.spd[2*i+1]
        recvpart[i].id[0], recvpart[i].id[1] = dom.idd[2*i], dom.idd[2*i+1]
      end

      dom.Nd = 0
      for i = 0, recvcount[0]-1 do
        append_xyz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_right, 1, mpi.int, rank_right, 0, recvcount, 1, mpi.int, rank_left, 0, comm)
      mpi.sendrecv(sendpart_right, sendcount_right[0], type_part, rank_right, 0, recvpart, recvcount[0], type_part, rank_left, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_yz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_left, 1, mpi.int, rank_left, 0, recvcount, 1, mpi.int, rank_right, 0, comm)
      mpi.sendrecv(sendpart_left, sendcount_left[0], type_part, rank_left, 0, recvpart, recvcount[0], type_part, rank_right, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_yz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_front, 1, mpi.int, rank_front, 0, recvcount, 1, mpi.int, rank_back, 0, comm)
      mpi.sendrecv(sendpart_front, sendcount_front[0], type_part, rank_front, 0, recvpart, recvcount[0], type_part, rank_back, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_z(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_back, 1, mpi.int, rank_back, 0, recvcount, 1, mpi.int, rank_front, 0, comm)
      mpi.sendrecv(sendpart_back, sendcount_back[0], type_part, rank_back, 0, recvpart, recvcount[0], type_part, rank_front, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_z(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_up, 1, mpi.int, rank_up, 0, recvcount, 1, mpi.int, rank_down, 0, comm)
      mpi.sendrecv(sendpart_up, sendcount_up[0], type_part, rank_up, 0, recvpart, recvcount[0], type_part, rank_down, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_dom(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_down, 1, mpi.int, rank_down, 0, recvcount, 1, mpi.int, rank_up, 0, comm)
      mpi.sendrecv(sendpart_down, sendcount_down[0], type_part, rank_down, 0, recvpart, recvcount[0], type_part, rank_up, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_dom(i)
      end

      neigh = true
    end
  end

  return self
end
