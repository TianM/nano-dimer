/*
 * Solvent particle sort.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}
${include "nanomotor/hilbert.cl"}

__kernel void
sort_fill(__global uint *restrict d_bin_offset)
{
  const uint gid = get_global_id(0);
  d_bin_offset[gid] = 0;
}

__kernel void
sort_count(__global const double3 *restrict d_rs,
           __global uint *restrict d_bin_offset,
           const uint Ns,
           const uint Ns_neigh)
{
  const uint gid = get_global_id(0);
  const uint3 nbin = (uint3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
  if (gid < Ns) {
    const double3 r = d_rs[gid];
    const uint3 cell = min(convert_uint3_rtz(minlocal(r)), nbin-1);
    uint index = hilbert_index(cell);
    if (gid >= Ns_neigh) index += ${nbins};
    atomic_inc(&d_bin_offset[index]);
  }
}

__kernel void
sort_permute(__global uint *restrict d_bin_offset,
             __global const double3 *restrict d_rs1,
             __global const double3 *restrict d_vs1,
             __global const char4 *restrict d_sps1,
             __global const short3 *restrict d_ims1,
             __global const int *restrict d_ids1,
             __global double3 *restrict d_rs2,
             __global double3 *restrict d_vs2,
             __global char4 *restrict d_sps2,
             __global short3 *restrict d_ims2,
             __global int *restrict d_ids2,
             const uint Ns,
             const uint Ns_neigh)
{
  const uint gid = get_global_id(0);
  const uint3 nbin = (uint3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
  if (gid < Ns) {
    const double3 r = d_rs1[gid];
    const uint3 cell = min(convert_uint3_rtz(minlocal(r)), nbin-1);
    uint index = hilbert_index(cell);
    if (gid >= Ns_neigh) index += ${nbins};
    const uint offset = atomic_inc(&d_bin_offset[index]);
    d_rs2[offset] = r;
    d_vs2[offset] = d_vs1[gid];
    d_sps2[offset] = d_sps1[gid];
    if (d_ims1) d_ims2[offset] = d_ims1[gid];
    if (d_ids1) d_ids2[offset] = d_ids1[gid];
  }
}
