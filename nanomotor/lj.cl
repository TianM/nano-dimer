/*
 * Lennard-Jones potential for dimer-solvent and dimer-dimer interactions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}

|local bit = require("bit")

|local L_global, periodic_global = box.L_global, box.periodic_global

|local work_size = 64

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
lj_update(__global const double3 *restrict d_rd,
          __global const double3 *restrict d_rs,
          __global double3 *restrict d_fd,
          __global double3 *restrict d_fs,
          __global const char4 *restrict d_spd,
          __global uint *restrict d_sps,
          __global const uint *restrict d_nlist_count_ds,
          __global const uint *restrict d_nlist_ds,
          __global const uint *restrict d_nlist_count_dd,
          __global const uint *restrict d_nlist_dd,
          __global double2 *restrict d_en,
          __global uint *restrict d_err,
          const uint Nd)
{
  const uint gid = get_global_id(0);
  const uint lid = get_local_id(0);
  const uint wid = get_group_id(0);
  __local double3 l_fd[${work_size}];
  __local double2 l_en[${work_size}];
  double en = 0;
  double vir = 0;
  double3 f = 0;
  const double3 rd = d_rd[wid];
  const char spd = d_spd[wid].s0;
  {
    const double r_cut2 = select((double)(${(r_cut*sigma.C)^2}), (double)(${(r_cut*sigma.N)^2}), (long)(spd));
    const double sigma2 = select((double)(${sigma.C^2}), (double)(${sigma.N^2}), (long)(spd));
    const double2 epsilon = select((double2)(${epsilon.CA}, ${epsilon.CB}), (double2)(${epsilon.NA}, ${epsilon.NB}), -(long2)(spd));
    const uint count = min(d_nlist_count_ds[wid], (uint)(${nlist_size.ds}));
    for (uint i = lid; i < count; i += ${work_size}) {
      const uint j = d_nlist_ds[i + wid*${nlist_size.ds}];
      const double3 d = mindist(rd - d_rs[j]);
      const double r2 = dot(d, d);
      if (r2 < r_cut2) {
        const char4 sps = as_char4(atomic_or(&d_sps[j], as_uint((char4)(0, 1, 0, 0))));
        if (sps.s1 != 0) atomic_inc(d_err);
        const double epsilon1 = select(epsilon.s0, epsilon.s1, (long)(sps.s0));
        const double r2i = sigma2 / r2;
        const double r6i = r2i*r2i*r2i;
        const double fc = 48*epsilon1*r2i*r6i*(r6i - 0.5) / sigma2;
        const double3 fd = fc * d;
        f += fd;
        en += epsilon1*(4*r6i*(r6i - 1) - ${4*(r_cut^-12 - r_cut^-6)});
        vir += fc * r2;
        d_fs[j] = -fd;
        |if reaction then
        if (spd == 0 && sps.s0 == 0) atomic_or(&d_sps[j], as_uint((char4)(1, 0, 0, 0)));
        |end
      }
    }
  }
  |if nlist_size.dd then
  {
    const double2 epsilon1 = select((double2)(${epsilon.CC}, ${epsilon.CN}), (double2)(${epsilon.CN}, ${epsilon.NN}), (long2)(-spd));
    const double2 sigma21 = select((double2)(${sigma.CC^2}, ${sigma.CN^2}), (double2)(${sigma.CN^2}, ${sigma.NN^2}), (long2)(-spd));
    const double2 r_cut21 = select((double2)(${(r_cut*sigma.CC)^2}, ${(r_cut*sigma.CN)^2}), (double2)(${(r_cut*sigma.CN)^2}, ${(r_cut*sigma.NN)^2}), (long2)(-spd));
    const uint count = min(d_nlist_count_dd[wid], (uint)(${nlist_size.dd}));
    for (uint i = lid; i < count; i += ${work_size}) {
      const uint j = d_nlist_dd[i + wid*${nlist_size.dd}];
      const char spd = d_spd[j].s0;
      const double epsilon = select(epsilon1.s0, epsilon1.s1, (long)(spd));
      const double sigma2 = select(sigma21.s0, sigma21.s1, (long)(spd));
      const double r_cut2 = select(r_cut21.s0, r_cut21.s1, (long)(spd));
      const double3 d = mindist(rd - d_rd[j]);
      const double r2 = dot(d, d);
      if (r2 < r_cut2) {
        const double r2i = sigma2 / r2;
        const double r6i = r2i*r2i*r2i;
        const double fc = 48*epsilon*r2i*r6i*(r6i - 0.5) / sigma2;
        f += fc * d;
        en += epsilon*(2*r6i*(r6i - 1) - ${2*(r_cut^-12 - r_cut^-6)});
        vir += 0.5 * fc * r2;
      }
    }
  }
  |end
  |for i = 1, 3 do
  |  if not periodic_global[i] then
  |    local r_cut, epsilon, sigma, dz = wall.r_cut, wall.epsilon, wall.sigma, wall.dz
  |    for _, sign in ipairs({1, -1}) do
  if (wid < Nd && lid == 0) {
    const double z = ${L_global[i]/2 - sign*L_global[i]/2} + ${sign}*rd.s${i-1} - ${dz};
    const double sigma = select((double)(${sigma.C}), (double)(${sigma.N}), (long)(spd));
    |if r_cut then
    const double r_cut = select((double)(${r_cut*sigma.C}), (double)(${r_cut*sigma.N}), (long)(spd)) - ${dz};
    if (z < r_cut)
    |end
    {
      const double zi = sigma / z;
      const double z2i = zi*zi;
      const double z4i = z2i*z2i;
      const double fc = ${3^3.5/2*epsilon}*z4i*(z4i*z2i - ${1/3}) / sigma;
      f.s${i-1} += ${sign}*fc;
      en += ${3^1.5/2*epsilon}*zi*(z4i*z4i - z2i);
      |if r_cut then
      en -= ${3^1.5/2*epsilon*(r_cut^-9 - r_cut^-3)};
      |end
      vir += fc * z;
    }
  }
  |    end
  |  end
  |end

  l_fd[lid] = f;
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_fd[lid] += l_fd[lid+${i}];
  |end
  if (lid == 0) d_fd[wid] = l_fd[0];

  l_en[lid] = (double2)(en, vir);
  |local i = work_size
  |while i > 1 do
  |  i = bit.rshift(i, 1)
  barrier(CLK_LOCAL_MEM_FENCE);
  if (lid < ${i}) l_en[lid] += l_en[lid+${i}];
  |end
  if (lid == 0) d_en[wid] = l_en[0];
}
