/*
 * Initialise solvent positions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

#if __OPENCL_VERSION__ < 120
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

${include "nanomotor/box.cl"}
${include "nanomotor/random.cl"}

|local bit = require("bit")
|local ffi = require("ffi")

|local comm = box.comm
|local mpi = comm and require("mpi")
|local rank = comm and comm:rank() or 0

|local seed = ffi.new("uint32_t[2]")
|if rank == 0 then
|  for i = 0, 1 do
|    seed[i] = math.random(0, 0xffffffff)
|  end
|end
|if comm then mpi.bcast(seed, 2, mpi.uint32, 0, comm) end
|seed = "0x"..bit.tohex(seed[0])..bit.tohex(seed[1])

|local L_local, O_local, periodic_local = box.L_local, box.O_local, box.periodic_local

__kernel void
position_fill(__global uint *restrict d_count)
{
  const uint gid = get_global_id(0);
  if (gid < ${nbins}) d_count[gid] = 0;
}

|local work_size = 64

__kernel void __attribute__((reqd_work_group_size(${work_size}, 1, 1)))
position_bin(__global const double3 *restrict d_rd,
             __global const char4 *restrict d_spd,
             __global uint *restrict d_count)
{
  const uint wid = get_group_id(0);
  const uint lid = get_local_id(0);
  const int3 nbin = (int3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
  const double3 nbin_frac = (double3)(${nbin[1]/L_local[1]}, ${nbin[2]/L_local[2]}, ${nbin[3]/L_local[3]});
  const double3 edges = (double3)(${L_local[1]/nbin[1]}, ${L_local[2]/nbin[2]}, ${L_local[3]/nbin[3]});
  const double3 O_local = (double3)(${O_local[1]}, ${O_local[2]}, ${O_local[3]});
  const char spd = d_spd[wid].s0;
  const double sigma = select((double)(${sigma.C}), (double)(${sigma.N}), (long)(spd));
  const double r_cut = sigma + ${math.sqrt(3)/2};
  const double r_cut2 = r_cut*r_cut;
  const double3 rd = d_rd[wid];
  const double3 r = minlocal(rd);
  const int3 p = convert_int3_rtn((r - sigma)*nbin_frac);
  const int3 q = convert_int3_rtp((r + sigma)*nbin_frac);
  const int3 periods = -(int3)(${periodic_local[1]}, ${periodic_local[2]}, ${periodic_local[3]});
  const int3 m = select(max(0, p), p + nbin, periods);
  const int3 n = select(min(nbin, q), q + nbin, periods);
  for (int z = m.z; z < n.z; z++) {
    for (int y = m.y + lid/8; y < n.y; y += ${work_size/8}) {
      for (int x = m.x + lid%8; x < n.x; x += 8) {
        const uint3 cell = (uint3)(x%${nbin[1]}, y%${nbin[2]}, z%${nbin[3]});
        const double3 mid = O_local + (convert_double3(cell) + 0.5)*edges;
        const double3 d = mindist(mid - rd);
        if (dot(d, d) < r_cut2) {
          const uint bin = cell.x + cell.y*${nbin[1]} + cell.z*${nbin[1]*nbin[2]};
          atomic_inc(&d_count[bin]);
        }
      }
    }
  }
}

__kernel void
position_random(__global double3 *restrict d_rs,
                __global int *restrict d_ids,
                __global const uint *restrict d_count,
                const uint Ns,
                const uint offset,
                const ulong seed)
{
  const uint gid = get_global_id(0);
  const double3 L_local = (double3)(${L_local[1]}, ${L_local[2]}, ${L_local[3]});
  const double3 O_local = (double3)(${O_local[1]}, ${O_local[2]}, ${O_local[3]});
  const uint3 nbin = (uint3)(${nbin[1]}, ${nbin[2]}, ${nbin[3]});
  const double3 nbin_frac = (double3)(${nbin[1]/L_local[1]}, ${nbin[2]/L_local[2]}, ${nbin[3]/L_local[3]});
  if (gid < Ns) {
    ulong4 c = (ulong4)(0, gid + ${rank*dom.Ns_max}UL, seed, ${seed}UL);
    double3 r;
    for (;;) {
      const double2 u = random_uniform(&c);
      |for i = 0, 1 do
      {
        const double2 v = random_uniform(&c);
        r = (double3)(u.s${i}, v) * L_local;
        const uint3 cell = min(convert_uint3_rtz(r*nbin_frac), nbin-1);
        const uint bin = cell.x + cell.y*${nbin[1]} + cell.z*${nbin[1]*nbin[2]};
        if (d_count[bin] == 0) break;
      }
      |end
    }
    d_rs[gid] = O_local + r;
    d_ids[gid] = offset + gid;
  }
}
