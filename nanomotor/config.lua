------------------------------------------------------------------------------
-- Load Lua config file.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

-- Cache library functions.
local error, loadfile, setmetatable = error, loadfile, setmetatable
-- Cache global environment table.
local _G = _G

--[[
Loads a configuration file into the table and returns the table.
]]
return function(t, filename, env)
  setmetatable(t, {__index = env or _G})
  local f, err = loadfile(filename, nil, t)
  if not f then return error(err) end
  f()
  return setmetatable(t, nil)
end
