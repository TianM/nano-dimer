------------------------------------------------------------------------------
-- Velocity-Verlet algorithm for solvent particles.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local compute = require("nanomotor.compute")
local ffi = require("ffi")

-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double_1 = ffi.typeof("cl_double[1]")
local cl_double3 = ffi.typeof("cl_double3")
local cl_int = ffi.typeof("cl_int")
local cl_short3 = ffi.typeof("cl_short3")
local cl_uint_1 = ffi.typeof("cl_uint[1]")
local cl_ulong_1 = ffi.typeof("cl_ulong[1]")
local int_1 = ffi.typeof("int[1]")

local particle = ffi.typeof[[
struct {
  cl_double3 r, v, f;
  cl_short3 im;
  cl_char4 sp;
  cl_int id;
}
]]
local particle_n = ffi.typeof("$[?]", particle)

return function(dom, args)
  local context, device, queue = dom.context, dom.device, dom.queue
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local timestep = args.timestep
  local skin = args.skin
  local reaction = args.reaction
  local Ns_send_max = args.Ns_send_max

  local program = compute.program(context, "nanomotor/verlet.cl", {
    dom = dom,
    box = box,
    timestep = timestep,
    skin = skin,
    reaction = reaction,
  })

  local self = {}

  local mem_queue = context:create_command_queue(device)
  local d_rs0 = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_double3"))
  local count = ffi.new("cl_uint[1]")
  local d_count = context:create_buffer("use_host_ptr", ffi.sizeof(count), count)

  local stream = 0
  local finalize = false
  local neigh = false

  do
    local kernel = program:create_kernel("verlet_integrate")
    local work_size = 128
    local nargs = kernel:get_info("num_args")

    function self.integrate()
      local delta_t = finalize and timestep or 0.5*timestep
      stream = stream + 1
      finalize = true
      local glob_size = math.ceil(dom.Ns_neigh/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, dom.d_vs)
      kernel:set_arg(2, dom.d_ims)
      kernel:set_arg(3, dom.d_sps)
      kernel:set_arg(4, dom.d_fs)
      kernel:set_arg(5, d_rs0)
      kernel:set_arg(6, d_count)
      if reaction and reaction.rate then
        kernel:set_arg(nargs-4, cl_ulong_1(dom.step))
      end
      kernel:set_arg(nargs-3, cl_uint_1(dom.Ns_neigh))
      kernel:set_arg(nargs-2, cl_uint_1(0))
      kernel:set_arg(nargs-1, cl_double_1(delta_t))
      local event = queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      mem_queue:enqueue_wait_for_events({event})
    end
  end

  function self.check_neigh()
    if neigh then return true end
    mem_queue:enqueue_map_buffer(d_count, true, "read")
    if comm then mpi.allreduce(mpi.in_place, count, 1, mpi.uint32, mpi.sum, comm) end
    local flag = count[0] > 0
    local event = queue:enqueue_unmap_mem_object(d_count, count)
    mem_queue:enqueue_wait_for_events({event})
    return flag
  end

  function self.post_neigh()
    if dom.Ns_neigh ~= 0 then queue:enqueue_copy_buffer(dom.d_rs, d_rs0, 0, 0, dom.Ns_neigh*ffi.sizeof(cl_double3)) end
    mem_queue:enqueue_map_buffer(d_count, true, "write")
    count[0] = 0
    queue:enqueue_unmap_mem_object(d_count, count)
    neigh = false
  end

  do
    local kernel = program:create_kernel("verlet_stream")
    local work_size = 128
    local nargs = kernel:get_info("num_args")

    function self.stream()
      local delta_t = stream*timestep
      stream = 0
      local glob_offset = math.floor(dom.Ns_neigh/work_size) * work_size
      local glob_size = math.ceil(dom.Ns/work_size) * work_size - glob_offset
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_rs)
      kernel:set_arg(1, dom.d_vs)
      kernel:set_arg(2, dom.d_ims)
      kernel:set_arg(3, dom.d_sps)
      if reaction then
        if reaction.radius then
          kernel:set_arg(4, dom.d_rd)
        end
        if reaction.rate then
          kernel:set_arg(nargs-4, cl_ulong_1(dom.step))
        end
      end
      kernel:set_arg(nargs-3, cl_uint_1(dom.Ns))
      kernel:set_arg(nargs-2, cl_uint_1(dom.Ns_neigh))
      kernel:set_arg(nargs-1, cl_double_1(delta_t))
      queue:enqueue_ndrange_kernel(kernel, {glob_offset}, {glob_size}, {work_size})
    end
  end

  function self.check_stream()
    return stream ~= 0
  end

  do
    local kernel = program:create_kernel("verlet_finalize")
    local work_size = 128

    function self.finalize()
      finalize = false
      local glob_size = math.ceil(dom.Ns/work_size) * work_size
      if glob_size == 0 then return end
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, dom.d_fs)
      kernel:set_arg(2, dom.d_sps)
      kernel:set_arg(3, cl_uint_1(dom.Ns))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
    end
  end

  function self.check_finalize()
    return finalize
  end

  do
    local kernel = program:create_kernel("verlet_sum")
    local work_size = kernel:get_work_group_info(device, "compile_work_group_size")[1]
    local num_groups = 512
    local glob_size = num_groups * work_size
    local en = ffi.new("cl_double4[?]", num_groups)
    local d_en = context:create_buffer("use_host_ptr", ffi.sizeof(en), en)

    function self.compute_en()
      kernel:set_arg(0, dom.d_vs)
      kernel:set_arg(1, d_en)
      kernel:set_arg(2, cl_uint_1(dom.Ns))
      queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
      queue:enqueue_map_buffer(d_en, true, "read")
      local en_kin = 0
      local px_cm, py_cm, pz_cm = 0, 0, 0
      for i = 0, num_groups-1 do
        en_kin = en_kin + en[i].s0
        px_cm = px_cm + en[i].s1
        py_cm = py_cm + en[i].s2
        pz_cm = pz_cm + en[i].s3
      end
      en_kin = 0.5*en_kin
      queue:enqueue_unmap_mem_object(d_en, en)
      return en_kin, {px_cm, py_cm, pz_cm}
    end
  end

  if comm then
    local Ns_send, Ns_recv

    local count_send = ffi.new("cl_uint[1]")
    local d_count_send = context:create_buffer("use_host_ptr", ffi.sizeof(count_send), count_send)

    local count do
      local kernel = program:create_kernel("verlet_count")
      local work_size = 128

      function count()
        Ns_send = 0
        local glob_size = math.ceil(dom.Ns/work_size) * work_size
        if glob_size == 0 then return end
        kernel:set_arg(0, dom.d_sps)
        kernel:set_arg(1, d_count_send)
        kernel:set_arg(2, cl_uint_1(dom.Ns))
        queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
        queue:enqueue_map_buffer(d_count_send, true, "write")
        Ns_send = count_send[0]
        count_send[0] = 0
        queue:enqueue_unmap_mem_object(d_count_send, count_send)
      end
    end

    local d_offset_send = context:create_buffer(dom.Ns_max*ffi.sizeof("cl_uint"))

    local offset do
      local kernel = program:create_kernel("verlet_offset")
      local work_size = 128

      function offset()
        local glob_size = math.ceil((dom.Ns - Ns_send)/work_size) * work_size
        if glob_size == 0 then return end
        kernel:set_arg(0, dom.d_sps)
        kernel:set_arg(1, d_offset_send)
        kernel:set_arg(2, d_count_send)
        kernel:set_arg(3, cl_uint_1((dom.Ns - Ns_send)))
        queue:enqueue_ndrange_kernel(kernel, nil, {glob_size}, {work_size})
        queue:enqueue_map_buffer(d_count_send, true, "write")
        count_send[0] = 0
        queue:enqueue_unmap_mem_object(d_count_send, count_send)
      end
    end

    local permute do
      local kernel = program:create_kernel("verlet_permute")
      local work_size = 128

      function permute()
        local glob_offset = math.floor((dom.Ns - Ns_send)/work_size) * work_size
        local glob_size = math.ceil(dom.Ns/work_size) * work_size - glob_offset
        if glob_size == 0 then return end
        kernel:set_arg(0, dom.d_rs)
        kernel:set_arg(1, dom.d_vs)
        kernel:set_arg(2, dom.d_fs)
        kernel:set_arg(3, dom.d_ims)
        kernel:set_arg(4, dom.d_sps)
        kernel:set_arg(5, dom.d_ids)
        kernel:set_arg(6, d_offset_send)
        kernel:set_arg(7, d_count_send)
        kernel:set_arg(8, cl_uint_1(dom.Ns))
        kernel:set_arg(9, cl_uint_1((dom.Ns - Ns_send)))
        queue:enqueue_ndrange_kernel(kernel, {glob_offset}, {glob_size}, {work_size})
        queue:enqueue_map_buffer(d_count_send, true, "write")
        count_send[0] = 0
        queue:enqueue_unmap_mem_object(d_count_send, count_send)
      end
    end

    local lengths = ffi.new("int[6]", 1)
    local displs = ffi.new("MPI_Aint[6]", ffi.offsetof(particle, "r"), ffi.offsetof(particle, "v"), ffi.offsetof(particle, "f"), ffi.offsetof(particle, "im"), ffi.offsetof(particle, "sp"), ffi.offsetof(particle, "id"))
    local types = ffi.new("MPI_Datatype[6]", dom.type_rs, dom.type_vs, dom.type_fs, dom.type_ims, dom.type_sps, dom.type_ids)
    local type_part = mpi.type_create_struct(6, lengths, displs, types)
    type_part = mpi.type_create_resized(type_part, 0, ffi.sizeof(particle))
    type_part:commit()

    local rank_left, rank_right = mpi.cart_shift(comm, 0, 1)
    local rank_back, rank_front = mpi.cart_shift(comm, 1, 1)
    local rank_down, rank_up = mpi.cart_shift(comm, 2, 1)

    local coords_right = rank_right and mpi.cart_coords(comm, rank_right)
    local coords_left = rank_left and mpi.cart_coords(comm, rank_left)
    local coords_front = rank_front and mpi.cart_coords(comm, rank_front)
    local coords_back = rank_back and mpi.cart_coords(comm, rank_back)
    local coords_up = rank_up and mpi.cart_coords(comm, rank_up)
    local coords_down = rank_down and mpi.cart_coords(comm, rank_down)

    local sendpart_right = rank_right and particle_n(Ns_send_max)
    local sendpart_left = rank_left and particle_n(Ns_send_max)
    local sendpart_front = rank_front and particle_n(Ns_send_max)
    local sendpart_back = rank_back and particle_n(Ns_send_max)
    local sendpart_up = rank_up and particle_n(Ns_send_max)
    local sendpart_down = rank_down and particle_n(Ns_send_max)
    local recvpart = particle_n(Ns_send_max)

    local sendcount_right = int_1()
    local sendcount_left = int_1()
    local sendcount_front = int_1()
    local sendcount_back = int_1()
    local sendcount_up = int_1()
    local sendcount_down = int_1()
    local recvcount = int_1()

    local function append_dom(i)
      local j = Ns_recv
      if j >= dom.Ns_max then error("receive buffer overflow") end
      dom.rs[j] = recvpart[i].r
      dom.vs[j] = recvpart[i].v
      dom.fs[j] = recvpart[i].f
      dom.ims[j] = recvpart[i].im
      dom.sps[j] = recvpart[i].sp
      dom.ids[j] = recvpart[i].id
      Ns_recv = j + 1
    end

    local function append_z(i)
      local z = recvpart[i].r.z
      local x, y
      x, y, z = box.mincoords(x, y, z)
      if rank_up and z == coords_up[3] then
        local j = sendcount_up[0]
        if j >= Ns_send_max then return error("up send buffer overflow") end
        sendpart_up[j] = recvpart[i]
        sendcount_up[0] = j + 1
      elseif rank_down and z == coords_down[3] then
        local j = sendcount_down[0]
        if j >= Ns_send_max then return error("down send buffer overflow") end
        sendpart_down[j] = recvpart[i]
        sendcount_down[0] = j + 1
      else
        return append_dom(i)
      end
    end

    local function append_yz(i)
      local y = recvpart[i].r.y
      local x, z
      x, y, z = box.mincoords(x, y, z)
      if rank_front and y == coords_front[2] then
        local j = sendcount_front[0]
        if j >= Ns_send_max then return error("front send buffer overflow") end
        sendpart_front[j] = recvpart[i]
        sendcount_front[0] = j + 1
      elseif rank_back and y == coords_back[2] then
        local j = sendcount_back[0]
        if j >= Ns_send_max then return error("back send buffer overflow") end
        sendpart_back[j] = recvpart[i]
        sendcount_back[0] = j + 1
      else
        return append_z(i)
      end
    end

    local function append_xyz(i)
      local x = recvpart[i].r.x
      local y, z
      x, y, z = box.mincoords(x, y, z)
      if rank_right and x == coords_right[1] then
        local j = sendcount_right[0]
        if j >= Ns_send_max then return error("right send buffer overflow") end
        sendpart_right[j] = recvpart[i]
        sendcount_right[0] = j + 1
      elseif rank_left and x == coords_left[1] then
        local j = sendcount_left[0]
        if j >= Ns_send_max then return error("left send buffer overflow") end
        sendpart_left[j] = recvpart[i]
        sendcount_left[0] = j + 1
      else
        return append_yz(i)
      end
    end

    function self.send()
      count()
      offset()
      permute()

      if Ns_send > Ns_send_max then error("send buffer overflow") end

      dom.Ns = dom.Ns - Ns_send
      queue:enqueue_read_buffer(dom.d_rs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_send*ffi.sizeof(cl_double3), dom.rs)
      queue:enqueue_read_buffer(dom.d_ims, false, dom.Ns*ffi.sizeof(cl_short3), Ns_send*ffi.sizeof(cl_short3), dom.ims)
      queue:enqueue_read_buffer(dom.d_vs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_send*ffi.sizeof(cl_double3), dom.vs)
      queue:enqueue_read_buffer(dom.d_fs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_send*ffi.sizeof(cl_double3), dom.fs)
      queue:enqueue_read_buffer(dom.d_sps, false, dom.Ns*ffi.sizeof(cl_char4), Ns_send*ffi.sizeof(cl_char4), dom.sps)
      queue:enqueue_read_buffer(dom.d_ids, true, dom.Ns*ffi.sizeof(cl_int), Ns_send*ffi.sizeof(cl_int), dom.ids)

      sendcount_left[0] = 0
      sendcount_right[0] = 0
      sendcount_back[0] = 0
      sendcount_front[0] = 0
      sendcount_down[0] = 0
      sendcount_up[0] = 0
      recvcount[0] = Ns_send

      for i = 0, recvcount[0]-1 do
        recvpart[i].r = dom.rs[i]
        recvpart[i].v = dom.vs[i]
        recvpart[i].f = dom.fs[i]
        recvpart[i].im = dom.ims[i]
        recvpart[i].sp = dom.sps[i]
        recvpart[i].id = dom.ids[i]
      end

      Ns_recv = 0
      for i = 0, recvcount[0]-1 do
        append_xyz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_right, 1, mpi.int, rank_right, 0, recvcount, 1, mpi.int, rank_left, 0, comm)
      mpi.sendrecv(sendpart_right, sendcount_right[0], type_part, rank_right, 0, recvpart, recvcount[0], type_part, rank_left, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_yz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_left, 1, mpi.int, rank_left, 0, recvcount, 1, mpi.int, rank_right, 0, comm)
      mpi.sendrecv(sendpart_left, sendcount_left[0], type_part, rank_left, 0, recvpart, recvcount[0], type_part, rank_right, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_yz(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_front, 1, mpi.int, rank_front, 0, recvcount, 1, mpi.int, rank_back, 0, comm)
      mpi.sendrecv(sendpart_front, sendcount_front[0], type_part, rank_front, 0, recvpart, recvcount[0], type_part, rank_back, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_z(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_back, 1, mpi.int, rank_back, 0, recvcount, 1, mpi.int, rank_front, 0, comm)
      mpi.sendrecv(sendpart_back, sendcount_back[0], type_part, rank_back, 0, recvpart, recvcount[0], type_part, rank_front, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_z(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_up, 1, mpi.int, rank_up, 0, recvcount, 1, mpi.int, rank_down, 0, comm)
      mpi.sendrecv(sendpart_up, sendcount_up[0], type_part, rank_up, 0, recvpart, recvcount[0], type_part, rank_down, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_dom(i)
      end

      recvcount[0] = 0
      mpi.sendrecv(sendcount_down, 1, mpi.int, rank_down, 0, recvcount, 1, mpi.int, rank_up, 0, comm)
      mpi.sendrecv(sendpart_down, sendcount_down[0], type_part, rank_down, 0, recvpart, recvcount[0], type_part, rank_up, 0, comm)
      for i = 0, recvcount[0]-1 do
        append_dom(i)
      end

      queue:enqueue_write_buffer(dom.d_rs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_recv*ffi.sizeof(cl_double3), dom.rs)
      queue:enqueue_write_buffer(dom.d_ims, false, dom.Ns*ffi.sizeof(cl_short3), Ns_recv*ffi.sizeof(cl_short3), dom.ims)
      queue:enqueue_write_buffer(dom.d_vs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_recv*ffi.sizeof(cl_double3), dom.vs)
      queue:enqueue_write_buffer(dom.d_fs, false, dom.Ns*ffi.sizeof(cl_double3), Ns_recv*ffi.sizeof(cl_double3), dom.fs)
      queue:enqueue_write_buffer(dom.d_sps, false, dom.Ns*ffi.sizeof(cl_char4), Ns_recv*ffi.sizeof(cl_char4), dom.sps)
      queue:enqueue_write_buffer(dom.d_ids, true, dom.Ns*ffi.sizeof(cl_int), Ns_recv*ffi.sizeof(cl_int), dom.ids)
      dom.Ns = dom.Ns + Ns_recv

      neigh = true
    end
  end

  return self
end
