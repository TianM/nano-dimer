------------------------------------------------------------------------------
-- Time correlation function.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local stats = require("nanomotor.statistics")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache C types.
local accum2_ptr = ffi.typeof("$ *", stats.accum2)

return function(integrate, file, tcf, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank() or 0
  local nranks = comm and comm:size() or 1
  local initial, final, interval = args.initial, args.final, args.interval
  local blockcount = args.blockcount
  local blocksize = args.blocksize
  local blockshift = args.blockshift
  local separation = args.separation
  local timestep = integrate.timestep

  local intervals = {}
  do
    local m, n = interval, interval*blockshift
    for i = 0, blockcount-1 do
      intervals[i] = m
      m, n = n, m*blocksize
    end
  end

  local dims = {blockcount, blocksize}
  for i = 1, #tcf.dims do
    dims[i+2] = tcf.dims[i]
  end

  local group = file:create_group(tcf.group)
  local space_time = hdf5.create_simple_space({blockcount, blocksize})
  local space_result = hdf5.create_simple_space(dims)
  local dset_time = group:create_dataset("time", hdf5.double, space_time)
  local dset_count = group:create_dataset("count", hdf5.uint64, space_result)
  local dset_value = group:create_dataset("value", hdf5.double, space_result)
  local dset_err = group:create_dataset("error", hdf5.double, space_result)
  group:close()
  space_time:close()
  space_result:close()
  dset_time:close()
  dset_count:close()
  dset_value:close()
  dset_err:close()

  local result = stats.accum2
  for i = #dims, 1, -1 do
    result = ffi.typeof("$[$]", result, dims[i])
  end
  result = result()

  local time = ffi.typeof("double[$][$]", blockcount, blocksize)()
  for i = 0, blockcount-1 do
    for j = 0, blocksize-1 do
      time[i][j] = j * (intervals[i]*timestep)
    end
  end

  local nelem = 1
  for i = 1, #dims do nelem = nelem*dims[i] end
  local count = ffi.new("uint64_t[?]", nelem)
  local value = ffi.new("double[?]", nelem)
  local err = ffi.new("double[?]", nelem)

  local function write()
    local result = ffi.cast(accum2_ptr, result)
    for i = 0, nelem-1 do
      count[i] = stats.count(result[i])
      value[i] = stats.mean(result[i])
      err[i] = stats.sem(result[i])
    end
    dset_time:write(time, hdf5.double)
    dset_count:write(count, hdf5.uint64)
    dset_value:write(value, hdf5.double)
    dset_err:write(err, hdf5.double)
  end

  local blocks = {}
  local origin = ffi.new("uint64_t[?]", blockcount, initial)
  local skip = ffi.new("uint64_t[?]", blockcount)

  for i = 0, blockcount-1 do
    blocks[i] = {}
    skip[i] = math.ceil(separation/intervals[i]) * intervals[i]
  end

  local self = {step = initial}

  function self.observe()
    local group = file:open_group(tcf.group)
    dset_time = group:open_dataset("time")
    dset_count = group:open_dataset("count")
    dset_value = group:open_dataset("value")
    dset_err = group:open_dataset("error")
    group:close()
    while self.step <= final do
      local sample
      for i = 0, blockcount-1 do
        if self.step % intervals[i] == 0 then
          local block = blocks[i]
          if self.step >= origin[i] then
            if not sample then
              coroutine.yield()
              sample = tcf.sample()
            end
            block[0] = sample
            origin[i] = origin[i] + skip[i]
          end
          for j = 0, blocksize-1 do
            if block[j] then
              if not sample then
                coroutine.yield()
                sample = tcf.sample()
              end
              tcf.correlate(sample, block[j], result[i][j])
            end
          end
          for j = blocksize-1, 0, -1 do
            block[j] = block[j-1]
          end
        end
      end
      self.step = self.step + interval
    end
    if rank == 0 then write() end
    dset_time:close()
    dset_count:close()
    dset_value:close()
    dset_err:close()
  end

  function self.snapshot(group)
    do
      local space_origin = hdf5.create_simple_space({blockcount})
      local space_result = hdf5.create_simple_space(dims)
      local dtype_result = stats.accum2:datatype()
      local dset_origin = group:create_dataset("origin", hdf5.uint64, space_origin)
      local dset_result = group:create_dataset("result", dtype_result, space_result)
      if rank == 0 then
        dset_origin:write(origin, hdf5.uint64, space_origin)
        dset_result:write(result, dtype_result, space_result)
      end
      space_origin:close()
      space_result:close()
      dtype_result:close()
      dset_origin:close()
      dset_result:close()
    end
    do
      local group = group:create_group("blocks")
      for i = 0, blockcount-1 do
        local group = group:create_group(tostring(i))
        for j = 0, blocksize-1 do
          local sample = blocks[i][j]
          if sample then
            local group = group:create_group(tostring(j))
            tcf.snapshot(group, sample)
            group:close()
          end
        end
        group:close()
      end
      group:close()
    end
  end

  function self.restore(group)
    do
      local space_origin = hdf5.create_simple_space({blockcount})
      local space_result = hdf5.create_simple_space(dims)
      local dtype_result = stats.accum2:datatype()
      local dset_origin = group:open_dataset("origin")
      local dset_result = group:open_dataset("result")
      if rank == 0 then
        dset_origin:read(origin, hdf5.uint64, space_origin)
        dset_result:read(result, dtype_result, space_result)
      end
      if comm then mpi.bcast(origin, blockcount, mpi.uint64, 0, comm) end
      space_origin:close()
      space_result:close()
      dtype_result:close()
      dset_origin:close()
      dset_result:close()
    end
    do
      local group = group:open_group("blocks")
      for i = 0, blockcount-1 do
        local group = group:open_group(tostring(i))
        for j = 0, blocksize-1 do
          if group:exists_link(tostring(j)) then
            local group = group:open_group(tostring(j))
            blocks[i][j] = tcf.restore(group)
            group:close()
          end
        end
        group:close()
      end
      group:close()
    end
  end

  return self
end
