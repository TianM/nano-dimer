------------------------------------------------------------------------------
-- Write solvent trajectory.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

local h5md = require("nanomotor.h5md")
local hdf5 = require("hdf5")
local ffi = require("ffi")

-- Cache library functions.
local floor = math.floor
local ipairs = ipairs
-- Cache C types.
local cl_char4 = ffi.typeof("cl_char4")
local cl_double3 = ffi.typeof("cl_double3")
local cl_int = ffi.typeof("cl_int")
local cl_short3 = ffi.typeof("cl_short3")
local int8_t_1 = ffi.typeof("int8_t[1]")
local int_1 = ffi.typeof("int[1]")

return function(integrate, file, args)
  local dom = integrate.dom
  local box = dom.box
  local comm = box.comm
  local mpi = comm and require("mpi")
  local rank = comm and comm:rank() or 0
  local nranks = comm and comm:size() or 1
  local timestep = integrate.timestep
  local initial, final, interval = args.initial, args.final, args.interval
  local dtype = hdf5[args.datatype]
  local species = args.species

  local Ns = int_1(dom.Ns)
  if comm then mpi.allreduce(mpi.in_place, Ns, 1, mpi.int, mpi.sum, comm) end
  Ns = Ns[0]

  local create = {
    position = function(group, step, time)
      local obs = h5md.create_observable(group, "position", step, time, dtype, {Ns, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "position")
        end,

        close = function()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset, 0}, nil, {1, dom.Ns, 3})
          local memspace = hdf5.create_simple_space({dom.Ns_max, 4})
          memspace:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_rs, true, 0, dom.Ns*ffi.sizeof(cl_double3), dom.rs)
          obs.value:write(dom.rs, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,

    image = function(group, step, time)
      local obs = h5md.create_observable(group, "image", step, time, hdf5.int16, {Ns, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "image")
        end,

        close = function()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset, 0}, nil, {1, dom.Ns, 3})
          local memspace = hdf5.create_simple_space({dom.Ns_max, 4})
          memspace:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_ims, true, 0, dom.Ns*ffi.sizeof(cl_short3), dom.ims)
          obs.value:write(dom.ims, hdf5.int16, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,

    velocity = function(group, step, time)
      local obs = h5md.create_observable(group, "velocity", step, time, dtype, {Ns, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "velocity")
        end,

        close = function()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset, 0}, nil, {1, dom.Ns, 3})
          local memspace = hdf5.create_simple_space({dom.Ns_max, 4})
          memspace:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_vs, true, 0, dom.Ns*ffi.sizeof(cl_double3), dom.vs)
          obs.value:write(dom.vs, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,

    force = function(group, step, time)
      local obs = h5md.create_observable(group, "force", step, time, dtype, {Ns, 3})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "force")
        end,

        close = function()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset, 0}, nil, {1, dom.Ns, 3})
          local memspace = hdf5.create_simple_space({dom.Ns_max, 4})
          memspace:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_fs, true, 0, dom.Ns*ffi.sizeof(cl_double3), dom.fs)
          obs.value:write(dom.fs, hdf5.double, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,

    species = function(group, step, time)
      local dtype = hdf5.int8:enum_create()
      for i = 0, #species-1 do
        dtype:enum_insert(species[i+1], int8_t_1(i))
      end
      local obs = h5md.create_observable(group, "species", step, time, dtype, {Ns})
      dtype:close()
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "species")
          dtype = obs.value:get_type()
        end,

        close = function()
          dtype:close()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset}, nil, {1, dom.Ns})
          local memspace = hdf5.create_simple_space({dom.Ns_max, 4})
          memspace:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 1})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_sps, true, 0, dom.Ns*ffi.sizeof(cl_char4), dom.sps)
          obs.value:write(dom.sps, dtype, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,

    id = function(group, step, time)
      local obs = h5md.create_observable(group, "id", step, time, hdf5.int32, {Ns})
      obs:close()

      return {
        open = function(group)
          obs = h5md.open_observable(group, "id")
        end,

        close = function()
          obs:close()
        end,

        write = function(offset)
          local filespace = obs:append(1)
          filespace:select_hyperslab("set", {0, offset}, nil, {1, dom.Ns})
          local memspace = hdf5.create_simple_space({dom.Ns_max})
          memspace:select_hyperslab("set", {0}, nil, {dom.Ns})
          local dxpl = hdf5.create_plist("dataset_xfer")
          if comm then dxpl:set_dxpl_mpio("collective") end
          dom.queue:enqueue_read_buffer(dom.d_ids, true, 0, dom.Ns*ffi.sizeof(cl_int), dom.ids)
          obs.value:write(dom.ids, hdf5.int32, memspace, filespace, dxpl)
          filespace:close()
          memspace:close()
          dxpl:close()
        end,
      }
    end,
  }

  local group = file:create_group("particles/solvent")

  h5md.write_box(group, {L = box.L_global, periodic = box.periodic_global})

  local step = {interval = interval, offset = initial}
  local time = {interval = interval*timestep, offset = initial*timestep}

  local elements = {}
  for i, name in ipairs(args.elements) do
    elements[i] = create[name](group, step, time)
  end

  group:close()

  local self = {step = initial}

  function self.observe()
    local group = file:open_group("particles/solvent")
    for _, element in ipairs(elements) do
      element.open(group)
    end
    group:close()
    while self.step <= final do
      coroutine.yield()
      local offset = int_1()
      if comm then mpi.exscan(int_1(dom.Ns), offset, 1, mpi.int, mpi.sum, comm) end
      offset = offset[0]
      for _, element in ipairs(elements) do
        element.write(offset)
      end
      self.step = self.step + interval
    end
    for _, element in ipairs(elements) do
      element.close()
    end
  end

  function self.snapshot(group)
    file:copy_object("particles/solvent", group, "particles/solvent")
  end

  function self.restore(group)
    file:delete_link("particles/solvent")
    group:copy_object("particles/solvent", file, "particles/solvent")
  end

  return self
end
