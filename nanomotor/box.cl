/*
 * Boundary conditions.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

|local comm = box.comm
|local mpi = comm and require("mpi")
|local ndom = comm and mpi.cart_get(comm)

|local L_global, periodic_global = box.L_global, box.periodic_global
|local L_local, O_local = box.L_local, box.O_local

/* Returns minimum-image distance vector. */
double3 mindist(double3 d)
{
  |for i = 1, 3 do
  |  if periodic_global[i] then
  d.s${i-1} -= round(d.s${i-1}*${1/L_global[i]})*${L_global[i]};
  |  end
  |end
  return d;
}

/* Returns minimum-image position vector. */
short3 minimage(double3 *r)
{
  short3 im = 0;
  |for i = 1, 3 do
  |  if periodic_global[i] then
  im.s${i-1} = convert_int_rtn((*r).s${i-1}*${1/L_global[i]});
  (*r).s${i-1} -= (double)(im.s${i-1})*${L_global[i]};
  |  end
  |end
  return im;
}

/* Returns position vector transformed to local domain. */
double3 minlocal(double3 r)
{
  const double3 mid_global = (double3)(${O_local[1] + L_local[1]/2}, ${O_local[2] + L_local[2]/2}, ${O_local[3] + L_local[3]/2});
  const double3 mid_local = (double3)(${L_local[1]/2}, ${L_local[2]/2}, ${L_local[3]/2});
  return mid_local + mindist(r - mid_global);
}

|if comm then
/* Returns Cartesian coordinates of domain that position lies in. */
uint3 mincoords(double3 r)
{
  const uint3 n = (uint3)(${ndom[1]}, ${ndom[2]}, ${ndom[3]});
  const double3 I = (double3)(${1/L_local[1]}, ${1/L_local[2]}, ${1/L_local[3]});
  minimage(&r);
  return min(convert_uint3_rtz(r*I), n-1);
}
|end
