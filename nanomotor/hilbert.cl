/*
 * Hilbert space-filling curve for domain with unequal side lengths.
 * Copyright © 2013–2015 Peter Colberg.
 * Distributed under the MIT license. (See accompanying file LICENSE.)
 */

|local bit = require("bit")
|local lshift = bit.lshift

|-- Principal dimension across which the Hilbert curve travels
|local P = 0
|-- Dimensionality of lattice
|local D = #M
|-- Maximum number of recursions
|local m = M[1]
|for i = 2, D do
|  if M[i] > m then m = M[i] end
|end
|-- Bit shifts
|local B = {}
|local i = 1
|while i < D do
|  B[#B+1] = i
|  i = 2*i
|end

/**
Returns Hilbert index given a point on a D-dimensional lattice.

This function computes compact Hilbert indices [@Hamilton2006, @Hamilton2008]
for the Hilbert space-filling curve [@Hilbert1891]. The Hilbert curve is a
bijection that maps the points of a D-dimensional cubic lattice to points on a
1-dimensional curve while preserving spatial locality. The compact Hilbert
curve is the generalization of the Hilbert curve to a rectangular lattice.
**/
uint hilbert_index(uint${D} p)
{
  uint h = 0;
  uint d = ${(P+1)%D};
  uint e = 0;
  uint l, t, w, c;
  |for i = m-1, 0, -1 do
  l = 0;
  |  for j = D-1, 0, -1 do
  l ^= ((p.s${j}>>${i}) & 1) << ${j};
  |  end
  t = l ^ e;
  t = ((t>>d) ^ (t<<(${D}-d))) & ${lshift(1, D)-1};
  w = t;
  |  for j = 1, #B do
  w ^= (w >> ${B[j]});
  |  end
  |  local n, s = 0, 0
  |  for j = D-1, 0, -1 do
  |    if M[j+1] > i then n, s = n+1, s+lshift(1, j) end
  |  end
  |  if n ~= D then
  |    for j = D-1, 0, -1 do
  if ((${s} >> ((d+${j})%${D})) & 1) h = (h<<1) ^ ((w>>${j}) & 1);
  |    end
  |  else
  h = (h<<${D}) ^ w;
  |  end
  e = l ^ (1<<d);
  c = 0;
  if (t) {
    |for j = #B, 1, -1 do
    if ((t&${lshift(1, B[j])-1}) == 0) { t >>= ${B[j]}; c ^= ${B[j]}; }
    |end
    c++;
  }
  d = (d + 1 + c)%${D};
  if ((w&1) == 0) e ^= (1 << (d == 0 ? ${D-1} : d-1));
  |end
  return h;
}
